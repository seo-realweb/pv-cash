<?

use \Realweb\Site\Site;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();
?>
</div>
</div>

<footer class="footer">
    <div class="container">
        <nav class="navbar navbar-expand-lg">
            <a class="navbar-brand" href="/executors/">
                <img src="<?php echo SITE_TEMPLATE_PATH; ?>/images/logo.png">
            </a>
            <button class="navbar-toggler"
                    type="button"
                    data-toggle="collapse"
                    data-target="#navbarFooter"
                    aria-controls="navbarFooter"
                    aria-expanded="false"
                    aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarFooter">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "bottom",
                    array(
                        "ALLOW_MULTI_SELECT" => "N",
                        "CHILD_MENU_TYPE" => "",
                        "DELAY" => "N",
                        "MAX_LEVEL" => "1",
                        "MENU_CACHE_GET_VARS" => array(""),
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_TYPE" => "A",
                        "MENU_CACHE_USE_GROUPS" => "N",
                        "ROOT_MENU_TYPE" => "bottom",
                        "USE_EXT" => "Y"
                    ),
                    false,
                    array(
                        'HIDE_ICONS' => 'Y'
                    )
                ); ?>
            </div>

            <div class="social footer__social">
<!--                <div class="social__item">-->
<!--                    <a href="#"><i class="fab fa-facebook-square"></i></a>-->
<!--                </div>-->
<!--                <div class="social__item">-->
<!--                    <a href="#"><i class="fab fa-twitter-square"></i></a>-->
<!--                </div>-->
                <div class="social__item">
                    <a href="https://t.me/PvCash" target="_blank"><i class="fab fa-telegram"></i></a>
					<a href="mailto:inbox@pvcash.ru"><i class="fa fa-envelope" aria-hidden="true"></i></a>
                </div>
            </div>
        </nav>
    </div>
</footer>

<?php $APPLICATION->IncludeComponent("realweb:blank", "popup", array()); ?>
<?php Site::epilogAction(); ?>
</body>
</html>


<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?php foreach ($arResult["ITEMS"] as $arItem): ?>
    <div class="banner banner_large">
        <div class="row align-items-center flex-lg-nowrap">
            <div class="col-12">
                <?php if ($arItem['CODE']): ?>
                    <a href="<?php echo $arItem['CODE']; ?>">
                        <img class="banner__picture"
                             src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                             alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>"
                             title="<?= $arItem["PREVIEW_PICTURE"]["TITLE"] ?>">
                    </a>
                <?php else: ?>
                    <img class="banner__picture"
                         src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                         alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>"
                         title="<?= $arItem["PREVIEW_PICTURE"]["TITLE"] ?>">
                <?php endif; ?>
            </div>
<!--            <div class="col-auto banner__col-text">-->
<!--                <h2 class="h2">--><?// echo $arItem["NAME"] ?><!--</h2>-->
<!--                <div class="banner__text">-->
<!--                    --><?// echo $arItem["PREVIEW_TEXT"] ?>
<!--                </div>-->
<!--                --><?php //if ($arItem['CODE']): ?>
<!--                    <a href="--><?php //echo $arItem['CODE']; ?><!--" class="btn btn-default lg">Подробнее</a>-->
<!--                --><?php //endif; ?>
<!--            </div>-->
        </div>
    </div>
<?php endforeach; ?>
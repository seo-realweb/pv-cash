<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="row mt-4">
    <?php foreach ($arResult["ITEMS"] as $arItem): ?>
        <div class="col-12 col-xl-6">
            <div class="banner banner_small">
                <div class="row align-items-center">
                    <div class="col-none"><img class="banner__picture"
                                               src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                                               alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>"
                                               title="<?= $arItem["PREVIEW_PICTURE"]["TITLE"] ?>"></div>
                    <div class="col">
                        <h2 class="h2"><? echo $arItem["NAME"] ?></h2>
                        <div class="banner__text">
                            <? echo $arItem["PREVIEW_TEXT"] ?>
                        </div>
                        <?php if ($arItem['CODE'] && !$arParams['IS_AUTH']): ?>
                            <a href="<?php echo $arItem['CODE']; ?>"
                               data-target="<?php echo $arItem['CODE']; ?>"
                               data-position="center"
                               class="btn btn-default lg <?php echo stripos($arItem['CODE'], 'popup') !== false ? 'js-popup-open' : ''; ?>">Регистрация</a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>

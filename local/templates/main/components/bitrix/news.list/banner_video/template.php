<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?php foreach ($arResult["ITEMS"] as $arItem): ?>
    <a href="<?php echo $arItem['CODE']; ?>" class="video" style="background-image: url(<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>)">
        <div class="video__text">
            <? echo $arItem["PREVIEW_TEXT"] ?>
            <i class="fab fa-youtube ml-1 align-middle"></i>
        </div>
    </a>
<?php endforeach; ?>
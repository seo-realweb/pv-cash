<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if (!empty($arResult)): ?>
    <ul class="navbar-nav mr-auto">
        <? foreach ($arResult as $arItem): ?>
            <li class="nav-item nav-item_btn">
                <a class="nav-link btn <?php echo $arItem['PARAMS']['CLASS']; ?> <?php if ($arItem['SELECTED'] && $arItem["LINK"] == \Realweb\Site\Main\Helper::getCurPage()): ?>active<?php endif; ?>"
                   href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
            </li>
        <? endforeach ?>
    </ul>
<? endif ?>
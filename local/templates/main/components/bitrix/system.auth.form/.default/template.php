<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
if (\Realweb\Site\User::getInstance()->isAuth() && $arResult['FORM_TYPE'] == 'logout' && \Bitrix\Main\Application::getInstance()->getContext()->getRequest()->get('login') == 'yes') {
    ?>
    <script>
        location.reload();
    </script>
    <?php
}
?>
<div class="popup__content">
    <div class="popup__header">
        <div class="popup__title">Авторизация</div>
        <a href="#" class="popup__close js-popup-close"><i class="fal fa-times-circle"></i></a>
    </div>
    <div class="popup__body">
        <?php if ($arResult["FORM_TYPE"] == "login"): ?>
            <?php if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR']): ?>
                <div class="alert alert-danger" role="alert">
                    <?php echo is_array($arResult['ERROR_MESSAGE']) ? $arResult['ERROR_MESSAGE']['MESSAGE'] : $arResult['ERROR_MESSAGE']; ?>
                </div>
            <?php endif; ?>
            <form name="system_auth_form<?= $arResult["RND"] ?>" method="post" action="<?= $arResult["AUTH_URL"] ?>">
                <? if ($arResult["BACKURL"] <> ''): ?>
                    <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
                <? endif ?>
                <? foreach ($arResult["POST"] as $key => $value): ?>
                    <input type="hidden" name="<?= $key ?>" value="<?= $value ?>"/>
                <? endforeach ?>
                <input type="hidden" name="AUTH_FORM" value="Y"/>
                <input type="hidden" name="TYPE" value="AUTH"/>
                <div class="form-group">
                    <input type="text" placeholder="Введите ник" name="USER_LOGIN" class="form-control" required>
                </div>
                <div class="form-group form-group_rel">
                    <input type="password" placeholder="Введите пароль" name="USER_PASSWORD" class="form-control"
                           required>
                    <a href="#" class="form-group__link js-toggle-password"><i class="fas fa-eye"></i></a>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-yellow lg w-100">Войти</button>
                </div>
                <div class="row justify-content-center align-items-center">
                    <div class="col-6 text-center">
                        <a href="#" class="js-popup-open" data-target="#popup-forgot" data-position="current">Забыли
                            пароль?</a>
                    </div>
                    <div class="col-6 text-center">
                        <a href="#" class="js-popup-open" data-target="#popup-registration" data-position="current">Зарегистрироваться</a>
                    </div>
                    <div class="col-auto">
                        Быстрый логин с помощью соц.сетей
                    </div>
                    <div class="col-auto">
                        <?php if ($arResult["AUTH_SERVICES"]): ?>
                            <?php $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "",
                                array(
                                    "AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
                                    "AUTH_URL" => $arResult["AUTH_URL"],
                                    "POST" => $arResult["POST"],
                                    "POPUP" => "N",
                                    "SUFFIX" => "form",
                                ),
                                false,
                                array("HIDE_ICONS" => "Y")
                            ); ?>
                        <?php endif ?>
                    </div>
                </div>
            </form>
        <?php else: ?>
            <div class="alert alert-success" role="alert">
                Вы успешно авторизовались
            </div>
        <?php endif; ?>
    </div>
</div>
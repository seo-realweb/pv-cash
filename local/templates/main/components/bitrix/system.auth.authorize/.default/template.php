<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$APPLICATION->SetPageProperty('HIDE_TITLE', 'Y');
?>
<div class="row justify-content-center">
    <div class="col-lg-5">
        <div class="system-auth">
            <? $APPLICATION->IncludeComponent("bitrix:system.auth.form", ".default", array(
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "FORGOT_PASSWORD_URL" => "",
                "PROFILE_URL" => "/profile/",
                "REGISTER_URL" => "",
                "SHOW_ERRORS" => "Y",
                "SUCCESS_URL" => '/executors/',
            ), false
            ); ?>
        </div>
    </div>
</div>
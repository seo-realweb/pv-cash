<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?><?


?>
<div class="popup__content">
    <div class="popup__header">
        <div class="popup__title">Восстановление пароля</div>
        <a href="#" class="popup__close js-popup-close"><i class="fal fa-times-circle"></i></a>
    </div>

    <div class="popup__body">
        <?php if ($APPLICATION->arAuthResult): ?>
            <?php if ($APPLICATION->arAuthResult['TYPE'] == 'ERROR'): ?>
                <div class="alert alert-danger" role="alert">
                    <?php echo $APPLICATION->arAuthResult['MESSAGE']; ?>
                </div>
            <?php elseif ($APPLICATION->arAuthResult['TYPE'] == 'OK'): ?>
                <div class="alert alert-success" role="alert">
                    <?php echo $APPLICATION->arAuthResult['MESSAGE']; ?>
                </div>
            <?php endif; ?>
        <?php endif; ?>
        <form class="popup__form" name="bform" method="post" action="<?= $arResult["AUTH_URL"] ?>">
            <?
            if (strlen($arResult["BACKURL"]) > 0) {
                ?>
                <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
                <?
            }
            ?>
            <input type="hidden" name="AUTH_FORM" value="Y">
            <input type="hidden" name="TYPE" value="SEND_PWD">

            <div class="form-group popup__form-text"><? echo GetMessage("sys_forgot_pass_label") ?></div>

            <div class="form-group">
                <input type="text" class="form-control" name="USER_LOGIN" value="<?= $arResult["LAST_LOGIN"] ?>"
                       placeholder="<?= GetMessage("sys_forgot_pass_login1") ?>"/>
                <input type="hidden" name="USER_EMAIL"/>
            </div>

            <? if ($arResult["PHONE_REGISTRATION"]): ?>

                <div style="margin-top: 16px">
                    <div><b><?= GetMessage("sys_forgot_pass_phone") ?></b></div>
                    <div><input type="text" class="form-control" name="USER_PHONE_NUMBER" value=""/></div>
                    <div><? echo GetMessage("sys_forgot_pass_note_phone") ?></div>
                </div>
            <? endif; ?>

            <? if ($arResult["USE_CAPTCHA"]): ?>
                <div class="form-group">
                    <div>
                        <input type="hidden" name="captcha_sid" value="<?= $arResult["CAPTCHA_CODE"] ?>"/>
                        <img src="/bitrix/tools/captcha.php?captcha_sid=<?= $arResult["CAPTCHA_CODE"] ?>" width="180"
                             height="40" alt="CAPTCHA"/>
                    </div>
                    <div><? echo GetMessage("system_auth_captcha") ?></div>
                    <div><input type="text" name="captcha_word" maxlength="50" value=""/></div>
                </div>
            <? endif ?>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-yellow lg w-100"><?= GetMessage("AUTH_SEND") ?></button>
                <input type="hidden" name="send_account_info" value="<?= GetMessage("AUTH_SEND") ?>"/>
            </div>
            <div class="popup__form-text"><? echo GetMessage("sys_forgot_pass_note_email") ?></div>
        </form>
    </div>
</div>
<script type="text/javascript">
    document.bform.onsubmit = function () {
        document.bform.USER_EMAIL.value = document.bform.USER_LOGIN.value;
    };
    document.bform.USER_LOGIN.focus();
</script>

$(document).on('click', '.js-telegram-auth', function () {
    var auth_key = $(this).data("key");
    var timerId = setInterval(function () {
        ajaxRequest({
            url: '/api/auth/telegram/auth/',
            params: {
                auth_key: auth_key
            }
        }, function (response) {
            if (response.data.result === 1) {
                location.href = response.data.url;
                clearInterval(timerId);
            }
        });
    }, 2000);
});
<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 * @global CUser $USER
 * @global CMain $APPLICATION
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

?>
<div class="popup__content">
    <div class="popup__header">
        <div class="popup__title">Регистрация</div>
        <a href="#" class="popup__close js-popup-close"><i class="fal fa-times-circle"></i></a>
    </div>
    <div class="popup__body">
        <?php if (!$USER->IsAuthorized()): ?>
            <?php if (count($arResult["ERRORS"]) > 0): ?>
                <div class="alert alert-danger" role="alert">
                    <?php echo implode("", $arResult["ERRORS"]); ?>
                </div>
            <?php endif; ?>
            <form method="post" action="<?= POST_FORM_ACTION_URI ?>" name="regform" enctype="multipart/form-data">
                <input type="hidden" name="register_submit_button" value="<?= GetMessage("AUTH_REGISTER") ?>"/>
                <? if ($arResult["BACKURL"] <> ''): ?>
                    <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
                <? endif; ?>
                <?php foreach ($arResult["SHOW_FIELDS"] as $FIELD): ?>
                    <div class="form-group form-group_rel">
                        <?php if ($FIELD == 'PASSWORD' || $FIELD == 'CONFIRM_PASSWORD'): ?>
                            <input type="password" name="REGISTER[<?= $FIELD ?>]"
                                   placeholder="<?php echo GetMessage("REGISTER_FIELD_" . $FIELD); ?>"
                                   value="<?= $arResult["VALUES"][$FIELD] ?>" autocomplete="off" class="form-control"
                                   required>
                            <a href="#" class="form-group__link js-toggle-password"><i class="fas fa-eye"></i></a>
                        <?php elseif ($FIELD == 'EMAIL'): ?>
                            <input type="email" name="REGISTER[<?= $FIELD ?>]"
                                   placeholder="<?php echo GetMessage("REGISTER_FIELD_" . $FIELD); ?>"
                                   value="<?= $arResult["VALUES"][$FIELD] ?>" class="form-control" required>
                        <?php else: ?>
                            <input type="text" placeholder="<?php echo GetMessage("REGISTER_FIELD_" . $FIELD); ?>"
                                   name="REGISTER[<?= $FIELD ?>]" value="<?= $arResult["VALUES"][$FIELD] ?>"
                                   class="form-control" required>
                        <?php endif; ?>
                    </div>
                <?php endforeach; ?>
                <div class="form-group">
                    <input id="registration-confirm" type="checkbox" class="custom-radio" required>
                    <label for="registration-confirm"><span>Согласен с <a href="/rules/">правилами</a></span></label>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-yellow lg w-100">Зарегистрироваться</button>
                </div>
                <div class="row justify-content-center align-items-center">
                    <div class="col-auto">
                        Быстрый логин с помощью соц.сетей
                    </div>
                    <div class="col-auto">
                        <?php if ($arResult["AUTH_SERVICES"]): ?>
                            <? $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "",
                                array(
                                    "AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
                                    "AUTH_URL" => $arResult["AUTH_URL"],
                                    "POST" => array(),
                                    "POPUP" => "N",
                                    "SUFFIX" => "form",
                                ),
                                false,
                                array("HIDE_ICONS" => "Y")
                            ); ?>
                        <?php endif ?>
                    </div>
                </div>
            </form>
        <?php endif; ?>
    </div>
</div>
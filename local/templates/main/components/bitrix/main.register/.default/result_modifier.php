<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

if (!\Realweb\Site\User::getInstance()->isAuth()) {
    if (\Bitrix\Main\Loader::includeModule("socialservices")) {
        $oAuthManager = new CSocServAuthManager();
        $arResult['AUTH_SERVICES'] = $oAuthManager->GetActiveAuthServices($arResult);
        $arParamsToDelete = array(
            "login",
            "login_form",
            "logout",
            "register",
            "forgot_password",
            "change_password",
            "confirm_registration",
            "confirm_code",
            "confirm_user_id",
            "logout_butt",
            "auth_service_id",
        );
        $arResult['AUTH_URL'] = $APPLICATION->GetCurPageParam("", $arParamsToDelete);
    }
    if (!empty($arResult['ERRORS'])) {
        foreach ($arResult['ERRORS'] as &$strError) {
            if (stripos($strError, 'Пользователь с таким email') !== false) {
                $strError = str_replace('уже существует.', 'уже существует. <a href="#" class="js-popup-open" data-target="#popup-forgot" data-position="current">Восстановить пароль</a>', $strError);
            }
        }
    }
}
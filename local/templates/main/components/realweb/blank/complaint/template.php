<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
/** @var \Realweb\Site\Model\Enum\Collection $obCauseCollection */
/** @var \Realweb\Site\Model\Enum\Entity $obCause */
$this->setFrameMode(true);
$obCauseCollection = \Realweb\Site\Main\ArrayHelper::getValue($arResult, 'cause');
?>
<div class="popup__content">
    <div class="popup__header">
        <div class="popup__title">Жалоба на заказ</div>
        <a href="#" class="popup__close js-popup-close"><i class="fal fa-times-circle"></i></a>
    </div>
    <div class="popup__body">
        <form class="js-submit-form" action="/api/complaint/index/add/" method="post">
            <input type="hidden" name="id" value="">
            <div class="form-group">
                <select class="form-control js-chosen-select" name="cause" data-placeholder="Выбрать причину">
                    <option></option>
                    <?php foreach ($obCauseCollection->getCollection() as $obCause): ?>
                        <option value="<?php echo $obCause->getId(); ?>"><?php echo $obCause->getValue(); ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <textarea placeholder="Написать комментарий" name="comment" class="form-control" required></textarea>
            </div>
            <div class="form-group m-0">
                <button type="submit" class="btn btn-yellow lg w-100">Отправить жалобу на заказ</button>
            </div>
            <div class="js-response popup__response popup__response_mt"></div>
        </form>
    </div>
</div>
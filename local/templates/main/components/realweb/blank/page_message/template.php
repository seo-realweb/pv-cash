<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
/** @var \Realweb\Site\Model\Introduction\Element\Entity $obItem */
$this->setFrameMode(true);
$obItem = \Realweb\Site\Main\ArrayHelper::getValue($arResult, 'item');
?>
<?php if ($obItem): ?>
    <div class="popup popup_success" id="popup-page-message">
        <div class="popup__content">
            <div class="popup__header">
                <div class="popup__title"><?php echo $obItem->getName(); ?></div>
            </div>
            <div class="popup__body">
                <div class="mb-3"><?php echo $obItem->getPreviewText(); ?></div>
                <div class="row justify-content-center">
                    <div class="col-auto">
                        <button type="button" class="btn btn-success js-popup-close">Хорошо</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            var fakeLink = document.createElement('a'),
                popupId = 'popup-page-message';
            fakeLink.dataset.position = 'center';
            fakeLink.dataset.target = '#' + popupId;
            popupOpen(fakeLink);
        });
    </script>
<?php endif; ?>
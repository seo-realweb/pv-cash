<?php

use Realweb\Site\Main\Helper;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */

if (\Realweb\Site\User::getInstance()->isAuth()) {
    if (!Realweb\Site\Main\Cookie\Collection::getInstance()->has('page_' . md5(Helper::getCurPage()))) {
        $arResult = array(
            'item' => (new \Realweb\Site\Controller\Introduction\Index())
                ->setParam('code', Helper::getCurPage())
                ->actionElement()
        );
        (new Realweb\Site\Main\Cookie\Entity)
            ->setName('page_' . md5(Helper::getCurPage()))
            ->setValue(1)
            ->setExpireMonth()
            ->save();
    }
}

<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="popup__content">
    <div class="popup__header">
        <div class="popup__title">Отчет об убийстве персонажа <span class="js-order-nickname"></span></div>
        <a href="#" class="popup__close js-popup-close"><i class="fal fa-times-circle"></i></a>
    </div>
    <div class="popup__body popup__body_pad">
        <form class="popup__form js-report-form" action="/api/screenshot/index/add/" method="post"
              enctype="multipart/form-data">
            <input type="hidden" name="id" value="">
            <input type="hidden" name="close_popup" value="true">
            <input type="hidden" name="replace" value="">
            <input type="hidden" id="screenshot-max" name="max" value="">
            <input class="d-none" id="screenshot-file" type="file" name="screenshot_file[]" multiple>
            <div class="row align-items-center">
                <div class="col-7">
                    <div class="form-group">
                        <div class="popup__form-label">
                            <i class="fal fa-image mr-2"></i> Загрузите скриншот с открытой картой
                        </div>
                    </div>
                </div>
                <div class="col-5">
                    <div class="form-group">
                        <a href="#" class="btn btn-default w-100" onclick="fakeInput.click()">Выбрать файл</a>
                    </div>
                </div>
            </div>
            <div id="drop-region">
                <div class="drop-message">
                    <div class="row" id="image-preview"></div>
                    <div id="drop-hint">вы можете перетащить скриншот или загрузить его через проводник</div>
                </div>
            </div>
            <div class="row">
                <div class="col-5">
                    <div class="popup__form-text">
                        Отправьте скриншот
                        заказчику на рассмотрение
                        и сопроводите текстом
                        (не обязательно)
                    </div>
                </div>
                <div class="col-7">
                    <div class="form-group">
                        <textarea name="comment" placeholder="Написать комментарий" class="form-control"></textarea>
                    </div>
                </div>
                <div class="col-5">
                    <div class="d-none js-report-info">
                        <div class="popup__form-sign"><span class="js-report-score"></span> к репутации
                        </div>
                        <div class="popup__form-text">В случае одобрения скриншота</div>
                    </div>
                </div>
                <div class="col-7">
                    <button disabled type="submit" class="btn btn-yellow lg w-100">Отправить отчёт об убийстве</button>
                </div>
            </div>
            <div class="js-response popup__response popup__response_mt"></div>
        </form>
    </div>
</div>
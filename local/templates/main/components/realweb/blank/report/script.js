var dropRegion, imagePreviewRegion, fakeInput, maxFiles, dtFiles = [];
$(document).ready(function () {
    dropRegion = document.getElementById("drop-region");
    imagePreviewRegion = document.getElementById("image-preview");
    fakeInput = document.getElementById("screenshot-file");
    maxFiles = document.getElementById("screenshot-max");
    // fakeInput.type = "file";
    // fakeInput.accept = "image/*";
    // fakeInput.multiple = true;
    dropRegion.addEventListener('click', function (e) {
        preventDefault(e);
        fakeInput.click();
    });

    fakeInput.addEventListener("change", function (e) {
        preventDefault(e);
        var files = fakeInput.files;
        handleFiles(files);
    });


    function preventDefault(e) {
        e.stopPropagation();
        e.preventDefault();
    }

    dropRegion.addEventListener('dragenter', preventDefault, false)
    dropRegion.addEventListener('dragleave', preventDefault, false)
    dropRegion.addEventListener('dragover', preventDefault, false)
    dropRegion.addEventListener('drop', preventDefault, false)


    function handleDrop(e) {
        preventDefault(e);
        var dt = e.dataTransfer,
            files = dt.files;

        if (files.length) {

            handleFiles(files);

        } else {

            // check for img
            var html = dt.getData('text/html'),
                match = html && /\bsrc="?([^"\s]+)"?\s*/.exec(html),
                url = match && match[1];


            if (url) {
                uploadImageFromURL(url);
                return;
            }

        }


        function uploadImageFromURL(url) {
            var img = new Image;
            var c = document.createElement("canvas");
            var ctx = c.getContext("2d");

            img.onload = function () {
                c.width = this.naturalWidth;     // update canvas size to match image
                c.height = this.naturalHeight;
                ctx.drawImage(this, 0, 0);       // draw in image
                c.toBlob(function (blob) {        // get content as PNG blob

                    // call our main function
                    handleFiles([blob]);

                }, "image/png");
            };
            img.onerror = function () {
                alert("Ошибка при загрузке");
            }
            img.crossOrigin = "";              // if from different origin
            img.src = url;
        }

    }

    dropRegion.addEventListener('drop', handleDrop, false);


    function handleFiles(files) {
        for (var i = 0, len = files.length; i < len; i++) {
            if (validateImage(files[i])) {
                previewAndUploadImage(files[i]);
            }
        }
        fakeInput.onchange = null;
        //fakeInput.files = dtFiles;
        fillReportInfo();
    }

    function fillReportInfo() {
        if (dtFiles.length > 0) {
            $('#popup-report').find('.js-report-info').removeClass('d-none');
            $('.js-report-score').text('+' + dtFiles.length + ' ' + morph(dtFiles.length, 'балл', 'балла', 'баллов'));
            $('#popup-report').find('[type="submit"]').attr('disabled', false);
            $('#popup-report').find('#drop-hint').addClass('d-none');
        } else {
            $('#popup-report').find('[type="submit"]').attr('disabled', true);
            $('#popup-report').find('.js-report-info').addClass('d-none');
            $('#popup-report').find('#drop-hint').removeClass('d-none');
        }
    }

    function validateImage(image) {
        var countFiles = dtFiles.length;
        if (parseInt(maxFiles.value) < countFiles + 1) {
            alert("Превышено максимальное количество скриншотов в заказе");
            return false;
        }
        // check the type
        var validTypes = ['image/jpeg', 'image/png', 'image/gif', 'image/bmp'];
        if (validTypes.indexOf(image.type) === -1) {
            alert("Не верный тип файла");
            return false;
        }

        // check the size
        var maxSizeInBytes = 30e6; // 30MB
        if (image.size > maxSizeInBytes) {
            alert("Файл слишком большой");
            return false;
        }

        return true;

    }

    function getChildNumber(node) {
        return Array.prototype.indexOf.call(node.parentNode.childNodes, node);
    }

    function removeFileInput(key) {
        var files = [];
        dtFiles.forEach(function (file, index) {
            if (index !== key) {
                files.push(file);
            }
        });
        dtFiles = files;
    }

    function previewAndUploadImage(image) {
        dtFiles.push(image);
        // container
        var imgContainer = document.createElement("div");
        imgContainer.className = "col-4 image-view";
        imagePreviewRegion.appendChild(imgContainer);

        var formGroup = document.createElement("div");
        formGroup.className = "form-group";
        imgContainer.appendChild(formGroup);

        var imgView = document.createElement("div");
        imgView.className = "popup__form-picture";
        formGroup.appendChild(imgView);

        var a = document.createElement("a");
        a.innerHTML = '<i class="fal fa-times-circle"></i>';
        a.className = 'popup__form-remove';
        a.href = '#';
        a.addEventListener('click', function (e) {
            preventDefault(e);
            var iNum = getChildNumber(imgContainer);
            removeFileInput(iNum);
            imagePreviewRegion.querySelectorAll('.image-view')[iNum].remove();
            fakeInput.onchange = null;
            //fakeInput.files = dtFiles;
            fillReportInfo();
        });
        formGroup.appendChild(a);

        // previewing image
        var img = document.createElement("img");
        imgView.appendChild(img);

        // progress overlay
        // var overlay = document.createElement("div");
        // overlay.className = "overlay";
        // imgView.appendChild(overlay);


        // read the image...
        var reader = new FileReader();
        reader.onload = function (e) {
            img.src = e.target.result;
        }
        reader.readAsDataURL(image);

        // create FormData
        // var formData = new FormData();
        // formData.append('image', image);
        //
        // // upload the image
        // var uploadLocation = 'https://api.imgbb.com/1/upload';
        // formData.append('key', 'bb63bee9d9846c8d5b7947bcdb4b3573');
        //
        // var ajax = new XMLHttpRequest();
        // ajax.open("POST", uploadLocation, true);
        //
        // ajax.onreadystatechange = function(e) {
        //     if (ajax.readyState === 4) {
        //         if (ajax.status === 200) {
        //             // done!
        //         } else {
        //             // error!
        //         }
        //     }
        // }
        //
        // ajax.upload.onprogress = function(e) {
        //
        //     // change progress
        //     // (reduce the width of overlay)
        //
        //     var perc = (e.loaded / e.total * 100) || 100,
        //         width = 100 - perc;
        //
        //     overlay.style.width = width;
        // }
        //
        // ajax.send(formData);

    }
});

$(document).on('submit', '.js-report-form', function (e) {
    e.preventDefault();
    var $form = $(this),
        data = new FormData($form[0]);
    dtFiles.forEach(function(image, i) {
        data.append('screenshot_file['+i+']', image);
    });
    submitForm($form, data);
});
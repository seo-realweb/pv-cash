<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$obExecutor = \Realweb\Site\User::getInstance()->getEntityExecutor();
$obCustomer = \Realweb\Site\User::getInstance()->getEntityCustomer();
?>
<div class="row">
    <div class="col-12 col-lg-45 mb-4 mb-lg-0">
        <div class="profile">
            <div class="row">
                <div class="col-5">
                    <div class="profile__picture-box">
                        <img class="profile__picture profile__picture_large"
                             src="<?php echo $obExecutor->getPhoto(); ?>">
                        <form class="js-submit-form" action="/api/user/index/save/" method="post"
                              enctype="multipart/form-data">
                            <input type="submit" name="submit" value="submit" class="d-none">
                            <input type="hidden" name="ajax_reload" value="true">
                            <input type="file" name="photo" class="js-change-submit d-none">
                            <a class="profile__picture-link js-change-file" href="#">Сменить фотографию</a>
                        </form>
                    </div>
                </div>
                <div class="col-7">
                    <div class="profile__info">
                        <div class="profile__name-lg"><?php echo $obExecutor->getLogin(); ?></div>
                        <div class="profile__caption">
                            Профиль наемника
                            <div class="profile__rating">
                                <i class="fas fa-star"></i>
                                <span class="profile__rating-count"><?php echo $obExecutor->getUserRating(); ?></span>
                            </div>
                        </div>

                        <div class="profile__text">Наёмник&nbsp;<b><?php echo $obExecutor->getRatingLevel(); ?></b>&nbsp;уровня
                        </div>
                        <div class="profile__text">Место в
                            рейтинге:&nbsp;<b><?php echo $obExecutor->getPosition(); ?></b></div>
                        <div class="progress lg progress_pe">
                            <div class="progress-bar" role="progressbar"
                                 style="width: <?php echo $obExecutor->getPercentForNextLevel() ?>%;"
                                 aria-valuenow="<?php echo $obExecutor->getPercentForNextLevel() ?>" aria-valuemin="0"
                                 aria-valuemax="100"></div>
                        </div>
                        <?php if ($obRating = $obExecutor->getNextRating()): ?>
                            <div class="profile__text sm"><?php echo $obExecutor->getTextForNextLevel() ?> до
                                уровня <?php echo $obRating->getUfLevel(); ?></div>
                        <?php endif; ?>
                        <div class="profile__text mt-3">
                            <i class="fal fa-bullseye-pointer"></i>Участие в
                            заказах:&nbsp;<b><?php echo $obExecutor->getCustom()->getActiveCount(); ?></b>
                        </div>
                        <div class="profile__text mt-2">
                            <i class="fal fa-bullseye-arrow"></i>
                            Убито игроков:&nbsp;<b><?php echo $obExecutor->getCountSuccessScreenshot(); ?>
                                &nbsp;(<?php echo $obExecutor->getPercentSuccessScreenshot(); ?>%)</b>
                        </div>

                        <div class="profile__caption mt-4">Профиль заказчика</div>
                        <div class="profile__text">Заказчик&nbsp;<b><?php echo $obCustomer->getRatingLevel(); ?></b>&nbsp;уровня
                        </div>
                        <div class="profile__text">Место в
                            рейтинге:&nbsp;<b><?php echo $obCustomer->getPosition(); ?></b></div>
                        <div class="profile__text mt-3">
                            <i class="fal fa-bullseye-pointer"></i>Всего
                            заявок:&nbsp;<b><?php echo $obCustomer->getCountOrder(); ?></b>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--    <div class="col-6 col-lg-30">-->
    <!--        <div class="group-btn group-btn_vertical">-->
    <!--            <a href="#" class="btn btn-default w-100 lg">Открыть мои заказы</a>-->
    <!--            <a href="#" class="btn btn-default w-100 lg">Взять заказ в работу</a>-->
    <!--            <a href="#" class="btn btn-default w-100 lg">Посмотреть кошелек</a>-->
    <!--            <a href="#" class="btn btn-default w-100 lg">Открыть мои заявки</a>-->
    <!--            <a href="#" class="btn btn-default w-100 lg">Подать заявку</a>-->
    <!--        </div>-->
    <!--    </div>-->
    <div class="col-12 col-lg-4">
        <div class="group-btn group-btn_vertical">
            <a href="#" class="btn btn-default w-100 lg js-popup-open" data-target="#popup-password"
               data-position="center">Сменить пароль</a>
            <a href="#" class="btn btn-default w-100 lg js-popup-open" data-target="#popup-email"
               data-position="center">Сменить e-mail</a>
            <a href="#" class="btn btn-default w-100 lg js-popup-open" data-target="#popup-login"
               data-position="center">Сменить никнейм</a>
            <a href="#" class="btn btn-default w-100 lg js-popup-open" data-target="#popup-support"
               data-position="center">Написать в поддержку</a>
            <a href="#" class="btn btn-default w-100 lg js-popup-open" data-target="#popup-purse"
               data-position="center">Пополнить кошелек</a>
            <a href="/?logout=yes" class="btn btn-default w-100 lg">Выйти из аккаунта</a>
        </div>
    </div>
</div>

<div class="popup" id="popup-password">
    <div class="popup__content">
        <div class="popup__header">
            <div class="popup__title">Сменить пароль</div>
            <a href="#" class="popup__close js-popup-close"><i class="fal fa-times-circle"></i></a>
        </div>
        <div class="popup__body">
            <form class="js-submit-form" action="/api/user/index/save/" method="post">
                <div class="form-group">
                    <input type="password" placeholder="Новый пароль" name="password" class="form-control" required>
                </div>
                <div class="form-group">
                    <input type="password" placeholder="Подтверждение пароля" name="confirm_password"
                           class="form-control" required>
                </div>
                <div class="form-group m-0">
                    <button type="submit" class="btn btn-yellow lg w-100">Сохранить</button>
                </div>
                <div class="js-response popup__response popup__response_mt"></div>
            </form>
        </div>
    </div>
</div>

<div class="popup" id="popup-email">
    <div class="popup__content">
        <div class="popup__header">
            <div class="popup__title">Сменить e-mail</div>
            <a href="#" class="popup__close js-popup-close"><i class="fal fa-times-circle"></i></a>
        </div>
        <div class="popup__body">
            <form class="js-submit-form" action="/api/user/index/save/" method="post">
                <div class="form-group">
                    <input type="email" placeholder="Новый email" name="email" class="form-control" required>
                </div>
                <div class="form-group m-0">
                    <button type="submit" class="btn btn-yellow lg w-100">Сохранить</button>
                </div>
                <div class="js-response popup__response popup__response_mt"></div>
            </form>
        </div>
    </div>
</div>

<div class="popup" id="popup-login">
    <div class="popup__content">
        <div class="popup__header">
            <div class="popup__title">Сменить никнейм</div>
            <a href="#" class="popup__close js-popup-close"><i class="fal fa-times-circle"></i></a>
        </div>
        <div class="popup__body">
            <form class="js-submit-form" action="/api/user/index/save/" method="post">
                <div class="form-group">
                    <input type="text" placeholder="Новый никнейм" name="login" class="form-control" required>
                </div>
                <div class="form-group m-0">
                    <button type="submit" class="btn btn-yellow lg w-100">Сохранить</button>
                </div>
                <div class="js-response popup__response popup__response_mt"></div>
            </form>
        </div>
    </div>
</div>

<div class="popup" id="popup-support">
    <div class="popup__content">
        <div class="popup__header">
            <div class="popup__title">Написать в поддержку</div>
            <a href="#" class="popup__close js-popup-close"><i class="fal fa-times-circle"></i></a>
        </div>
        <div class="popup__body">
            <form class="js-submit-form" action="/api/user/index/support/" method="post">
                <div class="form-group">
                    <textarea placeholder="Сообщение" name="message" class="form-control" required></textarea>
                </div>
                <div class="form-group m-0">
                    <button type="submit" class="btn btn-yellow lg w-100">Отправить</button>
                </div>
                <div class="js-response popup__response popup__response_mt"></div>
            </form>
        </div>
    </div>
</div>
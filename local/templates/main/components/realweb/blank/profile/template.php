<?

use Realweb\Site\User;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$obExecutor = User::getInstance()->getEntityExecutor();
$obCustomer = User::getInstance()->getEntityCustomer();
?>
<?php if (User::getInstance()->isAuth()): ?>
    <div class="popup__content">
        <div class="popup__body p-0">
            <ul class="nav nav-tabs m--1" id="myTab" role="tablist">
                <li class="nav-item w-50" role="presentation">
                    <a class="nav-link active" data-toggle="tab" href="#profile-mercenary">Наемник</a>
                </li>
                <li class="nav-item w-50" role="presentation">
                    <a class="nav-link" data-toggle="tab" href="#profile-customer">Заказчик</a>
                </li>
            </ul>

            <div class="tab-content pl-3 pr-3">
                <div class="tab-pane active" id="profile-mercenary">
                    <div class="profile">
                        <div class="profile__row">
                            <div class="profile__col-picture">
                                <img class="profile__picture" src="<?php echo $obExecutor->getPhoto(); ?>">
                            </div>
                            <div class="profile__col-info">
                                <div class="profile__info">
                                    <div class="profile__name"><?php echo $obExecutor->getLogin(); ?></div>
                                    <div class="profile__rating">
                                        <i class="fas fa-star"></i>
                                        <span class="profile__rating-count"><?php echo $obExecutor->getUserRating(); ?></span>
                                    </div>
                                    <div class="profile__text">
                                        Наёмник&nbsp;<b><?php echo $obExecutor->getRatingLevel(); ?></b>&nbsp;уровня
                                    </div>
                                    <div class="profile__text">Место в
                                        рейтинге:&nbsp;<b><?php echo $obExecutor->getPosition(); ?></b></div>
                                    <?php if ($obRating = $obExecutor->getNextRating()): ?>
                                        <div class="progress lg progress_pe">
                                            <div class="progress-bar" role="progressbar" style="width: <?php echo $obExecutor->getPercentForNextLevel() ?>%;"
                                                 aria-valuenow="<?php echo $obExecutor->getPercentForNextLevel() ?>" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                        <div class="profile__text sm">
                                            <?php echo $obExecutor->getTextForNextLevel() ?> до
                                            уровня <?php echo $obRating->getUfLevel(); ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <a href="/executors/work/" class="btn btn-primary w-100 mt-3 lg">
                            Заказы в работе:
                            <b><?php echo $obExecutor->getCustom()->getActiveCount(); ?></b>, можно взять еще <b><?php echo $obExecutor->getLeftOrder(); ?></b>
                        </a>
                        <a href="/personal/purse/" class="btn btn-yellow w-100 mt-3 lg">
                            Сумма в кошельке:
                            <b><?php echo $obExecutor->getBalanceFormatted(); ?></b>
                        </a>
                        <div class="row row-10 mt-3">
                            <div class="col-6">
                                <a class="btn btn-default md w-100" href="/personal/"><i class="fal fa-cog"></i>
                                    Настройки</a>
                            </div>
                            <div class="col-6">
                                <a class="btn btn-default md w-100" href="/?logout=yes"><i class="fal fa-sign-out"></i>
                                    Выход</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="profile-customer">
                    <div class="profile">
                        <div class="profile__row">
                            <div class="profile__col-picture">
                                <img class="profile__picture"
                                     src="<?php echo $obCustomer->getPhoto(); ?>">
                            </div>
                            <div class="profile__col-info">
                                <div class="profile__info">
                                    <div class="profile__name"><?php echo $obCustomer->getLogin(); ?></div>
                                    <div class="profile__rating">
                                        <i class="fas fa-star"></i>
                                        <span class="profile__rating-count"><?php echo $obCustomer->getUserRating(); ?></span>
                                    </div>
                                    <div class="profile__text">Заказчик&nbsp;<b><?php echo $obCustomer->getRatingLevel(); ?></b>&nbsp;уровня</div>
                                    <div class="profile__text">Место в рейтинге:&nbsp;<b><?php echo $obCustomer->getPosition(); ?></b></div>
                                    <?php if ($obRating = $obCustomer->getNextRating()): ?>
                                        <div class="progress lg progress_pe">
                                            <div class="progress-bar" role="progressbar" style="width: <?php echo $obCustomer->getPercentForNextLevel() ?>%;"
                                                 aria-valuenow="<?php echo $obCustomer->getPercentForNextLevel() ?>" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                        <div class="profile__text sm">
                                            <?php echo $obCustomer->getTextForNextLevel() ?> до
                                            уровня <?php echo $obRating->getUfLevel(); ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <a href="/customers/" class="btn btn-primary w-100 mt-3 lg">
                            Активные заказы: <b><?php echo $obCustomer->getCountActiveOrder(); ?></b>
                        </a>
                        <a href="/order/" class="btn btn-yellow w-100 mt-3 lg">Добавить заказ</a>
                        <div class="row row-10 mt-3">
                            <div class="col-6">
                                <a class="btn btn-default md w-100" href="/personal/"><i class="fal fa-cog"></i>
                                    Настройки</a>
                            </div>
                            <div class="col-6">
                                <a class="btn btn-default md w-100" href="/?logout=yes"><i class="fal fa-sign-out"></i>
                                    Выход</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="blackout"></div>
<div class="popup" id="popup-registration">
    <? $APPLICATION->IncludeComponent("bitrix:main.register", "", array(
            "AJAX_MODE" => "Y",
            "AJAX_OPTION_ADDITIONAL" => "",
            "AJAX_OPTION_HISTORY" => "Y",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "USER_PROPERTY_NAME" => "",
            "SEF_MODE" => "Y",
            "SHOW_FIELDS" => array("LOGIN", "PASSWORD", "CONFIRM_PASSWORD", "EMAIL"),
            "REQUIRED_FIELDS" => array("LOGIN", "PASSWORD", "CONFIRM_PASSWORD", "EMAIL"),
            "AUTH" => "Y",
            "USE_BACKURL" => "Y",
            "SUCCESS_PAGE" => "/executors/",
            "SET_TITLE" => "N",
            "USER_PROPERTY" => array(),
            "SEF_FOLDER" => "/",
            "VARIABLE_ALIASES" => array(),
            "PROFILE_URL" => "/personal/",
        )
    ); ?>
</div>
<div class="popup" id="popup-authorize">
    <?php $APPLICATION->IncludeComponent("bitrix:system.auth.form", ".default", array(
        "AJAX_MODE" => "Y",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "FORGOT_PASSWORD_URL" => "",
        "PROFILE_URL" => "/personal/",
        "REGISTER_URL" => "",
        "SHOW_ERRORS" => "Y",
        "SUCCESS_URL" => '/'
    ), false
    ); ?>
</div>

<div class="popup" id="popup-response">
    <div class="popup__content">
        <div class="popup__header">
            <div class="popup__title js-popup-title"></div>
            <a href="#" class="popup__close js-popup-close"><i class="fal fa-times-circle"></i></a>
        </div>
        <div class="popup__body">
            <div class="js-popup-body pb-3"></div>
        </div>
    </div>
</div>

<div class="popup" id="popup-complaint">
    <?php $APPLICATION->IncludeComponent("realweb:blank", "complaint", array()); ?>
</div>

<div class="popup popup_lg" id="popup-report">
    <?php $APPLICATION->IncludeComponent("realweb:blank", "report", array()); ?>
</div>

<div class="popup" id="popup-forgot">
    <? $APPLICATION->IncludeComponent("bitrix:system.auth.forgotpasswd", ".default", array(
        "AJAX_MODE" => "Y",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "Y",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "SHOW_ERRORS" => "Y"
    ), false
    ); ?>
</div>

<div class="popup popup_md p-0 mt-lg-3" id="popup-profile">
    <?php $APPLICATION->IncludeComponent("realweb:blank", "profile", array()); ?>
</div>

<div class="popup popup_tooltip" id="popup-notice">
    <?php $APPLICATION->IncludeComponent("realweb:blank", "notice", array()); ?>
</div>

<div class="popup" id="popup-purse">
    <div class="popup__content">
        <div class="popup__header">
            <div class="popup__title">Пополнить кошелек</div>
            <a href="#" class="popup__close js-popup-close"><i class="fal fa-times-circle"></i></a>
        </div>
        <div class="popup__body">
            <form class="js-submit-form" action="/api/payment/unitpay/index/" method="post">
                <input type="hidden" name="url" value="<?php echo \Realweb\Site\Site::getCurrentPage(); ?>">
                <div class="form-group">
                    <input type="number" placeholder="Сумма для пополнения" name="sum" class="form-control" required>
                </div>
                <div class="form-group m-0">
                    <button type="submit" class="btn btn-yellow lg w-100">Пополнить</button>
                </div>
                <div class="js-response popup__response popup__response_mt"></div>
            </form>
        </div>
    </div>
</div>

<div class="popup" id="popup-withdrawal">
    <div class="popup__content">
        <div class="popup__header">
            <div class="popup__title">Вывод денег</div>
            <a href="#" class="popup__close js-popup-close"><i class="fal fa-times-circle"></i></a>
        </div>
        <div class="popup__body">
            <div class="alert alert-primary">
                <?php echo \Realweb\Site\Site::getIncludeText('WITHDRAWAL_MONEY_MESSAGE'); ?>
            </div>
            <form class="js-submit-form" action="/api/payment/withdrawal/index/" method="post">
                <div class="form-group">
                    <select name="type" class="form-control js-chosen-select js-withdrawal-type" data-placeholder="Выберите платежную систему" required>
                        <option value=""></option>
                        <option value="qiwi">qiwi</option>
                        <option value="card">Банковская карта</option>
                        <option value="webmoney">webmoney</option>
                        <option value="yandex">Яндекс деньги</option>
                    </select>
                </div>
                <div class="form-group">
                    <input type="text" placeholder="Номер" name="purse" class="form-control js-withdrawal-number" required>
                </div>
                <div class="form-group">
                    <input type="number" placeholder="Сумма для вывода" name="sum" class="form-control" required>
                </div>
                <div class="form-group m-0">
                    <button type="submit" class="btn btn-yellow lg w-100">Вывести</button>
                </div>
                <div class="js-response popup__response popup__response_mt"></div>
            </form>
        </div>
    </div>
</div>

<div class="popup popup_danger" id="popup-danger">
    <div class="popup__content">
        <div class="popup__body">
            <div class="js-popup-body mb-3"></div>
            <div class="row justify-content-center">
                <div class="col-auto js-popup-action"></div>
            </div>
        </div>
    </div>
</div>

<div class="popup popup_success" id="popup-success">
    <div class="popup__content">
        <div class="popup__body">
            <div class="js-popup-body mb-3"></div>
            <div class="row justify-content-center">
                <div class="col-auto js-popup-action"></div>
            </div>
        </div>
    </div>
</div>
<?

use Bitrix\Main\Application;
use Realweb\Site\Main\Uri;
use Realweb\Site\User;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
/** @var \Realweb\Site\Model\User\Executor\Collection $obExecutorCollection */
/** @var \Realweb\Site\Model\User\Executor\Entity $obExecutor */
$this->setFrameMode(true);
$obExecutorCollection = \Realweb\Site\ArrayHelper::getValue($arResult, 'executor.collection');
?>
    <h2 class="h3 m-0 mt-4 mt-lg-0">Топ наемников <span class="lowercase"><?php echo FormatDate('F'); ?></span></h2>
<?php if ($obExecutorCollection): ?>
    <div class="acc">
        <div class="acc__header">
            <div class="row">
                <div class="col-30">
                    Убийства
                    <?php echo \Realweb\Site\Site::getSortLink('executor_index_index_sort', 'count_success_screenshot'); ?>
                </div>
                <div class="col-25">
                    Место
                    <?php echo \Realweb\Site\Site::getSortLink('executor_index_index_sort', 'position', 'desc'); ?>
                </div>
                <div class="col-45">Ник</div>
            </div>
        </div>
        <?php foreach ($obExecutorCollection->getCollection() as $obExecutor): ?>
            <div class="acc__unit">
                <div class="acc__item <?php echo Application::getInstance()->getContext()->getRequest()->get('position') && $obExecutor->getId() == User::getInstance()->getId() ? 'acc__item_selected' : ''; ?>">
                    <div class="acc__item-head">
                        <div class="row align-items-center">
                            <div class="col-30"><i
                                        class="fal fa-bullseye-arrow mr-1"></i> <?php echo $obExecutor->getCountSuccessScreenshot(); ?>
                            </div>
                            <div class="col-25"><i
                                        class="fal fa-trophy-alt mr-1"></i> <?php echo $obExecutor->getPosition(); ?>
                            </div>
                            <div class="col-45"><?php echo $obExecutor->getLogin(); ?></div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
        <?php if (User::getInstance()->isAuth()): ?>
            <a class="btn btn-primary lg w-100"
               href="<?php echo (new Uri())->deleteParams(array('executor_index_index_sort'))->addParam('position', User::getInstance()->getEntityExecutor()->getPosition())->getPathQuery(); ?>">
                Показать мое место в рейтинге
            </a>
        <?php else: ?>
            <a class="btn btn-primary lg w-100 js-popup-open" data-target="#popup-authorize" data-position="center"
               href="#">
                Показать мое место в рейтинге
            </a>
        <?php endif; ?>
    </div>
<?php endif; ?>
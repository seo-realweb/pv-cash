<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */

$arResult = array(
    'executor' => (new \Realweb\Site\Controller\Executor\Index())->setParam('page_size', 6)->actionIndex()
);
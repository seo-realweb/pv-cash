<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
/** @var \Realweb\Site\Model\Notice\Collection $obCollection */
/** @var \Realweb\Site\Model\Notice\Entity $obItem */
$this->setFrameMode(true);

$obCollection = \Realweb\Site\ArrayHelper::getValue($arResult, 'collection');
$arNavigation = \Realweb\Site\ArrayHelper::getValue($arResult, 'navigation');
?>
<div class="popup__content js-popup-content">
    <div class="popup__body">
        <div class="notification">
            <?php if ($obCollection && $obCollection->count() > 0): ?>
                <?php foreach ($obCollection->getCollection() as $obItem): ?>
                    <?php echo $obItem->getText(); ?>
                <?php endforeach; ?>
            <?php else: ?>
                <div class="text-center"><span>Новых уведомлений не найдено</span></div>
            <?php endif; ?>
            <div class="text-center"><a class="purple" href="/personal/notice/">История уведомлений</a></div>
        </div>
    </div>
</div>
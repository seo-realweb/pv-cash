<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */

$arResult = (new \Realweb\Site\Controller\Notice\Index())
    ->setParam('show_all', true)
    ->setParam('active', 1)
    ->actionIndex();
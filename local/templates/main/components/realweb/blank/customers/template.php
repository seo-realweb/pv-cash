<?

use Bitrix\Main\Application;
use Realweb\Site\Site;
use Realweb\Site\User;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
/** @var \Realweb\Site\Model\Server\Collection $obServerCollection */
/** @var \Realweb\Site\Model\Server\Entity $obServer */
/** @var \Realweb\Site\Model\Order\Element\Collection $obOrderCollection */
/** @var \Realweb\Site\Model\Order\Element\Entity $obOrder */
$this->setFrameMode(true);

$obOrderCollection = \Realweb\Site\ArrayHelper::getValue($arResult, 'order.collection');
$arNavigation = \Realweb\Site\ArrayHelper::getValue($arResult, 'order.navigation');
$obUser = User::getInstance()->getEntityCustomer();
?>

<?php if (User::getInstance()->isAuth()): ?>
    <div class="row">
        <div class="col-12 col-lg-4">
            <h1 class="h3">Ваши заказы</h1>
            <div class="row row-10 mb-2">
                <div class="col">
                    <a href="/customers/" class="btn <?php echo !empty($arParams['COMPLETED']) ? 'btn-default' : 'btn-yellow'; ?> w-100">Активные</a>
                </div>
                <div class="col">
                    <a href="/customers/completed/" class="btn <?php echo empty($arParams['COMPLETED']) ? 'btn-default' : 'btn-yellow'; ?> w-100">Завершенные</a>
                </div>
            </div>
            <?php $APPLICATION->IncludeComponent('realweb:blank', 'user', array(
                'user' => $obUser,
            )); ?>
        </div>
        <div class="col-12 col-lg-8">

            <?php $APPLICATION->IncludeComponent('realweb:blank', 'customers_order', array(
                'collection' => $obOrderCollection,
                'navigation' => $arNavigation,
                'completed' => !empty($arParams['COMPLETED']) ? 'Y' : 'N'
            )); ?>

        </div>
    </div>
<?php else: ?>
    <?php $APPLICATION->IncludeComponent(
        "realweb:blank", "text",
        array(
            'CODE' => 'CUSTOMERS_TEXT'
        ),
        false,
        array(
            "HIDE_ICONS" => "Y"
        )
    ); ?>
<?php endif; ?>
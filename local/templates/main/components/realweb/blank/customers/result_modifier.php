<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */

$arResult = array(
    'order' => (new \Realweb\Site\Controller\Order\Index())
        ->setParam('folder', $arParams['FOLDER'])
        ->setParam('customer_id', \Realweb\Site\User::getInstance()->getId())
        ->setParam('active', !empty($arParams['COMPLETED']) ? null : 'Y')
        ->setParam('active_count', !empty($arParams['COMPLETED']) ? null : true)
        ->setParam('completed', !empty($arParams['COMPLETED']) ? true : null)
        ->actionIndex(),
);
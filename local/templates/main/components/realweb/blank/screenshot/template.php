<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
/** @var \Realweb\Site\Model\Screenshot\Entity $obScreenshot */
$this->setFrameMode(true);
$obScreenshot = \Realweb\Site\ArrayHelper::getValue($arParams, 'item');
$isExecutor = \Realweb\Site\ArrayHelper::getValue($arParams, 'executor') == 'Y';
$strComment = $isExecutor ? $obScreenshot->getUfCustomerComment() : $obScreenshot->getUfComment();
if ($obScreenshot->isNull()) {
    if (!$isExecutor) {
        $strComment .= '<div class="row justify-content-center js-lightgallery-actions">';
        $strComment .= '<div class="col-auto"><a href="#" class="screenshot__check w120 js-lightgallery-action" data-target="#confirm-' . $obScreenshot->getId() . '"><i class="fas fa-check-circle"></i> Принять</a></div>';
        $strComment .= '<div class="col-auto"><a href="#" class="screenshot__check screenshot__check_cancel w120 js-lightgallery-action" data-target="#cancel-' . $obScreenshot->getId() . '"><i class="fas fa-times-circle"></i> Отклонить</a></div>';
        $strComment .= '</div>';
    } else {
        $strComment .= '<div class="row justify-content-center js-lightgallery-actions">';
        $strComment .= '<div class="col-auto"><a href="#" class="screenshot__check bg-danger w120 js-lightgallery-action" data-target="#remove-' . $obScreenshot->getId() . '"><i class="fas fa-times-circle"></i> Удалить</a></div>';
        $strComment .= '</div>';
    }
}
?>
<?php if ($obScreenshot): ?>
    <div class="col-6 col-xl-4 mb-20" id="screenshot-<?php echo $obScreenshot->getId(); ?>">
        <div class="screenshot js-screenshot-item">
            <?php if ($obScreenshot->isFail()): ?>
                <div class="screenshot__label screenshot__label_cancel"><i class="fas fa-times-circle"></i></div>
            <?php elseif ($obScreenshot->isSuccess()): ?>
                <div class="screenshot__label screenshot__label_success"><i class="fas fa-check-circle"></i></div>
            <?php else: ?>
                <div class="screenshot__point"></div>
            <?php endif; ?>
            <div class="js-lightgallery-item"
                 data-sub-html='<?php echo $strComment; ?>'
                 data-src="<?php echo $obScreenshot->getUfFileSrc(); ?>">
                <img src="<?php echo $obScreenshot->getPreviewPictureSrc(); ?>">
            </div>
            <div class="screenshot__action">
                <?php if ($obScreenshot->isFail()): ?>

                <?php elseif ($obScreenshot->isSuccess()): ?>
                    <div class="screenshot__action-item w-50">
                        <div class="screenshot__experience">+1 опыта</div>
                    </div>
                    <div class="screenshot__action-item w-50">
                        <div class="screenshot__rating <?php if (!$isExecutor): ?>js-rating<?php endif; ?>"
                            <?php if (!$isExecutor): ?>
                                data-replace="#screenshot-<?php echo $obScreenshot->getId(); ?>"
                                data-object="<?= CUtil::PhpToJSObject((array(
                                    'url' => '/api/screenshot/index/save/',
                                    'params' => array(
                                        'id' => $obScreenshot->getId(),
                                    )
                                )), false, true) ?>"
                            <?php endif; ?>
                             data-value="<?php echo $obScreenshot->getUfRating(); ?>">
                            <?php for ($i = 1; $i < 6; $i++): ?>
                                <i data-value="<?php echo $i; ?>"
                                   class="fas fa-star <?php echo $obScreenshot->getUfRating() >= $i ? "active" : ""; ?>"></i>
                            <?php endfor; ?>
                        </div>
                    </div>
                <?php else: ?>
                    <?php if ($iTime = $obScreenshot->getConfirmTime()): ?>
                        <div class="screenshot__action-item">
                            <div data-timer="<?php echo $iTime; ?>" data-toggle="tooltip" title="Счетчик отображает, сколько осталось времени до автоматического принятия скриншота" class="screenshot__time">00:00</div>
                        </div>
                    <?php endif; ?>
                    <?php if ($isExecutor): ?>
                        <div class="screenshot__action-item">
                            <a href="#"
                               id="remove-<?php echo $obScreenshot->getId(); ?>"
                               data-replace="#order-<?php echo $obScreenshot->getUfOrderId(); ?>"
                               data-object="<?= CUtil::PhpToJSObject((array(
                                   'url' => '/api/screenshot/index/remove/',
                                   'params' => array(
                                       'id' => $obScreenshot->getId(),
                                   )
                               )), false, true) ?>"
                               class="screenshot__check bg-danger js-btn-action">
                                <i class="fas fa-times-circle"></i> Удалить</a>
                        </div>
                    <?php else: ?>
                        <div class="screenshot__action-item">
                            <a href="#"
                               id="confirm-<?php echo $obScreenshot->getId(); ?>"
                               data-replace="#order-<?php echo $obScreenshot->getUfOrderId(); ?>"
                               data-reload-user="true"
                               data-object="<?= CUtil::PhpToJSObject((array(
                                   'url' => '/api/screenshot/index/confirm/',
                                   'params' => array(
                                       'id' => $obScreenshot->getId(),
                                       'rating' => 5
                                   )
                               )), false, true) ?>"
                               class="screenshot__check js-btn-action">
                                <i class="fas fa-check-circle"></i> Принять</a>
                        </div>
                        <div class="screenshot__action-item">
                            <a href="#"
                               id="cancel-<?php echo $obScreenshot->getId(); ?>"
                               data-replace="#order-<?php echo $obScreenshot->getUfOrderId(); ?>"
                               data-popup-input="Введите причину отклонения"
                               data-popup-type="danger"
                               data-popup-message="Вы точно хотите отклонить скриншот?"
                               data-object="<?= CUtil::PhpToJSObject((array(
                                   'url' => '/api/screenshot/index/cancel/',
                                   'params' => array(
                                       'id' => $obScreenshot->getId(),
                                   )
                               )), false, true) ?>"
                               class="screenshot__remove js-btn-action">
                                <i class="fas fa-times-circle"></i>
                            </a>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
        <?php if ($obScreenshot->isExpanded()): ?>
            <script>
                $(document).ready(function () {
                    $('#screenshot-<?php echo $obScreenshot->getId(); ?>').find('.js-lightgallery-item').click();
                });
            </script>
        <?php endif; ?>
    </div>
<?php endif; ?>
<?

use Realweb\Site\User;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
/** @var \Realweb\Site\Model\Order\Element\Entity $obOrder */
$this->setFrameMode(true);
$obOrder = \Realweb\Site\ArrayHelper::getValue($arParams, 'item');
$bCompleted = $arParams['completed'] == 'Y';
$iIdNewOrder = User::getInstance()->getSessionValue('new_order_id', 0);
?>
<?php if ($obOrder): ?>
    <div class="row acc__unit" data-parent="true" id="order-<?php echo $obOrder->getId(); ?>">
        <div class="col-12">
            <div class="acc__item<?php echo $obOrder->isHot() ? ' acc__item_hot' : ''; ?><?php echo $iIdNewOrder == $obOrder->getId() ? ' acc__item_new js-animate-shakex' : '' ?>">
                <div class="acc__item-head acc__item-head_collapse <?php echo $obOrder->isExpanded() ? 'collapsed' : ''; ?>"
                     data-toggle="collapse"
                     data-target=".acc-<?php echo $obOrder->getId(); ?>"
                     aria-expanded="<?php echo $obOrder->isExpanded() ? 'true' : 'false'; ?>">
                    <div class="row align-items-center">
                        <div class="col-20"><?php echo $obOrder->getNickname(); ?></div>
                        <div class="col-20">
                            <div class="box box_default">
                                <i class="fas fa-sword purple"></i> <?php echo $obOrder->getCountCustom(); ?>
                            </div>
                            <?php if (!$obOrder->isCompleted()): ?>
                                <?php if ($iCount = $obOrder->getScreenshot()->getNull()->count()): ?>
                                    <div class="box box_yellow">+<?php echo $iCount; ?></div>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                        <div class="col-15">
                            <div class="row">
                                <div class="col-auto">
                                    <div>
                                        <?php echo $obOrder->getCountScreenshot(); ?>
                                        из <?php echo $obOrder->getCountFormatted(); ?>
                                    </div>
                                    <div class="progress xs progress_pe mt-1">
                                        <div class="progress-bar" role="progressbar"
                                             style="width: <?php echo $obOrder->getPercent(); ?>%;"
                                             aria-valuenow="<?php echo $obOrder->getPercent(); ?>" aria-valuemin="0"
                                             aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-15"><?php echo $obOrder->getLevel()->getUfName(); ?> <span
                                    class="acc__level">LVL</span>
                        </div>
                        <div class="col-15"><?php echo $obOrder->getServer()->getUfName(); ?></div>
                        <div class="col-15 text-center">
                            <span class="acc__price acc__price_xl"
                                  style="background: <?php echo $obOrder->getColorPrice(); ?>"><?php echo $obOrder->getPriceFormatted(); ?></span>
                        </div>
                    </div>
                </div>
                <?php if (!$bCompleted): ?>
                    <div class="acc__item-head-action">
                        <a href="/order/<?php echo $obOrder->getId(); ?>/"><i class="fas fa-pencil purple"></i></a>
                        <a href="#"
                           class="js-btn-action"
                           data-popup-message="Вы действительно хотите удалить? Все залитые скриншоты будут приняты автоматически."
                           data-popup-type="danger"
                           data-reload="true"
                           data-object="<?= CUtil::PhpToJSObject((array(
                               'url' => '/api/order/index/delete/',
                               'params' => array(
                                   'id' => $obOrder->getId(),
                               )
                           )), false, true) ?>"
                        ><i class="fal fa-trash-alt red"></i></a>
                    </div>
                <?php else: ?>
                    <div class="acc__item-head-action justify-content-center">
                        <a href="#"
                           title="Повторить заказ"
                           id="link-repeat-<?php echo $obOrder->getId(); ?>"
                           data-container="#link-repeat-<?php echo $obOrder->getId(); ?>"
                           data-toggle="tooltip"
                           class="container-tooltip __compact js-btn-action"
                           data-object="<?= CUtil::PhpToJSObject((array(
                               'url' => '/api/order/index/repeat/',
                               'params' => array(
                                   'id' => $obOrder->getId(),
                               )
                           )), false, true) ?>"
                        ><i class="far fa-repeat purple"></i></a>
                    </div>
                <?php endif; ?>
                <div class="collapse acc-<?php echo $obOrder->getId(); ?> <?php echo $obOrder->isExpanded() ? 'show' : ''; ?>">
                    <div class="acc__item-body acc__item-body_full">
                        <div class="acc__item-description">
                            <div class="row">
                                <div class="col-3">
                                    <div class="acc__caption">Класс:</div>
                                    <?php echo $obOrder->getClass(); ?>
                                </div>
                                <div class="col-4">
                                    <div class="acc__caption">Локация:</div>
                                    <div>
                                        <?php echo $obOrder->getLocation()->getName(); ?>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="acc__caption">Комментарий:</div>
                                    <?php echo $obOrder->getPreviewText(); ?>
                                </div>
                            </div>
                        </div>
                        <?php if ($obOrder->getScreenshot()->count() > 0): ?>
                            <?php if ($obOrder->getScreenshot()->getNull()->count() > 0): ?>
                                <div class="acc__item-screenshot js-lightgallery">
                                    <div class="acc__item-title">Ожидают решения</div>
                                    <div class="row row-10">
                                        <?php foreach ($obOrder->getScreenshot()->getNull()->getCollection() as $obScreenshot): ?>
                                            <?php $APPLICATION->IncludeComponent('realweb:blank', 'screenshot', array(
                                                'item' => $obScreenshot,
                                            ), $component->getParent()); ?>
                                        <?php endforeach; ?>
                                    </div>
                                    <div class="group-btn group-btn_horizontal">
                                        <!--                                            <a href="#" class="btn btn-default sm">Смотреть еще</a>-->
                                        <a href="#"
                                           data-popup-message="Вы действительно хотите подвердить?"
                                           data-popup-type="success"
                                           data-replace="#order-<?php echo $obOrder->getId(); ?>"
                                           data-object="<?= CUtil::PhpToJSObject((array(
                                               'url' => '/api/screenshot/index/confirm-all/',
                                               'params' => array(
                                                   'id' => $obOrder->getId(),
                                               )
                                           )), false, true) ?>"
                                           class="btn btn-yellow js-btn-action sm">Принять все</a>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if ($obOrder->getScreenshot()->getSuccess()->count() > 0): ?>
                                <div class="acc__item-screenshot js-lightgallery">
                                    <div class="acc__item-title">Принятые</div>
                                    <div class="row row-10">
                                        <?php foreach ($obOrder->getScreenshot()->getSuccess()->getCollection() as $obScreenshot): ?>
                                            <?php $APPLICATION->IncludeComponent('realweb:blank', 'screenshot', array(
                                                'item' => $obScreenshot,
                                            ), $component->getParent()); ?>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if ($obOrder->getScreenshot()->getFail()->count() > 0): ?>
                                <div class="acc__item-screenshot js-lightgallery">
                                    <div class="acc__item-title">Отклоненные</div>
                                    <div class="row row-10">
                                        <?php foreach ($obOrder->getScreenshot()->getFail()->getCollection() as $obScreenshot): ?>
                                            <?php $APPLICATION->IncludeComponent('realweb:blank', 'screenshot', array(
                                                'item' => $obScreenshot,
                                            ), $component->getParent()); ?>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php else: ?>
                            <div class="acc__item-title">Скриншотов нет</div>
                        <?php endif; ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
<?php endif; ?>
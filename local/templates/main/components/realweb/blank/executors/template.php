<?

use Bitrix\Main\Application;
use Realweb\Site\Site;
use Realweb\Site\User;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
/** @var \Realweb\Site\Model\Server\Collection $obServerCollection */
/** @var \Realweb\Site\Model\Server\Entity $obServer */
/** @var \Realweb\Site\Model\Order\Element\Collection $obOrderCollection */
/** @var \Realweb\Site\Model\Order\Element\Entity $obOrder */
$this->setFrameMode(true);
$obServerCollection = \Realweb\Site\ArrayHelper::getValue($arResult, 'server');
$obOrderCollection = \Realweb\Site\ArrayHelper::getValue($arResult, 'order.collection');
$arNavigation = \Realweb\Site\ArrayHelper::getValue($arResult, 'order.navigation');
?>
<?php if (User::getInstance()->isAuth()): ?>
    <div class="row justify-content-between">
        <div class="col-12 col-lg-4 mb-4 order-2 order-lg-1">
            <?php if (!User::getInstance()->isAuth()): ?>
                <?php Site::showIncludeText('TEXT_NOT_AUTH'); ?>
                <div class="mb-3">
                    <a href="#" data-target="#popup-registration" data-position="top-right"
                       class="btn btn-outline-primary lg w-100 js-popup-open">Регистрация</a>
                </div>
                <div>
                    <a href="#" data-target="#popup-authorize" data-position="top-right"
                       class="btn btn-primary lg w-100 js-popup-open">Войти</a>
                </div>
            <?php else: ?>
                <h1 class="h3"><?php $APPLICATION->ShowTitle(false); ?></h1>
                <?php if (!empty($arParams['IS_MY'])): ?>
                    <div class="row row-10 mb-2">
                        <div class="col">
                            <a href="/executors/work/"
                               class="btn <?php echo !empty($arParams['COMPLETED']) ? 'btn-default' : 'btn-yellow'; ?> w-100">Активные</a>
                        </div>
                        <div class="col">
                            <a href="/executors/completed/"
                               class="btn <?php echo empty($arParams['COMPLETED']) ? 'btn-default' : 'btn-yellow'; ?> w-100">Завершенные</a>
                        </div>
                    </div>
                    <?php $APPLICATION->IncludeComponent('realweb:blank', 'user', array(
                        'user' => User::getInstance()->getEntityExecutor(),
                    )); ?>
                <?php else: ?>
                    <?php $APPLICATION->IncludeComponent('realweb:blank', 'user', array(
                        'user' => array(User::getInstance()->getEntityExecutor(), User::getInstance()->getEntityCustomer()),
                    )); ?>
                <?php endif; ?>
            <?php endif; ?>
        </div>
        <div class="col-12 col-lg-8 order-1 order-lg-2">
            <form action="<?php echo POST_FORM_ACTION_URI; ?>" method="get">
                <input type="submit" name="submit" value="submit" class="d-none">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group position-relative">
                            <input type="text" name="query" placeholder="Поиск по нику"
                                   value="<?php echo Application::getInstance()->getContext()->getRequest()->get('query'); ?>"
                                   class="form-control js-auto-submit js-change-submit">
                            <button type="submit" class="btn btn-link lg btn-link_icon"><i class="fal fa-search"></i>
                            </button>
                        </div>
                    </div>
                    <!--                <div class="col-5">-->
                    <!--                    <div class="form-group">-->
                    <!--                        <select class="form-control js-chosen-select js-change-submit" name="server"-->
                    <!--                                data-placeholder="Выберите сервер">-->
                    <!--                            <option></option>-->
                    <!--                            --><?php //foreach ($obServerCollection->getCollection() as $obServer): ?>
                    <!--                                <option --><?php //echo Application::getInstance()->getContext()->getRequest()->get('server') == $obServer->getId() ? 'selected' : ''; ?>
                    <!--                                        value="--><?php //echo $obServer->getId(); ?><!--">-->
                    <?php //echo $obServer->getUfName(); ?><!--</option>-->
                    <!--                            --><?php //endforeach; ?>
                    <!--                        </select>-->
                    <!--                    </div>-->
                    <!--                </div>-->
                </div>
            </form>
            <?php $APPLICATION->IncludeComponent('realweb:blank', 'executors_order', array(
                'collection' => $obOrderCollection,
                'navigation' => $arNavigation
            )); ?>
        </div>
    </div>
<?php else: ?>
    <?php $APPLICATION->IncludeComponent(
        "realweb:blank", "text",
        array(
            'CODE' => 'EXECUTORS_TEXT'
        ),
        false,
        array(
            "HIDE_ICONS" => "Y"
        )
    ); ?>
<?php endif; ?>
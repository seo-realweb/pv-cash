<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */

$obOrderController = (new \Realweb\Site\Controller\Order\Index())->setParam('folder', $arParams['FOLDER']);
if (\Realweb\Site\ArrayHelper::getValue($arParams, 'IS_MY') == 'Y') {
    $obOrderController->setParam('executor_id', \Realweb\Site\User::getInstance()->getId());
} else {
    $obOrderController->setParam('active_count', true);
}
if (\Realweb\Site\ArrayHelper::getValue($arParams, 'COMPLETED') == 'Y') {
    $obOrderController->setParam('active_custom', 0);
    $obOrderController->setParam('active', null);
}

$arResult = array(
    'server' => (new \Realweb\Site\Controller\Server\Index())->actionIndex(),
    'order' => $obOrderController->actionIndex(),
);
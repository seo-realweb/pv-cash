<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
/** @var \Realweb\Site\Model\Notice\Collection $obCollection */
/** @var \Realweb\Site\Model\Notice\Entity $obItem */
$this->setFrameMode(true);

$obCollection = \Realweb\Site\ArrayHelper::getValue($arResult, 'collection');
$arNavigation = \Realweb\Site\ArrayHelper::getValue($arResult, 'navigation');
?>
<div class="row">
    <div class="col-12 col-lg-4">
        <?php $APPLICATION->IncludeComponent('realweb:blank', 'user', array(
            'user' => array(\Realweb\Site\User::getInstance()->getEntityExecutor(), \Realweb\Site\User::getInstance()->getEntityCustomer()),
        )); ?>
    </div>
    <div class="col-12 col-lg-8" data-parent="true" id="notice-list">
        <?php if ($obCollection && $obCollection->count() > 0): ?>
            <div class="acc">
                <div class="acc__header">
                    <div class="row">
                        <div class="col-3">Дата
                            <?php echo \Realweb\Site\Site::getSortLink('notice_index_index_sort', 'uf_created'); ?>
                        </div>
                        <div class="col-9">Уведомление</div>
                    </div>
                </div>
                <div data-items="true">
                    <?php foreach ($obCollection->getCollection() as $obItem): ?>
                        <div class="acc__unit">
                            <div class="acc__item">
                                <div class="acc__item-head">
                                    <div class="row align-items-center">
                                        <div class="col-3"><?php echo $obItem->getCreatedFormatted('d.m.Y H:i'); ?></div>
                                        <div class="col-9">
                                            <div class="notification notification_simple">
                                                <?php echo $obItem->getText(); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <?php if ($arNavigation): ?>
                <?php $APPLICATION->IncludeComponent('realweb:blank', 'navigation', array(
                    'navigation' => $arNavigation,
                )); ?>
            <?php endif; ?>
        <?php else: ?>
            <div class="alert alert-primary">Уведомлений не найдено</div>
        <?php endif; ?>
    </div>
</div>
<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$arNavigation = \Realweb\Site\Main\ArrayHelper::getValue($arParams, 'navigation');
?>
<div data-navigation="true">
    <?php if ($arNavigation['total_pages'] > 1): ?>
        <?php if ($arNavigation['next_page_urls']): ?>
            <div>
                <a href="<?php echo $arNavigation['next_page_urls']['public_url']; ?>" target="_self"
                   class="btn btn-primary lg w-100 js-load-more">
                    <?php echo $arNavigation['more_text']; ?>
                </a>
            </div>
        <?php endif; ?>
        <nav aria-label="navigation">
            <ul class="pagination">
                <?php if ($arNavigation['previous_page_urls']): ?>
                    <li class="page-item">
                        <a class="page-link" href="<?php echo $arNavigation['previous_page_urls']['public_url']; ?>"><i
                                    class="far fa-angle-left"></i></a>
                    </li>
                <?php endif; ?>
                <?php foreach ($arNavigation['pages'] as $page): ?>
                    <li class="page-item <?php echo $page['current'] ? 'active' : ''; ?>">
                        <a class="page-link"
                           href="<?php echo !$page['current'] ? $page['urls']['public_url'] : ''; ?>"><?php echo $page['title']; ?></a>
                    </li>
                <?php endforeach; ?>
                <?php if ($arNavigation['next_page_urls']): ?>
                    <li class="page-item"><a class="page-link"
                                             href="<?php echo $arNavigation['next_page_urls']['public_url']; ?>"><i
                                    class="far fa-angle-right"></i></a></li>
                <?php endif; ?>
            </ul>
        </nav>
    <?php endif; ?>
</div>

<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
/** @var \Realweb\Site\Model\User\Customer\Entity|\Realweb\Site\Model\User\Executor\Entity $obUser */
$this->setFrameMode(true);
$arUser = \Realweb\Site\ArrayHelper::getValue($arParams, 'user');
$arUsers = array();
if (is_array($arUser)) {
    $arUsers = $arUser;
} else {
    $arUsers[] = $arUser;
}
?>
<?php if (count($arUsers) > 1): ?>
    <ul class="nav nav-tabs row row-10 mb-2" role="tablist">
        <?php foreach ($arUsers as $obUser): ?>
            <li class="nav-item col">
                <a href="#tab-profile-<?php if ($obUser->isExecutor()): ?>executor<?php else: ?>customer<?php endif; ?>"
                   data-toggle="tab"
                   class="btn btn-default btn-default_nav <?php echo $obUser->isExecutor() ? 'active' : ''; ?> w-100">
                    <?php if ($obUser->isExecutor()): ?>Наемник<?php else: ?>Заказчик<?php endif; ?>
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
    <div class="tab-content">
<?php endif; ?>
<?php foreach ($arUsers as $obUser): ?>
    <?php if (count($arUsers) > 1): ?>
        <div class="tab-pane <?php echo $obUser->isExecutor() ? 'active' : ''; ?>" id="tab-profile-<?php if ($obUser->isExecutor()): ?>executor<?php else: ?>customer<?php endif; ?>">
    <?php endif; ?>
    <div class="profile js-profile-<?php if ($obUser->isExecutor()): ?>executor<?php else: ?>customer<?php endif; ?>">
        <div class="profile__row">
            <div class="profile__col-picture">
                <img class="profile__picture" src="<?php echo $obUser->getPhoto(); ?>">
            </div>
            <div class="profile__col-info">
                <div class="profile__info">
                    <div class="profile__name">
                        <?php echo $obUser->getLogin(); ?>
                        <?php if (\Realweb\Site\Main\ArrayHelper::getValue($arParams, 'extended')): ?>
                            (<?php echo $obUser->getEmail(); ?>)
                        <?php endif; ?>
                    </div>
                    <?php if ($fRating = $obUser->getUserRating()): ?>
                        <div class="profile__rating">
                            <i class="fas fa-star"></i>
                            <span class="profile__rating-count"><?php echo $fRating; ?></span>
                        </div>
                    <?php endif; ?>
                    <?php if ($iLevel = $obUser->getRatingLevel()): ?>
                        <div class="profile__text">
                            <?php if ($obUser->isExecutor()): ?>
                                Наёмник
                            <?php else: ?>
                                Заказчик
                            <?php endif; ?>
                            &nbsp;<b><?php echo $iLevel; ?></b>&nbsp;уровня
                        </div>
                    <?php endif; ?>
                    <div class="profile__text">Место в рейтинге:&nbsp;<b><?php echo $obUser->getPosition(); ?></b></div>
                    <?php if ($obRating = $obUser->getNextRating()): ?>
                        <div class="progress lg progress_pe">
                            <div class="progress-bar" role="progressbar"
                                 style="width: <?php echo $obUser->getPercentForNextLevel() ?>%;"
                                 aria-valuenow="<?php echo $obUser->getPercentForNextLevel() ?>"
                                 aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <div class="profile__text sm">
                            <?php echo $obUser->getTextForNextLevel() ?> до
                            уровня <?php echo $obRating->getUfLevel(); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="profile__text mt-3">
            <i class="fal fa-bullseye-pointer"></i>
            <?php if ($obUser->isExecutor()): ?>
                Участие в заказах:&nbsp;<b><?php echo $obUser->getCustom()->count(); ?></b>
            <?php else: ?>
                Создано заказов:&nbsp;<b><?php echo $obUser->getCountOrder(); ?></b>
            <?php endif; ?>
        </div>
        <div class="profile__text mt-2">
            <i class="fal fa-bullseye-arrow"></i>
            <?php if ($obUser->isExecutor()): ?>
                Убито игроков:&nbsp;<b><?php echo $obUser->getCountSuccessScreenshot(); ?></b>&nbsp;
            <?php else: ?>
                Принято скриншотов:&nbsp;<b><?php echo $obUser->getCountSuccessScreenshot(); ?></b>&nbsp;
            <?php endif; ?>
            <span id="tooltip-profile-<?php echo $obUser->getTypeCode(); ?>"
                  class="position-relative"
                  data-toggle="tooltip"
                  data-html="true"
                  data-container="#tooltip-profile-<?php echo $obUser->getTypeCode(); ?>"
                  data-placement="right"
                  title='<?php echo \Realweb\Site\Site::getTooltipByCode($obUser->getTypeCode())->toHtml(); ?>'>
            (<span class="green"><?php echo $obUser->getPercentSuccessScreenshot(); ?>%</span>)
        </span>
        </div>
        <?php if ($obRating = $obUser->getNextRating()): ?>
            <div class="profile__progress">
                <div class="profile__progress-text">
                    <?php echo $obUser->getScreenshotForNextLevel(); ?>
                    до
                    <?php if ($obRating->getUfStatus()): ?>
                        статуса “<?php echo $obRating->getUfStatus(); ?>”
                    <?php else: ?>
                        уровня <?php echo $obRating->getUfLevel(); ?>
                    <?php endif; ?>
                </div>
                <div class="progress progress_ep sm">
                    <div class="progress-bar" role="progressbar"
                         style="width: <?php echo $obUser->getPercentForNextLevelRange(); ?>%"
                         aria-valuenow="<?php echo $obUser->getPercentForNextLevelRange(); ?>"
                         aria-valuemin="0" aria-valuemax="100"></div>
                    <?php if (($obCurrentRating = $obUser->getRating()) && ($obRating = $obCurrentRating)): ?>
                        <div class="progress-point progress-point_active"
                             data-value="<?php echo $obRating->getUfCountScreen(); ?>" style="left: 15%"></div>
                    <?php else: ?>
                        <div class="progress-point progress-point_active"
                             data-value="<?php echo $obRating->getUfCountScreen(); ?>" style="left: 15%"></div>
                    <?php endif; ?>
                    <?php if ($obRating = $obRating->getNextRating()): ?>
                        <div class="progress-point" data-value="<?php echo $obRating->getUfCountScreen(); ?>"
                             style="left: 35%"></div>
                        <?php if ($obRating = $obRating->getNextRating()): ?>
                            <div class="progress-point"
                                 data-value="<?php echo $obRating->getUfCountScreen(); ?>"
                                 style="left: 55%"></div>
                            <?php if ($obRating = $obRating->getNextRating()): ?>
                                <div class="progress-point" data-value="<?php echo $obRating->getUfCountScreen(); ?>"
                                     style="left: 75%"></div>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endif; ?>
                    <a href="/bonus/" class="progress-link"></a>
                </div>
            </div>
        <?php endif; ?>
        <?php if ($obUser->getId() === \Realweb\Site\User::getInstance()->getId()): ?>
            <?php if ($obUser->isExecutor()): ?>
                <?php if ($obUser->getCustom()->getActiveCount() < $obUser->getCountOrder()): ?>
                    <a href="/executors/" class="btn btn-primary w-100 mt-3 lg">Заказы в работе:
                        <b><?php echo $obUser->getCustom()->getActiveCount(); ?></b>, можно взять еще
                        <b><?php echo $obUser->getLeftOrder(); ?></b>
                    </a>
                <?php else: ?>
                    <a href="/executors/work/" class="btn btn-primary w-100 mt-3 lg">Заказы в работе:
                        <b>
                            <?php echo $obUser->getCustom()->getActiveCount(); ?> из <?php echo $obUser->getCountOrder(); ?>
                        </b>
                    </a>
                <?php endif; ?>
                <a href="/personal/purse/" class="btn btn-yellow w-100 mt-3 lg">Сумма в кошельке:
                    <b><?php echo $obUser->getBalanceFormatted(); ?></b>
                </a>
            <?php else: ?>
                <a href="/customers/" class="btn btn-primary w-100 mt-3 lg">Заданий в работе:
                    <b><?php echo $obUser->getCountActiveOrder(); ?></b></a>
                <a href="/order/" class="btn btn-yellow w-100 mt-3 lg">Разместить заказ</a>
            <?php endif; ?>
        <?php endif; ?>
    </div>
    <?php if (count($arUsers) > 1): ?>
        </div>
    <?php endif; ?>
<?php endforeach; ?>

<?php if (count($arUsers) > 1): ?>
    </div>
<?php endif; ?>

<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="popup popup_<?php echo $arParams['type']?>" id="popup-flash-message">
    <div class="popup__content">
        <div class="popup__body">
            <div class="text-center mb-3"><?php echo $arParams['message']; ?></div>
            <div class="row justify-content-center">
                <div class="col-auto">
                    <button type="button" class="btn btn-<?php echo $arParams['type']?> js-popup-close">Хорошо</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var fakeLink = document.createElement('a'),
            popupId = 'popup-flash-message';
        fakeLink.dataset.position = 'center';
        fakeLink.dataset.target = '#' + popupId;
        popupOpen(fakeLink);
    });
</script>
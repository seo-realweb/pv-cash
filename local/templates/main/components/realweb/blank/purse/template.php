<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
/** @var \Realweb\Site\Model\Transaction\Collection $obCollection */
/** @var \Realweb\Site\Model\Transaction\Entity $obItem */
$this->setFrameMode(true);
$obUser = \Realweb\Site\User::getInstance()->getEntityExecutor();
$obCollection = \Realweb\Site\ArrayHelper::getValue($arResult, 'collection');
$arNavigation = \Realweb\Site\ArrayHelper::getValue($arResult, 'navigation');
?>
<div class="row">
    <div class="col-12 col-lg-4">
        <div class="profile">
            <div class="profile__row">
                <div class="profile__col-picture">
                    <img class="profile__picture" src="<?php echo $obUser->getPhoto(); ?>">
                </div>
                <div class="profile__col-info">
                    <div class="profile__info">
                        <div class="profile__name"><?php echo $obUser->getLogin(); ?></div>
                        <div class="profile__rating">
                            <i class="fas fa-star"></i>
                            <span class="profile__rating-count"><?php echo $obUser->getUserRating(); ?></span>
                        </div>
                        <div class="profile__text">Наёмник&nbsp;<b><?php echo $obUser->getRatingLevel(); ?></b>&nbsp;уровня
                        </div>
                        <div class="profile__text">Место в рейтинге:&nbsp;<b><?php echo $obUser->getPosition(); ?></b>
                        </div>
                        <?php if ($obRating = $obUser->getNextRating()): ?>
                            <div class="progress lg progress_pe">
                                <div class="progress-bar" role="progressbar"
                                     style="width: <?php echo $obUser->getPercentForNextLevel() ?>%;"
                                     aria-valuenow="<?php echo $obUser->getPercentForNextLevel() ?>"
                                     aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <div class="profile__text sm">
                                <?php echo $obUser->getTextForNextLevel() ?> до
                                уровня <?php echo $obRating->getUfLevel(); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

            <a href="#" class="btn btn-default w-100 mt-3 lg">Сумма в кошельке:
                <b><?php echo $obUser->getBalanceFormatted(); ?></b>
            </a>
            <a href="#" class="btn btn-yellow w-100 mt-3 lg js-popup-open" data-target="#popup-withdrawal"
               data-position="center">Вывести деньги</a>
            <a href="#" class="btn btn-yellow w-100 mt-3 lg js-popup-open" data-target="#popup-purse"
               data-position="center">Пополнить кошелек</a>
        </div>
    </div>
    <div class="col-12 col-lg-8" data-parent="true" id="purse-list">
        <?php if ($obCollection && $obCollection->count() > 0): ?>
            <div class="acc">
                <div class="acc__header">
                    <div class="row">
                        <div class="col-35">Дата
                            <?php echo \Realweb\Site\Site::getSortLink('transaction_index_index_sort', 'uf_created'); ?>
                        </div>
                        <div class="col-25">Номер
                            <?php echo \Realweb\Site\Site::getSortLink('transaction_index_index_sort', 'id'); ?>
                        </div>
                        <div class="col-25">Статус
                            <?php echo \Realweb\Site\Site::getSortLink('transaction_index_index_sort', 'uf_status'); ?>
                        </div>
                        <div class="col-15 text-center">Сумма
                            <?php echo \Realweb\Site\Site::getSortLink('transaction_index_index_sort', 'uf_sum'); ?>
                        </div>
                    </div>
                </div>
                <div data-items="true">
                    <?php foreach ($obCollection->getCollection() as $obItem): ?>
                        <div class="acc__unit">
                            <div class="acc__item">
                                <div class="acc__item-head">
                                    <div class="row align-items-center">
                                        <div class="col-35"><?php echo $obItem->getCreatedFormatted(); ?></div>
                                        <div class="col-25">#<?php echo $obItem->getId(); ?></div>
                                        <div class="col-25">
                                            <?php if ($obItem->isProcess()): ?>
                                                <i class="fal fa-clock mr-1"></i>
                                            <?php elseif ($obItem->isFail()): ?>
                                                <i class="fal fa-ban mr-1"></i>
                                            <?php elseif ($obItem->isSuccess()): ?>
                                                <i class="fal fa-check-circle mr-1"></i>
                                            <?php endif; ?>
                                            <?php echo $obItem->getStatus()->getValue() ?: '-'; ?>
                                        </div>
                                        <div class="col-15 text-center">
                                            <?php if (!$obItem->isFail() && !$obItem->isPositive()): ?>
                                                <span class="acc__price acc__price_green">
                                                    <?php echo $obItem->getSumFormatted(); ?>
                                                </span>
                                            <?php elseif ($obItem->isPositive()): ?>
                                                <span class="acc__price acc__price_yellow">
                                                +<?php echo $obItem->getSumFormatted(); ?>
                                            </span>
                                            <?php else: ?>
                                                <span class="acc__price acc__price_red">
                                                    <?php echo $obItem->getSumFormatted(); ?>
                                                </span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <?php if ($arNavigation): ?>
                <?php $APPLICATION->IncludeComponent('realweb:blank', 'navigation', array(
                    'navigation' => $arNavigation,
                )); ?>
            <?php endif; ?>
        <?php else: ?>
            <div class="mb-4 text-center">
                <a href="#" class="btn btn-yellow mt-3 lg js-popup-open" data-target="#popup-purse"
                   data-position="center">Пополнить кошелек</a>
            </div>
            <div class="alert alert-primary">Транзакций не найдено</div>
        <?php endif; ?>
    </div>
</div>
<?php echo \Realweb\Site\Site::getIncludeText('PURSE_BOTTOM_TEXT'); ?>
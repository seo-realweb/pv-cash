<?

use Bitrix\Main\Application;
use Realweb\Site\Main\Uri;
use Realweb\Site\Site;
use Realweb\Site\User;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
/** @var \Realweb\Site\Model\User\Collection $obUserCollection */
/** @var \Realweb\Site\Model\User\Entity $obUser */
/** @var \Realweb\Site\Model\Message\Collection $obMessageCollection */
/** @var \Realweb\Site\Model\Message\Collection $obLastMessageCollection */
/** @var \Realweb\Site\Model\Message\Entity $obMessage */
$this->setFrameMode(true);
$obUserCollection = \Realweb\Site\ArrayHelper::getValue($arResult, 'users.collection');
$obMessageCollection = \Realweb\Site\ArrayHelper::getValue($arResult, 'message.collection');
$arNavigation = \Realweb\Site\ArrayHelper::getValue($arResult, 'message.navigation');
$iCurrentUserId = \Realweb\Site\ArrayHelper::getValue($arResult, 'message.user_id');
$obLastMessageCollection = \Realweb\Site\ArrayHelper::getValue($arResult, 'users.last_message');
?>
<div class="js-message" data-ajaxid="<?php echo $arParams['AJAX_ID']; ?>">
    <?php if (User::getInstance()->isAuth()): ?>
        <?php if ($obUserCollection): ?>
            <div class="row">
                <div class="col-12 col-lg-5">
                    <div class="message">
                        <?php foreach ($obUserCollection->getCollection() as $obUser): ?>
                            <a href="<?php echo (new Uri())->addParam('user_id', $obUser->getId())->getPathQuery(); ?>"
                               class="message__item <?php echo $obUser->getId() == $iCurrentUserId ? 'message__item_active' : ''; ?>">
                                <div class="message__ava"><img src="<?php echo $obUser->getPhoto(); ?>"></div>
                                <div class="message__about">
                                    <?php if ($obLastMessageCollection && ($obMessage = $obLastMessageCollection->getByUser($obUser->getId()))): ?>
                                        <div class="row flex-nowrap align-items-center justify-content-between">
                                            <div class="col-auto">
                                                <div class="message__name"><?php echo $obUser->getLogin(); ?></div>
                                            </div>
                                            <div class="col-auto">
                                                <div class="message__date"><?php echo $obMessage->getCreatedFormatted(); ?></div>
                                            </div>
                                        </div>
                                        <div class="message__text">
                                            <?php echo $obMessage->getUfMessage(); ?>
                                        </div>
                                    <?php else: ?>
                                        <div class="row flex-nowrap align-items-center justify-content-between">
                                            <div class="col-auto">
                                                <div class="message__name"><?php echo $obUser->getLogin(); ?></div>
                                            </div>
                                            <div class="col-auto">
                                                <div class="message__date"></div>
                                            </div>
                                        </div>
                                        <div class="message__text"></div>
                                    <?php endif; ?>
                                </div>
                            </a>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="col-12 col-lg-7 mt-3 mt-lg-0">
                    <div class="chat">
                        <form method="post" class="js-message-form js-submit-form" action="/api/message/index/add/">
                            <?php if ($obUser = $obUserCollection->getByKey($iCurrentUserId)): ?>
                                <input type="hidden" name="user_id" value="<?php echo $iCurrentUserId; ?>">
                                <input type="hidden" name="ajax_reload" value="true">
                                <div class="chat__header">
                                    <div class="chat__ava"><img src="<?php echo $obUser->getPhoto(); ?>"></div>
                                    <div class="chat__about">
                                        <div class="chat__name"><?php echo $obUser->getLogin(); ?></div>
                                        <div class="chat__date">Был в
                                            сети <?php echo $obUser->getLastActionFormatted(); ?></div>
                                    </div>
                                    <div class="chat__detail">
                                        <a href="#"
                                           data-object="<?= CUtil::PhpToJSObject((array(
                                               'url' => '/api/user/index/profile/',
                                               'params' => array(
                                                   'id' => $obUser->getId(),
                                                   'extended' => 1
                                               )
                                           )), false, true) ?>"
                                           class="chat__detail-link js-btn-action">
                                            <i class="fal fa-address-card"></i>
                                            Профиль
                                        </a>
                                    </div>
                                </div>
                                <div class="chat__body" id="chat-body">
                                    <?php if ($obMessageCollection->count() > 0): ?>
                                        <?php foreach ($obMessageCollection->getCollection() as $obMessage): ?>
                                            <div class="chat__message <?php echo !$obMessage->isMy() ? 'chat__message_answer' : ''; ?>">
                                                <?php echo $obMessage->getUfMessage(); ?>
                                                <div class="chat__message-time"><?php echo $obMessage->getCreatedFormatted(); ?></div>
                                            </div>
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                        <div class="chat__message">
                                            Сообщений нет
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <div class="chat__footer">
                                    <div class="chat__input">
                                        <input type="text" name="message" required class="form-control"
                                               placeholder="Написать...">
                                    </div>
                                    <div class="chat__btn">
                                        <button type="submit" class="btn btn-link-default"><i
                                                    class="fab fa-telegram-plane"></i>
                                        </button>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </form>
                    </div>
                </div>
            </div>
        <?php else: ?>
            <div class="alert alert-primary">Сообщений не найдено</div>
        <?php endif; ?>
    <?php endif; ?>
</div>

var timeoutMessageInputId;
var reloadMessageBlocked = false;

$(document).ready(function () {
    scrollToBottomChat();
    setInterval(function () {
        if (!reloadMessageBlocked) {
            reloadMessage();
        }
    }, 5000);
});

$(document).on('keydown', '.js-message-form [name="message"]', function (e) {
    reloadMessageBlocked = true;
    clearTimeout(timeoutMessageInputId);
    timeoutMessageInputId = setTimeout(function () {
        reloadMessageBlocked = false;
    }, 2000);
});

function reloadMessage() {
    var ajaxId = $('.js-message').attr('data-ajaxid');
    $.ajax({
        type: 'get',
        url: '/message/',
        data: {
            user_id: $('.js-message-form').find('[name="user_id"]').val(),
            bxajaxid: ajaxId,
        }
    }).done(function (response) {
        var message = $('.js-message-form').find('[name="message"]').val();
        $('#comp_' + ajaxId).html(response);
        $('.js-message-form').find('[name="message"]').focus().val(message);
        scrollToBottomChat();
    });
    // ajaxRequest({
    //     url: '/api/message/index/reload/',
    //     params: {
    //         user_id: $('.js-message-form').find('[name="user_id"]').val()
    //     }
    // }, function (response) {
    //     if (response.data.result === 1) {
    //         if (response.data.html) {
    //             var message = $('.js-message-form').find('[name="message"]').val();
    //             $('.js-message').replaceWith(response.data.html);
    //             $('.js-message-form').find('[name="message"]').focus().val(message);
    //         }
    //     }
    // });
}

function scrollToBottomChat() {
    var chatBody = document.getElementById("chat-body");
    if (chatBody) {
        chatBody.scrollTop = chatBody.scrollHeight;
    }
}

BX.addCustomEvent("onAjaxSuccess", function (data, params) {
    scrollToBottomChat();
});
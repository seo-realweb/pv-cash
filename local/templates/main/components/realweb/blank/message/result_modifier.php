<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */

$arResult = array();
$arResult['users'] = (new \Realweb\Site\Controller\Message\Index())->actionUsers();
$currentUserId = null;
/** @var \Realweb\Site\Model\User\Collection $obCollection */
if ($obCollection = \Realweb\Site\ArrayHelper::getValue($arResult, 'users.collection')) {
    if ($obCollection->current()) {
        $currentUserId = $obCollection->current()->getId();
    }
}
$arResult['message'] = (new \Realweb\Site\Controller\Message\Index())->setParam('current_user_id', $currentUserId)->actionIndex();
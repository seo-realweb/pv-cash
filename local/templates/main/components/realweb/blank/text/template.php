<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="row justify-content-between">
    <div class="col col-md-5 col-xl-4 mb-4">
        <?php \Realweb\Site\Site::showIncludeText('TEXT_NOT_AUTH'); ?>
        <?php if (!\Realweb\Site\User::getInstance()->isAuth()): ?>
            <div class="mb-3">
                <a href="#" data-target="#popup-registration" data-position="top-right"
                   class="btn btn-outline-primary lg w-100 js-popup-open">Регистрация</a>
            </div>
            <div>
                <a href="#" data-target="#popup-authorize" data-position="top-right"
                   class="btn btn-primary lg w-100 js-popup-open">Войти</a>
            </div>
        <?php endif; ?>
    </div>
    <div class="col col-md-7">
        <?php \Realweb\Site\Site::showIncludeText($arParams['CODE']); ?>
    </div>
</div>
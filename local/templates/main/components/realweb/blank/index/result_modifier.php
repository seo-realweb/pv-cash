<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */

$arResult = array(
    'order' => (new \Realweb\Site\Controller\Order\Index())
        ->setParam('page_size', 6)->setParam('folder', '/')
        ->actionIndex(),
    'sum' => (new \Realweb\Site\Controller\Order\Index())->actionGetSum(),
);
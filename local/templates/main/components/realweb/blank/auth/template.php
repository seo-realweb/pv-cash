<?

use Realweb\Site\User;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$obUser = User::getInstance()->getEntityExecutor();
?>
<div class="header__auth js-header-auth" id="header-auth">
    <?php if (User::getInstance()->isAuth()): ?>
        <div class="header__auth-item">
            <a href="#"
               class="header__auth-icon js-popup-open js-header-notice"
               data-tooltip="true"
               data-backout="blur"
               data-target="#popup-notice"
               data-close_callback="clearNotice"
               data-position="bottom-right">
                <i class="fas fa-bell"></i>
                <span class="header__auth-count js-header-notice-count"><?php echo $obUser->getNotice()->count(); ?></span>
            </a>
        </div>
        <div class="header__auth-item">
            <a href="/message/" class="header__auth-icon">
                <i class="fas fa-envelope"></i>
                <span class="header__auth-count"><?php echo $obUser->getCountMessage(); ?></span>
            </a>
        </div>
        <div class="header__auth-item">
            <a href="/personal/purse/" class="header__auth-box">
                <i class="fal fa-wallet mr-2"></i>
                <span class="header__auth-text"><?php echo $obUser->getBalanceFormatted(); ?></span>
            </a>
        </div>
        <div class="header__auth-item">
            <a href="#" class="header__auth-profile js-popup-open"
               data-target="#popup-profile"
               data-position="bottom-right">
                <img src="<?php echo $obUser->getPhoto(); ?>">
                <span class="header__auth-count">
                 <?php if ($iLevel = $obUser->getRatingLevel()): ?>
                     <?php echo $iLevel; ?>
                 <?php endif; ?>
            </span>
                <?php if ($obRating = $obUser->getNextRating()): ?>
                    <div class="header__auth-progress">
                        <?php echo $obUser->getPercentForNextLevel() ?>%
                        <div class="progress progress_pe">
                            <div class="progress-bar" role="progressbar"
                                 style="width: <?php echo $obUser->getPercentForNextLevel() ?>%;"
                                 aria-valuenow="<?php echo $obUser->getPercentForNextLevel() ?>" aria-valuemin="0"
                                 aria-valuemax="100"></div>
                        </div>
                    </div>
                <?php endif; ?>
            </a>
        </div>
    <?php else: ?>
        <div class="header__auth-item">
            <a href="#" data-target="#popup-registration" data-position="bottom-right"
               class="btn btn-outline-primary js-popup-open">Регистрация</a>
        </div>
        <div class="header__auth-item">
            <a href="#" data-target="#popup-authorize" data-position="bottom-right"
               class="btn btn-primary js-popup-open">Войти</a>
        </div>
    <?php endif; ?>
</div>

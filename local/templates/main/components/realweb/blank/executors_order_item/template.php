<?

use Realweb\Site\User;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
/** @var \Realweb\Site\Model\Order\Element\Entity $obOrder */
$this->setFrameMode(true);
$obOrder = \Realweb\Site\ArrayHelper::getValue($arParams, 'item');
$bExtended = \Realweb\Site\ArrayHelper::getValue($arParams, 'extended') == 'Y';
?>
<?php if ($obOrder): ?>
    <div class="row acc__unit<?php echo $bExtended ? ' acc__unit_extended' : ''; ?>" data-parent="true" id="order-<?php echo $obOrder->getId(); ?>">
        <div class="col-12 <?php echo $bExtended ? 'col-lg-8' : ''; ?>">
            <div class="acc__item acc__item_<?php echo $obOrder->isHot() ? 'hot' : ''; ?>">
                <div class="acc__item-head acc__item-head_collapse <?php echo $obOrder->isExpanded() ? 'collapsed' : ''; ?>"
                     data-toggle="collapse"
                     data-target=".acc-<?php echo $obOrder->getId(); ?>"
                     aria-expanded="<?php echo $obOrder->isExpanded() ? 'true' : 'false'; ?>">
                    <div class="row align-items-center">
                        <div class="col-35"><?php echo $obOrder->getNickname(); ?></div>
                        <div class="col-20">
                            <div class="row">
                                <div class="col-auto">
                                    <div>
                                        <?php echo $obOrder->getCountScreenshot(); ?>
                                        из <?php echo $obOrder->getCountFormatted(); ?>
                                    </div>
                                    <div class="progress xs progress_pe mt-1">
                                        <div class="progress-bar" role="progressbar"
                                             style="width: <?php echo $obOrder->getPercent(); ?>%;"
                                             aria-valuenow="<?php echo $obOrder->getPercent(); ?>" aria-valuemin="0"
                                             aria-valuemax="100"></div>
                                    </div>
                                </div>
                                <?php if (User::getInstance()->isAuth()): ?>
                                    <?php if ($iCount = $obOrder->getScreenshot()->getNull()->count()): ?>
                                        <div class="col-auto p-0">
                                            <div class="box box_yellow" data-toggle="tooltip" title="Количество добавленных и нерассмотренных заказчиком скриншотов">+<?php echo $iCount; ?></div>
                                        </div>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-15"><?php echo $obOrder->getLevel()->getUfName(); ?> <span
                                    class="acc__level">LVL</span>
                        </div>
                        <div class="col-15"><?php echo $obOrder->getServer()->getUfName(); ?></div>
                        <div class="col-15 text-center">
                            <span class="acc__price acc__price_xl"
                                  style="background: <?php echo $obOrder->getColorPrice(); ?>"><?php echo $obOrder->getPriceFormatted(); ?></span>
                        </div>
                    </div>
                </div>
                <div class="collapse acc-<?php echo $obOrder->getId(); ?> <?php echo $obOrder->isExpanded() ? 'show' : ''; ?>">
                    <div class="acc__item-body">
                        <div class="acc__item-description">
                            <div class="row">
                                <div class="col-3">
                                    <div class="acc__caption">Класс:</div>
                                    <?php echo $obOrder->getClass(); ?>
                                </div>
                                <div class="col-4">
                                    <div class="acc__caption">Локация:</div>
                                    <div>
                                        <?php echo $obOrder->getLocation()->getName(); ?>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="acc__caption">Комментарий:</div>
                                    <?php echo $obOrder->getPreviewText(); ?>
                                </div>
                            </div>
                        </div>

                        <?php if (User::getInstance()->isAuth() && ($obCollectionScreenshot = User::getInstance()->getEntityExecutor()->getScreenshot()->getByOrder($obOrder->getId()))): ?>
                            <?php if ($obCollectionScreenshot->getNull()->count() > 0): ?>
                                <div class="acc__item-screenshot js-lightgallery">
                                    <div class="acc__item-title">Ожидают решения заказчика</div>
                                    <div class="row row-10">
                                        <?php foreach ($obCollectionScreenshot->getNull()->getCollection() as $obScreenshot): ?>
                                            <?php $APPLICATION->IncludeComponent('realweb:blank', 'screenshot', array(
                                                'item' => $obScreenshot,
                                                'executor' => 'Y'
                                            ), $component->getParent()); ?>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if ($obCollectionScreenshot->getFail()->count() > 0): ?>
                                <div class="acc__item-screenshot js-lightgallery">
                                    <div class="acc__item-title">Отклоненные</div>
                                    <div class="row row-10">
                                        <?php foreach ($obCollectionScreenshot->getFail()->getCollection() as $obScreenshot): ?>
                                            <?php $APPLICATION->IncludeComponent('realweb:blank', 'screenshot', array(
                                                'item' => $obScreenshot,
                                                'executor' => 'Y'
                                            ), $component->getParent()); ?>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if ($obCollectionScreenshot->getSuccess()->count() > 0): ?>
                                <div class="acc__item-screenshot js-lightgallery">
                                    <div class="acc__item-title">Принятые</div>
                                    <div class="row row-10">
                                        <?php foreach ($obCollectionScreenshot->getSuccess()->getCollection() as $obScreenshot): ?>
                                            <?php $APPLICATION->IncludeComponent('realweb:blank', 'screenshot', array(
                                                'item' => $obScreenshot,
                                                'executor' => 'Y'
                                            ), $component->getParent()); ?>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>

                        <?php if (User::getInstance()->isAuth() && $obOrder->isAvailable() && User::getInstance()->getEntityExecutor()->getRating()->getUfComplainOrder()): ?>
                            <a href="#"
                               data-target="#popup-complaint"
                               data-position="center"
                               data-id="<?php echo $obOrder->getId(); ?>"
                               class="acc__link-complaint js-open-complaint">
                                <i class="fas fa-exclamation-triangle"></i>
                            </a>
                        <?php endif; ?>

                        <?php if (User::getInstance()->isAuth() && ($obCustom = User::getInstance()->getEntityExecutor()->getCustom()->getByOrder($obOrder->getId()))): ?>
                            <div class="row row-10 row-xl-15">
                                <div class="col">
                                    <a href="#" data-target="#popup-report" data-position="center"
                                       data-id="<?php echo $obOrder->getId(); ?>"
                                       data-nickname="<?php echo $obOrder->getNickname(); ?>"
                                       data-max="<?php echo $obOrder->getActiveCount(); ?>"
                                       class="btn btn-default w-100 lg-xl js-open-report">Отчёт об убийстве</a>
                                </div>
                                <?php if (User::getInstance()->getEntityExecutor()->getRating()->getUfSendMessage()): ?>
                                    <div class="col">
                                        <a href="/message/?user_id=<?php echo $obOrder->getCreatedBy(); ?>"
                                           class="btn btn-default w-100 lg-xl <?php echo $obOrder->getCreatedBy() == User::getInstance()->getId() ? 'disabled' : ''; ?>">
                                            Сообщение заказчику
                                        </a>
                                    </div>
                                <?php endif; ?>
                                <div class="col">
                                    <a href="#"
                                       data-reload="true"
                                       data-popup-type="danger"
                                       data-popup-message="Вы уверены что хотите завершить?"
                                       data-reload-user="true"
                                       data-object="<?= CUtil::PhpToJSObject((array(
                                           'url' => '/api/custom/index/end/',
                                           'params' => array(
                                               'id' => $obCustom->getId(),
                                           )
                                       )), false, true) ?>"
                                       class="btn btn-default w-100 lg-xl js-btn-action">Завершить охоту</a>
                                    <?php if ($obCollection = User::getInstance()->getEntityExecutor()->getScreenshot()->getByOrderToPay($obOrder->getId())): ?>
                                        <?php if ($obCollection->count() > 0): ?>
                                            <div class="acc__item-hint">
                                                +<?php echo $obCollection->getPriceFormatted(true); ?>
                                            </div>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php else: ?>
                            <div class="acc__item-action">
                                <?php if ($obOrder->isAvailable()): ?>
                                    <div class="row justify-content-between align-items-center">
                                        <div class="col">
                                            <div class="p-3">
                                                <?php if (User::getInstance()->isAuth()): ?>
                                                    Вы можете еще взять
                                                    <?php echo User::getInstance()->getEntityExecutor()->getCountTextNewOrder(); ?>
                                                    в работу.
                                                    <?php if (User::getInstance()->getEntityExecutor()->getCountTextNewOrder() < 1): ?>
                                                        <span id="tooltip-order-<?php echo $obOrder->getId(); ?>"
                                                              class="tooltip-link"
                                                              data-toggle="tooltip"
                                                              data-html="true"
                                                              data-container="#tooltip-order-<?php echo $obOrder->getId(); ?>"
                                                              data-placement="bottom"
                                                              title='<?php echo \Realweb\Site\Site::getTooltipByCode('order_count')->toHtml(); ?>'>
                                                            <span class="green">почему?</span>
                                                        </span>
                                                    <?php endif; ?>
                                                <?php else: ?>
                                                    Необходимо авторизоваться на сайте
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <?php if (User::getInstance()->isAuth()): ?>
                                                <?php if (User::getInstance()->getEntityExecutor()->getCountNewOrder() > 0): ?>
                                                    <a href="#"
                                                       data-reload="true"
                                                       data-object="<?= CUtil::PhpToJSObject((array(
                                                           'url' => '/api/custom/index/add/',
                                                           'params' => array(
                                                               'id' => $obOrder->getId(),
                                                           )
                                                       )), false, true) ?>"
                                                       class="btn btn-yellow w-100 lg-xl js-btn-action">Взять в
                                                        работу</a>
                                                <?php endif; ?>
                                            <?php else: ?>
                                                <a href="#"
                                                   class="btn btn-yellow w-100 lg-xl js-popup-open"
                                                   data-target="#popup-registration"
                                                   data-position="center">
                                                    Взять в работу
                                                </a>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                    </div>

                    <?php if ($obOrder->isHot()): ?>
                        <div class="acc__item-footer">
                            <div class="row align-items-center justify-content-center justify-content-md-between">
                                <div class="col-auto">
                                    <div class="acc__label acc__label_dr">Срочный заказ</div>
                                </div>
                                <div class="col-auto">Осталось: <span data-timer="<?php echo $obOrder->getHotTime() ?>">00:00</span>
                                </div>
                                <div class="col-auto">
                                    дополнительно за убийство:
                                    <span class="acc__label acc__label_wr ml-1">+<?php echo \Realweb\Site\Model\Order\Element\Entity::HOT_PRICE; ?>₽</span>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
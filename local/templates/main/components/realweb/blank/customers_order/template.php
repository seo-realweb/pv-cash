<?

use Realweb\Site\Site;
use Realweb\Site\User;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
/** @var \Realweb\Site\Model\Order\Element\Collection $obOrderCollection */
$this->setFrameMode(true);
$obOrderCollection = \Realweb\Site\ArrayHelper::getValue($arParams, 'collection');
$arNavigation = \Realweb\Site\ArrayHelper::getValue($arParams, 'navigation');
$bCompleted = $arParams['completed'] == 'Y';
?>
<?php if ($obOrderCollection): ?>
    <div id="customers-order" data-parent="true">
        <?php if ($obOrderCollection->count() > 0): ?>
            <div class="acc">
                <div class="row">
                    <div class="col-12">
                        <div class="acc__header <?php if (!$bCompleted): ?>acc__header_collapse<?php endif; ?>">
                            <div class="row">
                                <div class="col-20">Ник</div>
                                <div class="col-20">Наемников
                                    <?php echo Site::getSortLink('order_index_index_sort', 'count_custom'); ?>
                                </div>
                                <div class="col-15">Убийства
                                    <?php echo Site::getSortLink('order_index_index_sort', 'count'); ?>
                                </div>
                                <div class="col-15">Уровень
                                    <?php echo Site::getSortLink('order_index_index_sort', 'level_id'); ?>
                                </div>
                                <div class="col-15">Сервер
                                    <?php echo Site::getSortLink('order_index_index_sort', 'server_id'); ?>
                                </div>
                                <div class="col-15 text-center">Цена
                                    <?php echo Site::getSortLink('order_index_index_sort', 'total_price'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div data-items="true">
                    <?php foreach ($obOrderCollection->getCollection() as $obOrder): ?>
                        <?php $APPLICATION->IncludeComponent('realweb:blank', 'customers_order_item', array(
                            'item' => $obOrder,
                            'completed' => $arParams['completed']
                        ), $component); ?>
                    <?php endforeach; ?>
                </div>
            </div>
            <?php if ($arNavigation): ?>
                <?php $APPLICATION->IncludeComponent('realweb:blank', 'navigation', array(
                    'navigation' => $arNavigation,
                )); ?>
            <?php endif; ?>
        <?php else: ?>
            <div class="alert alert-primary">Заказов не найдено</div>
        <?php endif; ?>
    </div>
<?php endif; ?>
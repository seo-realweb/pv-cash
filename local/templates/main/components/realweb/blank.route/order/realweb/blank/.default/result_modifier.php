<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */

$arResult = array(
    'level' => (new \Realweb\Site\Controller\Level\Index())->actionIndex(),
    'server' => (new \Realweb\Site\Controller\Server\Index())->actionIndex(),
    'location' => (new \Realweb\Site\Controller\Location\Index())->actionIndex(),
    'range_price' => (new \Realweb\Site\Controller\Order\Index())->actionGetRangePrice(),
);

if (!empty($arParams['ID'])) {
    $arResult['order'] = (new \Realweb\Site\Controller\Order\Index())
        ->setParam('customer_id', \Realweb\Site\User::getInstance()->getId())
        ->setParam('id', $arParams['ID'])
        ->actionElement();
}
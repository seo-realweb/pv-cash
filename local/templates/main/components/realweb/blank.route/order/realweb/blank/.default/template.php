<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
/** @var \Realweb\Site\Model\Server\Collection $obServerCollection */
/** @var \Realweb\Site\Model\Server\Entity $obServer */
/** @var \Realweb\Site\Model\Level\Collection $obLevelCollection */
/** @var \Realweb\Site\Model\Level\Entity $obLevel */
/** @var \Realweb\Site\Model\Location\Section\Collection $obLocationCollection */
/** @var \Realweb\Site\Model\Location\Section\Entity $obLocationSection */
/** @var \Realweb\Site\Model\Location\Element\Entity $obLocationElement */
/** @var \Realweb\Site\Model\Order\Element\Entity $obOrder */
$this->setFrameMode(true);
$obServerCollection = \Realweb\Site\ArrayHelper::getValue($arResult, 'server');
$obLevelCollection = \Realweb\Site\ArrayHelper::getValue($arResult, 'level');
$obLocationCollection = \Realweb\Site\ArrayHelper::getValue($arResult, 'location');
$obOrder = \Realweb\Site\ArrayHelper::getValue($arResult, 'order');
$arRangePrice = \Realweb\Site\Main\ArrayHelper::getValue($arResult, array('range_price'));
$iMaxPrice = $arRangePrice['max'] ? $arRangePrice['max'] : 1000;
?>
<div class="row">
    <div class="col-12 col-xl-8">
        <form name="order_save" class="form form_order js-save-form js-submit-form" method="post" action="/api/order/index/save/">
            <?php if ($obOrder): ?>
                <input type="hidden" name="id" value="<?php echo $obOrder->getId(); ?>">
            <?php endif; ?>
            <div class="row align-items-center">
                <div class="col-7">
                    <div class="form-group">
                        <input type="text" placeholder="Введите ник жертвы" name="nickname" class="form-control"
                               value="<?php echo $obOrder ? $obOrder->getNickname() : ''; ?>"
                               required>
                    </div>
                </div>
                <div class="col-5">
                    <div class="form-group">
                        <select class="form-control js-chosen-select"
                                data-remember="true"
                                name="server"
                                data-placeholder="Выберите сервер"
                                required>
                            <option value=""></option>
                            <?php foreach ($obServerCollection->getCollection() as $obServer): ?>
                                <option <?php echo $obOrder && $obOrder->getServerId() == $obServer->getId() ? 'selected' : ''; ?>
                                        value="<?php echo $obServer->getId(); ?>"><?php echo $obServer->getUfName(); ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="form-group">
                        <input type="number" name="count" placeholder="Сколько раз убить жертву" class="form-control"
                               value="<?php echo $obOrder ? $obOrder->getCountFormatted() : ''; ?>" required>
                    </div>
                </div>
                <div class="col-12 col-md-8">
                    <div class="form-group">
                        <div class="row row-10 align-items-center">
                            <div class="col">
                                Популярные запросы:
                            </div>
                            <div class="col-auto">
                                <input type="radio" class="radio-link js-set-input-value" data-name="count"
                                       name="count_popular" value="5" id="count_popular-1">
                                <label for="count_popular-1">5</label>
                            </div>
                            <div class="col-auto">
                                <input type="radio" class="radio-link js-set-input-value" data-name="count"
                                       name="count_popular" value="10" id="count_popular-2">
                                <label for="count_popular-2">10</label>
                            </div>
                            <div class="col-auto">
                                <input type="radio" class="radio-link js-set-input-value" data-name="count"
                                       name="count_popular" value="25" id="count_popular-3">
                                <label for="count_popular-3">25</label>
                            </div>
                            <div class="col-auto">
                                <input type="radio" class="radio-link js-set-input-value" data-name="count"
                                       name="count_popular" value="50" id="count_popular-4">
                                <label for="count_popular-4">50</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="form-group">
                        <input type="number" data-value="order_price" name="price"
                               placeholder="Стоимость за одно убийство" class="form-control js-range-value" value="<?php echo $obOrder ? doubleval($obOrder->getPrice()) : ''; ?>" required>
                    </div>
                </div>
                <div class="col-12 col-md-8">
                    <div class="form-group">
                        <div class="form__slider">
                            <div class="row">
                                <div class="col">
                                    <input type="text"
                                           data-slider-min="0"
                                           data-slider-max="<?php echo $iMaxPrice; ?>"
                                           data-id="order_price"
                                           class="js-range-single"
                                           data-slider-step="1"
                                           data-slider-value="<?php echo $obOrder ? doubleval($obOrder->getPrice()) : '0'; ?>"/>
                                </div>
                                <div class="col-auto">
                                    <span class="form__slider-label" data-text="order_price"><?php echo $obOrder ? $obOrder->getPriceFormatted() : ''; ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group js-price-status d-none">
                        <div class="btn btn-red lg w-100 readonly">
                            <i class="far fa-frown mr-1"></i> Цена ниже средней!
                        </div>
                    </div>
                    <div class="form-group js-price-status d-none">
                        <div class="btn btn-orange lg w-100 readonly">
                            <i class="far fa-meh mr-1"></i> Средняя цена!
                        </div>
                    </div>
                    <div class="form-group js-price-status d-none">
                        <div class="btn btn-dark-green lg w-100 readonly">
                            <i class="far fa-smile mr-1"></i> Хорошая цена!
                        </div>
                    </div>
                    <div class="form-group js-price-status d-none">
                        <div class="btn btn-green lg w-100 readonly">
                            <i class="far fa-grin mr-1"></i>
                            Отличная цена! Лучше, чем у 80% заказчиков
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <div class="row align-items-center">
                            <div class="col-auto">
                                <label class="switch">
                                    <input type="checkbox" name="rush" value="1" <?php echo $obOrder && intval($obOrder->getRush()) > 0 ? 'checked' : ''; ?>>
                                    <span class="switch__slider round"></span>
                                </label>
                            </div>
                            <div class="col-auto">
                                Выделить объявление на 24 часа (+10₽/убийство)
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <div class="h3">Итоговая стоимость заказа: <span class="js-order-sum"><?php echo $obOrder ? ($obOrder->getSumFormatted()) : ''; ?></span></div>
                    </div>
                </div>
                <div class="col-7">
                    <div class="form-group">
                        <select class="form-control js-chosen-select" name="location[]" data-placeholder="Локация"
                                multiple>
                            <option value=""></option>
                            <?php foreach ($obLocationCollection->getCollection() as $obLocationSection): ?>
                                <optgroup label="<?php echo $obLocationSection->getName(); ?>">
                                    <?php foreach ($obLocationSection->getElements() as $obLocationElement): ?>
                                        <option <?php echo $obOrder && $obOrder->getLocation()->getByKey($obLocationElement->getId()) ? 'selected' : ''; ?>
                                                value="<?php echo $obLocationElement->getId(); ?>"><?php echo $obLocationElement->getName(); ?></option>
                                    <?php endforeach; ?>
                                </optgroup>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="col-5">
                    <div class="form-group">
                        <select class="form-control js-chosen-select" name="level" data-placeholder="Уровень">
                            <option value=""></option>
                            <?php foreach ($obLevelCollection->getCollection() as $obLevel): ?>
                                <option <?php echo $obOrder && $obOrder->getLevelId() == $obLevel->getId() ? 'selected' : ''; ?>
                                        value="<?php echo $obLevel->getId(); ?>"><?php echo $obLevel->getUfName(); ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <input type="text" placeholder="Класс" name="class" class="form-control" value="<?php echo $obOrder ? $obOrder->getClass() : ''; ?>">
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <textarea placeholder="Комментарий к заказу" name="comment" class="form-control"><?php echo $obOrder ? $obOrder->getPreviewText() : ''; ?></textarea>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <button type="submit" class="btn btn-yellow w-100 lg"><?php echo $obOrder ? 'Сохранить заказ' : 'Разместить заказ'; ?></button>
                    </div>
                </div>
            </div>
            <div class="js-response"></div>
        </form>
    </div>
    <div class="col-12 col-xl-4">
        <div class="text-smile">
            <p>Помните, что среди заказчиков разыгрываются ценные призы, которые невозможно передать и от них не
                получить выгоду на твинках.</p>

            <p><b>Перед заказом проверьте, пожалуйста, корректность введенных данных в профиле.
                    Бонус будет привязан к вашему никнейму.</b></p>
        </div>
    </div>
</div>
<script>
    window.rangePrice = <?php echo json_encode($arRangePrice['range']); ?>;
</script>
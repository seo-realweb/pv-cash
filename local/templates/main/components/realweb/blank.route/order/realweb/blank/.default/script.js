var priceTimeout = 0;
$(document).on('change keyup', '[name="price"]', function (e) {
    var $this = $(this), fPrice = parseFloat($this.val());
    if (fPrice && window.rangePrice) {
        $('.js-price-status').addClass('d-none');
        if (fPrice < window.rangePrice[0]) {
            $('.js-price-status:eq(0)').removeClass('d-none');
        } else if (fPrice >= window.rangePrice[0] && fPrice < window.rangePrice[1]) {
            $('.js-price-status:eq(0)').removeClass('d-none');
        } else if (fPrice >= window.rangePrice[1] && fPrice < window.rangePrice[2]) {
            $('.js-price-status:eq(1)').removeClass('d-none');
        } else if (fPrice >= window.rangePrice[2] && fPrice < window.rangePrice[3]) {
            $('.js-price-status:eq(2)').removeClass('d-none');
        } else if (fPrice >= window.rangePrice[3]) {
            $('.js-price-status:eq(3)').removeClass('d-none');
        }
        // clearTimeout(priceTimeout);
        // priceTimeout = setTimeout(function () {
        //     ajaxRequest({
        //         url: '/api/order/index/get-status-price/',
        //         params: {price: $this.val()}
        //     }, function (response) {
        //         $('.js-price-status').addClass('d-none');
        //         if (response.data.result === 1) {
        //             if (response.data.status === -1) {
        //                 $('.js-price-status:eq(0)').removeClass('d-none');
        //             } else if (response.data.status === 0) {
        //                 $('.js-price-status:eq(1)').removeClass('d-none');
        //             } else if (response.data.status === 1) {
        //                 $('.js-price-status:eq(2)').removeClass('d-none');
        //             } else if (response.data.status === 2) {
        //                 $('.js-price-status:eq(3)').removeClass('d-none');
        //             }
        //         }
        //     });
        // }, 1000);
    }
});

$(document).on('change', '[name="price"], [name="count"], [name="rush"]', function (e) {
    if ($('[name="price"]').val() && $('[name="count"]').val()) {
        var price = parseFloat($('[name="price"]').val()),
            count = parseFloat($('[name="count"]').val())
        isHot = $('[name="rush"]:checked').length ? true : false;
        if (isHot) {
            price += 10;
        }
        $('.js-order-sum').html((price * count) + '₽');
    }
});

$(document).on('change', '.js-save-form input[type="text"], .js-save-form input[type="number"], .js-save-form select, .js-save-form textarea', function () {
    var $this = $(this),
        $form = $this.parents('form');
    if ($this.attr('name')) {
        localStorage[$form.attr('name') + '_' + $this.attr('name')] = $this.val();
    }
});

$(document).ready(function () {
    $('.js-save-form input[type="text"], .js-save-form input[type="number"], .js-save-form select, .js-save-form textarea').each(function () {
        var $this = $(this),
            $form = $this.parents('form'),
            strValue = localStorage[$form.attr('name') + '_' + $this.attr('name')];
        if (strValue && $this.val() == '') {
            if ($this.is("select")) {
                $.each(strValue.split(","), function (i, e) {
                    $this.find("option[value='" + e + "']").prop("selected", true);
                });
                $this.trigger("chosen:updated");
            } else {
                $this.val(strValue);
                $this.trigger('input');
            }
        }
    });
});
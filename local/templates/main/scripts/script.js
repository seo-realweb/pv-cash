var modalOpenTriggers = document.querySelectorAll('.js-popup-open')
var modalCloseTrigger = document.querySelector('.js-popup-close')
var bodyBlackout = document.querySelector('.blackout')

// function initPopup() {
//     $()
//     modalOpenTriggers.forEach(function (trigger) {
//         trigger.addEventListener('click', function (event) {
//             event.preventDefault();
//             popupOpen(this);
//         })
//     });
// }

$(document).on('click', '.js-popup-open', function (event) {
    event.preventDefault();
    popupOpen(this);
});

$(document).on('click', '.blackout', function (event) {
    event.preventDefault();
    closePopup();
});

$(document).on('click', '.js-popup-close', function (event) {
    event.preventDefault();
    closePopup();
});

function getCoords(elem) {
    var box = elem.getBoundingClientRect();

    var body = document.body;
    var docEl = document.documentElement;

    var scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;
    var scrollLeft = window.pageXOffset || docEl.scrollLeft || body.scrollLeft;

    var clientTop = docEl.clientTop || body.clientTop || 0;
    var clientLeft = docEl.clientLeft || body.clientLeft || 0;

    var top = box.top + scrollTop - clientTop;
    var left = box.left + scrollLeft - clientLeft;

    return {
        top: Math.round(top),
        left: Math.round(left),
        right: (Math.round(left) + elem.offsetWidth),
        bottom: (Math.round(top) + elem.offsetHeight),
        width: elem.offsetWidth,
        height: elem.offsetHeight
    };
}

function popupOpen(_this) {
    var popupModal = document.querySelector(_this.dataset.target),
        //coords = _this.getBoundingClientRect(),
        coords = getCoords(_this),
        currentPopup = document.querySelector('.popup_open');

    popupModal.classList.add('popup_open');
    bodyBlackout.classList.add('blackout_in');
    if (_this.dataset.backout) {
        bodyBlackout.dataset.backout = _this.dataset.backout;
    } else {
        bodyBlackout.dataset.backout = '';
    }
    if (_this.dataset.close_callback) {
        popupModal.dataset.close_callback = _this.dataset.close_callback;
    }
    if (_this.dataset.position === 'bottom-right') {
        if (_this.dataset.tooltip) {
            popupModal.style.left = coords.right - popupModal.clientWidth + 20 + 'px';
            popupModal.style.top = coords.bottom + document.body.scrollTop + 20 + 'px';
        } else {
            popupModal.style.left = coords.right - popupModal.clientWidth + 'px';
            popupModal.style.top = coords.bottom + 10 + 'px';
        }
        popupModal.style.position = 'absolute';
    }

    if (_this.dataset.position === 'bottom-center') {
        popupModal.style.left = coords.left + Math.ceil(coords.width / 2) - Math.ceil(popupModal.clientWidth / 2) + 'px';
        popupModal.style.top = coords.bottom + 10 + 'px';
        popupModal.style.position = 'absolute';
    }

    if (_this.dataset.position === 'top-right') {
        popupModal.style.left = coords.right + 10 + 'px';
        popupModal.style.top = coords.top + 'px';
        popupModal.style.position = 'absolute';
    }

    if (_this.dataset.position === 'bottom') {
        popupModal.style.top = coords.bottom + 10 + 'px';
        popupModal.style.position = 'absolute';
    }

    if (_this.dataset.position === 'current' && currentPopup) {
        popupModal.style.top = currentPopup.style.top;
        popupModal.style.left = currentPopup.style.left;
        popupModal.style.position = 'absolute';
    }

    if (_this.dataset.right) {
        popupModal.style.right = _this.dataset.right + 'px';
    }

    if (_this.dataset.position === 'center') {
        popupModal.style.left = 'calc(50% - ' + (popupModal.clientWidth / 2) + 'px)';
        popupModal.style.top = 'calc(50% - ' + (popupModal.clientHeight / 2) + 'px)';
        popupModal.style.position = 'fixed';
    }

    if (currentPopup) {
        currentPopup.classList.remove('popup_open');
    }
}

function closePopup() {
    var popupModal = document.querySelector('.popup_open');
    popupModal.classList.remove('popup_open');
    bodyBlackout.classList.remove('blackout_in');
    if (popupModal.dataset.close_callback && window[popupModal.dataset.close_callback]) {
        window[popupModal.dataset.close_callback]();
    }
}

$(document).on('click', '[data-toggle="collapse"]', function () {
    if ($(this).parents('[data-parent]').length) {
        $(this).parents('[data-parent]').attr('data-expanded', $(this).attr('aria-expanded'));
    }
});

$(document).on('change', '.js-set-input-value', function (e) {
    $('[name="' + $(this).attr('data-name') + '"]').val($(this).val()).trigger('change');
});

function submitForm($form, data) {
    BX.closeWait();
    BX.showWait();
    $.ajax({
        type: $form.attr('method'),
        url: $form.attr('action'),
        data: data,
        contentType: false,
        processData: false,
        beforeSend: function () {
        }
    }).done(function (response) {
        response = jQuery.parseJSON(response);
        BX.closeWait();
        if (response.data.message) {
            if (response.data.result === 1) {
                $form.find('.js-response').html('<div class="alert alert-success">' + response.data.message + '</div>');
            } else {
                $form.find('.js-response').html('<div class="alert alert-danger">' + response.data.message + '</div>');
            }
        }
        if (response.data.result === 1) {
            if (response.data.url) {
                location.href = response.data.url;
            }
            // if (response.data.reload) {
            //     location.reload();
            // }
            if ($form.find('[name="ajax_reload"]').length) {
                ajaxReload($form);
            }
            if ($form.find('[name="close_popup"]').length) {
                setTimeout(function () {
                    closePopup();
                }, 500);
            }
            if ($form.find('[name="replace"]').length && response.data.html !== undefined) {
                $($form.find('[name="replace"]').val()).replaceWith(response.data.html);
            }
            if ($form.find('[name="callback"]').length) {
                var functionCallback = $form.find('[name="callback"]').val();
                if (window[functionCallback]) {
                    window[functionCallback]($form, response);
                }
            }
            $form.trigger("reset");
            $form.find('input, textarea, select').each(function () {
                if (!$(this).attr('data-remember')) {
                    delete localStorage[$form.attr('name') + '_' + $(this).attr('name')];
                }
            });
            initPlugins();
        }
    });
    return false;
}

$(document).on('submit', '.js-submit-form', function (e) {
    e.preventDefault();
    var $form = $(this),
        data = new FormData($form[0]);
    submitForm($form, data);
});

function ajaxReload($this) {
    var url = window.location.href.split('#')[0],
        bxajaxid = $this.parents('[id ^= comp_]').attr('id').replace("comp_", "");
    if (bxajaxid) {
        url = addParameterToURL(url, "bxajaxid=" + bxajaxid);
        BX.ajax.insertToNode(url, "comp_" + bxajaxid);
    }
}

function ajaxRequest(data, callback) {
    $.ajax({
        type: data.type ? data.type : 'post',
        url: data.url,
        data: data.params ? data.params : {},
        beforeSend: function () {
        }
    }).done(function (response) {
        response = jQuery.parseJSON(response);
        if (callback) {
            callback(response);
        }
    });
}

$(document).on('click', '.js-tooltip-action', function (e) {
    e.preventDefault();
    var $this = $(this);
    ajaxRequest({
        url: '/api/tooltip/index/edit/' + $(this).attr('href'),
    }, function (response) {
        if (response.data.message) {
            if (response.data.result === 1) {
                $this.parents('.tooltip').find('.js-response').html('<div class="alert alert-success m-0 mt-1">' + response.data.message + '</div>');
            } else {
                $this.parents('.tooltip').find('.js-response').html('<div class="alert alert-danger m-0 mt-1">' + response.data.message + '</div>');
            }
        }
    });
});

$(document).on('click', '.js-load-more', function (e) {
    e.preventDefault();
    var $this = $(this),
        $parent = $this.parents('[data-parent]'),
        $items = $parent.find('[data-items]'),
        $navigation = $parent.find('[data-navigation]'),
        id = $parent.attr('id');
    if ($parent.length && $items.length && $navigation.length) {
        $.ajax({
            type: 'get',
            url: $this.attr('href'),
            data: {},
            beforeSend: function () {
            }
        }).done(function (response) {
            var parser = new DOMParser()
            var html = parser.parseFromString(response, "text/html");
            var element = html.getElementById(id);
            if (element) {
                $items.append($(element).find('[data-items]').html());
                $navigation.html($(element).find('[data-navigation]').html());
            }
        });
    }
});

$(document).on('change', '.js-change-submit', function (e) {
    e.preventDefault();
    if ($(this).attr('type') !== 'text') {
        $(this).parents('form').find('[type="submit"]:eq(0)').click();
    }
});

$(document).on('click', '.js-change-file', function (e) {
    e.preventDefault();
    $(this).siblings('[type="file"]').trigger('click');
});

var timeoutInputId;
$(document).on('keydown', '.js-change-submit', function (e) {
    var $this = $(this);
    if ($this.attr('type') === 'text' && $this.val().length > 2) {
        clearTimeout(timeoutInputId);
        timeoutInputId = setTimeout(function () {
            $this.parents('form').find('[type="submit"]:eq(0)').click();
        }, 1000);
    }
});

function btnAction($this, callback) {
    var strObject = $this.attr('data-object'),
        object = eval('({data:' + strObject + '})'),
        confirm = $this.attr('data-confirm'),
        prompt = $this.attr('data-prompt'),
        popupType = $this.attr('data-popup-type'),
        popupInput = $this.attr('data-popup-input'),
        popupMessage = $this.attr('data-popup-message');
    if (confirm) {
        if (!window.confirm(confirm)) {
            return false;
        }
    }
    if (prompt) {
        var strPrompt = window.prompt(prompt);
        if (strPrompt) {
            object.data.params.prompt = strPrompt;
        }
    }
    if (popupType && popupMessage) {
        var fakeLink = document.createElement('a'),
            popupId = 'popup-' + popupType;
        fakeLink.dataset.position = 'center';
        fakeLink.dataset.target = '#' + popupId;
        if (popupInput) {
            popupMessage += '<input type="text" name="prompt" placeholder="' + popupInput + '" class="form-control mt-3">';
        }
        document.getElementById(popupId).querySelector('.js-popup-body').innerHTML = popupMessage;
        $('#' + popupId).find('.js-popup-action').html('<button type="button" class="btn btn-' + popupType + ' js-popup-action-button">Да</button> &nbsp; <button type="button" class="btn btn-' + popupType + ' js-popup-close">Нет</button>');
        $('#' + popupId).find('.js-popup-action').find('.js-popup-action-button').bind('click', function () {
            if ($('.popup_open').find('[name="prompt"]').length) {
                object.data.params.prompt = $('.popup_open').find('[name="prompt"]').val();
            }
            _sendRequest($this, object, callback);
            closePopup();
        });
        popupOpen(fakeLink);
    } else {
        _sendRequest($this, object, callback);
    }
}

function _sendRequest($this, params, callback) {
    BX.closeWait();
    BX.showWait();
    ajaxRequest({
        url: params.data.url,
        params: params.data.params
    }, function (response) {
        BX.closeWait();
        if ($this.attr('data-reload')) {
            ajaxReload($this);
        }
        if (response.data) {
            if (response.data.message) {
                if (response.data.type === 'popup' || response.data.result === 0) {
                    var fakeLink = document.createElement('a'),
                        popupType = response.data.result === 0 ? 'danger' : 'success';
                    fakeLink.dataset.position = 'center';
                    fakeLink.dataset.target = '#popup-' + popupType;
                    document.getElementById('popup-' + popupType).querySelector('.js-popup-body').innerHTML = response.data.message;
                    $('#popup-' + popupType).find('.js-popup-action').html('<button type="button" class="btn btn-' + popupType + ' js-popup-close">Хорошо</button>');
                    popupOpen(fakeLink);
                }
            }
            if (response.data.result === 1) {
                if ($this.attr('data-action-remove')) {
                    $this.removeClass('js-btn-action');
                }
                if (response.data.url) {
                    setTimeout(function () {
                        location.href = response.data.url;
                    }, response.data.message ? 2000 : 500);
                }
            }
            if ($this.attr('data-replace') && response.data.html !== undefined) {
                $($this.attr('data-replace')).replaceWith(response.data.html);
            }
            if (callback) {
                callback(response);
            }
            if ($this.attr('data-callback')) {
                var functionCallback = $this.attr('data-callback');
                if (window[functionCallback]) {
                    window[functionCallback]($this, response);
                }
            }
            if ($this.attr('data-reload-user')) {
                reloadUserData();
            }
            setTimeout(function () {
                initPlugins();
            }, 500);
        }
    });
}

$(document).on('click change', '.js-btn-action', function (e) {
    e.preventDefault();
    btnAction($(this));
});

function addParameterToURL(url, param) {
    _url = url;
    _url += (_url.split('?')[1] ? '&' : '?') + param;
    return _url;
}

function initPlugins() {
    $(".js-chosen-select").chosen({width: '100%', no_results_text: "Не найдено:"});
    $(".js-lightgallery").lightGallery({
        thumbnail: true,
        selector: '.js-lightgallery-item'
    });
    $('[data-timer]').each(function () {
        if ($(this).text() === '00:00') {
            startTimer($(this).attr('data-timer'), $(this));
        }
    });
    $('[data-toggle="tooltip"]').tooltip({boundary: 'window', delay: {"hide": 0}});
    //initPopup();
}

BX.addCustomEvent("onAjaxFailure", function (data, params) {
    BX.closeWait();
});

BX.addCustomEvent("onAjaxSuccess", function (data, params) {
    initPlugins();
});

$(document).on('click', '.js-open-report', function (e) {
    e.preventDefault();
    $('#popup-report').find('.js-order-nickname').text($(this).attr('data-nickname'));
    $('#popup-report').find('[name="id"]').val($(this).attr('data-id'));
    $('#popup-report').find('[name="max"]').val($(this).attr('data-max'));
    $('#popup-report').find('[name="replace"]').val('#order-' + $(this).attr('data-id'));
    $('#popup-report').find('.js-report-info').addClass('d-none');
    $('#popup-report').find('[type="submit"]').attr('disabled', true);
    $('#popup-report').find('#drop-hint').removeClass('d-none');
    $('#popup-report').find('.js-response').empty();
    $('#popup-report').find('#image-preview').empty();
    dtFiles = [];
    fakeInput.files = null;

    popupOpen(this);
});

$(document).on('click', '.js-open-complaint', function (e) {
    e.preventDefault();
    $('#popup-complaint').find('[name="id"]').val($(this).attr('data-id'));
    popupOpen(this);
});

$(document).on('click', '.js-open-purpose', function (e) {
    if ($(this).attr('data-price')) {
        $('#popup-purse').find('[name="sum"]').val($(this).attr('data-price'));
    }
});

$(document).on('click', '.js-lightgallery-action', function (e) {
    e.preventDefault();
    $($(this).attr('data-target')).trigger('click');
    if ($(this).parents('.lg-outer').find('.lg-thumb').length) {
        $(this).parents('.lg-outer').find('.deletePicture').trigger('click');
    } else {
        $(this).parents('.lg-outer').find('.lg-close').trigger('click');
    }
});

$(document).on('mouseover', '.js-rating i', function () {
    var onStar = parseInt($(this).data('value'), 10);
    $(this).parent().children('i').each(function (e) {
        if (e < onStar) {
            $(this).addClass('active');
        } else {
            $(this).removeClass('active');
        }
    });

}).on('mouseout', '.js-rating i', function () {
    var onStar = parseInt($(this).parent().data('value'), 10);
    $(this).parent().children('i').each(function (e) {
        if (e < onStar) {
            $(this).addClass('active');
        } else {
            $(this).removeClass('active');
        }
    });
});

$(document).on('click', '.js-rating i', function (e) {
    e.preventDefault();
    var $this = $(this),
        $parent = $this.parent(),
        strObject = $parent.attr('data-object'),
        object = eval('({data:' + strObject + '})');
    object.data.params.rating = $this.data('value');
    _sendRequest($parent, object);
});

$(document).on('click', '.js-toggle-password', function (e) {
    e.preventDefault();
    var $input = $(this).siblings('input');
    if ($input.attr('type') === 'password') {
        $(this).siblings('input').attr('type', 'text');
    } else {
        $(this).siblings('input').attr('type', 'password');
    }
});

$(window).on('load', function () {
    $('.js-animate-shakex').addClass('animate__animated animate__shakeX fadeIn');
});

$(document).on('change', '.js-withdrawal-type', function () {
    var $number = $(this).parents('form').find('.js-withdrawal-number');
    if ($(this).val() === 'qiwi') {
        $number.mask("+7 (999) 999-99-99", {placeholder: "+7 (xxx) xxx-xx-xx"});
    } else if ($(this).val() === 'card') {
        $number.mask("9999 9999 9999 9999", {placeholder: "xxxx xxxx xxxx xxxx"});
    } else if ($(this).val() === 'webmoney') {
        $number.mask("R9999999999999999", {placeholder: "Rxxxxxxxxxxxxxxxx"});
    } else {
        $number.unmask();
    }
});

$(document).ready(function () {
    initPlugins();
    if (document.body.clientWidth < 992) {
        $(".js-chosen-select").each(function () {
            $(this).find('option:eq(0)').text($(this).data('placeholder'));
        });
    }
    var singleSlider = $(".js-range-single").slider();
    $(".js-range-single").on("change", function (slideEvt) {
        var id = $(this).attr('data-id');
        $(this).parents('form').find('[data-text="' + id + '"]').text(slideEvt.value.newValue + " ₽");
        $(this).parents('form').find('[data-value="' + id + '"]').val(slideEvt.value.newValue).trigger('change');
    });
    $('.js-range-value').on('input', function () {
        singleSlider.slider('setValue', $(this).val())
    });

    $('body').on('click', function (e) {
        $('[data-toggle=tooltip]').each(function () {
            // hide any open popovers when the anywhere else in the body is clicked
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.tooltip').has(e.target).length === 0) {
                $(this).tooltip('hide');
            }
        });
    });

    realweb.reloadUserDataBlocked = false;
    setInterval(function () {
        if (!realweb.reloadUserDataBlocked) {
            reloadUserData();
        }
    }, 10000);
});

function morph($n, $f1, $f2, $f5) {
    $n = Math.abs(parseInt($n)) % 100;
    if ($n > 10 && $n < 20) {
        return $f5;
    }
    $n = $n % 10;
    if ($n > 1 && $n < 5) {
        return $f2;
    }
    if ($n === 1) {
        return $f1;
    }

    return $f5;
}

function startTimer(duration, display) {
    var timer = duration, minutes, hours;
    setInterval(function () {
        hours = Math.floor((timer / (60 * 60)) % 24);
        minutes = Math.floor((timer / 60) % 60);
        hours = hours < 10 ? "0" + hours : hours;
        minutes = minutes < 10 ? "0" + minutes : minutes;
        display.text(hours + ":" + minutes);
        if (--timer < 0) {
            timer = duration;
        }
    }, 1000);
}

function reloadUserData() {
    if (!realweb.reloadUserDataBlocked) {
        realweb.reloadUserDataBlocked = true;
        ajaxRequest({
            url: '/api/user/index/reload/'
        }, function (response) {
            if (response.data.result === 1) {
                if (response.data.auth) {
                    $('.js-header-auth').replaceWith(response.data.auth);
                }
                if (response.data.executor && $('.js-profile-executor').length && !$('.js-profile-executor').parents('.popup_open').length) {
                    $('.js-profile-executor').replaceWith(response.data.executor);
                }
                if (response.data.customer && $('.js-profile-customer').length && !$('.js-profile-customer').parents('.popup_open').length) {
                    $('.js-profile-customer').replaceWith(response.data.customer);
                }
                if (response.data.notice) {
                    if (response.data.notice.count > 0) {
                        $('.js-header-notice-count').html(response.data.notice.count);
                        $('#popup-notice .js-popup-content').replaceWith(response.data.notice.html);
                        if (!$('#popup-notice').hasClass('popup_open')) {
                            $('.js-header-notice').trigger('click');
                            var audio = new Audio(realweb.template_path + '/media/notification.wav');
                            audio.play();
                        }
                    }
                }

                setTimeout(function () {
                    initPlugins();
                    realweb.reloadUserDataBlocked = false;
                }, 200);
            }
        });
    }
}

function clearNotice() {
    ajaxRequest({
        url: '/api/notice/index/clear/',
    }, function (response) {
        if (response.data.result === 1) {
            $('.js-header-notice-count').html(response.data.count);
            $('#popup-notice .js-popup-content').replaceWith(response.data.html);
        }
    });
}
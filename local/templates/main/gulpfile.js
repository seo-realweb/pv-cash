"use strict";

// Load plugins
const autoprefixer = require("gulp-autoprefixer");
const browsersync = require("browser-sync").create();
const cleanCSS = require("gulp-clean-css");
const del = require("del");
const gulp = require("gulp");
const header = require("gulp-header");
const merge = require("merge-stream");
const plumber = require("gulp-plumber");
const rename = require("gulp-rename");
const sass = require("gulp-sass");
const uglify = require("gulp-uglify");
var concat = require("gulp-concat");

// BrowserSync
function browserSync(done) {
    browsersync.init({
        server: {
            baseDir: "./"
        },
        port: 3000
    });
    done();
}

// BrowserSync reload
function browserSyncReload(done) {
    browsersync.reload();
    done();
}

// Clean vendor
function clean() {
    return del(["./webfonts/"]);
}

// Bring third party dependencies from node_modules into vendor directory
function modules() {
    var fontAwesomeWebfonts = gulp.src('./libs/fontawesome-pro/webfonts/**/*')
        .pipe(gulp.dest('./webfonts'));

    return merge(fontAwesomeWebfonts);
}

// CSS task
function css() {
    return gulp
        .src("./scss/**/*.scss")
        .pipe(plumber())
        .pipe(sass({
            outputStyle: "expanded",
            includePaths: "./node_modules",
        }))
        .on("error", sass.logError)
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(gulp.dest("./css"))
        .pipe(rename({
            suffix: ".min"
        }))
        .pipe(cleanCSS())
        .pipe(gulp.dest("./css"))
        .pipe(browsersync.stream());
}

// JS task
function js() {
    return gulp
        .src([
            './node_modules/jquery/dist/jquery.js',
            './node_modules/bootstrap/dist/js/bootstrap.bundle.js',
            './node_modules/bootstrap-slider/dist/bootstrap-slider.js',
            './node_modules/chosen-js/chosen.jquery.js',
            './node_modules/owl.carousel/dist/owl.carousel.js',
            './node_modules/lightgallery/dist/js/lightgallery.js',
            './node_modules/lightgallery/modules/lg-fullscreen.js',
            './node_modules/lightgallery/modules/lg-thumbnail.js',
            './node_modules/jquery.maskedinput/src/jquery.maskedinput.js',
            './libs/lightgallery/modules/lg-deletebutton.js',
            './scripts/*.js',
        ])
        .pipe(concat("script.js"))
        .pipe(gulp.dest('./js'))
        .pipe(uglify())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('./js'))
        .pipe(browsersync.stream());
}

// Watch files
function watchFiles() {
    gulp.watch("./scss/**/*", css);
    gulp.watch(["./scripts/*"], js);
    gulp.watch("./**/*.php", browserSyncReload);
}

// Define complex tasks
const vendor = gulp.series(clean, modules);
const build = gulp.series(gulp.parallel(css, js));
const watch = gulp.series(build, gulp.parallel(watchFiles, browserSync));

// Export tasks
exports.css = css;
exports.js = js;
exports.clean = clean;
exports.vendor = vendor;
exports.build = build;
exports.watch = watch;
exports.default = build;

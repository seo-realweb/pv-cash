<?php

use \Bitrix\Main\Page\Asset;
use \Realweb\Site\Site;
use Realweb\Site\User;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();
?>
    <!DOCTYPE html>
    <html lang="ru">
    <head>
        <title><? $APPLICATION->ShowTitle(); ?></title>
        <script data-skip-moving="true">
            var realweb = <?php echo json_encode(array(
                'user' => array(
                    'is_auth' => User::getInstance()->isAuth()
                ),
                'template_path' => SITE_TEMPLATE_PATH
            )); ?>;
        </script>
        <? $APPLICATION->ShowHead(); ?>
        <?
        $APPLICATION->ShowMeta('og:locale');
        $APPLICATION->ShowMeta('og:type');
        $APPLICATION->ShowMeta('og:title');
        $APPLICATION->ShowMeta('og:description');
        $APPLICATION->ShowMeta('og:url');
        $APPLICATION->ShowMeta('og:site_name');
        $APPLICATION->ShowMeta('og:image');
        ?>
        <?
        Asset::getInstance()->addString('<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />');
        Asset::getInstance()->addString('<link rel="shortcut icon" type="image/x-icon" href="' . SITE_TEMPLATE_PATH . '/images/favicon.ico" />');
        Asset::getInstance()->addString('<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">');
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/css/style.min.css');
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/script.min.js');
        ?>
<meta name="yandex-verification" content="00b6474b413adaf4" />

        <script>
            var meta = document.querySelector("meta[name='viewport']");
            var windowWidth = (window.innerWidth < window.screen.width) ? window.innerWidth : window.screen.width;
            if (windowWidth < 540) {
                meta.setAttribute("content", "width=" + 540 + ", user-scalable=no");
            }
        </script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(70819729, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/70819729" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
    </head>
<body>
    <div id="panel">
        <? $APPLICATION->ShowPanel(); ?>
    </div>
    <header class="header<?php if (User::getInstance()->isAuth()): ?> header_auth<?php endif; ?>">
        <div class="container">
            <nav class="navbar navbar-light navbar-expand-lg">
                <a class="navbar-brand" href="/">
                    <img src="<?php echo SITE_TEMPLATE_PATH; ?>/images/logo.png">
                </a>
                <button class="navbar-toggler"
                        type="button"
                        data-toggle="collapse"
                        data-target="#navbarHeader"
                        aria-controls="navbarHeader"
                        aria-expanded="false"
                        aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarHeader">
                    <?php if (User::getInstance()->isAuth()): ?>
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:menu",
                            "top_auth",
                            array(
                                "ALLOW_MULTI_SELECT" => "N",
                                "CHILD_MENU_TYPE" => "",
                                "DELAY" => "N",
                                "MAX_LEVEL" => "1",
                                "MENU_CACHE_GET_VARS" => array(""),
                                "MENU_CACHE_TIME" => "3600",
                                "MENU_CACHE_TYPE" => "A",
                                "MENU_CACHE_USE_GROUPS" => "N",
                                "ROOT_MENU_TYPE" => "top_auth",
                                "USE_EXT" => "Y"
                            ),
                            false,
                            array(
                                'HIDE_ICONS' => 'Y'
                            )
                        ); ?>
                    <?php else: ?>
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:menu",
                            "top",
                            array(
                                "ALLOW_MULTI_SELECT" => "N",
                                "CHILD_MENU_TYPE" => "",
                                "DELAY" => "N",
                                "MAX_LEVEL" => "1",
                                "MENU_CACHE_GET_VARS" => array(""),
                                "MENU_CACHE_TIME" => "3600",
                                "MENU_CACHE_TYPE" => "A",
                                "MENU_CACHE_USE_GROUPS" => "N",
                                "ROOT_MENU_TYPE" => "top",
                                "USE_EXT" => "Y"
                            ),
                            false,
                            array(
                                'HIDE_ICONS' => 'Y'
                            )
                        ); ?>
                    <?php endif; ?>
                    <?php $APPLICATION->IncludeComponent('realweb:blank', 'auth', array()); ?>
                </div>
            </nav>
        </div>
    </header>

<div class="content<?php $APPLICATION->AddBufferContent(array('Realweb\Site\Site', 'addClassContent')); ?>">
    <div class="container">
<?php $APPLICATION->AddBufferContent(array('Realweb\Site\Site', 'showTitle')); ?>

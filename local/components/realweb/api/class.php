<?php

use \Bitrix\Main;
use \Bitrix\Main\Localization\Loc as Loc;
use \Bitrix\Main\SystemException as SystemException;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();

class RealwebApiComponent extends CBitrixComponent
{

    /**
     *
     * @var \Realweb\Site\Main\Error[]
     */
    private $_errors = array();

    /**
     *
     * @var array|null
     */
    private $_data = null;

    /**
     * Load language file
     */
    public function onIncludeComponentLang()
    {
        $this->includeComponentLang(basename(__FILE__));
        Loc::loadMessages(__FILE__);
    }

    /**
     * Prepare Component Params
     */
    public function onPrepareComponentParams($arParams)
    {
        \Bitrix\Main\Loader::includeModule('realweb.api');
        $arVariables = array();
        $arPart = explode("/", trim(\Bitrix\Main\Context::getCurrent()->getRequest()->getRequestedPageDirectory(), "/"));
        $arVariables['api_folder'] = \Realweb\Site\ArrayHelper::getValue($arPart, 0);
        $arVariables['module'] = \Realweb\Site\ArrayHelper::getValue($arPart, 1);
        $arVariables['controller'] = \Realweb\Site\ArrayHelper::getValue($arPart, 2, 'index');
        $arVariables['action'] = \Realweb\Site\ArrayHelper::getValue($arPart, 3, 'index');

        return $arVariables;
    }

    /**
     * Start Component
     */
    public function executeComponent()
    {
        \Bitrix\Main\Loader::includeModule('realweb.api');
        $obEntity = $this->getEntity();
        $arData = null;
        if ($obEntity instanceof \Realweb\Site\Main\Controller) {
            $obEntity->setArParams($this->arParams);
            try {
                $strAction = 'action' . $this->upFirst($this->arParams['action']);
                if (method_exists($obEntity, $strAction)) {
                    $arData = $obEntity->$strAction();
                } else {
                    $this->setError(\Realweb\Site\Main\Error::NO_METHOD);
                }
            } catch (SystemException $e) {
                $this->setError(\Realweb\Site\Main\Error::DEFAULT_ERROR);
            }
        } else {
            $this->setError(\Realweb\Site\Main\Error::NO_OBJECT);
        }

        $this->_setData($arData);

        $arResult = array(
            'result' => $this->_getResult(),
            'data' => $this->_getData(),
            'errors' => $this->_getErrors(),
        );

        if (!is_array($arResult)) {
            $arResult = array();
        }
        echo json_encode($arResult);
        \CMain::FinalActions();
    }

    private function getClassName()
    {
        $strClassName = '\\Realweb\\Site\\Controller\\' . $this->upFirst($this->arParams['module']) . '\\' . $this->upFirst($this->arParams['controller']);
        if (class_exists($strClassName)) {
            return $strClassName;
        }
        return false;
    }

    /**
     *
     * @return boolean|\Realweb\Site\Main\Controller
     */
    private function getEntity()
    {
        $strClassName = $this->getClassName();
        if ($strClassName) {
            $obEntity = new $strClassName($this->upFirst($this->arParams['action']));
            $obEntity->setComponent($this);
            return $obEntity;
        }
        return false;
    }

    private function upFirst($str, $encoding = 'UTF-8')
    {
        $arResult = array();
        $arPart = explode('-', $str);
        foreach ($arPart as $part) {
            $part = mb_ereg_replace('^[\ ]+', '', $part);
            $part = mb_strtolower($part);
            $part = mb_strtoupper(mb_substr($part, 0, 1, $encoding), $encoding) . mb_substr($part, 1, mb_strlen($part), $encoding);
            $arResult[] = $part;
        }

        return implode("", $arResult);
    }

    /**
     *
     * @param string $strErrorCode
     * @return $this
     */
    public function setError($strErrorCode)
    {
        $obError = new \Realweb\Site\Main\Error($strErrorCode);
        $this->_errors[] = $obError;
        return $this;
    }

    /**
     *
     * @return \Realweb\Site\Main\Error[]
     */
    public function getErrors()
    {
        return $this->_errors;
    }

    /**
     *
     * @return array
     */
    private function _getErrors()
    {
        $arErrors = array();
        /* @var \Realweb\Site\Main\Error $obError */
        foreach ($this->_errors as $obError) {
            $arErrors[$obError->getType()] = $obError->getMessage();
        }
        return $arErrors;
    }

    /**
     *
     * @param mixed $data
     * @return $this
     */
    private function _setData($data)
    {
        $this->_data = $data;
        return $this;
    }

    private function _getData()
    {
        return $this->_data;
    }

    /**
     *
     * @return int
     */
    private function _getResult()
    {
        if (count($this->_errors) > 0) {
            return 0;
        }
        return 1;
    }

}

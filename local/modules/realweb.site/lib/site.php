<?php

namespace Realweb\Site;

use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Loader;
use Bitrix\Main\Data\Cache;
use Bitrix\Main\UserFieldTable;
use Bitrix\Main\Web\Uri;
use Realweb\BaseInclude\BaseIncludeTable;
use Bitrix\Main\Page\Asset;
use Realweb\Site\Controller\Tooltip;
use Realweb\Site\Model\Tooltip\Entity;

class Site
{
    /**
     * @var Site
     */
    protected static $instance;

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public static function getIncludeText($code)
    {
        if (Loader::includeModule('realweb.baseinclude')) {
            if ($arItem = BaseIncludeTable::getByCode($code)->fetch()) {
                return $arItem['TEXT'];
            }
        }
    }

    public static function definders()
    {
        Loader::includeModule('iblock');
        Loader::includeModule('form');
        $rsResult = \Bitrix\Iblock\IblockTable::getList(array(
            'select' => array('ID', 'IBLOCK_TYPE_ID', 'CODE'),
        ));
        while ($row = $rsResult->fetch()) {
            $CONSTANT = ToUpper(implode('_', array('IBLOCK', $row['IBLOCK_TYPE_ID'], $row['CODE'])));
            if (!defined($CONSTANT)) {
                define($CONSTANT, $row['ID']);
            }
        }
        $by = 's_id';
        $order = 'desc';
        $rsForms = \CForm::GetList($by, $order, array(), $is_filtered);
        while ($arForm = $rsForms->Fetch()) {
            $CONSTANT = ToUpper(implode('_', array('WEB_FORM', $arForm['SID'])));
            if (!defined($CONSTANT)) {
                define($CONSTANT, $arForm['ID']);
            }
        }
    }

    /**
     * @param string $strCode
     * @return Data\Data|Entity
     * @throws \Exception
     */
    public static function getTooltipByCode(string $strCode)
    {
        $obController = new Tooltip\Index();
        $obCollection = $obController->actionIndex();

        return $obCollection->getByCode($strCode);
    }

    public static function getSortLink($strSort, $strCode, $strOrder = 'asc')
    {
        if ($strCurrentSort = Application::getInstance()->getContext()->getRequest()->get($strSort)) {
            $arSort = explode('-', $strCurrentSort);
            if ($arSort[0] == $strCode) {
                $strOrder = $arSort[1] == 'asc' ? 'desc' : 'asc';
            }
        }
        $obUri = new \Realweb\Site\Main\Uri();
        $obUri->addParam($strSort, $strCode . '-' . $strOrder);

        return '<a href="' . $obUri->getPathQuery() . '"><i class="fal fa-sort-amount-' . ($strOrder == 'asc' ? 'down' : 'up') . '"></i></a>';
    }


    public static function localRedirect301($url)
    {
        LocalRedirect($url, false, "301 Moved Permanently");
        exit();
    }

    public static function isMainPage()
    {
        global $APPLICATION;
        return $APPLICATION->GetCurPage(false) == SITE_DIR;
    }

    public static function isLocal()
    {
        return stripos(Application::getInstance()->getContext()->getRequest()->getHttpHost(), '.loc') !== false;
    }

    public static function isOrderPage()
    {
        global $APPLICATION;
        return $APPLICATION->GetCurPage(false) == '/basket/';
    }

    public static function is404Page()
    {
        return (defined("ERROR_404") && ERROR_404 == "Y");
    }

    public static function showIncludeText($code, $isHideChange = false)
    {
        global $APPLICATION;
        $APPLICATION->IncludeComponent(
            "realweb:base.include",
            ".default",
            array(
                "CODE" => $code,
                "COMPONENT_TEMPLATE" => ".default",
                "EDIT_TEMPLATE" => ""
            ),
            false,
            array(
                "SHOW_ICON" => $isHideChange ? "Y" : 'N',
            )
        );
    }

    public function showTitle()
    {
        global $APPLICATION;
        $html = '';
        $strTitle = $APPLICATION->GetTitle();
        if ($APPLICATION->GetPageProperty('HIDE_TITLE', "N") != "Y") {
            $html = "<h1 class='h3' id=\"pagetitle\">" . $strTitle . "</h1>";
        }

        return $html;
    }

    public function addClassContent()
    {
        global $APPLICATION;
        $class = $APPLICATION->GetPageProperty('CLASS_CONTENT', "");
        if ($class != "") {
            return " " . $class;
        }
    }

    public function addHeadingClass()
    {
        global $APPLICATION;
        $class = $APPLICATION->GetPageProperty('HEADING_CLASS', "justify-content-center");

        return " " . $class;
    }

    public function addClassContainer()
    {
        global $APPLICATION;
        if ($APPLICATION->GetPageProperty('IS_FLUID', 'N') == "Y") {
            return "container-fluid";
        } elseif ($APPLICATION->GetPageProperty('IS_FULL', 'N') == "Y") {
            return "container-full";
        } elseif ($APPLICATION->GetPageProperty('IS_NARROW', 'N') == "Y") {
            return "container container-narrow";
        } else {
            return "container";
        }
    }

    public static function setCookie($cookName, $value, $expire = false)
    {
        $context = \Bitrix\Main\Application::getInstance()->getContext();
        if ($cookName && $value) {
            $cookie = new \Bitrix\Main\Web\Cookie($cookName, $value, $expire ? $expire : (time() + 60 * 60 * 24));
            //$cookie->setDomain($context->getServer()->getHttpHost());
            $cookie->setDomain($_SERVER['SERVER_NAME']);
            $cookie->setHttpOnly(false);
            $context->getResponse()->addCookie($cookie);
            $context->getResponse()->flush("");
        }
    }

    public static function getMenu($iblockId, $level, $type = '')
    {
        global $APPLICATION;
        $aMenuLinksExt = array();
        if (Loader::IncludeModule('iblock')) {
            $arFilter = array(
                "SITE_ID" => SITE_ID,
                "ID" => $iblockId
            );

            $dbIBlock = \CIBlock::GetList(array('SORT' => 'ASC', 'ID' => 'ASC'), $arFilter);
            $dbIBlock = new \CIBlockResult($dbIBlock);

            if ($arIBlock = $dbIBlock->GetNext()) {
                if (defined("BX_COMP_MANAGED_CACHE"))
                    $GLOBALS["CACHE_MANAGER"]->RegisterTag("iblock_id_" . $arIBlock["ID"]);

                if ($arIBlock["ACTIVE"] == "Y") {
                    $filterSectionName = 'arrFilterSection' . $type;
                    $GLOBALS[$filterSectionName] = array(
                        'UF_HIDE_MENU' => false
                    );
                    $filterElementName = 'arrFilterElement' . $type;
                    $GLOBALS[$filterElementName] = array();
                    if ($type) {
                        $GLOBALS[$filterSectionName]['UF_' . strtoupper($type) . '_MENU'] = false;
                        $GLOBALS[$filterElementName]['PROPERTY_' . strtoupper($type) . '_MENU'] = false;
                    }
                    $aMenuLinksExt = $APPLICATION->IncludeComponent("realweb:menu.sections", "", array(
                        "IS_SEF" => "Y",
                        "SEF_BASE_URL" => "",
                        "SECTION_PAGE_URL" => $arIBlock['SECTION_PAGE_URL'],
                        "DETAIL_PAGE_URL" => $arIBlock['DETAIL_PAGE_URL'],
                        "IBLOCK_TYPE" => $arIBlock['IBLOCK_TYPE_ID'],
                        "IBLOCK_ID" => $arIBlock['ID'],
                        "DEPTH_LEVEL" => $level,
                        "CACHE_TYPE" => "N",
                        "SECTION_FILTER_NAME" => $filterSectionName,
                        "ELEMENT_FILTER_NAME" => $filterElementName
                    ), false, array('HIDE_ICONS' => 'Y'));
                }
            }

            if (defined("BX_COMP_MANAGED_CACHE"))
                $GLOBALS["CACHE_MANAGER"]->RegisterTag("iblock_id_new");
        }
        return $aMenuLinksExt;
    }

    public static function getUserFieldEnumValues($arFilter, $arFilterValues = array())
    {
        $arResult = array();
        $cacheTime = 3600;
        $cacheDir = __FUNCTION__;
        $cacheId = md5(__FUNCTION__ . "|" . serialize(array($arFilter, $arFilterValues)));
        $cache = Cache::createInstance();
        if ($cache->initCache($cacheTime, $cacheId, $cacheDir)) {
            $arResult = $cache->GetVars();
        } elseif ($cache->StartDataCache()) {
            $rows = UserFieldTable::getList(array(
                'select' => array('ID'),
                'filter' => $arFilter
            ))->fetch();
            $FIELD_STATUS_ID = $rows['ID'];
            $cUserFieldsEnum = new \CUserFieldEnum();
            $rows = $cUserFieldsEnum->GetList(array("ID" => "ASC"), array_merge(array('USER_FIELD_ID' => $FIELD_STATUS_ID), $arFilterValues));
            while ($row = $rows->Fetch()) {
                $arResult[$row['ID']] = $row;
            }
            $cache->endDataCache($arResult);
        }
        return $arResult;
    }

    public static function getPropEnumValues($arFilter)
    {
        $arValues = [];
        $rsPropertyEnums = \CIBlockPropertyEnum::GetList(array("DEF" => "DESC", "SORT" => "ASC"), $arFilter);
        while ($enumFields = $rsPropertyEnums->GetNext()) {
            $arValues[$enumFields["ID"]] = $enumFields;
        }
        return $arValues;
    }

    public static function getPropValue($CODE, $IBLOCK_ID, $ELEMENT_ID)
    {
        $arResult = array();
        if (Loader::includeModule('iblock')) {
            $dbProperty = \CIBlockElement::getProperty(
                $IBLOCK_ID,
                $ELEMENT_ID,
                "sort",
                "asc",
                array("CODE" => $CODE)
            );
            while ($arProperty = $dbProperty->Fetch()) {
                $arResult[] = $arProperty;
            }
        }
        return $arResult;
    }

    public static function setPropValue($CODE, $VALUE, $IBLOCK_ID, $ELEMENT_ID)
    {
        if (Loader::includeModule('iblock')) {
            \CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, $IBLOCK_ID, array($CODE => $VALUE ? $VALUE : false));
        }
    }

    public static function normalizePhone($phone)
    {
        return preg_replace('/[^0-9]/', "", $phone);
    }

    public static function getDomain()
    {
        return (\CMain::IsHTTPS() ? 'https' : 'http') . "://" . $_SERVER['SERVER_NAME'];
    }

    public static function getHost()
    {
        return $_SERVER['SERVER_NAME'];
    }

    public static function getIBlockElements($filter)
    {
        $arResult = array();
        if (Loader::includeModule("iblock")) {
            global $CACHE_MANAGER;
            $cacheTime = 3600;
            $cacheDir = "/" . __FUNCTION__;
            $cacheId = md5(__FUNCTION__ . "|" . serialize($filter));
            $cache = Cache::createInstance();
            if ($cache->initCache($cacheTime, $cacheId, $cacheDir)) {
                $arResult = $cache->GetVars();
            } elseif ($cache->StartDataCache()) {
                $CACHE_MANAGER->StartTagCache($cacheDir);
                $rsElement = \CIBlockElement::GetList(
                    array("SORT" => "ASC"),
                    $filter,
                    false,
                    false,
                    array("*", "PROPERTY_*")
                );
                while ($obElement = $rsElement->GetNextElement()) {
                    $arFields = $obElement->GetFields();
                    $arFields['FIELDS'] = $arFields;
                    if ($arFields['PREVIEW_PICTURE']) $arFields['PREVIEW_PICTURE'] = \CFile::GetFileArray($arFields['PREVIEW_PICTURE']);
                    if ($arFields['DETAIL_PICTURE']) $arFields['DETAIL_PICTURE'] = \CFile::GetFileArray($arFields['DETAIL_PICTURE']);
                    $arFields['PROPERTIES'] = $obElement->GetProperties();
                    foreach ($arFields['PROPERTIES'] as &$prop) {
                        if ($prop['PROPERTY_TYPE'] == 'F') {
                            if (is_array($prop['VALUE'])) {
                                foreach ($prop['VALUE'] as $val) {
                                    $prop['DISPLAY_VALUE'][] = \CFile::GetFileArray($val);
                                }
                            } else {
                                $prop['DISPLAY_VALUE'] = \CFile::GetFileArray($prop['VALUE']);
                            }
                        }
                        $arFields['PROPS'][$prop['CODE']] = $prop['VALUE'];
                        $arFields['DISPLAY_PROPERTIES'][$prop['CODE']] = \CIBlockFormatProperties::GetDisplayValue($arFields, $prop, "catalog_out");
                    }
                    $arResult[$arFields['ID']] = $arFields;
                }
                $CACHE_MANAGER->RegisterTag("iblock_id_" . $filter['IBLOCK_ID']);
                $CACHE_MANAGER->EndTagCache();
                $cache->endDataCache($arResult);
            }
        }
        return $arResult;
    }

    public static function getSections($filter, $cntElement = false, $withElements = false)
    {
        $arResult = array();
        if (Loader::includeModule("iblock")) {
            global $CACHE_MANAGER;
            $cacheTime = 3600;
            $cacheDir = "/" . __FUNCTION__;
            $cacheId = md5(__FUNCTION__ . "|" . serialize($filter) . "|" . $cntElement . "|" . $withElements);
            $cache = Cache::createInstance();
            if ($cache->initCache($cacheTime, $cacheId, $cacheDir)) {
                $arResult = $cache->GetVars();
            } elseif ($cache->StartDataCache()) {
                $CACHE_MANAGER->StartTagCache($cacheDir);
                $rsElement = \CIBlockSection::GetList(
                    array("SORT" => "ASC"),
                    $filter,
                    $cntElement,
                    array("*", "UF_*")
                );
                while ($obElement = $rsElement->GetNext()) {
                    if ($withElements && !$obElement['ELEMENT_CNT']) {
                        continue;
                    }
                    $arResult[$obElement['ID']] = $obElement;
                }
                $CACHE_MANAGER->RegisterTag("iblock_id_" . $filter['IBLOCK_ID']);
                $CACHE_MANAGER->EndTagCache();
                $cache->endDataCache($arResult);
            }
        }
        return $arResult;
    }

    public static function getTreeMenu($arResult)
    {
        $arItems = [];
        $currentItem = false;
        $i = 0;
        $prev = 0;
        $parent = false;
        foreach ($arResult as $arItem) {
            if ($prev > $arItem['DEPTH_LEVEL']) {
                $currentItem = &$parent;
            }
            if ($arItem['DEPTH_LEVEL'] == 1) {
                $arItems[$i] = $arItem;
                $currentItem = &$arItems[$i];
            } else {
                $currentItem['CHILD'][$i] = $arItem;
                if ($arItem['IS_PARENT']) {
                    $parent = &$currentItem;
                    $currentItem = &$currentItem['CHILD'][$i];
                }
            }
            $prev = $arItem['DEPTH_LEVEL'];
            $i++;
        }

        return $arItems;
    }

    /**
     * @return Uri
     * @throws \Bitrix\Main\SystemException
     */
    public static function getUri()
    {
        $uri = new Uri(Application::getInstance()->getContext()->getRequest()->getRequestUri());
        $uri->deleteParams(["bxajaxid"]);

        return $uri;
    }

    public static function isCheckGooglePageSpeed()
    {
        return strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome-Lighthouse') !== false;
    }

    public static function isMobile()
    {
        $obMobileDetect = new MobileDetect();

        return $obMobileDetect->isMobile() && !$obMobileDetect->isTablet();
    }

    public static function GetProductByIdOffer($ID)
    {
        if (\Bitrix\Main\Loader::includeModule("iblock")) {
            $res = \CIBlockElement::GetList(
                array(),
                array(
                    'ID' => $ID,
                    'ACTIVE' => 'Y',
                    'IBLOCK_ID' => IBLOCK_CATALOG_OFFERS
                ),
                false,
                array('nTopCount' => '1'),
                array('ID', 'IBLOCK_ID', 'PROPERTY_CML2_LINK')
            );
            if ($element = $res->GetNext()) {
                return $element['PROPERTY_CML2_LINK_VALUE'];
            }
        }
        return false;
    }

    public static function getProperty($CODE, $IBLOCK_ID, $ELEMENT_ID)
    {
        $arResult = array();
        if (\Bitrix\Main\Loader::includeModule('iblock')) {
            $dbProperty = \CIBlockElement::getProperty(
                $IBLOCK_ID,
                $ELEMENT_ID,
                "sort",
                "asc",
                array("CODE" => $CODE)
            );
            while ($arProperty = $dbProperty->Fetch()) {
                $arResult[] = $arProperty;
            }
        }
        return $arResult;
    }

    public static function epilogAction()
    {
        global $APPLICATION;
        $request = Application::getInstance()->getContext()->getRequest();
        foreach ($request->toArray() as $key => $value) {
            if (stripos($key, 'PAGEN_') !== false) {
                Asset::getInstance()->addString('<link rel="canonical" href="' . self::getDomain() . $APPLICATION->GetCurPage(false) . '"/>', true);
            }
        }
        $APPLICATION->SetPageProperty('title', str_replace(array('"', '&nbsp;'), array('', ' '), htmlspecialchars_decode($APPLICATION->GetPageProperty('title'))));
        $APPLICATION->SetPageProperty('description', str_replace(array('"', '&nbsp;'), array('', ' '), htmlspecialchars_decode($APPLICATION->GetPageProperty('description'))));
        $APPLICATION->SetPageProperty('og:locale', 'ru_RU');
        $APPLICATION->SetPageProperty('og:type', 'website');
        $APPLICATION->SetPageProperty('og:title', $APPLICATION->GetPageProperty('title'));
        $APPLICATION->SetPageProperty('og:description', $APPLICATION->GetPageProperty('description'));
        $APPLICATION->SetPageProperty('og:url', Site::getDomain() . \Bitrix\Main\Application::getInstance()->getContext()->getRequest()->getRequestUri());
        $APPLICATION->SetPageProperty('og:site_name', 'Pv-cash');
        $APPLICATION->SetPageProperty('og:image', Site::getDomain() . ($APPLICATION->GetPageProperty('IMAGE') ?: (SITE_TEMPLATE_PATH . "/local/templates/main/images/logo.png")));
        User::getInstance()->setSessionValue('new_order_id', null);

        if ($arMessage = User::getInstance()->getFlashMessage()) {
            $APPLICATION->IncludeComponent("realweb:blank", "flash_message", $arMessage);
        }
        $APPLICATION->IncludeComponent("realweb:blank", "page_message", array());
    }

    public static function getCurrentPage()
    {
        global $APPLICATION;
        return $APPLICATION->GetCurPage(false);
    }

    public static function getIp()
    {
        return Application::getInstance()->getContext()->getServer()->get('REMOTE_ADDR');
    }

    public static function getCommissionSum($iSum, $strType)
    {
        if ($fPercent = doubleval(Option::get('realweb.site', 'commission_' . $strType . '_percent'))) {
            $iCommission = $iSum * ($fPercent / 100);
            if ($fMin = doubleval(Option::get('realweb.site', 'commission_' . $strType . '_min'))) {
                if ($iCommission < $fMin) {
                    $iCommission = $fMin;
                }
            }
            $iCommission = ceil($iCommission);
            if ($strType == 'in') {
                $iSum += $iCommission;
            } else {
                $iSum -= $iCommission;
            }
        }

        return $iSum;
    }

    public static function getCommissionReturnSum($iSum, $strType)
    {
        if ($fPercent = doubleval(Option::get('realweb.site', 'commission_' . $strType . '_percent'))) {
            $iCommission = $iSum * ($fPercent / 100);
            if ($fMin = doubleval(Option::get('realweb.site', 'commission_' . $strType . '_min'))) {
                if ($iCommission < $fMin) {
                    $iCommission = $fMin;
                }
            }
            $iCommission = ceil($iCommission);
            if ($strType == 'in') {
                $iSum -= $iCommission;
            } else {
                $iSum += $iCommission;
            }
        }

        return $iSum;
    }
}

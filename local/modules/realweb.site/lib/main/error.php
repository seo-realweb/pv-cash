<?php

namespace Realweb\Site\Main;
/**
 * Class \Realweb\Api\Model\Main\Error
 *
 */
class Error
{

    const DEFAULT_ERROR = 'DEFAULT';
    const NO_OBJECT = 'NO_OBJECT';
    const NO_METHOD = 'NO_METHOD';
    const NO_PARAMS = 'NO_PARAMS';
    const FAILURE = 'FAILURE';
    const VALID = 'INVALID';
    const NO_ENTITY = 'NO_ENTITY';
    const PERMISSION_DENIED = 'PERMISSION_DENIED';
    const NO_DATA = 'NO_DATA';

    public static $arErrors = array(
        self::DEFAULT_ERROR => array(
            'message' => 'Ошибка'
        ),
        self::NO_OBJECT => array(
            'message' => 'Объект запроса не найден'
        ),
        self::NO_METHOD => array(
            'message' => 'Метод не поддерживается'
        ),
        self::NO_PARAMS => array(
            'message' => 'Недостаточно параметров для запроса'
        ),
        self::FAILURE => array(
            'message' => 'Ошибка'
        ),
        self::VALID => array(
            'message' => 'Данные не валидны'
        ),
        self::NO_ENTITY => array(
            'message' => 'Сущность не найдена'
        ),
        self::PERMISSION_DENIED => array(
            'message' => 'Доступ запрещен'
        ),
        self::NO_DATA => array(
            'message' => 'Нет данных'
        ),
    );

    /**
     *
     * @var string
     */
    private $type = "DEFAULT";
    private $message = "";
    private $field;
    private $options;

    public function __construct($strErrorCode = "DEFAULT", $strFieldName = null)
    {
        $this->type = $strErrorCode;
        $this->field = $strFieldName;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setMessage(string $strMessage)
    {
        $this->message = $strMessage;
    }

    public function getMessage()
    {
        $arMessage = array();
        if (isset(Error::$arErrors[$this->type])) {
            $arMessage[] = Error::$arErrors[$this->type]['message'];
        }
        if (strlen($this->message) > 0) {
            $arMessage[] = $this->message;
        }
        return implode(': ', $arMessage);
    }

    public function setField(string $strFieldName)
    {
        $this->field = $strFieldName;
    }

    public function getField()
    {
        return $this->field;
    }

    /**
     *
     * @param array $arOptions
     */
    public function setOptions(array $arOptions)
    {
        $this->options = $arOptions;
    }

    /**
     *
     * @return mixed array|null
     */
    public function getOptions()
    {
        return $this->options;
    }

}

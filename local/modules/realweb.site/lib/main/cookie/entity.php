<?php

namespace Realweb\Site\Main\Cookie;

use Bitrix\Main\Application;
use Realweb\Site\Data\Data;

/**
 * Class Cookie
 * @package Realweb\Api\Model\Main\Cookie
 * @method string getName()
 * @method string getValue()
 * @method int getExpire()
 * @method string getPath()
 * @method string getDomain()
 * @method bool getSecure()
 * @method bool getHttpOnly()
 * @method string hasName()
 * @method string hasValue()
 * @method int hasExpire()
 * @method string hasPath()
 * @method string hasDomain()
 * @method bool hasSecure()
 * @method bool hasHttpOnly()
 * @method Entity setName(string $name)
 * @method Entity setValue(string $value)
 * @method Entity setExpire(int $expire)
 * @method Entity setPath(string $path)
 * @method Entity setDomain(string $domain)
 * @method Entity setSecure(bool $secure)
 * @method Entity setHttpOnly(bool $httpOnly)
 */
class Entity extends Data
{
    public function __construct($arData = array())
    {
        $this->setData($arData);
    }

    public function getData()
    {
        return $this->_data;
    }

    public function setExpireMonth()
    {
        $this->setExpire(time() + 3600 * 24 * 30);

        return $this;
    }

    public function save()
    {
        if (!$this->hasExpire()) {
            $this->setExpire(0);
        }
        if (!$this->hasDomain()) {
            $this->setDomain(Application::getInstance()->getContext()->getServer()->getHttpHost());
        }
        if (!$this->hasPath()) {
            $this->setPath("/");
        }
        if (!$this->hasSecure()) {
            $this->setSecure(false);
        }
        if (!$this->hasHttpOnly()) {
            $this->setHttpOnly(false);
        }
        if ($this->hasName() && $this->hasValue()) {
            setcookie($this->getName(), $this->getValue(), $this->getExpire(), $this->getPath(), $this->getDomain(), $this->getSecure(), $this->getHttpOnly());
            Collection::getInstance()->addItem($this);

            return true;
        }

        return false;
    }

    public function delete()
    {
        if ($this->hasName()) {
            $this->setValue('');
            $this->setExpire(0);
            $this->save();

            return true;
        }

        return false;
    }
}

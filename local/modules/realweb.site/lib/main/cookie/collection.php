<?php

namespace Realweb\Site\Main\Cookie;

/**
 * Class Collection
 * @package Realweb\Api\Model\Main\Cookie
 */
class Collection extends \Realweb\Site\Data\Collection
{
    protected static $instance = null;

    protected static function getModel()
    {
        return null;
    }

    /**
     * Collection constructor.
     */
    public function __construct()
    {
        foreach ($_COOKIE as $name => $value) {
            $this->_keys[] = $name;
            $this->_collection[] = new Entity(array(
                'name' => $name,
                'value' => $value,
                'expire' => null,
                'domain' => '',
                'path' => '/',
                'secure' => false
            ));
        }
    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param \Realweb\Site\Data\Data|\Realweb\Site\Data\Entity|Entity $obItem
     * @return $this|mixed
     */
    public function addItem($obItem)
    {
        if (($iCollectionKey = array_search($obItem->getName(), $this->_keys)) !== false) {
            $this->_collection[$iCollectionKey] = $obItem;
        } else {
            $this->_collection[] = $obItem;
            $this->_keys[] = $obItem->getName();
        }

        return $this;
    }

    /**
     * @param $strKey
     * @return \Realweb\Site\Data\Data|null|Entity
     */
    public function getByName($strKey)
    {
        return parent::getByKey($strKey);
    }

    /**
     * @param $strKey
     * @param null $defaultValue
     * @return string|null
     */
    public function getValue($strKey, $defaultValue = null)
    {
        /** @var Entity $obItem */
        if ($obItem =  parent::getByKey($strKey)) {
            return $obItem->getValue();
        }

        return $defaultValue;
    }

    /**
     * @param $name
     * @return bool
     */
    public function has($name)
    {
        return in_array($name, $this->_keys, true);
    }

    /**
     * @param $name
     * @return bool
     */
    public function remove($name)
    {
        /** @var Entity $obItem */
        if ($obItem = $this->getByName($name)) {
            $obItem->delete();
            $iKey = array_search($name, $this->_keys);
            unset($this->_collection[$iKey], $this->_keys[$name]);

            return true;
        }

        return false;
    }
}

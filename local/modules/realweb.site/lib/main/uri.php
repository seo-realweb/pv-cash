<?php

namespace Realweb\Site\Main;

/**
 * Class \Realweb\Api\Model\Utils\Url\Bitrix
 *
 */
class Uri extends \Bitrix\Main\Web\Uri
{
    private $params;

    /**
     * @param string $url
     */
    public function __construct($url = "")
    {
        if (strlen($url) == 0) {
            global $APPLICATION;
            $url = $APPLICATION->GetCurUri();
        }
        parent::__construct($url);
    }

    public function getParams()
    {
        if (is_null($this->params)) {
            $arParams = array();
            parse_str($this->query, $arParams);
            $this->params = $arParams;
        }
        return $this->params;
    }

    public function addParam($strKey, $strValue)
    {
        $this->getParams();
        $this->params[$strKey] = $strValue;
        $this->query = http_build_query($this->params, "", "&");
        return $this;
    }

    public function getFullUrl()
    {
        return $this->getScheme() . '://' . $this->getHost() . $this->getPathQuery();
    }

    /**
     * @return string
     */
    public function getQueryString()
    {
        $strQuery = $this->getQuery();
        if (strlen($strQuery) > 0) {
            return "?" . $strQuery;
        }

        return "";
    }

    /**
     *
     * @param int $port
     * @return $this
     */
    public function setPort($port)
    {
        $this->port = $port;
        return $this;
    }

    /**
     *
     * @param string $scheme
     * @return $this
     */
    public function setScheme($scheme)
    {
        if ($scheme == 'https') {
            $this->setPort(443);
        } else {
            $this->setPort(80);
        }
        $this->scheme = $scheme;
        return $this;
    }

}

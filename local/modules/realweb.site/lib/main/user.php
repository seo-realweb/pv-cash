<?php

namespace Realweb\Site\Main;

/**
 * Class User
 * @package Realweb\Site\Main
 */
class User
{
    /**
     * @var User
     */
    protected static $instance;

    /**
     * @var Session
     */
    protected $_session = null;

    /**
     * @var \CUser
     */
    protected $_user;

    const SESSION_KEY = "REALWEBUSER";

    protected function __construct()
    {

    }

    /**
     * @return User|\Realweb\Site\User
     */
    public static function getInstance()
    {
        if (is_null(static::$instance)) {
            static::$instance = new static();
        }
        return static::$instance;
    }

    /**
     *
     * @param string $strKey
     * @param mixed $value
     * @return $this
     */
    public function setSessionValue(string $strKey, $value)
    {
        $this->getSession()->setValue($strKey, $value);
        return $this;
    }

    /**
     * @param $strKey
     * @param null $default
     * @return mixed
     */
    public function getSessionValue($strKey, $default = null)
    {
        return $this->getSession()->getValue($strKey, $default);
    }

    /**
     * @return Session
     */
    protected function getSession()
    {
        if (is_null($this->_session)) {
            $this->_session = new Session(self::SESSION_KEY);
        }
        return $this->_session;
    }

    public function getCUser()
    {
        if ($this->_user === null) {
            global $USER;
            if (!is_object($USER)) {
                $USER = new \CUser;
            }
            $this->_user = $USER;
        }

        return $this->_user;
    }

    public function getId()
    {
        return $this->getCUser()->GetID();
    }

    public function isAuth()
    {
        return $this->getCUser()->IsAuthorized();
    }

    public function authorize()
    {
        $this->getCUser()->Authorize($this->getId());
    }

    public function isAdmin()
    {
        return $this->getCUser()->IsAdmin();
    }

    public function logout()
    {
        $this->getCUser()->Logout();
    }
}

<?php

namespace Realweb\Site\Main;

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Web\Cookie;
use Bitrix\Main\Web\Uri;

/**
 * Class Helper
 * @package Realweb\Site\Main
 */
class Helper
{

    public static $info = array();

    public static function declOfNum($number, $titles)
    {
        $cases = array(2, 0, 1, 1, 1, 2);
        return $titles[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]];
    }

    public static function formatPrice($price, $currency = 'RUB')
    {
        return number_format($price, 0, '&nbsp;', '&nbsp;');
    }

    public static function translit($STRING)
    {
        $params = array("replace_space" => "-", "replace_other" => "-");
        $result = \CUtil::translit($STRING, "ru", $params);
        return $result;
    }

    public static function upFirst($str, $encoding = 'UTF-8')
    {
        $str = mb_ereg_replace('^[\ ]+', '', $str);
        $str = mb_strtolower($str);
        $str = mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding) .
            mb_substr($str, 1, mb_strlen($str), $encoding);
        return $str;
    }

    public static function toLower($strString)
    {
        if (is_string($strString)) {
            return strtolower($strString);
        } elseif (is_array($strString)) {
            $arLowerString = array();
            foreach ($strString as $key => $value) {
                if (is_numeric($key)) {
                    $lowerKey = $key;
                } else {
                    $lowerKey = self::toLower($key);
                }
                if (is_array($value)) {
                    $arLowerString[$lowerKey] = self::toLower($value);
                } else {
                    $arLowerString[$lowerKey] = $value;
                }
            }
            return $arLowerString;
        }
    }

    public static function toUpper($strString)
    {
        if (is_string($strString)) {
            return ToUpper($strString);
        } elseif (is_array($strString)) {
            $arLowerString = array();
            foreach ($strString as $key => $value) {
                if (is_numeric($key)) {
                    $lowerKey = $key;
                } else {
                    $lowerKey = self::toUpper($key);
                }
                if (is_array($value)) {
                    $arLowerString[$lowerKey] = self::toUpper($value);
                } else {
                    $arLowerString[$lowerKey] = $value;
                }
            }
            return $arLowerString;
        }
    }

    public static function getMenuTree($arItems)
    {
        $arMenu = array();
        $iDepthLevel = 0;
        $iLastInd = 0;
        $arParents = array();


        foreach ($arItems as $arItem) {
            $iDepthLevel = intval($arItem['depth_level']);

            if ($arItem['is_parent']) {
                $arItem['items'] = array();
            }
            if ($iDepthLevel == 1) {
                $arMenu[] = $arItem;
                $iLastInd = count($arMenu) - 1;
                $arParents[$iDepthLevel] = &$arMenu[$iLastInd];
            } else {
                $arParents[$iDepthLevel - 1]['items'][] = $arItem;
                $iLastInd = count($arParents[$iDepthLevel - 1]['items']) - 1;
                $arParents[$iDepthLevel] = &$arParents[$iDepthLevel - 1]['items'][$iLastInd];
            }
        }
        return $arMenu;
    }

    public static function getInfoByMask($strMask, $isArray = false, $strSiteId = "")
    {
        $arResult = array();
        if (Loader::includeModule('realweb.main.include')) {
            $arFilter = array(
                "CODE" => $strMask . "%"
            );
            if (strlen($strSiteId) > 0) {
                $arFilter['SITE_ID'] = $strSiteId;
            }

            $rsIndexOptions = \Realweb\RealwebMainIncludeTable::getList(array(
                "filter" => $arFilter
            ));
            $strLowMask = self::toLower($strMask);

            foreach ($rsIndexOptions as $arOption) {
                $strKey = str_replace($strLowMask, '', self::toLower($arOption['CODE']));
                $arResult[$strKey] = $isArray ? self::toLower($arOption) : $arOption['TEXT'];
            }
        }

        return $arResult;
    }

    public static function getInfoByCode($strCode, $default = null, $strSiteId = "")
    {
        $strResult = $default;
        if (isset(static::$info[$strCode])) {
            $strResult = static::$info[$strCode];
        } else {
            if (Loader::includeModule('realweb.main.include')) {
                $arFilter = array(
                    "CODE" => $strCode
                );
                if (strlen($strSiteId) > 0) {
                    $arFilter['SITE_ID'] = $strSiteId;
                }

                $rsIndexOptions = \Realweb\RealwebMainIncludeTable::getList(array(
                    "filter" => $arFilter
                ));
                if ($arRow = $rsIndexOptions->fetch()) {
                    $strResult = $arRow['TEXT'];
                    static::$info[$strCode] = $strResult;
                }
            }
        }
        return $strResult;
    }

    public static function getPhone($strPhone)
    {
        $re = '/[^\d]/';
        $strPhone = preg_replace($re, "", $strPhone);
        $strPhone = strip_tags($strPhone);
        $strFormatPhone = '+' . substr($strPhone, 0, 1) . '(' . substr($strPhone, 1, 3) . ') ' . substr($strPhone, 4, 3) . '-' . substr($strPhone, 7, 2) . '-' . substr($strPhone, 9, 2);
        return array(
            'value' => $strPhone,
            'format' => $strFormatPhone
        );
    }

    /**
     *
     * @param string $strType
     * @param mixed $bMultiple
     * @param mixed $strValue
     * @return boolean
     */
    public static function isEmpty($strType, $bMultiple, &$strValue)
    {
        if (is_string($bMultiple)) {
            if ($bMultiple === "Y") {
                $bMultiple = true;
            } else {
                $bMultiple = false;
            }
        }

        if ($bMultiple) {
            if (!is_array($strValue)) {
                $strValue = array();
            }
            if (count($strValue) == 0) {
                return true;
            }
            return false;
        } else {
            if ($strType == "hlblock" || $strType == "iblock_section" || $strType == "iblock_element") {
                if (intval($strValue) == 0) {
                    return true;
                }
                return false;
            } elseif ($strType == "string") {
                if (strlen($strValue) == 0) {
                    return true;
                }
                return false;
            } elseif ($strType == "integer") {
                if (intval($strValue) == 0) {
                    return true;
                }
                return false;
            } elseif ($strType == "boolean") {
                if (intval($strValue) > 0) {
                    return false;
                }
                return true;
            }
        }

        if (empty($strValue)) {
            return true;
        }
        return false;
    }

    /**
     *
     * @return string
     */
    public static function getCurPage()
    {
        $obRequest = Application::getInstance()->getContext()->getRequest();
        $obRri = new Uri($obRequest->getRequestUri());
        return $obRri->getPath();
    }

    /**
     * @param array $items
     * @param int $parentId
     *
     * @return array
     */
    public static function getSectionTree(array &$items, $parentId = 0)
    {
        $result = array();
        foreach ($items as $item) {
            if ($item['iblock_section_id'] == $parentId) {
                $child = self::getSectionTree($items, $item['id']);
                if ($child) {
                    $item['items'] = $child;
                }
                $result[] = $item;
                unset($items[$item['id']]);
            }
        }

        return $result;
    }

    public static function getSiteHost()
    {
        $obContext = \Bitrix\Main\Application::getInstance()->getContext();
        $obRequest = $obContext->getRequest();
        $strHttp = ($obRequest->isHttps() ? 'https://' : 'http://');
        return $strHttp . $obContext->getServer()->getHttpHost();
    }

    /**
     * @param array $array
     * @param \SimpleXMLElement $obXml
     */
    public static function arrayToXml(array $array, \SimpleXMLElement &$obXml)
    {
        foreach ($array as $key => $value) {
            $xmlKey = preg_replace_callback('/_?([a-z])([a-z]+)_?/', function ($word) {
                return $word[1] . strtolower($word[2]);
            }, $key);
            if (is_array($value)) {
                if (isset($value['xml_key'])) {
                    $xmlKey = $value['xml_key'];
                    unset($value['xml_key']);
                }
                $subNode = $obXml->addChild("$xmlKey");
                self::arrayToXml($value, $subNode);
            } else {
                $obXml->addChild("$xmlKey", htmlspecialchars("$value"));
            }
        }
    }

    /**
     * @param $cookName
     * @param null $default
     * @return string|null
     * @throws \Bitrix\Main\SystemException
     */
    public static function getCookie($cookName, $default = null)
    {
        $request = Application::getInstance()->getContext()->getRequest();
        if ($value = $request->getCookie($cookName)) {
            return $value;
        }

        return $default;
    }

    /**
     * @param $cookName
     * @param $value
     * @param bool $expire
     * @throws \Bitrix\Main\SystemException
     */
    public static function setCookie($cookName, $value, $expire = false)
    {
        $context = Application::getInstance()->getContext();
        if ($cookName && $value) {
            $cookie = new Cookie($cookName, $value, $expire ? $expire : (time() + 60 * 60 * 24));
            $cookie->setDomain($context->getServer()->getHttpHost());
            $cookie->setHttpOnly(false);
            $context->getResponse()->addCookie($cookie);
            $context->getResponse()->flush("");
        }
    }

    /**
     * Склонение словоформы
     * @param $n
     * @param $f1
     * @param $f2
     * @param $f5
     * @return mixed
     */
    public static function morph($n, $f1, $f2, $f5)
    {
        $n = abs(intval($n)) % 100;
        if ($n > 10 && $n < 20) {
            return $f5;
        };
        $n = $n % 10;
        if ($n > 1 && $n < 5) {
            return $f2;
        }
        if ($n == 1) {
            return $f1;
        }

        return $f5;
    }

    /**
     * Make array for multiple file
     * @param array $arFile
     * @return array
     */
    public static function makeFileArray(array $arFile)
    {
        $arResult = array();
        foreach ($arFile as $field => $arValues) {
            foreach ($arValues as $key => $value) {
                $arResult[$key][$field] = $value;
            }
        }

        return $arResult;
    }

    /**
     * @param int $iTime
     * @return string
     */
    public static function formatTime(int $iTime)
    {
        $iHour = floor($iTime / 3600);
        $iSec = $iTime - ($iHour * 3600);
        $iMin = floor($iSec / 60);
        $iSec = $iSec - ($iMin * 60);

        if ($iMin < 10) {
            $iMin = '0' . $iMin;
        }
        if ($iHour < 10) {
            $iHour = '0' . $iHour;
        }
        if ($iSec < 10) {
            $iSec = '0' . $iSec;
        }

        return implode(':', array(
            $iHour,
            $iMin,
            $iSec
        ));
    }

    public static function redirect($strUrl, $is301 = true)
    {
        if ($is301) {
            LocalRedirect($strUrl, false, "301 Moved permanently");
        } else {
            LocalRedirect($strUrl);
        }
    }

    public static function getMethodByField($strField, $strType = "get")
    {
        $strResult = $strType;
        $arParts = explode("_", $strField);
        foreach ($arParts as $strPart) {
            $strResult .= ucfirst(self::toLower($strPart));
        }

        return $strResult;
    }

    public static function normalizePhone($phone)
    {
        return preg_replace('/[^0-9]/', "", $phone);
    }
}

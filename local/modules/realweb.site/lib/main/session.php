<?php

namespace Realweb\Site\Main;

/**
 * Class Session
 * @package Realweb\Site\Model\Main
 */
class Session
{

    private $_key;
    private $_data;

    /**
     * Session constructor.
     * @param string $strSessionKey
     */
    public function __construct(string $strSessionKey)
    {
        $this->_key = $strSessionKey;
        $this->setSessionKey();
    }

    private function setSessionKey()
    {
        if (!isset($_SESSION[$this->_key])) {
            $_SESSION[$this->_key] = array();
        }
        $this->_data = &$_SESSION[$this->_key];
    }

    public function setValue($strKey, $value)
    {
        $this->_data[$strKey] = $value;
        return $this;
    }

    public function getValue($strKey, $default = null)
    {
        if (isset($this->_data[$strKey])) {
            return $this->_data[$strKey];
        }
        return $default;
    }

}

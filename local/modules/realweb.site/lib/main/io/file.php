<?php


namespace Realweb\Site\Main\IO;


class File extends \Bitrix\Main\IO\File
{
    private $_resize_path = array();

    /**
     * @var Directory
     */
    private $_directory = null;

    public function __construct($path, $siteId = null)
    {
        $strDocumentRoot = $this->getSiteDocumentRoot();
        if (stripos($path, $strDocumentRoot) !== 0) {
            $path = $strDocumentRoot . $path;
        }
        parent::__construct($path, $siteId);
    }

    public function getPublicPath()
    {
        return str_replace(self::getDocumentRoot(SITE_ID), '', $this->getPhysicalPath());
    }

    public function getDirectory()
    {
        if ($this->_directory === null) {
            $this->_directory = new Directory($this->getDirectoryName());
        }

        return $this->_directory;
    }

    public function getImageSize()
    {
        if ($this->isExists()) {
            return getimagesize($this->getPath());
        }
        return null;
    }

    public function getWidth()
    {
        $arSize = $this->getImageSize();
        if (is_array($arSize)) {
            return $arSize[0];
        }
        return 0;
    }

    public function getHeight()
    {
        $arSize = $this->getImageSize();
        if (is_array($arSize)) {
            return $arSize[1];
        }
        return 0;
    }

    public function getResizeFile($iWidth = null, $iHeight = null)
    {
        $strHash = md5(serialize(array(
            'height' => $iHeight,
            'width' => $iWidth,
        )));
        if (!key_exists($strHash, $this->_resize_path)) {
            if (is_null($iWidth)) {
                $iWidth = round(($iHeight * $this->getWidth()) / $this->getHeight());
            }
            if (is_null($iHeight)) {
                $iHeight = round(($iWidth * $this->getHeight()) / $this->getWidth());
            }
            $arDestinationPath = array(
                \Bitrix\Main\Config\Option::get("main", "upload_dir", "upload"),
                'resize_cache',
                $this->getSubDirectory(),
                implode("_", array(
                    $iWidth,
                    $iHeight,
                    BX_RESIZE_IMAGE_PROPORTIONAL
                )),
                $this->getName(),
            );
            $strDestinationFile = $this->getSiteDocumentRoot() . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, $arDestinationPath);
            $obFile = new File($strDestinationFile);
            if (!$obFile->isExists()) {
                $obDir = $obFile->getDirectory();
                if (!$obDir->isExists()) {
                    $obDir->create();
                }
                $strFileName = $obFile->getPhysicalPath();
                $bResize = \CFile::ResizeImageFile($this->getPhysicalPath(), $strFileName, array('width' => $iWidth, 'height' => $iHeight), BX_RESIZE_IMAGE_PROPORTIONAL);
                if (!$bResize) {
                    $obFile->rename($this->getPhysicalPath());
                }
            }
            $this->_resize_path[$strHash] = $obFile;
        }

        return $this->_resize_path[$strHash];
    }

    public function getSubDirectory()
    {
        $obParent = $this->getDirectory();
        $strPath = str_replace(DIRECTORY_SEPARATOR . \COption::GetOptionString("main", "upload_dir", "upload"), "", $obParent->getPublicPath());

        return trim($strPath, DIRECTORY_SEPARATOR);
    }

    public function getSiteId()
    {
        if (defined('SITE_ID')) {
            return SITE_ID;
        } else {
            return null;
        }
    }

    public function getSiteDocumentRoot()
    {
        return self::getDocumentRoot($this->getSiteId());
    }
}
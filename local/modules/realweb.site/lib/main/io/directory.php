<?php


namespace Realweb\Site\Main\IO;


class Directory extends \Bitrix\Main\IO\Directory
{
    public function __construct($path, $siteId = null)
    {
        $strDocumentRoot = $this->getSiteDocumentRoot();
        if (stripos($path, $strDocumentRoot) !== 0) {
            $path = $strDocumentRoot . $path;
        }
        parent::__construct($path, $siteId);
    }

    public function getPublicPath()
    {
        return str_replace(self::getDocumentRoot(SITE_ID), '', $this->getPhysicalPath());
    }

    public function getSiteId()
    {
        if (defined('SITE_ID')) {
            return SITE_ID;
        } else {
            return null;
        }
    }

    public function getSiteDocumentRoot()
    {
        return self::getDocumentRoot($this->getSiteId());
    }
}
<?php

namespace Realweb\Site\Main;

use Bitrix\Main\Application;
use Bitrix\Main\HttpRequest;
use Realweb\Site\Data\PageNavigation;

/**
 * Class \Realweb\Api\Controller
 *
 */
class Controller
{
    const api_folder = "/api";

    public static $strApifolder = self::api_folder;
    public static $strBitrixfolder = "/bitrix";
    protected $module;
    protected $controller;
    protected $action;
    protected $arParams = array();
    /** @var HttpRequest */
    protected $_request = null;

    /**
     * @var array
     */
    protected $_request_params = null;

    /**
     *
     * @var \RealwebApiComponent
     */
    protected $_component;

    public function __construct($strAction = "index")
    {
        $this->action = $strAction;

        if (class_exists('\\CBitrixComponent')) {
            $strComponentClass = \CBitrixComponent::includeComponentClass('realweb:api');
            if (!is_null($strComponentClass)) {
                $this->setComponent(new $strComponentClass);
            }
        }
    }

    /**
     * @param $component
     */
    public function setComponent($component)
    {
        $this->_component = $component;
    }

    /**
     *
     * @return \RealwebApiComponent
     */
    public function getComponent()
    {
        return $this->_component;
    }

    public function getModule()
    {
        return $this->module;
    }

    public function getController()
    {
        if ($this->controller === null) {
            $this->controller = 'index';
        }

        return $this->controller;
    }

    public function getAction()
    {
        if ($this->action === null) {
            $this->action = 'index';
        }

        return $this->action;
    }

    public function setArParams($arParams)
    {
        $this->arParams = $arParams;
        return $this;
    }

    public function setParam($strKey, $value)
    {
        $this->arParams[$strKey] = $value;
        return $this;
    }

    public function getArParams()
    {
        return $this->arParams;
    }

    public function getParam($strKey, $default = null)
    {
        if (array_key_exists($strKey, $this->arParams)) {
            return $this->arParams[$strKey];
        }
        return $default;
    }

    public function action()
    {
        $arData = null;
        $strAction = $this->action;
        if (method_exists($this, $strAction)) {
            $arData = $this->$strAction();
        } else {
            $this->getComponent()->setError(Error::NO_METHOD);
        }

        return $arData;
    }

    /**
     * @param array $arParams
     * @return $this
     */
    public function setRequestParams($arParams)
    {
        $this->_request_params = $arParams;
        return $this;
    }

    public function getRequestParams($strName = null, $default = null)
    {
        if (is_null($this->_request_params)) {
            $obContext = \Bitrix\Main\Application::getInstance()->getContext();
            $obRequest = $obContext->getRequest();
            $arPost = $obRequest->getPostList()->toArray();
            $arGet = $obRequest->getQueryList()->toArray();
            $arFiles = $obRequest->getFileList()->toArray();
            $arParams = array_merge($arPost, $arGet, $arFiles);
            $this->_request_params = $arParams;
        }

        if (!is_null($strName)) {
            if (key_exists($strName, $this->_request_params)) {
                return $this->_request_params[$strName];
            }
            return $default;
        }

        return $this->_request_params;
    }

    public function getApiUrl()
    {
        $arUrl = array(
            $this::$strApifolder,
        );
        $arModuleController = array($this->getModule());
        $strController = $this->getController();
        if ($strController != 'index') {
            $arModuleController[] = $strController;
        }
        $arUrl[] = implode('-', $arModuleController);

        $strId = $this->getParam('id', '');
        if (strlen($strId) > 0) {
            $arUrl[] = $strId;
        }

        return Helper::toLower(implode('/', $arUrl) . '/');
    }

    /**
     * @param int $pageSize
     * @return PageNavigation
     */
    public function getNav($pageSize = 0)
    {
        $obNav = new PageNavigation($this->getModule() . "_" . $this->getController() . "_" . $this->getAction() . "_page");
        $arApiUrl = array(
            self::$strApifolder,
            $this->getModule(),
            $this->getController(),
            $this->getAction(),
        );
        $obNav->setUrl('api_url', implode('/', $arApiUrl) . "/");
        $strFolder = trim($this->getParam('folder', $this->getModule()), "/");
        $obNav->setUrl('public_url', '/' . $strFolder . '/');
        if (intval($pageSize) > 0) {
            $obNav->allowAllRecords(false)->setPageSize($pageSize);
        } else {
            $obNav->allowAllRecords(true);
        }
        $obNav->initFromUri();
        $strOrderParam = $this->getModule() . '_' . $this->getController() . '_' . $this->getAction() . '_sort';
        if ($strOrder = $this->getRequestParams($strOrderParam)) {
            $arOrder = explode('-', $strOrder);
            $obNav->setParam('order', array(Helper::toUpper($arOrder[0]) => Helper::toUpper($arOrder[1])));
            $obNav->setQuery($strOrderParam, $strOrder);
        }

        return $obNav;
    }

    public function getRequest()
    {
        if ($this->_request === null) {
            $this->_request = Application::getInstance()->getContext()->getRequest();
        }

        return $this->_request;
    }

    public function isAjaxRequest()
    {
        return $this->getRequest()->isAjaxRequest();
    }

    protected function _renderComponent(string $strComponent, string $strTemplate, array $arParams = array())
    {
        global $APPLICATION;
        ob_start();
        $APPLICATION->IncludeComponent($strComponent, $strTemplate, $arParams);

        return ob_get_clean();
    }
}

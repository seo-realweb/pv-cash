<?php


namespace Realweb\Site\Model\Suspect;


class Collection extends \Realweb\Site\Data\Collection
{
    private $_user_keys = [];

    protected static function getModel()
    {
        return Model::class;
    }

    /**
     * @param \Realweb\Site\Data\Data|\Realweb\Site\Data\Entity|Entity $obItem
     * @return $this|mixed
     */
    public function addItem($obItem)
    {
        $this->_collection[] = $obItem;
        $this->_keys[] = $obItem->getId();
        $this->_user_keys[] = $obItem->getUfUserId();

        return $this;
    }

    /**
     * @param $strKey
     * @return \Realweb\Site\Data\Data|Entity
     */
    public function getByUser($strKey)
    {
        $iCollectionKey = array_search($strKey, $this->_user_keys);
        if ($iCollectionKey !== false) {
            return $this->_collection[$iCollectionKey];
        }
    }
}
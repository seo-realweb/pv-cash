<?php


namespace Realweb\Site\Model\Suspect;

/**
 * Class Entity
 * @package Realweb\Site\Model\Blacklist
 * @method string getUfUserId()
 * @method int setUfUserId(int $int)
 */
class Entity extends \Realweb\Site\Data\Entity
{
    protected static function getModel()
    {
        return Model::class;
    }
}
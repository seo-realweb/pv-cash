<?php


namespace Realweb\Site\Model\Location\Element;


class Collection extends \Realweb\Site\Model\Iblock\Element\Collection
{
    protected static function getModel()
    {
        return Model::class;
    }

    public function getName()
    {
        $arResult = array();
        /** @var Entity $obItem */
        foreach ($this->getCollection() as $obItem) {
            $arResult[] = $obItem->getName();
        }

        return implode(", ", $arResult);
    }
}
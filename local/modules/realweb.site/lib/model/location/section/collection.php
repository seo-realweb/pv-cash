<?php


namespace Realweb\Site\Model\Location\Section;


class Collection extends \Realweb\Site\Model\Iblock\Section\Collection
{
    protected static function getModel()
    {
        return Model::class;
    }
}
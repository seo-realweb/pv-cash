<?php

namespace Realweb\Site\Model\Payment\Unitpay;

use Bitrix\Main\Application;
use Realweb\Site\Main\Helper;

/**
 * Class Payment
 * @package Realweb\Site\Model\Payment\Unitpay
 */
class Payment
{
    const TEST_SECRET_KEY = '3723C9A8DBD-366CBCA98AD-27D3217BE0';
    const SECRET_KEY = '54e32287ba8d846fa08c86555beb276e';
    const SECRET_KEY_PAYOUT = 'F6938EDA212-A2E83D2B05E-5230D77188';

    const DOMAIN = 'unitpay.money';
    const ACCOUNT = 'rammydm@gmail.com';
    const PROJECT_ID = '338161';

    private $supportedCurrencies = array('EUR', 'UAH', 'BYR', 'USD', 'RUB');
    private $supportedUnitpayMethods = array('initPayment', 'getPayment', 'massPayment', 'massPaymentStatus');
    private $requiredUnitpayMethodsParams = array(
        'initPayment' => array('desc', 'account', 'sum'),
        'getPayment' => array('paymentId')
    );
    private $supportedPartnerMethods = array('check', 'pay', 'error');
    private $supportedUnitpayIp = array(
        '188.120.235.14',
        '127.0.0.1'
    );

    private $secretKey;
    private $secretKeyPayout;

    private $params = array();

    private $apiUrl;
    private $formUrl;
    private $_test = false;

    public function __construct()
    {
        if ($this->isTest()) {
            $this->secretKey = self::TEST_SECRET_KEY;
            $this->secretKeyPayout = self::TEST_SECRET_KEY;
        } else {
            $this->secretKey = self::SECRET_KEY;
            $this->secretKeyPayout = self::SECRET_KEY_PAYOUT;
        }
        $this->apiUrl = "https://" . self::DOMAIN . "/api";
        $this->formUrl = "https://" . self::DOMAIN . "/pay/";
    }

    /**
     * Create SHA-256 digital signature
     *
     * @param array $params
     * @param $method
     *
     * @return string
     */
    function getSignature(array $params, $method = null)
    {
        ksort($params);
        unset($params['sign']);
        unset($params['signature']);
        array_push($params, $this->secretKey);

        if ($method) {
            array_unshift($params, $method);
        }

        return hash('sha256', join('{up}', $params));
    }

    /**
     * Return IP address
     *
     * @return string
     */
    protected function getIp()
    {
        return $_SERVER['REMOTE_ADDR'];
    }

    /**
     * Get URL for pay through the form
     *
     * @param string $publicKey
     * @param string|float|int $sum
     * @param string $account
     * @param string $desc
     * @param string $currency
     * @param string $locale
     *
     * @return string
     */
    public function form($publicKey, $sum, $account, $desc, $currency = 'RUB', $locale = 'ru')
    {
        $vitalParams = array(
            'account' => $account,
            'currency' => $currency,
            'desc' => $desc,
            'sum' => $sum
        );

        $this->params = array_merge($this->params, $vitalParams);

        if ($this->secretKey) {
            $this->params['signature'] = $this->getSignature($vitalParams);
        }

        $this->params['locale'] = $locale;

        return $this->formUrl . $publicKey . '?' . http_build_query($this->params);
    }

    /**
     * Set customer email
     *
     * @param string $email
     *
     * @return Payment
     */
    public function setCustomerEmail($email)
    {
        $this->params['customerEmail'] = $email;
        return $this;
    }

    /**
     * Set customer phone number
     *
     * @param string $phone
     *
     * @return Payment
     */
    public function setCustomerPhone($phone)
    {
        $this->params['customerPhone'] = $phone;
        return $this;
    }

    /**
     * Set list of paid goods
     *
     * @param CashItem[] $items
     *
     * @return Payment
     */
    public function setCashItems($items)
    {
        $this->params['cashItems'] = base64_encode(
            json_encode(
                array_map(function ($item) {
                    /** @var CashItem $item */
                    return array(
                        'name' => $item->getName(),
                        'count' => $item->getCount(),
                        'price' => $item->getPrice(),
                        'nds' => $item->getNds(),
                        'type' => $item->getType(),
                        'paymentMethod' => $item->getPaymentMethod(),
                    );
                }, $items)));

        return $this;
    }

    /**
     * Set callback URL
     *
     * @param string $backUrl
     * @return Payment
     */
    public function setBackUrl($backUrl)
    {
        $this->params['backUrl'] = $backUrl;
        return $this;
    }

    /**
     * Call API
     *
     * @param $method
     * @param array $params
     *
     * @return object
     *
     * @throws \InvalidArgumentException
     * @throws \UnexpectedValueException
     */
    public function api($method, $params = array())
    {
        if (!in_array($method, $this->supportedUnitpayMethods)) {
            throw new \UnexpectedValueException('Method is not supported');
        }

        if (isset($this->requiredUnitpayMethodsParams[$method])) {
            foreach ($this->requiredUnitpayMethodsParams[$method] as $rParam) {
                if (!isset($params[$rParam])) {
                    throw new \InvalidArgumentException('Param ' . $rParam . ' is null');
                }
            }
        }
        if (in_array($method, array('massPayment', 'massPaymentStatus'))) {
            $params['secretKey'] = $this->secretKeyPayout;
        } else {
            $params['secretKey'] = $this->secretKey;
        }

        if (empty($params['secretKey'])) {
            throw new \InvalidArgumentException('SecretKey is null');
        }

        $requestUrl = $this->apiUrl . '?' . http_build_query([
                'method' => $method,
                'params' => $params
            ], null, '&', PHP_QUERY_RFC3986);

        $response = json_decode(file_get_contents($requestUrl));
        if (!is_object($response)) {
            throw new \InvalidArgumentException('Temporary server error. Please try again later.');
        }

        return $response;
    }

    /**
     * Check request on handler from UnitPay
     *
     * @return bool
     *
     * @throws \InvalidArgumentException
     * @throws \UnexpectedValueException
     */
    public function checkHandlerRequest()
    {
        $ip = $this->getIp();
        if (!isset($_GET['method'])) {
            throw new \InvalidArgumentException('Method is null');
        }

        if (!isset($_GET['params'])) {
            throw new \InvalidArgumentException('Params is null');
        }

        list($method, $params) = array($_GET['method'], $_GET['params']);

        if (!in_array($method, $this->supportedPartnerMethods)) {
            throw new \UnexpectedValueException('Method is not supported');
        }

        if (!isset($params['signature']) || $params['signature'] != $this->getSignature($params, $method)) {
            throw new \InvalidArgumentException('Wrong signature');
        }

        /**
         * IP address check
         * @link http://help.unitpay.ru/article/67-ip-addresses
         * @link http://help.unitpay.money/article/67-ip-addresses
         */
//        if (!in_array($ip, $this->supportedUnitpayIp)) {
//            throw new \InvalidArgumentException('IP address Error');
//        }

        return true;
    }

    /**
     * Response for UnitPay if handle success
     *
     * @param $message
     *
     * @return string
     */
    public function getSuccessHandlerResponse($message)
    {
        return json_encode(array(
            "result" => array(
                "message" => $message
            )
        ));
    }

    /**
     * Response for UnitPay if handle error
     *
     * @param $message
     *
     * @return string
     */
    public function getErrorHandlerResponse($message)
    {
        return json_encode(array(
            "error" => array(
                "message" => $message
            )
        ));
    }

    public function massPayment(int $orderId, float $orderSum, string $strPurse, string $strType)
    {
        $arParams = array(
            'transactionId' => $orderId,
            'sum' => $orderSum,
            'paymentType' => $strType,
            'purse' => $strPurse,
            'currency' => 'RUB',
            'projectId' => self::PROJECT_ID,
            'login' => self::ACCOUNT
        );
        $response = $this->api('massPayment', $arParams);
        if (isset($response->result)) {
            return array('result' => 1);
        } elseif (isset($response->error->message)) {
            $error = $response->error->message;

            return array('result' => 0, 'error' => $error);
        } else {
            return array('result' => 0, 'error' => 'Ошибка оплаты');
        }
    }

    public function massPaymentStatus(int $orderId)
    {
        $arParams = array(
            'transactionId' => $orderId,
            'projectId' => self::PROJECT_ID,
            'login' => self::ACCOUNT
        );
        $response = $this->api('massPaymentStatus', $arParams);
        if (isset($response->result)) {
            return array('result' => 1, 'status' => $response->result->status);
        } elseif (isset($response->error->message)) {
            $error = $response->error->message;

            return array('result' => 0, 'error' => $error);
        } else {
            return array('result' => 0, 'error' => 'Ошибка оплаты');
        }
    }

    public function getPaymentLink(int $orderId, float $orderSum)
    {
        $arParams = array(
            'account' => $orderId,//self::ACCOUNT,
            'desc' => 'Пополнение кошелька',
            'sum' => $orderSum,
            'paymentType' => 'card',
            'currency' => 'RUB',
            'projectId' => self::PROJECT_ID,
            'resultUrl' => Helper::getSiteHost(),
            'ip' => $this->getIp(),
            'signature' => $this->getFormSignature($orderId, $orderSum)
        );
        if ($this->isTest()) {
            $arParams['test'] = 1;
        }
        $response = $this->api('initPayment', $arParams);

        if (isset($response->result->type) && $response->result->type == 'redirect') {
            $redirectUrl = $response->result->redirectUrl;
            $paymentId = $response->result->paymentId;

            return array('result' => 1, 'payment_id' => $paymentId, 'url' => $redirectUrl);
        } elseif (isset($response->result->type)
            && $response->result->type == 'invoice') {
            $receiptUrl = $response->result->receiptUrl;
            $paymentId = $response->result->paymentId;
            $invoiceId = $response->result->invoiceId;

            return array('result' => 1, 'payment_id' => $paymentId, 'invoice_id' => $invoiceId, 'url' => $receiptUrl);
        } elseif (isset($response->error->message)) {
            $error = $response->error->message;

            return array('result' => 0, 'error' => $error);
        }
    }

    /**
     * @return bool
     */
    public function isTest(): bool
    {
        return $this->_test;
    }

    /**
     * @param bool $test
     */
    public function setTest(bool $test): void
    {
        $this->_test = $test;
    }

    function getFormSignature($account, $sum, $desc = 'Пополнение кошелька')
    {
        $hashStr = $account . '{up}' . 'RUB' . '{up}' . $desc . '{up}' . $sum . '{up}' . $this->secretKey;

        return hash('sha256', $hashStr);
    }
}
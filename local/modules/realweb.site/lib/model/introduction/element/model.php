<?php

namespace Realweb\Site\Model\Introduction\Element;

use Realweb\Site\Main\ArrayHelper;
use Realweb\Site\Model\Iblock\Element\Table;

class Model extends \Realweb\Site\Model\Iblock\Element\Model
{
    public static function getIblockId()
    {
        return IBLOCK_CONTENT_INTRODUCTION;
    }

    public static function getCollection()
    {
        return Collection::class;
    }

    public static function getEntity()
    {
        return Entity::class;
    }

    public static function getOrmEntity()
    {
        return Table::getEntity();
    }

    public static function getQuery($obNav)
    {
        $obQuery = parent::getQuery($obNav)
            ->where('ACTIVE', '=', 'Y')
            ->where('IBLOCK_ID', '=', self::getIblockId())
            ->setOrder(array('SORT' => 'ASC'));

        if ($obNav !== null) {
            $arFilter = $obNav->getParam('filter');
            if (($code = ArrayHelper::getValue($arFilter, 'code')) !== null) {
                $obQuery->whereCustom('CODE', $code);
            }
        }

        return $obQuery;
    }
}
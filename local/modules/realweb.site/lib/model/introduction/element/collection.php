<?php


namespace Realweb\Site\Model\Introduction\Element;


class Collection extends \Realweb\Site\Model\Iblock\Element\Collection
{
    protected static function getModel()
    {
        return Model::class;
    }

    public function toJson()
    {
        $arResult = array();
        /** @var Entity $obItem */
        foreach ($this->getCollection() as $obItem) {
            $arResult[] = array(
                "element" => $obItem->getCode(),
                "popover" => array(
                    "section_id" => $obItem->getIblockSectionId(),
                    "id" => $obItem->getId(),
                    "title" => $obItem->getName(),
                    "description" => $obItem->getPreviewText(),
                    "position" => $obItem->getPosition(),
                )
            );
        }

        return $arResult;
    }
}
<?php


namespace Realweb\Site\Model\Introduction\Element;


use Realweb\Site\Site;

/**
 * Class Entity
 * @package Realweb\Site\Model\Introduction\Element
 * @method string getPositionXmlId()
 */
class Entity extends \Realweb\Site\Model\Iblock\Element\Entity
{
    protected static function getModel()
    {
        return Model::class;
    }

    public function getPosition()
    {
        return Site::isMobile() ? 'top' : ($this->getPositionXmlId() ? : 'left');
    }
}
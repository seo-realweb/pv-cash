<?php

namespace Realweb\Site\Model\Introduction\Section;

use Realweb\Site\ArrayHelper;
use Realweb\Site\Model\Iblock\Section\Table;

class Model extends \Realweb\Site\Model\Iblock\Section\Model
{
    public static function getElementModel()
    {
        return \Realweb\Site\Model\Introduction\Element\Model::class;
    }

    public static function getIblockId()
    {
        return IBLOCK_CONTENT_INTRODUCTION;
    }

    public static function getCollection()
    {
        return Collection::class;
    }

    public static function getEntity()
    {
        return Entity::class;
    }

    public static function getOrmEntity()
    {
        return Table::getEntity();
    }

    public static function getQuery($obNav)
    {
        $obQuery = Table::query()
            ->setSelect(self::getFields())
            ->where('ACTIVE', '=', 'Y')
            ->where('IBLOCK_ID', '=', self::getIblockId())
            ->setOrder(array('SORT' => 'ASC'));

        if ($obNav !== null) {
            $arFilter = $obNav->getParam('filter');
            if ($id = ArrayHelper::getValue($arFilter, '!id')) {
                $obQuery->whereNotIn('ID', $id);
            }
        }

        return $obQuery;
    }
}
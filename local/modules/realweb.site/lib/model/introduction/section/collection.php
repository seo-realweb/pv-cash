<?php


namespace Realweb\Site\Model\Introduction\Section;


class Collection extends \Realweb\Site\Model\Iblock\Section\Collection
{
    private $_code_keys = array();

    /**
     * @param \Realweb\Site\Data\Data|\Realweb\Site\Data\Entity|Entity $obItem
     * @return $this|mixed|Collection
     */
    public function addItem($obItem)
    {
        $this->_collection[] = $obItem;
        $this->_keys[] = $obItem->getId();
        $this->_code_keys[] = $obItem->getCode();

        return $this;
    }

    /**
     * @param string $strCode
     * @return \Realweb\Site\Data\Data|Entity
     */
    public function getByCode(string $strCode)
    {
        $iCollectionKey = array_search($strCode, $this->_code_keys);
        if ($iCollectionKey !== false) {
            return $this->_collection[$iCollectionKey];
        }
    }

    protected static function getModel()
    {
        return Model::class;
    }
}
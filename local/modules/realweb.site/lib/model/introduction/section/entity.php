<?php


namespace Realweb\Site\Model\Introduction\Section;


class Entity extends \Realweb\Site\Model\Iblock\Section\Entity
{
    private static $_driver_count = 0;

    private $_driver = null;

    protected static function getModel()
    {
        return Model::class;
    }

    public function getDriverName()
    {
        return 'realweb.driver[' . self::$_driver_count . ']';
    }

    public function getDriverStepName()
    {
        return 'realweb.driverSteps[' . self::$_driver_count . ']';
    }

    public function getDriver()
    {
        if ($this->_driver === null) {
            if ($obCollection = $this->getElements()) {
                if ($obCollection->count() > 0) {
                    $strDriverName = $this->getDriverName();
                    $this->_driver = '
                         ' . $this->getDriverStepName() . ' = ' . json_encode($obCollection->toJson()) . ';
                        ' . $strDriverName . ' = new Driver({code:"' . $this->getCode() . '",...realweb.driverParams});
                        ' . $strDriverName . '.defineSteps(' . json_encode($obCollection->toJson()) . ');
                    ';
                    self::$_driver_count++;
                }
            }
        }

        return $this->_driver;
    }
}
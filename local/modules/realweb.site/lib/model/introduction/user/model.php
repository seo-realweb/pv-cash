<?php

namespace Realweb\Site\Model\Introduction\User;

use Realweb\Site\ArrayHelper;

class Model extends \Realweb\Site\Data\Model
{
    public static function getFields()
    {
        return array(
            'USER_ID',
            'INTRODUCTION_ID'
        );
    }

    public static function getCollection()
    {
        return Collection::class;
    }

    public static function getEntity()
    {
        return Entity::class;
    }

    public static function getOrmEntity()
    {
        return Table::getEntity();
    }

    public static function getTag()
    {
        return Table::getTableName();
    }

    public static function clearCache()
    {
        global $CACHE_MANAGER;
        $CACHE_MANAGER->ClearByTag(static::getTag());
    }

    public static function getQuery($obNav)
    {
        $obQuery = Table::query()
            ->setSelect(self::getFields());

        if ($obNav !== null) {
            $arFilter = $obNav->getParam('filter');
            if (($introductionId = ArrayHelper::getValue($arFilter, 'introduction_id')) !== null) {
                $obQuery->whereCustom('INTRODUCTION_ID', $introductionId);
            }
            if (($userId = ArrayHelper::getValue($arFilter, 'user_id')) !== null) {
                $obQuery->whereCustom('USER_ID', $userId);
            }
        }

        return $obQuery;
    }
}
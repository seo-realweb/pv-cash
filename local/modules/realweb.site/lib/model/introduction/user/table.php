<?php

namespace Realweb\Site\Model\Introduction\User;

use Bitrix\Main,
    Bitrix\Main\Localization\Loc;
use Realweb\Site\Data\Orm\Query;

Loc::loadMessages(__FILE__);

/**
 * Class Table
 * @package Realweb\Site\Model\Introduction\User
 */
class Table extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'realweb_user_introduction';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'USER_ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'title' => Loc::getMessage('INTRODUCTION_ENTITY_USER_ID_FIELD'),
            ),
            'INTRODUCTION_ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'title' => Loc::getMessage('INTRODUCTION_ENTITY_INTRODUCTION_ID_FIELD'),
            ),
        );
    }

    public static function query()
    {
        return new Query(static::getEntity());
    }
}
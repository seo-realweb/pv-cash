<?php


namespace Realweb\Site\Model\Introduction\User;

/**
 * Class Entity
 * @package Realweb\Site\Model\Introduction\User
 * @method int getUserId()
 * @method int getIntroductionId()
 * @method Entity setUserId(int $int)
 * @method Entity setIntroductionId(int $int)
 */
class Entity extends \Realweb\Site\Data\Entity
{
    protected static function getModel()
    {
        return Model::class;
    }

    public function getHash()
    {
        return $this->getUserId() . '_' . $this->getIntroductionId();
    }
}
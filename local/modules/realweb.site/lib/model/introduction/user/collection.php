<?php


namespace Realweb\Site\Model\Introduction\User;


class Collection extends \Realweb\Site\Data\Collection
{
    private $_introduction_keys = array();

    protected static function getModel()
    {
        return Model::class;
    }

    /**
     * @param \Realweb\Site\Data\Data|\Realweb\Site\Data\Entity|Entity $obItem
     * @return $this|mixed
     */
    public function addItem($obItem)
    {
        $this->_collection[] = $obItem;
        $this->_keys[] = $obItem->getHash();
        $this->_introduction_keys[] = $obItem->getIntroductionId();

        return $this;
    }

    public function getIntroductionKeys()
    {
        return $this->_introduction_keys;
    }

    /**
     * @param $strId
     * @return \Realweb\Site\Data\Data|Entity
     */
    public function getByIntroductionId($strId)
    {
        $iCollectionKey = array_search($strId, $this->_introduction_keys);
        if ($iCollectionKey !== false) {
            return $this->_collection[$iCollectionKey];
        }
    }
}
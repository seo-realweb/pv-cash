<?php

namespace Realweb\Site\Model\Order\Element;

use Bitrix\Main\DB\SqlExpression;
use Bitrix\Main\Entity\ExpressionField;
use Bitrix\Main\ORM\Fields\Relations\Reference;
use Bitrix\Main\ORM\Query\Join;
use Realweb\Site\ArrayHelper;
use Realweb\Site\Data\Orm\Query;
use Realweb\Site\Data\PageNavigation;
use Realweb\Site\Model\Custom\Table;

class Model extends \Realweb\Site\Model\Iblock\Element\Model
{
    public static function getFields()
    {
        return ArrayHelper::merge(parent::getFields(), array('CREATED_BY', 'DATE_CREATE'));
    }

    public static function getProps()
    {
        return array('RUSH', 'PRICE', 'COUNT', 'NICKNAME', 'LEVEL_ID', 'SERVER_ID', 'LOCATION_ID', 'CLASS', 'TEST', 'FULL_PRICE');
    }

    public static function getIblockId()
    {
        return IBLOCK_CONTENT_ORDER;
    }

    public static function getCollection()
    {
        return Collection::class;
    }

    public static function getEntity()
    {
        return Entity::class;
    }

    public static function getQuery($obNav)
    {
        $obSubQueryScreenshot = \Realweb\Site\Model\Screenshot\Model::getQuery(null)
            ->setSelect(array('COUNT_SCREENSHOT'))
            ->where('UF_ORDER_ID', "=", new SqlExpression('%s'))
            ->where('STATUS.XML_ID', '=', 'success')
            ->registerRuntimeField('COUNT_SCREENSHOT', new ExpressionField('COUNT_SCREENSHOT', 'COUNT(%s)', array('ID')));

        $obSubQueryCustom = \Realweb\Site\Model\Custom\Model::getQuery(null)
            ->setSelect(array('COUNT_CUSTOM'))
            ->where('UF_ORDER_ID', "=", new SqlExpression('%s'))
            ->where('UF_ACTIVE', '=', 1)
            ->registerRuntimeField('COUNT_CUSTOM', new ExpressionField('COUNT_CUSTOM', 'COUNT(%s)', array('ID')));

        $obTable = \Realweb\Site\Model\Iblock\Element\Property\Single\Table::getInstance(self::getIblockId());
        $arPriceField = $obTable->getField('PRICE');
        $arRushField = $obTable->getField('RUSH');
        $obQuery = parent::getQuery($obNav)
            //->where('ACTIVE', '=', 'Y')
            ->where('IBLOCK_ID', '=', self::getIblockId())
            ->registerFields(array('RUSH', 'PRICE', 'COUNT', 'NICKNAME', 'LEVEL_ID', 'SERVER_ID', 'CLASS', 'TEST', 'FULL_PRICE'), self::getIblockId())
            ->registerRuntimeField(
                'TOTAL_PRICE',
                new ExpressionField(
                    'TOTAL_PRICE',
                    '(%s + (case when %s > 0 AND (%s >= now() - INTERVAL 1 DAY) then 10 else 0 end))',
                    array('S_PROPS.' . $arPriceField['COLUMN_NAME'], 'S_PROPS.' . $arRushField['COLUMN_NAME'], 'DATE_CREATE')
                )
            )
            ->addSelect('TOTAL_PRICE')
            ->registerRuntimeField(
                'HB_SERVER',
                new Reference(
                    'HB_SERVER',
                    \Realweb\Site\Model\Server\Table::getEntity(),
                    Join::on('this.SERVER_ID', 'ref.ID')
                )
            )
            ->registerRuntimeField(
                'HB_LEVEL',
                new Reference(
                    'HB_LEVEL',
                    \Realweb\Site\Model\Level\Table::getEntity(),
                    Join::on('this.LEVEL_ID', 'ref.ID')
                )
            )
            ->registerRuntimeField('COUNT_SCREENSHOT', new ExpressionField('COUNT_SCREENSHOT', '(' . $obSubQueryScreenshot->getQuery() . ')', array('ID')))
            ->addSelect('COUNT_SCREENSHOT')
            ->registerRuntimeField('COUNT_CUSTOM', new ExpressionField('COUNT_CUSTOM', '(' . $obSubQueryCustom->getQuery() . ')', array('ID')))
            ->addSelect('COUNT_CUSTOM')
            ->addSelect('HB_SERVER.*', 'HB_SERVER_')
            ->addSelect('HB_LEVEL.*', 'HB_LEVEL_')
            ->setOrder(array('SORT' => 'ASC', 'ID' => 'DESC'));


        if ($obNav !== null) {
            $arFilter = $obNav->getParam('filter');
            if ($arOrder = $obNav->getParam('order')) {
                $obQuery->setOrder($arOrder);
            }
            if (($id = ArrayHelper::getValue($arFilter, 'id')) !== null) {
                $obQuery->whereCustom('ID', $id);
            }
            if (($active = ArrayHelper::getValue($arFilter, 'active')) !== null) {
                $obQuery->whereCustom('ACTIVE', $active);
            }
            if (($server = ArrayHelper::getValue($arFilter, 'server')) !== null) {
                $obQuery->whereCustom('SERVER_ID', $server);
            }
            if (($test = ArrayHelper::getValue($arFilter, 'test')) !== null) {
                $obQuery->where(Query::filter()
                    ->logic('or')
                    ->where('TEST', '=', $test)
                    ->whereNull('TEST')
                );
            }
            if (ArrayHelper::getValue($arFilter, 'completed')) {
                $obQuery->where(Query::filter()
                    ->logic('or')
                    ->where('ACTIVE', '=', 'N')
                    ->whereColumn('COUNT', '<=', 'COUNT_SCREENSHOT')
                );
            }
            if (($activeCount = ArrayHelper::getValue($arFilter, 'active_count')) !== null) {
                if ($activeCount) {
                    $obQuery->whereColumn('COUNT', '>', 'COUNT_SCREENSHOT');
                } else {
                    $obQuery->whereColumn('COUNT', '<=', 'COUNT_SCREENSHOT');
                }
            }
            if (($strQuery = ArrayHelper::getValue($arFilter, 'query')) !== null) {
                $obQuery->where(Query::filter()
                    ->logic('or')
                    ->whereLike('NICKNAME', '%' . $strQuery . '%')
                //->whereLike('HB_SERVER_UF_NAME', '%' . $strQuery . '%')
                //->whereLike('HB_LEVEL_UF_NAME', '%' . $strQuery . '%')
                );
            }
            if (($executorId = ArrayHelper::getValue($arFilter, 'executor_id')) !== null) {
                $obJoin =  Join::on('this.ID', 'ref.UF_ORDER_ID')
                    ->where('ref.UF_USER_ID', '=', $executorId);
                if (($activeCustom = ArrayHelper::getValue($arFilter, 'active_custom')) !== null) {
                    $obJoin->where('ref.UF_ACTIVE', '=', $activeCustom);
                }
                $obQuery->registerRuntimeField(
                    'CUSTOM',
                    (new Reference(
                        'CUSTOM',
                        Table::class,
                        $obJoin
                    ))->configureJoinType('INNER')
                );
            }
            if (($customerId = ArrayHelper::getValue($arFilter, 'customer_id')) !== null) {
                $obQuery->whereCustom('CREATED_BY', $customerId);
            }
        }

        return $obQuery;
    }

    public static function getSingleTable()
    {
        return \Realweb\Site\Model\Iblock\Element\Property\Single\Table::getInstance(self::getIblockId());
    }

    public static function getPrice()
    {
        $obSingleTable = self::getSingleTable();
        if ($arField = $obSingleTable->getField('PRICE')) {
            $obQuery = parent::getQuery(null)
                //->where('ACTIVE', '=', 'Y')
                ->where('IBLOCK_ID', '=', self::getIblockId())
                ->registerFields(array('PRICE'), self::getIblockId())
                ->setSelect(array('MAX_PRICE', 'MIN_PRICE'))
                ->registerRuntimeField('MIN_PRICE', new ExpressionField('MIN_PRICE', 'MIN(%s)', array('S_PROPS.' . $arField['COLUMN_NAME'])))
                ->registerRuntimeField('MAX_PRICE', new ExpressionField('MAX_PRICE', 'MAX(%s)', array('S_PROPS.' . $arField['COLUMN_NAME'])));

            return $obQuery->exec()->fetch();
        }
    }

    public static function getSum()
    {
        $obSingleTable = self::getSingleTable();
        $arPriceField = $obSingleTable->getField('PRICE');
        $arCountField = $obSingleTable->getField('COUNT');
        if ($arPriceField && $arCountField) {
            $obQuery = parent::getQuery(null)
                ->where('ACTIVE', '=', 'Y')
                ->where('IBLOCK_ID', '=', self::getIblockId())
                ->registerFields(array('PRICE', 'COUNT'), self::getIblockId())
                ->setSelect(array('SUM_PRICE', 'SUM_COUNT'))
                ->registerRuntimeField('SUM_PRICE', new ExpressionField('SUM_PRICE', 'SUM(%s)', array('S_PROPS.' . $arPriceField['COLUMN_NAME'])))
                ->registerRuntimeField('SUM_COUNT', new ExpressionField('SUM_COUNT', 'SUM(%s)', array('S_PROPS.' . $arCountField['COLUMN_NAME'])));

            return $obQuery->exec()->fetch();
        }
    }

    public static function getPosition(PageNavigation $obNav)
    {
        $obNavClone = clone $obNav;
        $obNavClone->setAllRecords();
        $obQuery = self::getQuery($obNavClone);
        $arItems = $obQuery->exec()->fetchAll();
        $arItems = ArrayHelper::getColumn($arItems, 'ID');
        $iKey = array_search($obNav->getParam('order_id'), $arItems);

        return $iKey;
    }
}
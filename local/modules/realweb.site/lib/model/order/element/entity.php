<?php


namespace Realweb\Site\Model\Order\Element;

use Bitrix\Main\Type\DateTime;
use Realweb\Site\Controller\Executor;
use Realweb\Site\Controller\Order;
use Realweb\Site\Controller\Screenshot;
use Realweb\Site\Controller\Complaint;
use Realweb\Site\Controller\Custom;
use Realweb\Site\Main\ArrayHelper;
use Realweb\Site\Site;
use Realweb\Site\User;

/**
 * Class Entity
 * @package Realweb\Site\Model\Order\Element
 * @method int getRush()
 * @method int getCount()
 * @method float getPrice()
 * @method int getServerId()
 * @method int getLevelId()
 * @method string getNickname()
 * @method Entity setRush(int $int)
 * @method Entity setCount(int $int)
 * @method Entity setPrice(float $float)
 * @method Entity setServerId(int $int)
 * @method Entity setLevelId(int $int)
 * @method Entity setNickname(string $string)
 * @method Entity setLocationId(array $array)
 * @method bool hasLocationId()
 * @method int getCreatedBy()
 * @method int getCountScreenshot()
 * @method int getCountCustom()
 * @method string getClass()
 * @method Entity setClass(string $string)
 * @method DateTime getDateCreate()
 * @method Entity setDateCreate(DateTime $dateTime)
 * @method Entity setCreatedBy(int $int)
 * @method int getTest()
 * @method Entity setTest(int $int)
 * @method int setCountScreenshot(int $int)
 * @method float getFullPrice()
 * @method Entity setFullPrice(float $float)
 */
class Entity extends \Realweb\Site\Model\Iblock\Element\Entity
{
    const HOT_PRICE = 10;
    const SCREENSHOT_REMOVE_COMMENT = 'Скриншот не принят так как заказчик завершил заказ';
    /**
     * @var \Realweb\Site\Model\Server\Entity
     */
    private $_server = null;

    /**
     * @var \Realweb\Site\Model\Level\Entity
     */
    private $_level = null;

    /**
     * @var \Realweb\Site\Model\Location\Element\Collection
     */
    private $_location = null;

    /**
     * @var \Realweb\Site\Model\Screenshot\Collection
     */
    private $_screenshot = null;

    /**
     * @var \Realweb\Site\Model\Complaint\Collection
     */
    private $_complaint = null;

    /**
     * @var \Realweb\Site\Model\User\Customer\Entity
     */
    private $_customer = null;

    /**
     * @var bool
     */
    private $_expanded = false;

    /**
     * @var \Realweb\Site\Model\Custom\Collection
     */
    private $_custom = null;

    public function setData(array $arData)
    {
        $arServer = array();
        $arLevel = array();
        foreach ($arData as $key => $value) {
            if (stripos($key, 'HB_SERVER_') === 0) {
                $strKey = str_replace('HB_SERVER_', '', $key);
                $arServer[$strKey] = $value;
                unset($arData[$key]);
            } elseif (stripos($key, 'HB_LEVEL_') === 0) {
                $strKey = str_replace('HB_LEVEL_', '', $key);
                $arLevel[$strKey] = $value;
                unset($arData[$key]);
            }
        }
        if (!empty($arServer['ID'])) {
            $this->setServer(new \Realweb\Site\Model\Server\Entity($arServer));
        }
        if (!empty($arLevel['ID'])) {
            $this->setLevel(new \Realweb\Site\Model\Level\Entity($arLevel));
        }

        return parent::setData($arData);
    }

    /**
     * @return \Realweb\Site\Model\Iblock\Element\Model|string|Model
     */
    protected static function getModel()
    {
        return Model::class;
    }

    public function getLevel()
    {
        if ($this->_level === null) {
            $this->_level = new \Realweb\Site\Model\Level\Entity(array('UF_NAME' => '-'));
        }

        return $this->_level;
    }

    public function setLevel(\Realweb\Site\Model\Level\Entity $obLevel)
    {
        $this->_level = $obLevel;

        return $this;
    }

    public function getServer()
    {
        return $this->_server;
    }

    public function setServer(\Realweb\Site\Model\Server\Entity $obServer)
    {
        $this->_server = $obServer;

        return $this;
    }

    public function getLocation()
    {
        if ($this->_location === null) {
            $this->_location = $this->getMultiPropertyByModel(\Realweb\Site\Model\Location\Element\Model::class, 'LOCATION_ID');
        }

        return $this->_location;
    }

    public function setLocation(\Realweb\Site\Model\Location\Element\Entity $obLocation)
    {
        if ($this->_location === null) {
            $this->_location = new \Realweb\Site\Model\Location\Element\Collection();
        }
        $this->_location->addItem($obLocation);

        return $this;
    }

    public function getPriceFormatted()
    {
        return number_format($this->getPriceValue(), 0, '.', '') . '₽';
    }

    public function isHot()
    {
        return intval($this->getRush()) > 0 && ($this->getDateCreate()->getTimestamp() + 3600 * 24) > time();
    }

    public function getCountFormatted()
    {
        return intval($this->getCount());
    }

    public function getPercent()
    {
        return round(($this->getCountScreenshot() / $this->getCount()) * 100);
    }

    public function getScreenshot()
    {
        if ($this->_screenshot === null) {
            $obController = new Screenshot\Index();
            $obController->setParam('order_id', $this->getId());
            $obController->setParam('show_all', true);
            /** @var \Realweb\Site\Model\Screenshot\Collection $obCollection */
            $this->_screenshot = $obController->getCollection();
        }

        return $this->_screenshot;
    }

    public function getCountAllScreenshot()
    {
        return $this->getScreenshot()->getNull()->count() + $this->getScreenshot()->getSuccess()->count();
    }

    public function setScreenshot(\Realweb\Site\Model\Screenshot\Entity $obScreenshot)
    {
        if ($this->_screenshot === null) {
            $this->_screenshot = new \Realweb\Site\Model\Screenshot\Collection();
        }
        $this->_screenshot->addItem($obScreenshot);

        return $this;
    }

    public function clearScreenshot()
    {
        $this->_screenshot = null;

        return $this;
    }

    public function getPriceValue()
    {
        if ($this->isHot()) {
            return self::HOT_PRICE + $this->getPrice();
        } else {
            return $this->getPrice();
        }
    }

    public function getPriceByCount(int $iCount)
    {
        $fSum = 0;
        if ($iCount) {
            $fSum = $this->getPriceValue() * $iCount;
        }

        return $fSum;
    }

    public function getPriceFormattedByCount(int $iCount)
    {
        if ($fPrice = $this->getPriceByCount($iCount)) {
            return number_format($fPrice, 0, '.', '') . '₽';
        }
    }

    public function getSum()
    {
        return $this->getPriceValue() * $this->getCount();
    }

    public function getSumFormatted()
    {
        return number_format($this->getSum(), 0, '.', '') . '₽';
    }

    public function save()
    {
        if (!$this->hasLocationId() && $this->getLocation()) {
            $this->setLocationId($this->getLocation()->getKeys());
        }
        return parent::save();
    }

    public function hasComplaint()
    {
        return $this->_complaint !== null;
    }

    public function getComplaint()
    {
        if ($this->_complaint === null) {
            $obController = new Complaint\Index();
            $obController->setParam('order_id', $this->getId());
            $obController->setParam('show_all', true);
            /** @var \Realweb\Site\Model\Screenshot\Collection $obCollection */
            $this->_complaint = $obController->getCollection();
        }

        return $this->_complaint;
    }

    public function setComplaint(\Realweb\Site\Model\Complaint\Entity $obComplaint)
    {
        if ($this->_complaint === null) {
            $this->_complaint = new \Realweb\Site\Model\Complaint\Collection();
        }
        $this->_complaint->addItem($obComplaint);

        return $this;
    }

    public function getCustomer()
    {
        if ($this->_customer === null) {
            $this->_customer = \Realweb\Site\Model\User\Customer\Model::getByPrimary($this->getCreatedBy());
        }

        return $this->_customer;
    }

    public function isAvailable()
    {
        if ($this->getCount() > $this->getCountScreenshot() && $this->isActive() && $this->getCreatedBy() != User::getInstance()->getId()) {
            return true;
        } else {
            return false;
        }
    }

    public function getHotTime()
    {
        if ($this->getDateCreate()) {
            $iTime = $this->getDateCreate()->getTimestamp() + 3600 * 24;
            if ($iTime > time()) {
                return $iTime - time();
            }
        }
    }

    public function getStatusPrice()
    {
        $obController = new Order\Index();
        $obController->setParam('price', doubleval($this->getPrice()));
        $arResult = $obController->actionGetStatusPrice();

        return ArrayHelper::getValue($arResult, 'status', 0);
    }

    public function getColorPrice()
    {
        switch ($this->getStatusPrice()) {
            case -2:
                $strColor = '#90bacf';
                break;
            case -1:
                $strColor = '#85c5e6';
                break;
            case 0:
                $strColor = '#67b6ff';
                break;
            case 1:
                $strColor = '#0169df';
                break;
            case 2:
                $strColor = '#001eff';
                break;
            default:
                $strColor = '#67b6ff';
                break;
        }

        return $strColor;
    }

    public function getActiveCount()
    {
        return $this->getCount() - $this->getCountScreenshot();
    }

    /**
     * @return bool
     */
    public function isExpanded(): bool
    {
        return $this->_expanded;
    }

    /**
     * @param bool $expanded
     */
    public function setExpanded(bool $expanded = true): void
    {
        $this->_expanded = $expanded;
    }

    public function isTest()
    {
        return intval($this->getTest()) == 1;
    }

    public function isActive()
    {
        return $this->getActive() == 'Y';
    }

    public function isCompleted()
    {
        return $this->getCount() <= $this->getCountScreenshot() || !$this->isActive();
    }

    public function getCustom()
    {
        if ($this->_custom === null) {
            $obController = new Custom\Index();
            $obController->setParam('order_id', $this->getId());
            $obController->setParam('show_all', true);
            $this->_custom = $obController->getCollection();
        }

        return $this->_custom;
    }

    public function setCustom(\Realweb\Site\Model\Custom\Entity $obCustom)
    {
        if ($this->_custom === null) {
            $this->_custom = new \Realweb\Site\Model\Custom\Collection();
        }
        $this->_custom->addItem($obCustom);

        return $this;
    }

    public function remove()
    {
        $this->clearScreenshot();
        $arExecutorsIds = array();
        /** @var \Realweb\Site\Model\Screenshot\Entity $obItem */
        foreach ($this->getScreenshot()->getNull()->getCollection() as $obItem) {
            if (!$this->isCompleted()) {
                $this->setCountScreenshot($this->getCountScreenshot() + 1);
                $obItem->confirm();
            } else {
                $obItem->cancel(self::SCREENSHOT_REMOVE_COMMENT);
                $arExecutorsIds[$obItem->getUfUserId()] = $obItem->getUfUserId();
            }
        }
        foreach ($arExecutorsIds as $iId) {
            \Realweb\Site\Model\Notice\Entity::create('other', $iId, 'Заказчик завершил заказ #' . $this->getId() . ', ваши скриншоты были отклонены');
        }
        /** @var \Realweb\Site\Model\Custom\Entity $obItem */
        foreach ($this->getCustom()->getCollection() as $obItem) {
            $obItem->setUfActive(0);
            $obItem->save();
        }
        $fPrice = 0;
        $arUserPrice = array();
        $obCollection = $this->getScreenshot()->getByOrderToPay($this->getId());
        /** @var \Realweb\Site\Model\Screenshot\Entity $obItem */
        foreach ($obCollection->getCollection() as $obItem) {
            if (empty($arUserPrice[$obItem->getUfUserId()])) {
                $arUserPrice[$obItem->getUfUserId()] = 0;
            }
            $arUserPrice[$obItem->getUfUserId()] += $obItem->getUfPrice();
            $fPrice += $obItem->getUfPrice();
            $obItem->setUfMoneyTaken(1);
            $obItem->save();
        }
        if ($arUserPrice) {
            $obCollection = (new Executor\Index())
                ->setParam('show_all', true)
                ->setParam('id', array_keys($arUserPrice))
                ->getCollection();
            /** @var \Realweb\Site\Model\User\Executor\Entity $obItem */
            foreach ($obCollection->getCollection() as $obItem) {
                if ($fUserPrice = ArrayHelper::getValue($arUserPrice, $obItem->getId())) {
                    $obItem->updateBalance(Site::getCommissionSum($fUserPrice, 'order'));
                }
            }
        }
        if ($fFullPrice = $this->getFullPrice() - $fPrice) {
            $this->getCustomer()->updateBalance($fFullPrice, true);
        }
        $this->setFullPrice(0);
        $this->setActive('N');

        return $this->save();
    }
}
<?php


namespace Realweb\Site\Model\Order\Element;


use Realweb\Site\Controller\Screenshot;
use Realweb\Site\Controller\Complaint;
use Realweb\Site\Controller\Custom;

class Collection extends \Realweb\Site\Model\Iblock\Element\Collection
{
    public function prependItem($obItem)
    {
        if (!($obExistItem = $this->getByKey($obItem->getId()))) {
            array_unshift($this->_collection, $obItem);
            array_unshift($this->_keys, $obItem->getId());
        } else {
            $iKey = array_search($obItem->getId(), $this->_keys);
            if ($iKey !== false) {
                unset($this->_collection[$iKey]);
                unset($this->_keys[$iKey]);
                array_unshift($this->_collection, $obItem);
                array_unshift($this->_keys, $obItem->getId());
            }
        }

        return $this;
    }

    protected static function getModel()
    {
        return Model::class;
    }

    public function getLocation()
    {
        if ($this->count() > 0) {
            $this->getMultiPropertyByModel(\Realweb\Site\Model\Location\Element\Model::class, 'LOCATION_ID', 'setLocation');
        }
    }

    public function getScreenshot()
    {
        if ($this->count() > 0) {
            $obController = new Screenshot\Index();
            $obController->setParam('order_id', $this->getKeys());
            $obController->setParam('show_all', true);
            /** @var \Realweb\Site\Model\Screenshot\Collection $obCollection */
            $obCollection = $obController->getCollection();
            /** @var \Realweb\Site\Model\Screenshot\Entity $obScreenshot */
            foreach ($obCollection->getCollection() as $obScreenshot) {
                /** @var Entity $obItem */
                if ($obItem = $this->getByKey($obScreenshot->getUfOrderId())) {
                    $obItem->setScreenshot($obScreenshot);
                }
            }
        }
    }

    public function getComplaint()
    {
        if ($this->count() > 0) {
            $obController = new Complaint\Index();
            $obController->setParam('order_id', $this->getKeys());
            $obController->setParam('show_all', true);
            /** @var \Realweb\Site\Model\Complaint\Collection $obCollection */
            $obCollection = $obController->getCollection();
            /** @var \Realweb\Site\Model\Complaint\Entity $obComplaint */
            foreach ($obCollection->getCollection() as $obComplaint) {
                /** @var Entity $obItem */
                if ($obItem = $this->getByKey($obComplaint->getUfOrderId())) {
                    $obItem->setComplaint($obComplaint);
                }
            }
        }
    }

    /**
     * @return Entity
     */
    public function getOrderTest()
    {
        /** @var Entity $obItem */
        foreach ($this->getCollection() as $obItem) {
            if ($obItem->isTest()) {
                return $obItem;
            }
        }
    }

    public function getActiveOrder()
    {
        $obCollection = new Collection();
        /** @var Entity $obItem */
        foreach ($this->getCollection() as $obItem) {
            if (!$obItem->isCompleted()) {
                $obCollection->addItem($obItem);
            }
        }

        return $obCollection;
    }

    public function getCustom()
    {
        if ($this->count() > 0) {
            $obController = new Custom\Index();
            $obController->setParam('order_id', $this->getKeys());
            $obController->setParam('show_all', true);
            $obCollection = $obController->getCollection();
            /** @var \Realweb\Site\Model\Custom\Entity $obItem */
            foreach ($obCollection->getCollection() as $obItem) {
                /** @var Entity $obOrder */
                if ($obOrder = $this->getByKey($obItem->getUfOrderId())) {
                    $obOrder->setCustom($obItem);
                }
            }
        }
    }

    public function getPrice()
    {
        $fPrice = 0;
        if ($this->count() > 0) {
            /** @var Entity $obItem */
            foreach ($this->getCollection() as $obItem) {
                $fPrice += $obItem->getPriceValue() * ($obItem->getCount() - $obItem->getCountScreenshot());
            }
        }

        return $fPrice;
    }
}
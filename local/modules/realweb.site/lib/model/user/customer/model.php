<?php

namespace Realweb\Site\Model\User\Customer;

use Bitrix\Main\DB\SqlExpression;
use Bitrix\Main\Entity\ExpressionField;
use Bitrix\Main\ORM\Fields\Relations\Reference;
use Bitrix\Main\ORM\Query\Join;
use Realweb\Site\ArrayHelper;
use Realweb\Site\Model\Iblock\Element;

class Model extends \Realweb\Site\Model\User\Model
{
    public static function getFields()
    {
        return ArrayHelper::merge(parent::getFields(), array('UF_CUSTOMER_RATING_ID', 'UF_CUSTOMER_RATING', 'UF_CUSTOMER_POSITION'));
    }

    public static function getEntity()
    {
        return Entity::class;
    }

    public static function getCollection()
    {
        return Collection::class;
    }

    public static function getQuery($obNav)
    {
        $obQuery = parent::getQuery($obNav)
            ->registerRuntimeField(
                'RATING',
                new Reference(
                    'RATING',
                    \Realweb\Site\Model\Rating\Table::getEntity(),
                    Join::on('this.UF_CUSTOMER_RATING_ID', 'ref.ID')
                )
            )
            ->addSelect('RATING.*', 'RATING_')
            ->addSelect('UF_CUSTOMER_POSITION', 'POSITION')
            ->setOrder(array('POSITION' => 'ASC'));
        $obSubQuery = \Realweb\Site\Model\Screenshot\Model::getQuery(null)
            ->setSelect(array('COUNT_SCREENSHOT'))
            ->where('ORDER.CREATED_BY', "=", new SqlExpression('%s'))
            ->where('STATUS.XML_ID', '=', 'success')
            ->registerRuntimeField(
                'ORDER',
                new Reference(
                    'ORDER',
                    Element\Table::class,
                    Join::on('this.UF_ORDER_ID', 'ref.ID')
                )
            )
            ->registerRuntimeField('COUNT_SCREENSHOT', new ExpressionField('COUNT_SCREENSHOT', 'COUNT(%s)', array('ID')));
        $obQuery->registerRuntimeField('COUNT_SUCCESS_SCREENSHOT', new ExpressionField('COUNT_SUCCESS_SCREENSHOT', '(' . $obSubQuery->getQuery() . ')', array('ID')))
            ->addSelect('COUNT_SUCCESS_SCREENSHOT');

        $obSubQuery = \Realweb\Site\Model\Screenshot\Model::getQuery(null)
            ->setSelect(array('COUNT_SCREENSHOT'))
            ->where('ORDER.CREATED_BY', "=", new SqlExpression('%s'))
            ->where('STATUS.XML_ID', '=', 'fail')
            ->registerRuntimeField(
                'ORDER',
                new Reference(
                    'ORDER',
                    Element\Table::class,
                    Join::on('this.UF_ORDER_ID', 'ref.ID')
                )
            )
            ->registerRuntimeField('COUNT_SCREENSHOT', new ExpressionField('COUNT_SCREENSHOT', 'COUNT(%s)', array('ID')));
        $obQuery->registerRuntimeField('COUNT_FAIL_SCREENSHOT', new ExpressionField('COUNT_FAIL_SCREENSHOT', '(' . $obSubQuery->getQuery() . ')', array('ID')))
            ->addSelect('COUNT_FAIL_SCREENSHOT');

//        $obSubQuery = Element\Table::query()
//            ->setSelect(array('COUNT'))
//            ->where('CREATED_BY', "=", new SqlExpression('%s'))
//            ->where('IBLOCK_ID', '=', \Realweb\Site\Model\Order\Element\Model::getIblockId())
//            ->registerRuntimeField('COUNT', new ExpressionField('COUNT', 'COUNT(%s)', array('ID')));
//        $obQuery->registerRuntimeField('COUNT_ORDER', new ExpressionField('COUNT_ORDER', '(' . $obSubQuery->getQuery() . ')', array('ID')))
//            ->addSelect('COUNT_ORDER');

//        $obSubQuery = Element\Table::query()
//            ->setSelect(array('COUNT'))
//            ->where('CREATED_BY', "=", new SqlExpression('%s'))
//            ->where('ACTIVE', '=', 'Y')
//            ->where('IBLOCK_ID', '=', \Realweb\Site\Model\Order\Element\Model::getIblockId())
//            ->registerRuntimeField('COUNT', new ExpressionField('COUNT', 'COUNT(%s)', array('ID')));
//        $obQuery->registerRuntimeField('COUNT_ACTIVE_ORDER', new ExpressionField('COUNT_ACTIVE_ORDER', '(' . $obSubQuery->getQuery() . ')', array('ID')))
//            ->addSelect('COUNT_ACTIVE_ORDER');

        if ($obNav !== null) {
            if ($arOrder = $obNav->getParam('order')) {
                $obQuery->setOrder($arOrder);
            }
        }

        return $obQuery;
    }
}
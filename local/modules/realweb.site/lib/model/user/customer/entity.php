<?php


namespace Realweb\Site\Model\User\Customer;

use Realweb\Site\Main\Helper;
use Realweb\Site\Model;
use Realweb\Site\Controller\Order;

/**
 * Class Entity
 * @package Realweb\Site\Model\User\Customer
 * @method float getUfCustomerRating()
 * @method Entity setUfCustomerRating(int $int)
 * @method int getUfCustomerRatingId()
 * @method Entity setUfCustomerRatingId(int $int)
 * @method int getUfCustomerPosition()
 * @method Entity setUfCustomerPosition(int $int)
 */
class Entity extends \Realweb\Site\Model\User\Entity
{
    public const RATING = 5;

    protected static function getModel()
    {
        return Model\User\Executor\Model::class;
    }

    public function getUserRating()
    {
        return doubleval(round($this->getUfCustomerRating(), 1));
    }

    public function getTextForNextLevel()
    {
        if ($iCount = $this->getCountScreenshotForNextLevel()) {
            return $iCount . ' ' . Helper::morph($iCount, 'скриншот', 'скриншота', 'скриншотов');
        }
    }

    public function getOrder()
    {
        if ($this->_order === null) {
            $obController = new Order\Index();
            $obController->setParam('customer_id', $this->getId());
            $obController->setParam('show_all', true);
            $this->_order = $obController->findCollection();
        }

        return $this->_order;
    }

    public function getCountActiveOrder()
    {
        return $this->getOrder()->getActiveOrder()->count();
    }

    public function getCountOrder()
    {
        return $this->getOrder()->count();
    }

    public function updateUserRating()
    {
        if ($this->getOrder()) {
            $fRating = self::RATING;
            $this->getOrder()->getComplaint();
            /** @var Model\Order\Element\Entity $obOrder */
            foreach ($this->getOrder() as $obOrder) {
                if ($obOrder->hasComplaint()) {
                    $fRating -= $obOrder->getComplaint()->count() * 0.1;
                }
            }
            if ($this->getUserRating() != $fRating) {
                $this->setUfCustomerRating($fRating);
                $this->save();
            }
        }
    }
}
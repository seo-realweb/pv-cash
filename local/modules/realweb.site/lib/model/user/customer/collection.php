<?php


namespace Realweb\Site\Model\User\Customer;


class Collection extends \Realweb\Site\Model\User\Collection
{
    protected static function getModel()
    {
        return Model::class;
    }
}
<?php

namespace Realweb\Site\Model\User;

use Bitrix\Main\ORM\Fields\Relations\Reference;
use Bitrix\Main\ORM\Query\Join;
use Realweb\Site\ArrayHelper;
use Realweb\Site\Data\PageNavigation;

class Model extends \Realweb\Site\Data\Model
{
    public static function getFields()
    {
        return array(
            'ID', 'XML_ID', 'NAME',
            'EMAIL', 'LOGIN', 'TITLE', 'CONFIRM_CODE',
            'LAST_NAME', 'SECOND_NAME', 'PERSONAL_PHONE',
            'PERSONAL_PHOTO', 'PERSONAL_GENDER', 'PERSONAL_CITY',
            'LAST_LOGIN',
            'UF_BALANCE',
            'UF_BALANCE_HOLD',
            'UF_LAST_ACTION'
        );
    }

    public static function getCollection()
    {
        return Collection::class;
    }

    public static function getEntity()
    {
        return Entity::class;
    }

    public static function getOrmEntity()
    {
        return Table::getEntity();
    }

    public static function getQuery($obNav)
    {
        $obQuery = Table::query()
            ->setSelect(static::getFields())
            ->registerFileField('PERSONAL_PHOTO');

        if ($obNav !== null) {
            $arFilter = $obNav->getParam('filter');
            if (($id = ArrayHelper::getValue($arFilter, 'id')) !== null) {
                $obQuery->whereCustom('ID', $id);
            }
            if (($email = ArrayHelper::getValue($arFilter, 'email')) !== null) {
                $obQuery->whereCustom('EMAIL', $email);
            }
            if (($xmlId = ArrayHelper::getValue($arFilter, 'xml_id')) !== null) {
                $obQuery->whereCustom('XML_ID', $xmlId);
            }
            if (($confirmCode = ArrayHelper::getValue($arFilter, 'confirm_code')) !== null) {
                $obQuery->whereCustom('CONFIRM_CODE', $confirmCode);
            }
        }

        return $obQuery;
    }

    public static function getTag()
    {
        return Table::getTableName();
    }

    public static function clearCache()
    {
        global $CACHE_MANAGER;
        $CACHE_MANAGER->ClearByTag(static::getTag());
    }

    public static function getByEmail(string $strEmail)
    {
        $obNav = new PageNavigation();
        $obNav->setParam('filter', array(
            'email' => $strEmail
        ));
        $obItem = self::getObject($obNav);
        if ($obItem->isExist()) {
            return $obItem;
        }
    }
}
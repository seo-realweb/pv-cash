<?php


namespace Realweb\Site\Model\User;


class Collection extends \Realweb\Site\Data\Collection
{
    protected static function getModel()
    {
        return Model::class;
    }

    /**
     * @param \Realweb\Site\Data\Data|\Realweb\Site\Data\Entity|Entity $obItem
     * @return $this|mixed
     */
    public function addItem($obItem)
    {
        $this->_collection[] = $obItem;
        $this->_keys[] = $obItem->getId();

        return $this;
    }
}
<?php

namespace Realweb\Site\Model\User;

use Bitrix\Main\UserTable;
use Realweb\Site\Data\Orm\Query;

class Table extends UserTable
{
    public static function query()
    {
        return new Query(static::getEntity());
    }
}
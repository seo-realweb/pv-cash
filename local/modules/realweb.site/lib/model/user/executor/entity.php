<?php


namespace Realweb\Site\Model\User\Executor;

use Realweb\Site\Controller\Custom;
use Realweb\Site\Controller\Screenshot;
use Realweb\Site\Main\Helper;
use Realweb\Site\Model;
use Realweb\Site\Controller\Order;

/**
 * Class Entity
 * @package Realweb\Site\Model\User\Executor
 * @method Entity setUfRatingId(int $int)
 * @method int getUfRatingId()
 * @method float getUfExecutorRating()
 * @method Entity setUfExecutorRating(int $int)
 * @method int getUfExecutorRatingId()
 * @method Entity setUfExecutorRatingId(int $int)
 * @method int getUfExecutorPosition()
 * @method Entity setUfExecutorPosition(int $int)
 */
class Entity extends \Realweb\Site\Model\User\Entity
{
    /**
     * @var Model\Custom\Collection
     */
    private $_custom = null;

    /**
     * @var Model\Screenshot\Collection
     */
    private $_screenshot = null;

    protected static function getModel()
    {
        return Model\User\Executor\Model::class;
    }

    public function getCustom()
    {
        if ($this->_custom === null) {
            $obController = new Custom\Index();
            $obController->setParam('user_id', $this->getId());
            $this->_custom = $obController->actionIndex();
        }

        return $this->_custom;
    }

    public function getOrder()
    {
        if ($this->_order === null) {
            $obController = new Order\Index();
            $obController->setParam('id', $this->getCustom()->getOrderKeys());
            $this->_order = $obController->findCollection();
        }

        return $this->_order;
    }

    public function getUserRating()
    {
        return doubleval(round($this->getUfExecutorRating(), 1));
    }

    public function getCountOrder()
    {
        if ($this->getRating()) {
            return $this->getRating()->getUfCountOrder();
        } else {
            return 0;
        }
    }

    public function getLeftOrder()
    {
        return $this->getCountOrder() - $this->getCustom()->getActiveCount();
    }

    public function getCountNewOrder()
    {
        if ($this->getRating() && $this->getCustom()) {
            return $this->getRating()->getUfCountOrder() - $this->getCustom()->getActiveCount();
        } else {
            return 0;
        }
    }

    public function getCountTextNewOrder()
    {
        return '<b class="red">' . $this->getCountNewOrder() . '</b> ' . Helper::morph($this->getCountNewOrder(), 'заказ', 'заказа', 'заказов');
    }


    public function getTextForNextLevel()
    {
        if ($iCount = $this->getCountScreenshotForNextLevel()) {
            return $iCount . ' ' . Helper::morph($iCount, 'убийство', 'убийства', 'убийств');
        }
    }

    public function getScreenshot()
    {
        if ($this->_screenshot === null) {
            $obController = new Screenshot\Index();
            $obController->setParam('user_id', $this->getId());
            //$obController->setParam('status', 'success');
            //$obController->setParam('money_taken', 0);
            $obController->setParam('show_all', true);
            $this->_screenshot = $obController->getCollection();
        }

        return $this->_screenshot;
    }
}
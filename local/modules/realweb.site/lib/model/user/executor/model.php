<?php

namespace Realweb\Site\Model\User\Executor;

use Bitrix\Main\DB\SqlExpression;
use Bitrix\Main\Entity\ExpressionField;
use Bitrix\Main\ORM\Fields\Relations\Reference;
use Bitrix\Main\ORM\Query\Join;
use Realweb\Site\ArrayHelper;

class Model extends \Realweb\Site\Model\User\Model
{
    public static function getFields()
    {
        return ArrayHelper::merge(parent::getFields(), array('UF_EXECUTOR_RATING_ID', 'UF_EXECUTOR_RATING', 'UF_EXECUTOR_POSITION'));
    }

    public static function getEntity()
    {
        return Entity::class;
    }

    public static function getCollection()
    {
        return Collection::class;
    }

    public static function getQuery($obNav)
    {
        $obQuery = parent::getQuery($obNav)
            ->registerRuntimeField(
                'RATING',
                new Reference(
                    'RATING',
                    \Realweb\Site\Model\Rating\Table::getEntity(),
                    Join::on('this.UF_EXECUTOR_RATING_ID', 'ref.ID')
                )
            )
            ->addSelect('RATING.*', 'RATING_')
            ->addSelect('UF_EXECUTOR_POSITION', 'POSITION')
            ->setOrder(array('POSITION' => 'ASC'));

        $obSubQuery = \Realweb\Site\Model\Screenshot\Model::getQuery(null)
            ->setSelect(array('COUNT_SCREENSHOT'))
            ->where('UF_USER_ID', "=", new SqlExpression('%s'))
            ->where('STATUS.XML_ID', '=', 'success')
            ->registerRuntimeField('COUNT_SCREENSHOT', new ExpressionField('COUNT_SCREENSHOT', 'COUNT(%s)', array('ID')));
        $obQuery->registerRuntimeField('COUNT_SUCCESS_SCREENSHOT', new ExpressionField('COUNT_SUCCESS_SCREENSHOT', '(' . $obSubQuery->getQuery() . ')', array('ID')))
            ->addSelect('COUNT_SUCCESS_SCREENSHOT');

        $obSubQuery = \Realweb\Site\Model\Screenshot\Model::getQuery(null)
            ->setSelect(array('COUNT_SCREENSHOT'))
            ->where('UF_USER_ID', "=", new SqlExpression('%s'))
            ->where('STATUS.XML_ID', '=', 'fail')
            ->registerRuntimeField('COUNT_SCREENSHOT', new ExpressionField('COUNT_SCREENSHOT', 'COUNT(%s)', array('ID')));
        $obQuery->registerRuntimeField('COUNT_FAIL_SCREENSHOT', new ExpressionField('COUNT_FAIL_SCREENSHOT', '(' . $obSubQuery->getQuery() . ')', array('ID')))
            ->addSelect('COUNT_FAIL_SCREENSHOT');

        if ($obNav !== null) {
            if ($arOrder = $obNav->getParam('order')) {
                $obQuery->setOrder($arOrder);
            }
        }

        return $obQuery;
    }
}
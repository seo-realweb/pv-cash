<?php


namespace Realweb\Site\Model\User;

use Bitrix\Main\Type\DateTime;
use Realweb\Site\ArrayHelper;
use Realweb\Site\Controller\Message;
use Realweb\Site\Controller\Notice;
use Realweb\Site\Main\Helper;
use Realweb\Site\Controller\Rating;
use Realweb\Site\Controller\Introduction;
use Realweb\Site\Model\Order;
use Realweb\Site\Site;

/**
 * Class Entity
 * @package Realweb\Site\Model\User
 * @method string getPersonalPhone()
 * @method float getUfBalance()
 * @method string getLogin()
 * @method string getEmail()
 * @method int getPersonalPhoto()
 * @method string getPersonalPhotoSrc()
 * @method Entity setUfBalance(float $float)
 * @method DateTime getLastLogin()
 * @method Entity setLogin(string $string)
 * @method Entity setEmail(string $string)
 * @method Entity setXmlId(string $string)
 * @method Entity setPersonalPhone(string $string)
 * @method Entity setName(string $string)
 * @method Entity setLastName(string $string)
 * @method Entity setSecondName(string $string)
 * @method Entity setTitle(string $string)
 * @method string getConfirmCode()
 * @method Entity setConfirmCode(string $string)
 * @method int getCountSuccessScreenshot()
 * @method int getCountFailScreenshot()
 * @method int getPosition()
 * @method Entity setPassword(string $string)
 * @method Entity setPersonalPhoto(array $array)
 * @method DateTime getUfLastAction()
 * @method Entity setUfLastAction(DateTime $dateTime)
 * @method float getUfBalanceHold()
 * @method Entity setUfBalanceHold(float $float)
 */
class Entity extends \Realweb\Site\Data\Entity
{
    /**
     * @var \Realweb\Site\Model\Rating\Entity
     */
    private $_rating = null;

    /**
     * @var \Realweb\Site\Model\Rating\Entity
     */
    private $_next_rating = null;

    /**
     * @var \Realweb\Site\Model\Rating\Entity
     */
    private $_prev_rating = null;

    /**
     * @var \CUser
     */
    private $_c_user = null;

    /**
     * @var int
     */
    private $_count_message = null;

    /**
     * @var \Realweb\Site\Model\Notice\Collection
     */
    private $_notice = null;

    /**
     * @var Order\Element\Collection
     */
    protected $_order = null;

    protected static function getModel()
    {
        return Model::class;
    }

    public function setData(array $arData)
    {
        $arRating = array();
        foreach ($arData as $key => $value) {
            if (stripos($key, 'RATING_') === 0) {
                $strKey = str_replace('RATING_', '', $key);
                $arRating[$strKey] = $value;
                unset($arData[$key]);
            }
        }
        if (!empty($arRating['ID'])) {
            $this->setRating(new \Realweb\Site\Model\Rating\Entity($arRating));
        } else {
            $this->setRating(new \Realweb\Site\Model\Rating\Entity());
        }
        return parent::setData($arData);
    }

    public function updateRating()
    {
        $obController = new Rating\Index();
        $obController->setParam('count_screenshot', $this->getCountSuccessScreenshot());
        if ($obRating = $obController->actionCurrent()) {
            if ($this->getRating()->getId() != $obRating->getId()) {
                \Realweb\Site\Model\Notice\Entity::create('level_up', $this->getId());
                if (in_array(intval($obRating->getUfLevel()), array(3, 5, 7, 9, 10, 20, 40, 85), true)) {
                    \Realweb\Site\Model\Notice\Entity::create('bonus', $this->getId(), $obRating->getUfLevel());
                }
            }
            $this->_next_rating = null;
            $this->setRating($obRating);
            if ($this->isExecutor()) {
                $this->setUfExecutorRatingId($obRating->getId());
            } else {
                $this->setUfCustomerRatingId($obRating->getId());
            }
            $this->save();
        }
    }

    public function setRating(\Realweb\Site\Model\Rating\Entity $obRating)
    {
        $this->_rating = $obRating;

        return $this;
    }

    public function getRating()
    {
        return $this->_rating;
    }

    public function getRatingLevel()
    {
        if ($this->getRating()->getUfLevel()) {
            return $this->getRating()->getUfLevel();
        } else {
            return 1;
        }
    }

    public function getNextRating()
    {
        if ($this->_next_rating === null) {
            $obController = new Rating\Index();
            $obCollection = $obController->actionIndex();
            /** @var \Realweb\Site\Model\Rating\Entity $obItem */
            foreach ($obCollection->getCollection() as $obItem) {
                if ($this->getRatingLevel() < $obItem->getUfLevel()) {
                    $this->_next_rating = $obItem;
                    break;
                }
            }
        }

        return $this->_next_rating;
    }

    public function getPrevRating()
    {
        if ($this->_prev_rating === null) {
            $obController = new Rating\Index();
            $obCollection = $obController->actionIndex();
            /** @var \Realweb\Site\Model\Rating\Entity $obItem */
            foreach ($obCollection->getCollection() as $obItem) {
                if ($this->getRatingLevel() > $obItem->getUfLevel()) {
                    $this->_prev_rating = $obItem;
                    break;
                }
            }
        }

        return $this->_prev_rating;
    }

    public function getCountScreenshotForNextLevel()
    {
        if ($this->getNextRating()) {
            return $this->getNextRating()->getUfCountScreen() - $this->getCountSuccessScreenshot();
        }
    }

    public function getTextForNextLevel()
    {
        return '';
    }

    public function getScreenshotForNextLevel()
    {
        if ($iCount = $this->getCountScreenshotForNextLevel()) {
            return $iCount . ' ' . Helper::morph($iCount, 'скриншот', 'скриншота', 'скриншотов');
        }
    }

    public function getPercentForNextLevel()
    {
        if ($this->getNextRating()) {
            if ($this->getCountSuccessScreenshot()) {
                return round((($this->getCountSuccessScreenshot() - $this->getRating()->getUfCountScreen()) / ($this->getNextRating()->getUfCountScreen() - $this->getRating()->getUfCountScreen())) * 100);
            } else {
                return 0;
            }
        }
    }

    public function getPercentForNextLevelRange()
    {
        $iResult = 0;
        if ($this->getNextRating()) {
            $iPercent = $this->getPrevRating() ? 20 : 15;
            $iResult = round((($this->getCountSuccessScreenshot() - $this->getRating()->getUfCountScreen()) / ($this->getNextRating()->getUfCountScreen() - $this->getRating()->getUfCountScreen())) * $iPercent);
            if ($this->getPrevRating()) {
                $iResult += 20;
            } elseif ($iResult) {
                $iResult += 5;
            }
        }

        return $iResult;
    }

    public function getPercentSuccessScreenshot()
    {
        if ($this->getCountSuccessScreenshot()) {
            return round(($this->getCountSuccessScreenshot() * 100) / ($this->getCountSuccessScreenshot() + $this->getCountFailScreenshot()));
        } else {
            return 0;
        }
    }

    public function getCUser()
    {
        if ($this->_c_user === null) {
            global $USER;
            if (!is_object($USER)) {
                $USER = new \CUser;
            }
            $this->_c_user = $USER;
        }

        return $this->_c_user;
    }

    public function save()
    {
        $arFields = $this->getData();
        $obUser = $this->getCUser();
        if (!empty($arFields['PERSONAL_PHOTO']) && !is_array($arFields['PERSONAL_PHOTO'])) {
            unset($arFields['PERSONAL_PHOTO']);
        }
        if ($this->isExist()) {
            $obUser->Update($this->getId(), $arFields);
        } else {
            $bConfirmReq = (\COption::GetOptionString("main", "new_user_registration_email_confirmation", "N") == "Y" && \COption::GetOptionString("main", "new_user_email_required", "Y") <> "N");
            $arFields["LID"] = SITE_ID;
            if (empty($arFields["GROUP_ID"])) {
                $arFields["GROUP_ID"] = [];
            }
            $defGroup = explode(",", \COption::GetOptionString("main", "new_user_registration_def_group", ""));
            if ($defGroup) {
                $arFields["GROUP_ID"] = ArrayHelper::merge($defGroup, $arFields["GROUP_ID"]);
            }
            $strPassword = (isset($arFields['PASSWORD']) && strlen($arFields['PASSWORD']) > 0 ? $arFields['PASSWORD'] : randString(10));
            $arFields['ACTIVE'] = $bConfirmReq ? "N" : "Y";
            if (empty($arFields['CONFIRM_CODE'])) {
                $arFields['CONFIRM_CODE'] = $bConfirmReq ? randString(8) : "";
            }
            $arFields['PASSWORD'] = $strPassword;
            $arFields['CONFIRM_PASSWORD'] = $strPassword;
            $iId = $obUser->Add($arFields);
            $this->setPrimary($iId);
        }
        unset($this->_data['PASSWORD']);
        if ($obUser->LAST_ERROR) {
            $this->getOrmResult()->addError(new \Bitrix\Main\Error($obUser->LAST_ERROR));

            return false;
        } else {
            self::getModel()::clearCache();

            return true;
        }
    }

    public function getBalanceFormatted()
    {
        return number_format($this->getUfBalance(), 0, '.', '') . '₽';
    }

    public function getBalanceValue()
    {
        return doubleval($this->getUfBalance());
    }

    public function getPhoto()
    {
        if ($this->getPersonalPhoto()) {
            return $this->getPersonalPhotoSrc();
        } else {
            return SITE_TEMPLATE_PATH . '/images/no-avatar.png';
        }
    }

    public function updateBalance(float $fBalance, bool $bSkipTransaction = false, bool $bHold = false)
    {
        $this->setUfBalance(doubleval($this->getUfBalance()) + doubleval($fBalance));
        if ($bHold) {
            $this->setUfBalanceHold(doubleval($this->getUfBalanceHold()) + Site::getCommissionSum(abs(doubleval($fBalance)), 'out'));
        }
        if (!$bSkipTransaction) {
            $obTransaction = new \Realweb\Site\Model\Transaction\Entity();
            $obTransaction->setUfUserId($this->getId());
            $obTransaction->setUfSum($fBalance);
            $obTransaction->save();
        }

        return $this->save();
    }

    public function getLastLoginFormatted()
    {
        if ($this->getLastLogin()) {
            return FormatDate("x", $this->getLastLogin()->getTimestamp());
        }
    }

    public function getLastActionFormatted()
    {
        if ($this->getUfLastAction()) {
            return FormatDate("x", $this->getUfLastAction()->getTimestamp());
        }
    }

    public function isExecutor()
    {
        if ($this instanceof \Realweb\Site\Model\User\Executor\Entity) {
            return true;
        }
    }

    public function isCustomer()
    {
        if ($this instanceof \Realweb\Site\Model\User\Customer\Entity) {
            return true;
        }
    }

    public function getTypeCode()
    {
        if ($this->isExecutor()) {
            return 'executor';
        } else {
            return 'customer';
        }
    }

    public function getCountMessage()
    {
        if ($this->_count_message === null) {
            $obController = new Message\Index();
            $obController->setParam('user_to_id', $this->getId());
            $obController->setParam('read', 0);
            $this->_count_message = $obController->actionCount();
        }

        return $this->_count_message;
    }

    public function getNotice()
    {
        if ($this->_notice === null) {
            $obController = new Notice\Index();
            $obController->setParam('show_all', true);
            $obController->setParam('active', 1);
            $this->_notice = $obController->getCollection();
        }

        return $this->_notice;
    }

    public function updateLastAction()
    {
        $this->setUfLastAction(new DateTime());
        $this->save();
    }

    public function refresh()
    {
        $this->_order = null;

        return $this;
    }

    /**
     * @return bool
     */
    public function isWithdrawal(): bool
    {
        if ($this->getRating()) {
            $iDelay = intval($this->getRating()->getUfDelayFunds());
            if ($iDelay > -1) {
                return true;
            }
        }

        return false;
    }

    public function getDateWithdrawal()
    {
        if ($this->isWithdrawal()) {
            $iDelay = intval($this->getRating()->getUfDelayFunds());
            if ($iDelay > 0) {
                return (new DateTime())->add('-' . $iDelay . ' days');
            }
        }
    }
}
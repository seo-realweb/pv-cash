<?php


namespace Realweb\Site\Model\Auth;


use Bitrix\Main\UserTable;
use Realweb\Site\Data\PageNavigation;
use Realweb\Site\Model\User\Entity;
use Realweb\Site\Model\User\Model;
use Realweb\Site\Site;

class Telegram
{
    const NAME = 'pv_cash_local_bot';
    const TOKEN = '1282480410:AAF_RTd5kwl3sDaoefq4mP4Sm9j6Fx0d0NA';
    //https://api.telegram.org/bot1282480410:AAF_RTd5kwl3sDaoefq4mP4Sm9j6Fx0d0NA/setwebhook?url=https://pv-cash.frybytes.ru/api/auth/telegram/index/

    public static $strKey = null;

    public static function getKey()
    {
        if (self::$strKey === null) {
            self::$strKey = md5(uniqid());
        }

        return self::$strKey;
    }

    public static function getName()
    {
        return self::NAME;
    }

    /** Инициализируем работу класса
     * @return bool
     */
    public function init()
    {
        // получаем данные от АПИ и преобразуем их в ассоциативный массив
        $rawData = json_decode(file_get_contents('php://input'), true);
        // направляем данные из бота в метод
        // для определения дальнейшего выбора действий
        $this->router($rawData);
        // в любом случае вернем true для бот апи
        return true;
    }

    /** Роутер
     * @param $data array
     * @return bool
     */
    private function router($data)
    {
        // проверяем на объект Message
        if (array_key_exists("message", $data)) {
            // получаем чат
            $chat_id = $data['message']['chat']['id'];
            // проверяем на наличие объекта Text
            if (array_key_exists("text", $data['message'])) {
                // Получаем значение отправленных данных
                $text = $data['message']['text'];
                // Если это просто старт бота
                if ($text == "/start") {
                    // отправляем сообщение
                    $this->botApiQuery("sendMessage", [
                        'chat_id' => $chat_id,
                        'text' => 'Бот авторизации'
                    ]);
                    // Если это старт бота с передаваемым значением параметра start
                } elseif (preg_match('~^(\/start)+ ([a-f0-9]{32}+)$~', $text, $matches)) {
                    // передаем в метод авторизации
                    $this->setAuth($data['message'], $matches[2]);
                    // если это какой-то другой текст
                } else {
                    // просто пишем чего-нибудь в чат
                    $this->botApiQuery("sendMessage", [
                        'chat_id' => $chat_id,
                        'text' => 'Бот авторизации'
                    ]);
                }
                // если это что-то другое - картинка или еще что-то
            } else {
                // просто пишем чего-нибудь в чат
                $this->botApiQuery("sendMessage", [
                    'chat_id' => $chat_id,
                    'text' => 'Бот авторизации'
                ]);
            }
        }
        // другие объекты не рассматриваем
        return true;
    }

    /**
     * @param $data
     * @param $key
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    private function setAuth($data, $key)
    {
        $strResult = 'error';
        $obNav = new PageNavigation();
        $obNav->setParam('filter', array('xml_id' => $data['chat']['id']));
        /** @var Entity $obUser */
        $obUser = Model::getObject($obNav);
        if ($obUser->isExist()) {
            $obUser->setConfirmCode($key);
            if ($obUser->save()) {
                $strResult = "success";
            }
        } else {
            $obUser->setConfirmCode($key);
            $obUser->setXmlId($data['chat']['id']);
            $obUser->setLogin($data['chat']['id']);
            $obUser->setName($data['chat']['first_name']);
            $obUser->setLastName($data['chat']['last_name']);
            $obUser->setTitle($data['chat']['username']);
            $obUser->setEmail($data['chat']['id'] . '@' . Site::getHost());
            if ($obUser->save()) {
                $strResult = "success";
            }
        }
        $this->botApiQuery("sendMessage", [
            'chat_id' => $data['chat']['id'],
            'text' => $strResult
        ]);
    }

    /**
     * @param $method
     * @param array $fields
     * @return mixed
     */
    private function botApiQuery($method, $fields = array())
    {
        $ch = curl_init('https://api.telegram.org/bot' . self::TOKEN . '/' . $method);
        curl_setopt_array($ch, array(
            CURLOPT_POST => count($fields),
            CURLOPT_POSTFIELDS => http_build_query($fields),
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_TIMEOUT => 10
        ));
        $r = json_decode(curl_exec($ch), true);
        curl_close($ch);

        return $r;
    }
}
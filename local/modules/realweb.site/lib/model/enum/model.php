<?php

namespace Realweb\Site\Model\Enum;


use Realweb\Site\Main\ArrayHelper;

class Model extends \Realweb\Site\Data\Model
{
    public static $arFields = array('ID', 'USER_FIELD_ID', 'VALUE', 'DEF', 'SORT', 'XML_ID');

    /**
     * @return array
     */
    public static function getFields()
    {
        return self::$arFields;
    }

    public static function getCollection()
    {
        return Collection::class;
    }

    public static function getEntity()
    {
        return Entity::class;
    }

    public static function getOrmEntity()
    {
        return Table::getEntity();
    }

    public static function getTag()
    {
        return Table::getTableName();
    }

    public static function clearCache()
    {
        global $CACHE_MANAGER;
        $CACHE_MANAGER->ClearByTag(static::getTag());
    }

    /**
     * @param \Realweb\Site\Data\PageNavigation $obNav
     * @return \Bitrix\Main\ORM\Query\Query|\Realweb\Site\Data\Orm\Query
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getQuery($obNav)
    {
        $obQuery = Table::query()
            ->setSelect(self::getFields())
            ->setOrder(array('SORT' => 'ASC'));
        if ($obNav !== null) {
            $arFilter = $obNav->getParam('filter');
            if (($id = ArrayHelper::getValue($arFilter, 'id')) !== null) {
                $obQuery->whereCustom('ID', $id);
            }
            if ($code = ArrayHelper::getValue($arFilter, 'code')) {
                $obQuery->where('FIELD.FIELD_NAME', '=', $code);
            }
            if ($entityId = ArrayHelper::getValue($arFilter, 'entity_id')) {
                $obQuery->where('FIELD.ENTITY_ID', '=', $entityId);
            }
        }

        return $obQuery;
    }
}
<?php

namespace Realweb\Site\Model\Enum;

/**
 * Class Collection
 * @package Realweb\Api\Module\Enum\Model
 */
class Collection extends \Realweb\Site\Data\Collection
{
    private $_xml_key = array();

    protected static function getModel()
    {
        return Model::class;
    }

    /**
     * @param Entity $obItem
     * @return $this
     */
    public function addItem($obItem)
    {
        $this->_collection[] = $obItem;
        $this->_keys[] = $obItem->getId();
        $this->_xml_key[] = $obItem->getXmlId();

        return $this;
    }

    /**
     * @param $strXmlId
     * @return \Realweb\Site\Data\Data|Entity
     */
    public function getByXmlId($strXmlId)
    {
        $iCollectionKey = array_search($strXmlId, $this->_xml_key);
        if ($iCollectionKey !== false) {
            return $this->_collection[$iCollectionKey];
        }
    }
}

<?php

namespace Realweb\Site\Model\Enum;

/**
 * Class Entity
 * @package Realweb\Site\Model\Enum
 * @method int getId()
 * @method string getValue()
 * @method string getXmlId()
 */
class Entity extends \Realweb\Site\Data\Entity
{
    /**
     * @return \Realweb\Site\Data\Model|string|Model
     */
    protected static function getModel()
    {
        return Model::class;
    }
}

<?php


namespace Realweb\Site\Model\Enum;

use Bitrix\Main;
use Bitrix\Main\Localization\Loc;
use Realweb\Site\Data\Orm\Query;

class Table extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_user_field_enum';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('FIELD_ENUM_ENTITY_ID_FIELD'),
            ),
            'USER_FIELD_ID' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('FIELD_ENUM_ENTITY_USER_FIELD_ID_FIELD'),
            ),
            'VALUE' => array(
                'data_type' => 'string',
                'required' => true,
                'validation' => array(__CLASS__, 'validateValue'),
                'title' => Loc::getMessage('FIELD_ENUM_ENTITY_VALUE_FIELD'),
            ),
            'DEF' => array(
                'data_type' => 'boolean',
                'values' => array('N', 'Y'),
                'title' => Loc::getMessage('FIELD_ENUM_ENTITY_DEF_FIELD'),
            ),
            'SORT' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('FIELD_ENUM_ENTITY_SORT_FIELD'),
            ),
            'XML_ID' => array(
                'data_type' => 'string',
                'required' => true,
                'validation' => array(__CLASS__, 'validateXmlId'),
                'title' => Loc::getMessage('FIELD_ENUM_ENTITY_XML_ID_FIELD'),
            ),
            'FIELD' => new Main\Entity\ReferenceField(
                'FIELD',
                '\Bitrix\Main\UserFieldTable',
                array('=this.USER_FIELD_ID' => 'ref.ID'),
                array('join_type' => 'INNER')
            ),
        );
    }

    /**
     * Returns validators for VALUE field.
     *
     * @return array
     */
    public static function validateValue()
    {
        return array(
            new Main\Entity\Validator\Length(null, 255),
        );
    }

    /**
     * Returns validators for XML_ID field.
     *
     * @return array
     */
    public static function validateXmlId()
    {
        return array(
            new Main\Entity\Validator\Length(null, 255),
        );
    }

    public static function query()
    {
        return new Query(static::getEntity());
    }
}
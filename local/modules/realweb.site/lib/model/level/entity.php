<?php


namespace Realweb\Site\Model\Level;

/**
 * Class Entity
 * @package Realweb\Site\Model\Level
 * @method string getUfName()
 */
class Entity extends \Realweb\Site\Data\Entity
{
    protected static function getModel()
    {
        return Model::class;
    }
}
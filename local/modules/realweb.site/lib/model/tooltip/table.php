<?php


namespace Realweb\Site\Model\Tooltip;


use Bitrix\Main,
    Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * Class TooltipTable
 * @package Realweb\Site\Model\Tooltip
 */
class Table extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_hl_tooltip';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('TOOLTIP_ENTITY_ID_FIELD'),
            ),
            'UF_NAME' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('TOOLTIP_ENTITY_UF_NAME_FIELD'),
            ),
            'UF_CODE' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('TOOLTIP_ENTITY_UF_CODE_FIELD'),
            ),
            'UF_DESCRIPTION' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('TOOLTIP_ENTITY_UF_DESCRIPTION_FIELD'),
            ),
            'UF_DETAIL_DESCRIPTION' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('TOOLTIP_ENTITY_UF_DETAIL_DESCRIPTION_FIELD'),
            ),
            'UF_USEFUL' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('TOOLTIP_ENTITY_UF_USEFUL_FIELD'),
            ),
            'UF_USELESS' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('TOOLTIP_ENTITY_UF_USELESS_FIELD'),
            ),
        );
    }
}
<?php


namespace Realweb\Site\Model\Tooltip;

/**
 * Class Entity
 * @package Realweb\Site\Model\Tooltip
 * @method string getUfName()
 * @method string getUfCode()
 * @method string getUfDescription()
 * @method string getUfDetailDescription()
 * @method int getUfUseful()
 * @method int getUfUseless()
 * @method Entity setUfUseful(int $int)
 * @method Entity setUfUseless(int $int)
 */
class Entity extends \Realweb\Site\Data\Entity
{
    protected static function getModel()
    {
        return Model::class;
    }

    public function toHtml()
    {
        return '<div class="d-flex justify-content-end">
        <div class="tooltip-name">' . $this->getUfName() . '</div>
    </div>
    <div class="tooltip-text">
        ' . htmlspecialcharsback($this->getUfDescription()) . '
    </div>
    <div class="tooltip-text tooltip-text_mute">
        ' . htmlspecialcharsback($this->getUfDetailDescription()) . '
    </div>
    <div class="tooltip-info">Центр информации PVCash<i class="fas fa-info-circle"></i></div>
    <div class="tooltip-action">
        Была ли подсказка полезной?
        <div>
            <a target="_self" href="?id=' . $this->getId() . '&action=useful" class="tooltip-action-up js-tooltip-action"><i class="fas fa-thumbs-up"></i></a>
            <a target="_self" href="?id=' . $this->getId() . '&action=useless" class="tooltip-action-down js-tooltip-action"><i class="fas fa-thumbs-down"></i></a>
        </div>
    </div>
    <div class="js-response"></div>';
    }
}
<?php


namespace Realweb\Site\Model\Tooltip;


class Collection extends \Realweb\Site\Data\Collection
{
    private $_code_keys = [];

    protected static function getModel()
    {
        return Model::class;
    }

    /**
     * @param \Realweb\Site\Data\Data|\Realweb\Site\Data\Entity|Entity $obItem
     * @return $this|mixed
     */
    public function addItem($obItem)
    {
        $this->_collection[] = $obItem;
        $this->_keys[] = $obItem->getId();
        $this->_code_keys[] = $obItem->getUfCode();

        return $this;
    }

    public function getByCode($strCode)
    {
        $iCollectionKey = array_search($strCode, $this->_code_keys);
        if ($iCollectionKey !== false) {
            return $this->_collection[$iCollectionKey];
        } else {
            return new Entity();
        }
    }
}
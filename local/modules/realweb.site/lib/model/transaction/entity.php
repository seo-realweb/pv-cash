<?php


namespace Realweb\Site\Model\Transaction;

use Bitrix\Main\Type\DateTime;
use Realweb\Site\ArrayHelper;

/**
 * Class Entity
 * @package Realweb\Site\Model\Transaction
 * @method float getUfSum()
 * @method DateTime getUfCreated()
 * @method int getUfUserId()
 * @method int getUfStatus()
 * @method Entity setUfSum(float $float)
 * @method Entity setUfCreated(DateTime $dateTime)
 * @method Entity setUfUserId(int $int)
 * @method Entity setUfStatus(int $int)
 * @method string getUfParams()
 * @method Entity setUfParams(string $string)
 */
class Entity extends \Realweb\Site\Data\Entity
{
    /**
     * @var \Realweb\Site\Model\Enum\Entity
     */
    private $_status = null;

    /**
     * @var \Realweb\Site\Model\Enum\Collection
     */
    private $_status_collection = null;

    protected static function getModel()
    {
        return Model::class;
    }

    public function setData(array $arData)
    {
        $arStatus = array();
        foreach ($arData as $key => $value) {
            if (stripos($key, 'STATUS_') === 0) {
                $strKey = str_replace('STATUS_', '', $key);
                $arStatus[$strKey] = $value;
                unset($arData[$key]);
            }
        }
        if (!empty($arStatus['ID'])) {
            $this->setStatus(new \Realweb\Site\Model\Enum\Entity($arStatus));
        } else {
            $this->setStatus(new \Realweb\Site\Model\Enum\Entity());
        }

        return parent::setData($arData);
    }

    public function getStatus()
    {
        return $this->_status;
    }

    public function setStatus(\Realweb\Site\Model\Enum\Entity $obStatus)
    {
        $this->_status = $obStatus;

        return $this;
    }

    public function isSuccess()
    {
        return $this->getStatus()->getXmlId() == 'success';
    }

    public function isFail()
    {
        return $this->getStatus()->getXmlId() == 'fail';
    }

    public function isProcess()
    {
        return $this->getStatus()->getXmlId() === 'process';
    }

    public function isNew()
    {
        return $this->getStatus()->getXmlId() === 'new';
    }

    public function isNull()
    {
        return !$this->getStatus()->getXmlId();
    }

    public function save()
    {
        if (!$this->getUfCreated()) {
            $this->setUfCreated(new DateTime());
            //$this->setUfStatus($this->getStatusCollection()->getByXmlId('process')->getId());
        }

        return parent::save();
    }

    public function getStatusCollection()
    {
        if ($this->_status_collection === null) {
            $this->_status_collection =(new \Realweb\Site\Controller\Enum\Index())
                ->setParam('code', 'UF_STATUS')
                ->setParam('entity_id', 'HLBLOCK_9')
                ->actionIndex();
        }

        return $this->_status_collection;
    }

    public function getSum()
    {
        return doubleval($this->getUfSum());
    }

    public function getAbsSum()
    {
        return abs(doubleval($this->getUfSum()));
    }

    public function getSumFormatted()
    {
        return number_format($this->getSum(), 0, '.', '') . '₽';
    }

    public function isPositive()
    {
        return $this->getSum() > 0;
    }

    public function getCreatedFormatted()
    {
        return FormatDate("d F Y\, H:i", $this->getUfCreated()->getTimestamp());
    }

    public function getParams()
    {
        if ($this->getUfParams()) {
            return unserialize($this->getUfParams());
        }
    }

    public function getParam(string $strKey)
    {
        if ($arParams = $this->getParams()) {
            return ArrayHelper::getValue($arParams, $strKey);
        }
    }

    public function setParams(array $arParams)
    {
        $this->setUfParams(serialize($arParams));

        return $this;
    }

    public function setProcessStatus()
    {
        if ($obStatus = Model::getStatus()->getByXmlId('process')) {
            $this->setUfStatus($obStatus->getId());
            return $this->save();
        }
    }

    public function setSuccessStatus()
    {
        if ($obStatus = Model::getStatus()->getByXmlId('success')) {
            $this->setUfStatus($obStatus->getId());
            return $this->save();
        }
    }

    public function setFailStatus()
    {
        if ($obStatus = Model::getStatus()->getByXmlId('fail')) {
            $this->setUfStatus($obStatus->getId());
            return $this->save();
        }
    }
}
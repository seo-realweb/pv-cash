<?php


namespace Realweb\Site\Model\Transaction;


class Collection extends \Realweb\Site\Data\Collection
{
    protected static function getModel()
    {
        return Model::class;
    }

    public function addItem($obItem)
    {
        $this->_collection[] = $obItem;
        $this->_keys[] = $obItem->getId();

        return $this;
    }
}
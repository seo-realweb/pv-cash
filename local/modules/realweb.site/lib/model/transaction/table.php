<?php


namespace Realweb\Site\Model\Transaction;


use Bitrix\Main,
    Bitrix\Main\Localization\Loc;
use Realweb\Site\Data\Orm\Query;

Loc::loadMessages(__FILE__);

/**
 * Class Table
 * @package Realweb\Site\Module\Level
 */
class Table extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_hl_transaction';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('TRANSACTION_ENTITY_ID_FIELD'),
            ),
            'UF_USER_ID' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('TRANSACTION_ENTITY_UF_USER_ID_FIELD'),
            ),
            'UF_SUM' => array(
                'data_type' => 'float',
                'title' => Loc::getMessage('TRANSACTION_ENTITY_UF_SUM_FIELD'),
            ),
            'UF_CREATED' => array(
                'data_type' => 'datetime',
                'title' => Loc::getMessage('TRANSACTION_ENTITY_UF_CREATED_FIELD'),
            ),
            'UF_STATUS' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('TRANSACTION_ENTITY_UF_STATUS_FIELD'),
            ),
            'UF_PARAMS' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('TRANSACTION_ENTITY_UF_PARAMS_FIELD'),
            ),
        );
    }

    public static function query()
    {
        return new Query(static::getEntity());
    }
}

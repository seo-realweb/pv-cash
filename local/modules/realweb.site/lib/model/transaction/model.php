<?php

namespace Realweb\Site\Model\Transaction;

use Bitrix\Main\ORM\Fields\Relations\Reference;
use Bitrix\Main\ORM\Query\Join;
use Realweb\Site\ArrayHelper;

class Model extends \Realweb\Site\Data\Model
{
    public static function getCollection()
    {
        return Collection::class;
    }

    public static function getEntity()
    {
        return Entity::class;
    }

    public static function getOrmEntity()
    {
        return Table::getEntity();
    }

    public static function getTag()
    {
        return Table::getTableName();
    }

    public static function clearCache()
    {
        global $CACHE_MANAGER;
        $CACHE_MANAGER->ClearByTag(static::getTag());
    }

    public static function getQuery($obNav)
    {
        $obQuery = Table::query()
            ->setSelect(self::getFields())
            ->registerRuntimeField(
                'STATUS',
                new Reference(
                    'STATUS',
                    \Realweb\Site\Model\Enum\Table::getEntity(),
                    Join::on('this.UF_STATUS', 'ref.ID')
                )
            )
            ->addSelect('STATUS.*', 'STATUS_')
            ->setOrder(array('UF_CREATED' => 'DESC'));

        if ($obNav !== null) {
            $arFilter = $obNav->getParam('filter');
            if ($arOrder = $obNav->getParam('order')) {
                $obQuery->setOrder($arOrder);
            }
            if (($userId = ArrayHelper::getValue($arFilter, 'user_id')) !== null) {
                $obQuery->whereCustom('UF_USER_ID', $userId);
            }
            if (($status = ArrayHelper::getValue($arFilter, 'status')) !== null) {
                $obQuery->whereCustom('STATUS_XML_ID', $status);
            }
            if (($dataTo = ArrayHelper::getValue($arFilter, 'date_to')) !== null) {
                $obQuery->where('UF_CREATED', '<=', $dataTo);
            }
            if (($sum = ArrayHelper::getValue($arFilter, 'sum')) !== null) {
                if ($sum) {
                    $obQuery->where('UF_SUM', '>', 0);
                } else {
                    $obQuery->where('UF_SUM', '<', 0);
                }
            }
        }

        return $obQuery;
    }

    public static function getStatus()
    {
        return (new \Realweb\Site\Controller\Enum\Index())
            ->setParam('code', 'UF_STATUS')
            ->setParam('entity_id', 'HLBLOCK_9')
            ->actionIndex();
    }
}
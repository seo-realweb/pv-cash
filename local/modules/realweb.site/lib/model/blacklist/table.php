<?php


namespace Realweb\Site\Model\Blacklist;


use Bitrix\Main,
    Bitrix\Main\Localization\Loc;
use Realweb\Site\Data\Orm\Query;

Loc::loadMessages(__FILE__);

/**
 * Class Table
 * @package Realweb\Site\Module\Level
 */
class Table extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_hl_blacklist';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('LEVEL_ENTITY_ID_FIELD'),
            ),
            'UF_IP' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('LEVEL_ENTITY_UF_IP_FIELD'),
            ),
        );
    }

    public static function query()
    {
        return new Query(static::getEntity());
    }
}
<?php


namespace Realweb\Site\Model\Blacklist;

/**
 * Class Entity
 * @package Realweb\Site\Model\Blacklist
 * @method string getUfIp()
 */
class Entity extends \Realweb\Site\Data\Entity
{
    protected static function getModel()
    {
        return Model::class;
    }
}
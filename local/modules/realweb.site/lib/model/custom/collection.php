<?php


namespace Realweb\Site\Model\Custom;

use Realweb\Site\Controller\Order;


class Collection extends \Realweb\Site\Data\Collection
{
    private $_order_keys = [];

    protected static function getModel()
    {
        return Model::class;
    }

    /**
     * @param \Realweb\Site\Data\Data|\Realweb\Site\Data\Entity|Entity $obItem
     * @return $this|mixed
     */
    public function addItem($obItem)
    {
        $this->_collection[] = $obItem;
        $this->_keys[] = $obItem->getId();
        $this->_order_keys[] = $obItem->getUfOrderId();

        return $this;
    }

    public function getOrderKeys()
    {
        return $this->_order_keys;
    }

    public function getByOrder($iOrderId, bool $isActive = true)
    {
        $iCollectionKey = array_search($iOrderId, $this->_order_keys);
        if ($iCollectionKey !== false) {
            /** @var Entity $obItem */
            if ($obItem = $this->_collection[$iCollectionKey]) {
                if ($isActive) {
                    if ($obItem->isActive()) {
                        return $obItem;
                    }
                } else {
                    return $obItem;
                }
            }
        }
    }

    public function getByUserOrder(int $iUserId, int $iOrderId)
    {
        /** @var Entity $obItem */
        foreach ($this->getCollection() as $obItem) {
            if ($obItem->getUfOrderId() == $iOrderId && $obItem->getUfUserId() == $iUserId) {
                return $obItem;
            }
        }
    }

    public function getActiveCount()
    {
        $iCount = 0;
        /** @var Entity $obItem */
        foreach ($this->getCollection() as $obItem) {
            if ($obItem->isActive()) {
                $iCount++;
            }
        }

        return $iCount;
    }

    public function getActive()
    {
        $obCollection = new Collection();
        /** @var Entity $obItem */
        foreach ($this->getCollection() as $obItem) {
            if ($obItem->isActive()) {
                $obCollection->addItem($obItem);
            }
        }

        return $obCollection;
    }

    public function getOrders()
    {
        if ($this->count() > 0) {
            $obController = new Order\Index();
            $obController->setParam('id', $this->getOrderKeys());
            $obCollection = $obController->findCollection();
            /** @var Entity $obItem */
            foreach ($this->getCollection() as $obItem) {
                /** @var \Realweb\Site\Model\Order\Element\Entity $obOrder */
                if ($obOrder = $obCollection->getByKey($obItem->getUfOrderId())) {
                    $obItem->setOrder($obOrder);
                }
            }
        }
    }
}
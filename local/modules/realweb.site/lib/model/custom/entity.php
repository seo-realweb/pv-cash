<?php


namespace Realweb\Site\Model\Custom;

use Realweb\Site\Controller\Order;

/**
 * Class Entity
 * @package Realweb\Site\Model\Custom
 * @method int getUfUserId()
 * @method int getUfOrderId()
 * @method int getUfActive()
 * @method float getUfPrice()
 * @method Entity setUfUserId(int $int)
 * @method Entity setUfOrderId(int $int)
 * @method Entity setUfActive(int $int)
 * @method Entity setUfPrice(float $float)
 */
class Entity extends \Realweb\Site\Data\Entity
{
    /**
     * @var \Realweb\Site\Model\Order\Element\Entity
     */
    private $_order = null;

    protected static function getModel()
    {
        return Model::class;
    }

    public function isActive()
    {
        return intval($this->getUfActive()) == 1;
    }

    public function save()
    {
        $bSave = parent::save();
        \Realweb\Site\Model\Order\Element\Model::clearCache();

        return $bSave;
    }

    public function getOrder()
    {
        if ($this->_order === null) {
            $obController = new Order\Index();
            $obController->setParam('id', $this->getUfOrderId());
            $this->_order = $obController->actionElement();
        }

        return $this->_order;
    }

    public function setOrder(\Realweb\Site\Model\Order\Element\Entity $obOrder)
    {
        $this->_order = $obOrder;

        return $this;
    }
}
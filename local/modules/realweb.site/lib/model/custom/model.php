<?php

namespace Realweb\Site\Model\Custom;


use Realweb\Site\ArrayHelper;

class Model extends \Realweb\Site\Data\Model
{
    public static function getFields()
    {
        return array(
            'ID',
            'UF_USER_ID',
            'UF_ORDER_ID',
            'UF_ACTIVE',
            'UF_PRICE'
        );
    }

    public static function getCollection()
    {
        return Collection::class;
    }

    public static function getEntity()
    {
        return Entity::class;
    }

    public static function getOrmEntity()
    {
        return Table::getEntity();
    }

    public static function getQuery($obNav)
    {
        $obQuery = Table::query()
            ->setSelect(self::getFields());

        if ($obNav !== null) {
            $arFilter = $obNav->getParam('filter');
            if (($userId = ArrayHelper::getValue($arFilter, 'user_id')) !== null) {
                $obQuery->whereCustom('UF_USER_ID', $userId);
            }
            if (($orderId = ArrayHelper::getValue($arFilter, 'order_id')) !== null) {
                $obQuery->whereCustom('UF_ORDER_ID', $orderId);
            }
        }

        return $obQuery;
    }

    public static function getTag()
    {
        return Table::getTableName();
    }

    public static function clearCache()
    {
        global $CACHE_MANAGER;
        $CACHE_MANAGER->ClearByTag(static::getTag());
    }
}
<?php


namespace Realweb\Site\Model\Custom;


use Bitrix\Main,
    Bitrix\Main\Localization\Loc;
use Realweb\Site\Data\Orm\Query;

Loc::loadMessages(__FILE__);

/**
 * Class Table
 * @package Realweb\Site\Module\Level
 */
class Table extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_hl_custom';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('LEVEL_ENTITY_ID_FIELD'),
            ),
            'UF_USER_ID' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('LEVEL_ENTITY_UF_USER_ID_FIELD'),
            ),
            'UF_ORDER_ID' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('LEVEL_ENTITY_UF_ORDER_ID_FIELD'),
            ),
            'UF_ACTIVE' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('LEVEL_ENTITY_UF_ACTIVE_FIELD'),
            ),
            'UF_PRICE' => array(
                'data_type' => 'float',
                'title' => Loc::getMessage('LEVEL_ENTITY_UF_PRICE_FIELD'),
            ),
            'ORDER' => new Main\ORM\Fields\Relations\Reference(
                'ORDER',
                \Realweb\Site\Model\Iblock\Element\Table::class,
                array('=this.UF_ORDER_ID' => 'ref.ID'),
                array('join_type' => 'INNER')
            ),
        );
    }

    public static function query()
    {
        return new Query(static::getEntity());
    }
}
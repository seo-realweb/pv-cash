<?php


namespace Realweb\Site\Model\Complaint;


use Bitrix\Main,
    Bitrix\Main\Localization\Loc;
use Realweb\Site\Data\Orm\Query;

Loc::loadMessages(__FILE__);

/**
 * Class Table
 * @package Realweb\Site\Model\Complaint
 */
class Table extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_hl_complaint';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('COMPLAINT_ENTITY_ID_FIELD'),
            ),
            'UF_USER_ID' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('COMPLAINT_ENTITY_UF_USER_ID_FIELD'),
            ),
            'UF_ORDER_ID' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('COMPLAINT_ENTITY_UF_ORDER_ID_FIELD'),
            ),
            'UF_CREATED' => array(
                'data_type' => 'datetime',
                'title' => Loc::getMessage('COMPLAINT_ENTITY_UF_CREATED_FIELD'),
            ),
            'UF_COMMENT' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('COMPLAINT_ENTITY_UF_COMMENT_FIELD'),
            ),
            'UF_CAUSE' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('COMPLAINT_ENTITY_UF_CAUSE_FIELD'),
            ),
        );
    }

    public static function query()
    {
        return new Query(static::getEntity());
    }
}
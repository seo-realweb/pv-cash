<?php


namespace Realweb\Site\Model\Complaint;

use Bitrix\Main\Type\DateTime;

/**
 * Class Entity
 * @package Realweb\Site\Model\Complaint
 * @method Entity setUfUserId(int $int)
 * @method Entity setUfOrderId(int $int)
 * @method Entity setUfCause(int $int)
 * @method Entity setUfComment(string $string)
 * @method Entity setUfCreated(DateTime $dateTime)
 * @method DateTime getUfCreated()
 * @method DateTime getUfOrderId()
 */
class Entity extends \Realweb\Site\Data\Entity
{
    protected static function getModel()
    {
        return Model::class;
    }

    public function save()
    {
        if (!$this->getUfCreated()) {
            $this->setUfCreated(new DateTime());
        }
        return parent::save();
    }
}
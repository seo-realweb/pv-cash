<?php


namespace Realweb\Site\Model\Server;

/**
 * Class Entity
 * @package Realweb\Site\Model\Server
 * @method string getUfName()
 */
class Entity extends \Realweb\Site\Data\Entity
{
    protected static function getModel()
    {
        return Model::class;
    }
}
<?php

namespace Realweb\Site\Model\Iblock\Element;

use Realweb\Site\ArrayHelper;

abstract class Model extends \Realweb\Site\Data\Model
{
    public static abstract function getIblockId();

    public static function getFields()
    {
        return array(
            'ID',
            'IBLOCK_ID',
            'IBLOCK_SECTION_ID',
            'NAME',
            'SORT',
            'ACTIVE',
            'PREVIEW_TEXT',
            'CODE'
        );
    }

    public static function getCollection()
    {
        return Collection::class;
    }

    public static function getEntity()
    {
        return Entity::class;
    }

    public static function getOrmEntity()
    {
        return Table::getEntity();
    }

    public static function getTag()
    {
        return 'iblock_id_' . static::getIblockId();
    }

    public static function clearCache()
    {
        global $CACHE_MANAGER;
        $CACHE_MANAGER->ClearByTag(static::getTag());
    }

    public static function getQuery($obNav)
    {
        $obQuery = Table::query()
            ->setSelect(static::getFields())
            ->setOrder(array('SORT' => 'ASC'));

        if ($obNav !== null) {
            $arFilter = $obNav->getParam('filter');
            if (($iblockSectionId = ArrayHelper::getValue($arFilter, 'iblock_section_id')) !== null) {
                $obQuery->whereCustom('IBLOCK_SECTION_ID', $iblockSectionId);
            }
        }

        return $obQuery;
    }
}
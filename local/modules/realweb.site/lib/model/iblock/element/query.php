<?php

namespace Realweb\Site\Model\Iblock\Element;

use Bitrix\Iblock\PropertyTable;
use Bitrix\Main\DB\SqlExpression;
use Bitrix\Main\Entity\ReferenceField;
use Bitrix\Main\ORM\Fields\ExpressionField;
use Bitrix\Main\ORM\Query\Join;
use Realweb\Site\Main\ArrayHelper;

/**
 * Class Element
 * @package Realweb\Site\Data\Orm\Query
 */
class Query extends \Realweb\Site\Data\Orm\Query
{
    /**
     * @param array $select
     * @return \Realweb\Site\Data\Orm\Query|Query
     */
    public function setSelect(array $select)
    {
        return parent::setSelect($select);
    }

    /**
     * @param string|null $name
     * @param null $fieldInfo
     * @return \Realweb\Site\Data\Orm\Query|Query
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    public function registerRuntimeField($name, $fieldInfo = null)
    {
        return parent::registerRuntimeField($name, $fieldInfo);
    }

    /**
     * @param mixed $order
     * @return \Realweb\Site\Data\Orm\Query|Query
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    public function setOrder($order)
    {
        return parent::setOrder($order);
    }


    /**
     * @param $strName
     * @param $iBlockId
     * @return $this
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    public function registerDropdownField($strName, $iBlockId)
    {
        $obTable = \Realweb\Site\Model\Iblock\Element\Property\Single\Table::getInstance($iBlockId);
        $obEntity = $obTable->getOrmEntity();
        if ($obTable->isFieldExist($strName)) {
            $strFieldName = $strName . '_PROPERTY';
            $strFieldNameEnum = $strName . '_ENUM';
            $arField = $obTable->getField($strName);
            $this->registerRuntimeField(
                $strFieldName, new ReferenceField(
                    $strFieldName, $obEntity, \Bitrix\Main\Entity\Query\Join::on('this.ID', 'ref.IBLOCK_ELEMENT_ID')
                )
            )->registerRuntimeField(
                $strFieldNameEnum, new ReferenceField(
                    $strFieldNameEnum, \Bitrix\Iblock\PropertyEnumerationTable::class, \Bitrix\Main\Entity\Query\Join::on('this.' . $strName, 'ref.ID')
                )
            );
            $this->addSelect($strFieldName . '.' . $arField['COLUMN_NAME'], $strName)
                ->addSelect($strFieldNameEnum . '.*', $strName . '_');
        }

        return $this;
    }

    /**
     * @param $strName
     * @param $iBlockId
     * @return $this
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    public function registerMultiDropdownField($strName, $iBlockId)
    {
        $obTable = \Realweb\Site\Model\Iblock\Element\Property\Single\Table::getInstance($iBlockId);
        $obEntity = $obTable->getOrmEntity();
        $strFieldName = $strName . '_PROPERTY';
        $strFieldNameEnum = $strName . '_ENUM';
        $this->registerRuntimeField(
            $strFieldName,
            new \Bitrix\Main\Entity\ReferenceField(
                $strFieldName,
                $obEntity,
                \Bitrix\Main\Entity\Query\Join::on('this.ID', 'ref.IBLOCK_ELEMENT_ID')
                    ->whereIn('ref.IBLOCK_PROPERTY_ID', PropertyTable::query()
                        ->setSelect(array('ID'))
                        ->where('IBLOCK_ID', '=', $iBlockId)
                        ->where('CODE', '=', $strName)
                    )
            )
        )
            ->registerRuntimeField(
                $strFieldNameEnum,
                new \Bitrix\Main\Entity\ReferenceField(
                    $strFieldNameEnum,
                    \Bitrix\Iblock\PropertyEnumerationTable::class,
                    \Bitrix\Main\Entity\Query\Join::on('this.' . $strName, 'ref.ID')
                )
            );

        $this->addSelect($strFieldName . '.VALUE', $strName);
        $this->addSelect($strFieldNameEnum . '.*', $strName . '_');

        return $this;
    }


    /**
     * @return $this
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    public function registerPreviewPicture()
    {
        $this->registerFileField('PREVIEW_PICTURE');

        return $this;
    }

    /**
     * @return $this
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    public function registerDetailPicture()
    {
        $this->registerFileField('DETAIL_PICTURE');

        return $this;
    }


    /**
     * @param array $arFields
     * @param $iBlockId
     * @param bool $withDescription
     * @return $this
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    public function registerFields(array $arFields, $iBlockId, bool $withDescription = false)
    {
        $strName = 'S_PROPS';
        $obTable = \Realweb\Site\Model\Iblock\Element\Property\Single\Table::getInstance($iBlockId);
        $obEntity = $obTable->getOrmEntity();
        $this->registerRuntimeField(
            $strName, new ReferenceField(
                $strName, $obEntity, \Bitrix\Main\Entity\Query\Join::on('this.ID', 'ref.IBLOCK_ELEMENT_ID')
            )
        );
        foreach ($arFields as $strField) {
            if ($obTable->isFieldExist($strField)) {
                $arField = $obTable->getField($strField);
                $this->addSelect($strName . '.' . $arField['COLUMN_NAME'], $strField);
                if ($withDescription && ArrayHelper::getValue($arField, 'WITH_DESCRIPTION', 'N') === 'Y') {
                    $this->addSelect($strName . '.' . 'DESCRIPTION_' . $arField['ID'], $strField . '_DESCRIPTION');
                }
            }
        }

        return $this;
    }


    /**
     *
     * @return $this
     */
    public function registerMultiParentSectionField()
    {
        $strSectionField = "SECTION";
        $strSectionElementField = "SECTION_ELEMENT";
        $this
            ->registerRuntimeField(
                $strSectionElementField, new ReferenceField(
                    $strSectionElementField, \Realweb\Api\Model\Iblock\Table\SectionElementTable::class, \Bitrix\Main\Entity\Query\Join::on('this.ID', 'ref.IBLOCK_ELEMENT_ID')
                )
            )
            ->registerRuntimeField(
                $strSectionField, new ReferenceField(
                    $strSectionField, \Realweb\Api\Model\Iblock\Table\SectionTable::class, \Bitrix\Main\Entity\Query\Join::on('this.SECTION_ID', 'ref.ID')
                )
            );

        $this
            ->addSelect('SECTION_ELEMENT.IBLOCK_SECTION_ID', 'SECTION_ID')
            ->addSelect('SECTION.NAME', 'SECTION_NAME')
            ->addSelect('SECTION.CODE', 'SECTION_CODE')
            ->addSelect('SECTION.SORT', 'SECTION_SORT');

        return $this;
    }

    /**
     *
     * @param string $strName
     * @param int $iBlockId
     * @return $this
     */
    public function registerMultiSectionLinkField($strName, $iBlockId)
    {
        $this->registerMultiField($strName, $iBlockId);
        $strSectionField = $strName . '_IBLOCK_SECTION';

        $this->registerRuntimeField(
            $strSectionField, new ReferenceField(
                $strSectionField, \Realweb\Api\Model\Iblock\SectionTable::class, \Bitrix\Main\Entity\Query\Join::on('this.' . $strName, 'ref.ID')
            )
        );

        $this
            ->addSelect($strSectionField . '.NAME', $strSectionField . "_NAME")
            ->addSelect($strSectionField . '.CODE', $strSectionField . "_CODE")
            ->addSelect($strSectionField . '.SORT', $strSectionField . "_SORT");

        return $this;
    }


    /**
     * @param $strName
     * @param $iBlockId
     * @param array $arProps
     * @param null $iBlockIdElement
     * @return $this
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    public function registerElementLinkField($strName, $iBlockId)
    {
        $obTable = \Realweb\Api\Model\Iblock\Table\Property\SingleTable::getInstance($iBlockId);
        $obEntity = $obTable->getOrmEntity();
        if ($obTable->isFieldExist($strName)) {
            $strFieldName = $strName . '_PROPERTY';
            $strElementField = $strName . '_ELEMENT';
            $arField = $obTable->getField($strName);
            $this->registerRuntimeField(
                $strFieldName,
                new ReferenceField(
                    $strFieldName,
                    $obEntity,
                    Join::on('this.ID', 'ref.IBLOCK_ELEMENT_ID')
                )
            );
            $this->registerRuntimeField(
                $strElementField,
                new ReferenceField(
                    $strElementField,
                    \Realweb\Api\Model\Iblock\Table\ElementTable::class,
                    Join::on('this.' . $strFieldName . '.' . $arField['COLUMN_NAME'], 'ref.ID')
                )
            );
            $this->addSelect($strFieldName . '.' . $arField['COLUMN_NAME'], $strName);
            $this->addSelect($strElementField . '.NAME', $strName . '_NAME');
            $this->addSelect($strElementField . '.CODE', $strName . '_CODE');
        }

        return $this;
    }

    /**
     * @param $strName
     * @param $iBlockId
     * @param array $arProps
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    public function registerElementLinkPropertyFields($strName, $iBlockId, $arProps = array())
    {
        $obTableElement = Table::getInstance($iBlockId);
        $strFieldPropName = $strName . '_ELEMENT_PROPERTY';
        $this->registerRuntimeField(
            $strFieldPropName,
            new ReferenceField(
                $strFieldPropName,
                $obTableElement->getOrmEntity(),
                \Bitrix\Main\Entity\Query\Join::on('this.' . $strName, 'ref.IBLOCK_ELEMENT_ID')
            )
        );
        foreach ($arProps as $strCode) {
            if ($arField = $obTableElement->getField($strCode)) {
                $this->addSelect($strFieldPropName . '.' . $arField['COLUMN_NAME'], $strName . '_' . $strCode);
            }
        }

        return $this;
    }

    /**
     * @param string $strName
     * @param string $strPropName
     * @param int $iBlockId
     * @return $this
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    public function registerCountField(string $strName, string $strPropName, int $iBlockId)
    {
        $obTable = Table::getInstance($iBlockId);
        $obEntity = $obTable->getOrmEntity();
        if ($obTable->isFieldExist($strPropName)) {
            $arField = $obTable->getField($strPropName);
            $strFieldName = $strPropName . 'PROPERTY';
            $strColumnName = $arField['COLUMN_NAME'];
            $obSubQuery = ElementTable::query()
                ->setSelect(array('COUNT_ELEMENTS'))
                ->registerRuntimeField($strFieldName,
                    (new ReferenceField(
                        $strFieldName,
                        $obEntity,
                        Join::on('this.ID', 'ref.IBLOCK_ELEMENT_ID')
                    ))->configureJoinType("inner")
                )
                ->where($strFieldName . '.' . $strColumnName, '=', new SqlExpression('%s'))
                ->where('ACTIVE', '=', 'Y')
                ->registerRuntimeField(
                    'COUNT_ELEMENTS',
                    new ExpressionField('COUNT_ELEMENTS', 'COUNT(%s)', array('ID'))
                )
                ->setTableAliasPostfix('_count');
            $this->registerRuntimeField(
                $strName,
                new ExpressionField($strName, '(' . $obSubQuery->getQuery() . ')', array('ID'))
            );
            $this->addSelect($strName);
        }

        return $this;
    }


    /**
     *
     * @param string $strName
     * @param int $iBlockId
     * @return $this
     */
    public function registerField($strName, $iBlockId)
    {
        $obTable = \Realweb\Api\Model\Iblock\Table\Property\SingleTable::getInstance($iBlockId);
        $obEntity = $obTable->getOrmEntity();
        if ($obTable->isFieldExist($strName)) {
            $strFieldName = $strName . '_PROPERTY';
            $arField = $obTable->getField($strName);
            $this->registerRuntimeField($strFieldName, new ReferenceField(
                    $strFieldName, $obEntity, \Bitrix\Main\Entity\Query\Join::on('this.ID', 'ref.IBLOCK_ELEMENT_ID'))
            );
            $this->addSelect($strFieldName . '.' . $arField['COLUMN_NAME'], $strName);
        }
        return $this;
    }


    /**
     *
     * @param string $strName
     * @param int $iBlockId
     * @return $this
     */
    public function registerMultiField($strName, $iBlockId)
    {
        $obTable = \Realweb\Api\Model\Iblock\Table\Property\MultiTable::getInstance($iBlockId);
        $obEntity = $obTable->getOrmEntity();

        if ($obTable->isFieldExist($strName)) {
            $strFieldName = $strName . '_PROPERTY';
            $arField = $obTable->getField($strName);

            $this
                ->registerRuntimeField($strFieldName, new ReferenceField(
                        $strFieldName, $obEntity, \Bitrix\Main\Entity\Query\Join::on('this.ID', 'ref.IBLOCK_ELEMENT_ID')->where('ref.IBLOCK_PROPERTY_ID', $arField['ID']))
                );
            $this
                ->addSelect($strFieldName . '.ID', $strName . '_ID')
                ->addSelect($strFieldName . '.VALUE', $strName)
                ->addSelect($strFieldName . '.DESCRIPTION', $strName . '_DESCRIPTION');
        }
        return $this;
    }

    /**
     * @param $arFields
     * @param $iBlockId
     * @return $this
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    public function registerMultiFields($arFields, $iBlockId, bool $withDescription = false)
    {
        $obTable = \Realweb\Api\Model\Iblock\Table\Property\MultiTable::getInstance($iBlockId);
        $obEntity = $obTable->getOrmEntity();
        foreach ($arFields as $strName) {
            if ($obTable->isFieldExist($strName)) {
                $arField = $obTable->getField($strName);
                if (!is_null($arField)) {
                    $strFieldName = $strName . '_PROPERTY';
                    $this->registerRuntimeField(
                        $strFieldName,
                        new ReferenceField(
                            $strFieldName,
                            $obEntity,
                            Join::on('this.ID', 'ref.IBLOCK_ELEMENT_ID')->where('ref.IBLOCK_PROPERTY_ID', $arField['ID'])
                        )
                    );
                    $this->addSelect($strFieldName . '.VALUE', $strName);
                    if ($withDescription && \Realweb\Api\Model\Utils\ArrayHelper::getValue($arField, 'WITH_DESCRIPTION', 'N') === 'Y') {
                        $this->addSelect($strFieldName . '.' . 'DESCRIPTION', $strName . '_DESCRIPTION');
                    }
                }
            }
        }

        return $this;
    }

    /**
     *
     * @param string $strName
     * @param int $iBlockId deprecated
     * @return $this
     */
    public function registerPriceField($strName, $iBlockId = null)
    {
        \Bitrix\Main\Loader::includeModule('catalog');
        $this->registerRuntimeField($strName, new ReferenceField(
                $strName, \Bitrix\Catalog\PriceTable::class, \Bitrix\Main\Entity\Query\Join::on('this.ID', 'ref.PRODUCT_ID'))
        );
        $arSelect = $this->getSelect();
        $arSelect[$strName . "_PRICE"] = $strName . '.PRICE';

        $this->setSelect($arSelect);
        return $this;
    }

    public function registerSectionElementCodePath()
    {

        $strSectionField = 'PARENT_SECTION';
        $this->registerRuntimeField(
            $strSectionField, new ReferenceField(
                $strSectionField, \Realweb\Api\Model\Iblock\Table\SectionTable::class, Join::on('this.IBLOCK_SECTION_ID', 'ref.ID')
            )
        );

        $obSubQuery = \Realweb\Api\Model\Iblock\Table\SectionTable::query()
            ->setSelect(array('PARENT_SECTION_CODE_PATH'))
            ->where('LEFT_MARGIN', "<=", new SqlExpression('%s'))
            ->where('RIGHT_MARGIN', ">=", new SqlExpression('%s'))
            ->where('IBLOCK_ID', "=", new SqlExpression('%s'))
            ->registerRuntimeField(
                'PARENT_SECTION_CODE_PATH', new \Bitrix\Main\Entity\ExpressionField(
                    'PARENT_SECTION_CODE_PATH', 'GROUP_CONCAT(CODE ORDER BY LEFT_MARGIN ASC SEPARATOR \'/\')'
                )
            )
            ->setTableAliasPostfix('_groups');
        $this->registerRuntimeField(
            'SECTION_ELEMENT_CODE_PATH', new \Bitrix\Main\Entity\ExpressionField(
                'SECTION_ELEMENT_CODE_PATH', 'CONCAT_WS(\'/\',(' . $obSubQuery->getQuery() . '),realweb_api_model_iblock_table_element.CODE)', array(
                    $strSectionField . '.' . 'LEFT_MARGIN',
                    $strSectionField . '.' . 'RIGHT_MARGIN',
                    $strSectionField . '.' . 'IBLOCK_ID'
                )
            )
        )
            ->addSelect('SECTION_ELEMENT_CODE_PATH');
        return $this;
    }


}
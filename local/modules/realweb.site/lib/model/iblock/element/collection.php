<?php


namespace Realweb\Site\Model\Iblock\Element;


use Bitrix\Iblock\PropertyTable;
use Bitrix\Main\ORM\Fields\Relations\Reference;
use Bitrix\Main\ORM\Query\Join;
use Realweb\Site\Main\Helper;

class Collection extends \Realweb\Site\Data\Collection
{
    /**
     * @return \Realweb\Site\Data\Model|string|Model
     */
    protected static function getModel()
    {
        return Model::class;
    }

    public function addItem($obItem)
    {
        if (!($obExistItem = $this->getByKey($obItem->getId()))) {
            $this->_collection[] = $obItem;
            $this->_keys[] = $obItem->getId();
        }

        return $this;
    }

    public function getMultiPropertyByModel($model, $strProperty, $method = null)
    {
        $iIblockId = static::getModel()::getIblockId();
        $strReference = $strProperty . "_ELEMENT";
        $strField = $strReference . "_ID";
        $obSubQuery = PropertyTable::query()
            ->setSelect(array('ID'))
            ->where('IBLOCK_ID', '=', $iIblockId)
            ->where('CODE', '=', $strProperty);
        /** @var \Realweb\Site\Data\Model $model */
        $arItems = $model::getQuery(null)
            ->registerRuntimeField(
                $strReference,
                (new Reference(
                    $strReference,
                    \Realweb\Site\Model\Iblock\Element\Property\Multi\Table::getInstance($iIblockId)->getOrmEntity(),
                    Join::on('ref.VALUE', 'this.ID')
                        ->whereIn('ref.IBLOCK_PROPERTY_ID', $obSubQuery)
                ))->configureJoinType('INNER')
            )
            ->addSelect($strReference . '.IBLOCK_ELEMENT_ID', $strField)
            ->whereIn($strField, $this->getKeys())
            ->exec()
            ->fetchAll();

        $collection = $model::getCollection();
        $entity = $model::getEntity();
        /** @var Collection $obCollection */
        $obCollection = new $collection;
        foreach ($arItems as $arItem) {
            $obCollection->addItem(new $entity($arItem));
        }
        if ($method) {
            foreach ($arItems as $arItem) {
                if ($obElement = $obCollection->getByKey($arItem['ID'])) {
                    if ($obItem = $this->getByKey($arItem[$strField])) {
                        if (method_exists($obItem, $method)) {
                            $obItem->$method($obElement);
                        }
                    }
                }
            }
        }

        return $arItems;
    }
}
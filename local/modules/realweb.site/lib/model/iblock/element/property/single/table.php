<?php

namespace Realweb\Site\Model\Iblock\Element\Property\Single;

use Bitrix\Main\Entity\Base;


/**
 * Class Table
 * @package Realweb\Site\Model\Iblock\Element\Property\Single
 */
class Table
{
    /**
     * @var Table[]
     */
    private static $instance = array();
    private $_iblock_id = null;
    private $arFields = array();

    /**
     *
     * @var \Bitrix\Main\ORM\Entity
     */
    private $_orm_entity = null;

    protected function __construct($iBlockId)
    {
        $this->_iblock_id = $iBlockId;
        $entityName = $this->getClassName();
        $tableName = 'b_iblock_element_prop_s' . $this->_iblock_id;
        $arFields = array(
            'IBLOCK_ELEMENT_ID' => new \Bitrix\Main\Entity\IntegerField('IBLOCK_ELEMENT_ID')
        );
        $rsProps = \Bitrix\Iblock\PropertyTable::getList(array(
            'select' => array('ID', "NAME", 'CODE', 'MULTIPLE', 'LINK_IBLOCK_ID', 'WITH_DESCRIPTION'),
            'filter' => array(
                '=IBLOCK_ID' => $iBlockId,
                'MULTIPLE' => 'N'
            )
        ));
        while ($arProp = $rsProps->fetch()) {
            $strKey = $this->_getFieldKey($arProp['ID']);
            $arFields[$strKey] = new \Bitrix\Main\Entity\StringField($strKey);
            if ($arProp['WITH_DESCRIPTION'] == 'Y') {
                $strDescriptionKey = $this->_getDescriptionFieldKey($arProp['ID']);
                $arFields[$strDescriptionKey] = new \Bitrix\Main\Entity\StringField($strDescriptionKey);
            }
            $arProp['COLUMN_NAME'] = $strKey;
            $this->arFields[$arProp['CODE']] = $arProp;
        }

        $this->_orm_entity = Base::compileEntity(
            $entityName, $arFields, ['table_name' => $tableName, 'namespace' => __NAMESPACE__]
        );
    }

    /**
     *
     * @param int $iBlockId
     * @return Table
     */
    public static function getInstance($iBlockId)
    {
        if (is_null(self::$instance[$iBlockId])) {
            self::$instance[$iBlockId] = new self($iBlockId);
        }
        return self::$instance[$iBlockId];
    }

    /**
     *
     * @return \Bitrix\Main\ORM\Entity
     */
    public function getOrmEntity()
    {
        return $this->_orm_entity;
    }

    public function getClassName()
    {
        return 'OrmProperty' . $this->_iblock_id;
    }

    private function _getFieldKey($strFieldName)
    {
        return "PROPERTY_" . $strFieldName;
    }

    private function _getDescriptionFieldKey($strFieldName)
    {
        return "DESCRIPTION_" . $strFieldName;
    }

    public function isFieldExist($strFieldName)
    {

        if (isset($this->arFields[$strFieldName])) {
            return true;
        }
        return false;
    }

    public function getField($strFieldName)
    {
        if ($this->isFieldExist($strFieldName)) {
            return $this->arFields[$strFieldName];
        }
        return false;
    }

    public function getFields()
    {
        return $this->arFields;
    }

    /**
     * @return \Realweb\Site\Data\Orm\Query
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    public function getQuery()
    {
        return new \Realweb\Site\Data\Orm\Query($this->getOrmEntity());
    }

}

<?php

namespace Realweb\Site\Model\Iblock\Element\Property\Multi;

use Bitrix\Main\Entity;
use Realweb\Site\ArrayHelper;
use Realweb\Site\Data\Orm\Query;

/**
 * Class Table
 * @package Realweb\Site\Model\Iblock\Element\Property\Multi
 */
class Table
{
    /**
     * @var Table[]
     */
    private static $instance = array();
    private $iBlockId;
    private $arFields = array();

    /**
     *
     * @var \Bitrix\Main\ORM\Entity
     */
    private $ormEntity;

    protected function __construct($iBlockId)
    {
        $this->iBlockId = $iBlockId;
        $entityName = $this->getClassName();
        $tableName = 'b_iblock_element_prop_m' . $this->iBlockId;
        $arFields = array(
            'ID' => new Entity\IntegerField('ID'),
            'IBLOCK_ELEMENT_ID' => new Entity\IntegerField('IBLOCK_ELEMENT_ID'),
            'IBLOCK_PROPERTY_ID' => new Entity\IntegerField('IBLOCK_PROPERTY_ID'),
            'VALUE' => new Entity\StringField('VALUE'),
            'VALUE_ENUM' => new Entity\IntegerField('VALUE_ENUM'),
            'DESCRIPTION' => new Entity\StringField('DESCRIPTION'),
        );

        $this->ormEntity = Entity\Base::compileEntity(
            $entityName, $arFields, ['table_name' => $tableName, 'namespace' => __NAMESPACE__]
        );
    }

    /**
     * @param $iBlockId
     * @return Table
     */
    public static function getInstance($iBlockId)
    {
        if (is_null(self::$instance[$iBlockId])) {
            self::$instance[$iBlockId] = new self($iBlockId);
        }
        return self::$instance[$iBlockId];
    }

    /**
     * @return \Bitrix\Main\ORM\Entity
     */
    public function getOrmEntity()
    {
        return $this->ormEntity;
    }

    public function getClassName()
    {
        return 'OrmMultiProperty' . $this->iBlockId;
    }

    /**
     * @param $strFieldName
     * @return bool
     */
    public function isFieldExist($strFieldName)
    {
        $arField = $this->getField($strFieldName);
        if (is_array($arField) && intval($arField['ID']) > 0) {
            return true;
        }
        return false;
    }

    public function getField($strFieldName)
    {
        $arFields = $this->getFields();

        return ArrayHelper::getValue($arFields, $strFieldName);
    }

    public function getFields()
    {
        if (!$this->arFields) {
            $obSelect = \Bitrix\Iblock\PropertyTable::query()
                ->setSelect(array(
                    'ID', 'CODE', 'WITH_DESCRIPTION'
                ))
                ->where('MULTIPLE', '=', 'Y')
                ->where("IBLOCK_ID", "=", $this->iBlockId);
            $rsResult = $obSelect->exec();
            $arItems = $rsResult->fetchAll();
            foreach ($arItems as $arItem) {
                $arItem['COLUMN_NAME'] = 'PROPERTY_' . $arItem['ID'];
                $this->arFields[$arItem['CODE']] = $arItem;
            }
        }

        return $this->arFields;
    }

    /**
     * @return Query
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    public function getQuery()
    {
        return new Query($this->getOrmEntity());
    }
}

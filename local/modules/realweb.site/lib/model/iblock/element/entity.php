<?php


namespace Realweb\Site\Model\Iblock\Element;

use Bitrix\Iblock\PropertyTable;
use Bitrix\Main\ORM\Fields\Relations\Reference;
use Bitrix\Main\ORM\Query\Join;

/**
 * Class Entity
 * @package Realweb\Site\Model\Iblock\Element
 * @method string getName()
 * @method string getCode()
 * @method int getIblockSectionId()
 * @method string getPreviewText()
 * @method string getActive()
 * @method int getSort()
 * @method Entity setActive(string $string)
 * @method Entity setSort(int $int)
 * @method Entity setName(string $string)
 * @method Entity setPreviewText(string $string)
 * @method int getIblockId()
 */
abstract class Entity extends \Realweb\Site\Data\Entity
{
    /**
     * @return Model
     */
    abstract protected static function getModel();

    /**
     * @var bool
     */
    protected $_refresh_data = true;

    /**
     * @var \CIBlockElement
     */
    protected $_ciblock_element = null;

    /**
     * @return bool
     */
    public function isRefreshData(): bool
    {
        return $this->_refresh_data;
    }

    /**
     * @param bool $refresh_data
     * @return $this
     */
    public function setRefreshData(bool $refresh_data = true)
    {
        $this->_refresh_data = $refresh_data;

        return $this;
    }

    /**
     * @return \CIBlockElement
     */
    public function getCIblockElement(): \CIBlockElement
    {
        if ($this->_ciblock_element === null) {
            $this->_ciblock_element = new \CIBlockElement();
        }

        return $this->_ciblock_element;
    }

    public function save()
    {
        $bResult = false;
        $arData = array();
        foreach (static::getModel()::getFields() as $strField) {
            $arData[$strField] = $this->_data[$strField];
        }
        $arProperties = array();
        if (method_exists(static::getModel(), 'getProps')) {
            foreach (static::getModel()::getProps() as $strProperty) {
                $arProperties[$strProperty] = $this->_data[$strProperty];
            }
        }
        unset($arData['ID']);
        $arData['IBLOCK_ID'] = static::getModel()::getIblockId();
        if ($this->isExist()) {
            if (isset($arData['PREVIEW_PICTURE']) && is_numeric($arData['PREVIEW_PICTURE'])) {
                unset($arData['PREVIEW_PICTURE']);
            }
            if (isset($arData['DETAIL_PICTURE']) && is_numeric($arData['DETAIL_PICTURE'])) {
                unset($arData['DETAIL_PICTURE']);
            }
            $bResult = $this->getCIblockElement()->Update($this->getPrimary(), $arData);
        } else {
            if ($iId = $this->getCIblockElement()->Add($arData)) {
                $this->setId($iId);
                $this->setPrimary($iId);
                $this->setData($arData);
                $bResult = true;
            }
        }
        if ($this->getPrimary() > 0) {
            \CIBlockElement::SetPropertyValuesEx($this->getPrimary(), static::getModel()::getIblockId(), $arProperties);
        }
        if ($this->isRefreshData()) {
            $this->_data = null;
            $this->getData();
            global $CACHE_MANAGER;
            $CACHE_MANAGER->ClearByTag('iblock_id_' . static::getModel()::getIblockId());
        }

        return $bResult;
    }

    public function getMultiPropertyByModel($model, $strProperty)
    {
        $iIblockId = $this->getIblockId();
        $strReference = $strProperty . "_ELEMENT";
        $strField = $strReference . "_ID";
        $obSubQuery = PropertyTable::query()
            ->setSelect(array('ID'))
            ->where('IBLOCK_ID', '=', $iIblockId)
            ->where('CODE', '=', $strProperty);
        /** @var \Realweb\Site\Data\Model $model */
        $arItems = $model::getQuery(null)
            ->registerRuntimeField(
                $strReference,
                (new Reference(
                    $strReference,
                    \Realweb\Site\Model\Iblock\Element\Property\Multi\Table::getInstance($iIblockId)->getOrmEntity(),
                    Join::on('ref.VALUE', 'this.ID')
                        ->whereIn('ref.IBLOCK_PROPERTY_ID', $obSubQuery)
                ))->configureJoinType('INNER')
            )
            ->addSelect($strReference . '.IBLOCK_ELEMENT_ID', $strField)
            ->whereIn($strField, $this->getId())
            ->exec()
            ->fetchAll();
        $collection = $model::getCollection();
        $entity = $model::getEntity();
        /** @var Collection $obCollection */
        $obCollection = new $collection;
        foreach ($arItems as $arItem) {
            $obCollection->addItem(new $entity($arItem));
        }

        return $obCollection;
    }
}
<?php

namespace Realweb\Site\Model\Iblock\Element;

use Bitrix\Iblock\ElementTable;

class Table extends ElementTable
{
    public static function query()
    {
        return new Query(static::getEntity());
    }
}
<?php


namespace Realweb\Site\Model\Iblock\Section;


use Realweb\Site\Data\PageNavigation;
use Realweb\Site\Model\Iblock\Element;

/**
 * Class Entity
 * @package Realweb\Site\Model\Iblock\Section
 * @method string getName()
 * @method string getCode()
 */
abstract class Entity extends \Realweb\Site\Data\Entity
{
    /**
     * @return Model
     */
    abstract protected static function getModel();

    /**
     * @var Element\Collection|null
     */
    private $_elements = null;

    public function getElements()
    {
        if ($this->_elements === null && static::getModel()::getElementModel() !== null) {
            $obNav = new PageNavigation();
            $obNav->setParam('filter', array(
                'iblock_section_id' => $this->getId()
            ));
            $this->_elements = static::getModel()::getElementModel()::getObjectCollection($obNav);
        }

        return $this->_elements;
    }

    public function setElement(Element\Entity $obItem)
    {
        if ($this->_elements === null) {
            $collection = static::getModel()::getElementModel()::getCollection();
            $this->_elements = new $collection();
        }
        $this->_elements->addItem($obItem);

        return $this;
    }

    public function hasElements()
    {
        return $this->_elements !== null;
    }
}
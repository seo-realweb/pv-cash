<?php


namespace Realweb\Site\Model\Iblock\Section;


use Realweb\Site\Data\PageNavigation;
use Realweb\Site\Model\Iblock\Element;

class Collection extends \Realweb\Site\Data\Collection
{
    /**
     * @return \Realweb\Site\Data\Model|string|Model
     */
    protected static function getModel()
    {
        return Model::class;
    }

    public function addItem($obItem)
    {
        $this->_collection[] = $obItem;
        $this->_keys[] = $obItem->getId();

        return $this;
    }

    public function getElements()
    {
        if ($this->count() > 0 && static::getModel()::getElementModel()) {
            $obNav = new PageNavigation();
            $obNav->setAllRecords();
            $obNav->setParam('filter', array(
                'iblock_section_id' => $this->getKeys()
            ));
            $obCollection = static::getModel()::getElementModel()::getObjectCollection($obNav);
            /** @var Element\Entity $obItem */
            foreach ($obCollection->getCollection() as $obItem) {
                /** @var Entity $obSection */
                if ($obSection = $this->getByKey($obItem->getIblockSectionId())) {
                    $obSection->setElement($obItem);
                }
            }
        }
    }
}
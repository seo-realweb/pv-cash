<?php

namespace Realweb\Site\Model\Iblock\Section;

use Realweb\Site\Model\Iblock\Element;

abstract class Model extends \Realweb\Site\Data\Model
{
    public static abstract function getIblockId();

    /**
     * @return Element\Model|null
     */
    public static function getElementModel()
    {
        return null;
    }

    public static function getFields()
    {
        return array(
            'ID',
            'IBLOCK_ID',
            'IBLOCK_SECTION_ID',
            'NAME',
            'ACTIVE',
            'SORT',
            'CODE'
        );
    }

    public static function getCollection()
    {
        return Collection::class;
    }

    public static function getEntity()
    {
        return Entity::class;
    }

    public static function getOrmEntity()
    {
        return Table::getEntity();
    }

    public static function getTag()
    {
        return 'iblock_id_' . static::getIblockId();
    }

    public static function clearCache()
    {
        global $CACHE_MANAGER;
        $CACHE_MANAGER->ClearByTag(static::getTag());
    }

    public static function getQuery($obNav)
    {
        $obQuery = Table::query()
            ->setSelect(self::getFields())
            ->setOrder(array('SORT' => 'ASC'));

        if ($obNav !== null) {

        }

        return $obQuery;
    }
}
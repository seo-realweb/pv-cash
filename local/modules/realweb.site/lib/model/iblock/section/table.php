<?php

namespace Realweb\Site\Model\Iblock\Section;

use Bitrix\Iblock\SectionTable;
use Realweb\Site\Data\Orm\Query;

class Table extends SectionTable
{
    public static function query()
    {
        return new Query(static::getEntity());
    }
}
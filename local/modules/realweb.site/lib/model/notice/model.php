<?php

namespace Realweb\Site\Model\Notice;

use Bitrix\Main\ORM\Fields\Relations\Reference;
use Bitrix\Main\ORM\Query\Join;
use Realweb\Site\ArrayHelper;

class Model extends \Realweb\Site\Data\Model
{
    public static function getCollection()
    {
        return Collection::class;
    }

    public static function getEntity()
    {
        return Entity::class;
    }

    public static function getOrmEntity()
    {
        return Table::getEntity();
    }

    public static function getTag()
    {
        return Table::getTableName();
    }

    public static function clearCache()
    {
        global $CACHE_MANAGER;
        $CACHE_MANAGER->ClearByTag(static::getTag());
    }

    public static function getQuery($obNav)
    {
        $obQuery = Table::query()
            ->setSelect(self::getFields())
            ->registerRuntimeField(
                'TYPE',
                new Reference(
                    'TYPE',
                    \Realweb\Site\Model\Enum\Table::getEntity(),
                    Join::on('this.UF_TYPE', 'ref.ID')
                )
            )
            ->addSelect('TYPE.*', 'TYPE_')
            ->setOrder(array('UF_CREATED' => 'DESC'));

        if ($obNav !== null) {
            $arFilter = $obNav->getParam('filter');
            if ($arOrder = $obNav->getParam('order')) {
                $obQuery->setOrder($arOrder);
            }
            if (($active = ArrayHelper::getValue($arFilter, 'active')) !== null) {
                $obQuery->whereCustom('UF_ACTIVE', $active);
            }
            if (($userId = ArrayHelper::getValue($arFilter, 'user_id')) !== null) {
                $obQuery->whereCustom('UF_USER_ID', $userId);
            }
        }

        return $obQuery;
    }
}
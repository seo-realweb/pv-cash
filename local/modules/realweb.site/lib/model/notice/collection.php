<?php


namespace Realweb\Site\Model\Notice;


class Collection extends \Realweb\Site\Data\Collection
{
    protected static function getModel()
    {
        return Model::class;
    }

    public function addItem($obItem)
    {
        $this->_collection[] = $obItem;
        $this->_keys[] = $obItem->getId();

        return $this;
    }

    public function getText()
    {
        $strResult = '<div class="tooltip-text tooltip-text_sm m-0">';
        if ($this->count() > 0) {
            $strResult .= '<ul>';
            /** @var Entity $obItem */
            foreach ($this->getCollection() as $obItem) {
                $strResult .= '<li><span>' . $obItem->getCreatedFormatted() . '</span> ' . $obItem->getUfText() . '</li>';
            }
            $strResult .= '</ul>';
        } else {
            $strResult .= '<div class="text-center"><span>Новых уведомлений не найдено</span></div>';
        }
        $strResult .= '<div class="text-center"><a href="/personal/notice/">История уведомлений</a></div>';
        $strResult .= '</div>';

        return $strResult;
    }

    public function clear()
    {
        /** @var Entity $obItem */
        foreach ($this->getCollection() as $obItem) {
            $obItem->clear();
        }

        return $this;
    }
}
<?php


namespace Realweb\Site\Model\Notice;

use Bitrix\Main\Type\DateTime;
use Realweb\Site\ArrayHelper;

/**
 * Class Entity
 * @package Realweb\Site\Model\Notice
 * @method DateTime getUfCreated()
 * @method int getUfUserId()
 * @method int getUfActive()
 * @method int getUfText()
 * @method Entity setUfSum(float $float)
 * @method Entity setUfCreated(DateTime $dateTime)
 * @method Entity setUfActive(int $int)
 * @method Entity setUfUserId(int $int)
 * @method Entity setUfText(string $string)
 * @method int getUfType()
 * @method Entity setUfType(int $int)
 * @method string getUfParams()
 * @method Entity setUfParams(string $string)
 */
class Entity extends \Realweb\Site\Data\Entity
{
    /**
     * @var \Realweb\Site\Model\Enum\Entity
     */
    private $_type = null;

    protected static function getModel()
    {
        return Model::class;
    }

    public function setData(array $arData)
    {
        $arType = array();
        foreach ($arData as $key => $value) {
            if (stripos($key, 'TYPE_') === 0) {
                $strKey = str_replace('TYPE_', '', $key);
                $arType[$strKey] = $value;
                unset($arData[$key]);
            }
        }
        if (!empty($arType['ID'])) {
            $this->setType(new \Realweb\Site\Model\Enum\Entity($arType));
        } else {
            $this->setType(new \Realweb\Site\Model\Enum\Entity());
        }

        return parent::setData($arData);
    }

    public function getType()
    {
        return $this->_type;
    }

    public function setType(\Realweb\Site\Model\Enum\Entity $obType)
    {
        $this->_type = $obType;

        return $this;
    }

    public function getParams()
    {
        if ($this->getUfParams()) {
            return unserialize($this->getUfParams());
        }
    }

    public function getParam(string $strKey)
    {
        if ($arParams = $this->getParams()) {
            return ArrayHelper::getValue($arParams, $strKey);
        }
    }

    public function setParams(array $arParams)
    {
        $this->setUfParams(serialize($arParams));

        return $this;
    }

    public function getCreatedFormatted(string $strFormat = "H:i")
    {
        return FormatDate($strFormat, $this->getUfCreated()->getTimestamp());
    }

    public function save()
    {
        if (!$this->getUfCreated()) {
            $this->setUfCreated(new DateTime());
            $this->setUfActive(1);
        }

        return parent::save();
    }

    public function clear()
    {
        $this->setUfActive(0);
        $this->save();
    }

    public function getText()
    {
        $strHtml = '<div data-id="' . $this->getId() . '" class="notification__item" data-type="' . $this->getType()->getXmlId() . '">';
        switch ($this->getType()->getXmlId()) {
            case 'screenshot_confirm':
                $strHtml .= '<span class="notification__icon purple"><i class="far fa-image"></i></span><span class="notification__text">Ваш скриншот принят! <a href="/executors/?order_id=' . $this->getUfText() . '">#' . $this->getUfText() . '</a></span><span class="notification__hint">' . $this->getParam('price') . '</span>';
                break;
            case 'screenshot_cancel':
                $strHtml .= '<span class="notification__icon red"><i class="far fa-image"></i></span><span class="notification__text">Ваш скриншот отклонен <a href="/executors/?order_id=' . $this->getUfText() . '&screenshot_id=' . $this->getParam('id') . '">#' . $this->getUfText() . '</a></span>';
                break;
            case 'top':
                $strHtml .= '<span class="notification__icon purple"><i class="fas fa-trophy"></i></span><span class="notification__text">Поздравляем! Вы попали в топ!</span>';
                break;
            case 'top_change':
                $strHtml .= '<span class="notification__icon purple"><i class="fas fa-trophy"></i></span><span class="notification__text">Ваше место в топе изменилось</span><span class="notification__hint">' . $this->getUfText() . '</span>';
                break;
            case 'screenshot_new':
                $strHtml .= '<span class="notification__icon purple"><i class="far fa-image"></i></span><span class="notification__text">Новый скриншот в <a href="/customers/?order_id=' . $this->getUfText() . '">заказе!</a></span>';
                break;
            case 'screenshot_max':
                $strHtml .= '<span class="notification__icon red"><i class="far fa-image"></i></span><span class="notification__text">В заказ добавлено максимальное<br> количество скриншотов</span>';
                break;
            case 'screenshot_auto':
                $strHtml .= '<span class="notification__icon red"><i class="fas fa-image"></i></span><span class="notification__text">Скриншот будет автоматически<br>принят через ' . $this->getUfText() . '</span>';
                break;
            case 'level_up':
                $strHtml .= '<span class="notification__icon orange"><i class="fas fa-trophy"></i></span><span class="notification__text">Поздравляем! Вы повысили уровень!</span>';
                break;
            case 'bonus':
                $strHtml .= '<span class="notification__icon orange"><i class="fas fa-trophy"></i></span><span class="notification__text">Открыта новая возможность. <a href="/bonus/">Смотреть</a></span>';
                break;
            case 'order_change':
                $strHtml .= '<span class="notification__icon purple"><i class="far fa-edit"></i></span><span class="notification__text">В заказе <a href="/customers/?order_id=' . $this->getUfText() . '">#' . $this->getUfText() . '</a> произошли изменения</span>';
                break;
            case 'order_new':
                $strHtml .= '<span class="notification__icon purple"><i class="far fa-edit"></i></span><span class="notification__text">Создан новый заказ <a href="/customers/?order_id=' . $this->getUfText() . '">#' . $this->getUfText() . '</a></span>';
                break;
            case 'order_taken':
                $strHtml .= '<span class="notification__icon purple"><i class="far fa-edit"></i></span><span class="notification__text">Заказ <a href="/customers/?order_id=' . $this->getUfText() . '">#' . $this->getUfText() . '</a> взят в работу</span>';
                break;
            case 'order_max':
                $strHtml .= '<span class="notification__icon red"><i class="far fa-edit"></i></span><span class="notification__text">На данном уровне нельзя создать больше заказов. Повышайте свой уровень для того чтобы создать больше заказов.</span>';
                break;
            default:
                $strHtml .= '<span class="notification__icon purple"><i class="far fa-exclamation-triangle"></i></span><span class="notification__text">' . $this->getUfText() . '</span>';
                break;
        }
        $strHtml .= '</div>';

        return $strHtml;
    }

    public static function create(string $strType, int $iUserId, string $strText = '', array $arParams = null)
    {
        $obTypeCollection = (new \Realweb\Site\Controller\Enum\Index())
            ->setParam('code', 'UF_TYPE')
            ->setParam('entity_id', 'HLBLOCK_10')
            ->actionIndex();
        $obNotice = new Entity();
        $obNotice->setUfText($strText);
        $obNotice->setUfUserId($iUserId);
        /** @var \Realweb\Site\Model\Enum\Entity $obType */
        if ($obType = $obTypeCollection->getByXmlId($strType)) {
            $obNotice->setUfType($obType->getId());
        }
        if ($arParams) {
            $obNotice->setUfParams(serialize($arParams));
        }

        return $obNotice->save();
    }
}

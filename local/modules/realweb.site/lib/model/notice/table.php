<?php


namespace Realweb\Site\Model\Notice;


use Bitrix\Main,
    Bitrix\Main\Localization\Loc;
use Realweb\Site\Data\Orm\Query;

Loc::loadMessages(__FILE__);

/**
 * Class Table
 * @package Realweb\Site\Module\Level
 */
class Table extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_hl_notice';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('NOTICE_ENTITY_ID_FIELD'),
            ),
            'UF_USER_ID' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('NOTICE_ENTITY_UF_USER_ID_FIELD'),
            ),
            'UF_CREATED' => array(
                'data_type' => 'datetime',
                'title' => Loc::getMessage('NOTICE_ENTITY_UF_CREATED_FIELD'),
            ),
            'UF_ACTIVE' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('NOTICE_ENTITY_UF_ACTIVE_FIELD'),
            ),
            'UF_TEXT' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('NOTICE_ENTITY_UF_TEXT_FIELD'),
            ),
            'UF_TYPE' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('NOTICE_ENTITY_UF_TYPE_FIELD'),
            ),
            'UF_PARAMS' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('NOTICE_ENTITY_UF_PARAMS_FIELD'),
            ),
        );
    }

    public static function query()
    {
        return new Query(static::getEntity());
    }
}

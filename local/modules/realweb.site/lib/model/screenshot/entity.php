<?php


namespace Realweb\Site\Model\Screenshot;

use Bitrix\Main\Type\DateTime;
use Realweb\Site\Controller\Order;
use Realweb\Site\Main\Helper;
use Realweb\Site\Main\IO\File;
use Realweb\Site\Controller\Custom;

/**
 * Class Entity
 * @package Realweb\Site\Model\Screenshot
 * @method int getUfOrderId()
 * @method int getUfMoneyTaken()
 * @method Entity setUfMoneyTaken(int $int)
 * @method string getComment()
 * @method Entity setUfComment(string $string)
 * @method Entity setUfOrderId(int $int)
 * @method Entity setUfStatus(int $int)
 * @method Entity setUfFile(int $int)
 * @method Entity setUfUserId(int $int)
 * @method string getUfFileSrc()
 * @method int getUfRating()
 * @method Entity setUfRating(int $int)
 * @method DateTime getUfCreated()
 * @method Entity setUfCreated(DateTime $dateTime)
 * @method int getUfUserId()
 * @method string getUfComment()
 * @method string getUfCustomerComment()
 * @method Entity setUfCustomerComment(string $string)
 * @method float getUfPrice()
 * @method Entity setUfPrice(float $float)
 */
class Entity extends \Realweb\Site\Data\Entity
{
    /**
     * @var \Realweb\Site\Model\Enum\Entity
     */
    private $_status = null;

    /**
     * @var \Realweb\Site\Model\User\Executor\Entity
     */
    private $_executor = null;

    /**
     * @var \Realweb\Site\Model\User\Customer\Entity
     */
    private $_customer = null;

    /**
     * @var \Realweb\Site\Model\Order\Element\Entity
     */
    private $_order = null;

    /**
     * @var File
     */
    private $_picture = null;

    /**
     * @var \Realweb\Site\Model\Custom\Entity
     */
    private $_custom = null;

    private $_expanded = false;

    protected static function getModel()
    {
        return Model::class;
    }

    public function setData(array $arData)
    {
        $arStatus = array();
        foreach ($arData as $key => $value) {
            if (stripos($key, 'STATUS_') === 0) {
                $strKey = str_replace('STATUS_', '', $key);
                $arStatus[$strKey] = $value;
                unset($arData[$key]);
            }
        }
        if (!empty($arStatus['ID'])) {
            $this->setStatus(new \Realweb\Site\Model\Enum\Entity($arStatus));
        } else {
            $this->setStatus(new \Realweb\Site\Model\Enum\Entity());
        }

        return parent::setData($arData);
    }

    public function getStatus()
    {
        return $this->_status;
    }

    public function setStatus(\Realweb\Site\Model\Enum\Entity $obStatus)
    {
        $this->_status = $obStatus;

        return $this;
    }

    public function isSuccess()
    {
        return $this->getStatus()->getXmlId() == 'success';
    }

    public function isFail()
    {
        return $this->getStatus()->getXmlId() == 'fail';
    }

    public function isNull()
    {
        return $this->getStatus()->getXmlId() === null;
    }

    public function cancel(string $strComment = '')
    {
        $obStatusCollection = (new \Realweb\Site\Controller\Enum\Index())
            ->setParam('code', 'UF_STATUS')
            ->setParam('entity_id', 'HLBLOCK_5')
            ->actionIndex();
        $this->setUfCustomerComment($strComment);
        $this->setUfStatus($obStatusCollection->getByXmlId('fail')->getId());
        if ($this->save()) {
            \Realweb\Site\Model\Notice\Entity::create('screenshot_cancel', $this->getExecutor()->getId(), $this->getUfOrderId(), array('id' => $this->getId()));

            return true;
        }
    }

    public function confirm(int $iRating = 5)
    {
        $obStatusCollection = (new \Realweb\Site\Controller\Enum\Index())
            ->setParam('code', 'UF_STATUS')
            ->setParam('entity_id', 'HLBLOCK_5')
            ->actionIndex();
        $this->setUfRating($iRating);
        $this->setUfStatus($obStatusCollection->getByXmlId('success')->getId());
        if ($this->getCustom()) {
            $fPrice = $this->getCustom()->getUfPrice();
            if ($this->getOrder() && $this->getOrder()->isHot()) {
                $fPrice += \Realweb\Site\Model\Order\Element\Entity::HOT_PRICE;
            }
            $this->setUfPrice($fPrice);
        }
        if ($this->save()) {
            $this->getOrder()->setCountScreenshot($this->getOrder()->getCountScreenshot() + 1);
            if ($this->getExecutor()) {
                $this->getExecutor()->updateRating();
            }
            if ($this->getCustomer()) {
                $this->getCustomer()->updateRating();
            }
            \Realweb\Site\Model\Notice\Entity::create('screenshot_confirm', $this->getExecutor()->getId(), $this->getUfOrderId(), array('price' => "+" . $this->getPriceFormatted()));

            return true;
        }
    }

    public function getPriceFormatted()
    {
        return number_format($this->getUfPrice(), 0, '.', '') . '₽';
    }

    public function save()
    {
        if (!$this->getUfCreated()) {
            $this->setUfCreated(new DateTime());
        }
        $bSave = parent::save();
        \Realweb\Site\Model\Order\Element\Model::clearCache();

        return $bSave;
    }

    public function getConfirmTime()
    {
        if ($this->getUfCreated()) {
            $iTime = $this->getUfCreated()->getTimestamp() + 3600 * 24;
            if ($iTime > time()) {
                return $iTime - time();
            }
        }
    }

    public function getOrder()
    {
        if ($this->_order === null) {
            $obController = new Order\Index();
            $obController->setParam('id', $this->getUfOrderId());
            $this->_order = $obController->actionElement();
        }

        return $this->_order;
    }

    public function setOrder(\Realweb\Site\Model\Order\Element\Entity $obOrder)
    {
        $this->_order = $obOrder;

        return $this;
    }

    public function getExecutor()
    {
        if ($this->_executor === null) {
            $this->_executor = \Realweb\Site\Model\User\Executor\Model::getByPrimary($this->getUfUserId());
        }

        return $this->_executor;
    }

    public function getCustomer()
    {
        if ($this->_customer === null) {
            if ($this->getOrder()) {
                $this->_customer = \Realweb\Site\Model\User\Customer\Model::getByPrimary($this->getOrder()->getCreatedBy());
            }
        }

        return $this->_customer;
    }

    public function getPicture()
    {
        if ($this->getUfFileSrc()) {
            $this->_picture = new File($this->getUfFileSrc());
        }

        return $this->_picture;
    }

    public function getPreviewPicture()
    {
        if ($this->getPicture()) {
            $obFile = $this->getPicture()->getResizeFile(230, 120);
            if ($obFile && $obFile->isExists()) {
                return $obFile;
            } else {
                return $this->getPicture();
            }
        }
    }

    public function getPreviewPictureSrc()
    {
        if ($obPicture = $this->getPreviewPicture()) {
            return $obPicture->getPublicPath();
        } else {
            return $this->getUfFileSrc();
        }
    }

    public function getCustom()
    {
        if ($this->_custom === null) {
            $obController = new Custom\Index();
            $obController->setParam('order_id', $this->getUfOrderId());
            $obController->setParam('user_id', $this->getUfUserId());
            $this->_custom = $obController->getCollection()->current();
        }

        return $this->_custom;
    }

    public function setCustom(\Realweb\Site\Model\Custom\Entity $obCustom)
    {
        $this->_custom = $obCustom;

        return $this;
    }

    public function getAutoConfirmTime()
    {
        if ($this->isNull()) {
            $iTime = $this->getUfCreated()->getTimestamp() + 24 * 3600;
            if (time() < $iTime) {
                return Helper::formatTime($iTime - time());
            }
        }
    }

    /**
     * @param bool $expanded
     */
    public function setExpanded(bool $expanded = true): void
    {
        $this->_expanded = $expanded;
    }

    public function isExpanded()
    {
        return $this->_expanded;
    }
}
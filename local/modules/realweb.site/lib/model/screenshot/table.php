<?php


namespace Realweb\Site\Model\Screenshot;


use Bitrix\Main,
    Bitrix\Main\Localization\Loc;
use Realweb\Site\Data\Orm\Query;

Loc::loadMessages(__FILE__);

/**
 * Class Table
 * @package Realweb\Site\Model\Screenshot
 */
class Table extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_hl_screenshot';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('SCREENSHOT_ENTITY_ID_FIELD'),
            ),
            'UF_USER_ID' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('SCREENSHOT_ENTITY_UF_USER_ID_FIELD'),
            ),
            'UF_FILE' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('SCREENSHOT_ENTITY_UF_FILE_FIELD'),
            ),
            'UF_STATUS' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('SCREENSHOT_ENTITY_UF_STATUS_FIELD'),
            ),
            'UF_RATING' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('SCREENSHOT_ENTITY_UF_RATING_FIELD'),
            ),
            'UF_ORDER_ID' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('SCREENSHOT_ENTITY_UF_ORDER_ID_FIELD'),
            ),
            'UF_MONEY_TAKEN' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('SCREENSHOT_ENTITY_UF_MONEY_TAKEN_FIELD'),
            ),
            'UF_COMMENT' => array(
                'data_type' => 'string',
                'title' => Loc::getMessage('SCREENSHOT_ENTITY_UF_COMMENT_FIELD'),
            ),
            'UF_CREATED' => array(
                'data_type' => 'datetime',
                'title' => Loc::getMessage('MESSAGE_ENTITY_UF_CREATED_FIELD'),
            ),
            'UF_CUSTOMER_COMMENT' => array(
                'data_type' => 'string',
                'title' => Loc::getMessage('SCREENSHOT_ENTITY_UF_CUSTOMER_COMMENT_FIELD'),
            ),
            'UF_PRICE' => array(
                'data_type' => 'float',
                'title' => Loc::getMessage('LEVEL_ENTITY_UF_PRICE_FIELD'),
            ),
        );
    }

    public static function query()
    {
        return new Query(static::getEntity());
    }
}
<?php


namespace Realweb\Site\Model\Screenshot;

use Realweb\Site\Controller\Order;
use Realweb\Site\Controller\Custom;
use Realweb\Site\Site;

class Collection extends \Realweb\Site\Data\Collection
{
    protected static function getModel()
    {
        return Model::class;
    }

    public function addItem($obItem)
    {
        $this->_collection[] = $obItem;
        $this->_keys[] = $obItem->getId();

        return $this;
    }

    public function getByOrder(int $iOrderId)
    {
        $obCollection = new Collection();
        /** @var Entity $obItem */
        foreach ($this->getCollection() as $obItem) {
            if ($obItem->getUfOrderId() == $iOrderId) {
                $obCollection->addItem($obItem);
            }
        }

        return $obCollection;
    }

    public function getByOrderToPay(int $iOrderId)
    {
        $obCollection = new Collection();
        /** @var Entity $obItem */
        foreach ($this->getCollection() as $obItem) {
            if ($obItem->getUfOrderId() == $iOrderId) {
                if (!$obItem->getUfMoneyTaken() && $obItem->isSuccess()) {
                    $obCollection->addItem($obItem);
                }
            }
        }

        return $obCollection;
    }

    public function getSuccess()
    {
        $obCollection = new Collection();
        /** @var Entity $obItem */
        foreach ($this->getCollection() as $obItem) {
            if ($obItem->isSuccess()) {
                $obCollection->addItem($obItem);
            }
        }

        return $obCollection;
    }

    public function getFail()
    {
        $obCollection = new Collection();
        /** @var Entity $obItem */
        foreach ($this->getCollection() as $obItem) {
            if ($obItem->isFail()) {
                $obCollection->addItem($obItem);
            }
        }

        return $obCollection;
    }

    public function getNull()
    {
        $obCollection = new Collection();
        /** @var Entity $obItem */
        foreach ($this->getCollection() as $obItem) {
            if ($obItem->isNull()) {
                $obCollection->addItem($obItem);
            }
        }

        return $obCollection;
    }

    public function getOrderKeys()
    {
        $arResult = array();
        if ($this->count()) {
            /** @var Entity $obItem */
            foreach ($this->getCollection() as $obItem) {
                $arResult[$obItem->getUfOrderId()] = $obItem->getUfOrderId();
            }
        }

        return array_values($arResult);
    }

    public function getUserKeys()
    {
        $arResult = array();
        if ($this->count()) {
            /** @var Entity $obItem */
            foreach ($this->getCollection() as $obItem) {
                $arResult[$obItem->getUfUserId()] = $obItem->getUfUserId();
            }
        }

        return array_values($arResult);
    }

    public function getOrders()
    {
        if ($this->count() > 0) {
            $obController = new Order\Index();
            $obController->setParam('id', $this->getOrderKeys());
            $obCollection = $obController->findCollection();
            /** @var Entity $obItem */
            foreach ($this->getCollection() as $obItem) {
                /** @var \Realweb\Site\Model\Order\Element\Entity $obOrder */
                if ($obOrder = $obCollection->getByKey($obItem->getUfOrderId())) {
                    $obItem->setOrder($obOrder);
                }
            }
        }
    }

    public function getCustom()
    {
        if ($this->count() > 0) {
            $obController = new Custom\Index();
            $obController->setParam('order_id', $this->getOrderKeys());
            $obController->setParam('user_id', $this->getUserKeys());
            $obController->setParam('show_all', true);
            $obCollection = $obController->getCollection();
            /** @var \Realweb\Site\Model\Custom\Entity $obItem */
            foreach ($obCollection->getCollection() as $obItem) {
                /** @var Entity $obScreenshot */
                if ($obScreenshot = $this->getByKey($obItem->getUfOrderId())) {
                    $obScreenshot->setCustom($obItem);
                }
            }
        }
    }

    public function getPrice()
    {
        $fPrice = 0;
        /** @var Entity $obItem */
        foreach ($this->getCollection() as $obItem) {
            if ($obItem->isSuccess() && !$obItem->getUfMoneyTaken()) {
                $fPrice += $obItem->getUfPrice();
            }
        }

        return $fPrice;
    }

    public function getPriceFormatted(bool $bWithCommission = false)
    {
        $fPrice = $this->getPrice();
        if ($bWithCommission) {
            $fPrice = Site::getCommissionSum($fPrice, 'order');
        }

        return number_format($fPrice, 0, '.', '') . '₽';
    }
}
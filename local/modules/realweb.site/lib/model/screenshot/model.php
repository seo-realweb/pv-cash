<?php

namespace Realweb\Site\Model\Screenshot;

use Bitrix\Main\ORM\Fields\Relations\Reference;
use Bitrix\Main\ORM\Query\Join;
use Realweb\Site\ArrayHelper;

class Model extends \Realweb\Site\Data\Model
{
    public static function getCollection()
    {
        return Collection::class;
    }

    public static function getEntity()
    {
        return Entity::class;
    }

    public static function getOrmEntity()
    {
        return Table::getEntity();
    }

    public static function getQuery($obNav)
    {
        $obQuery = Table::query()
            ->setSelect(self::getFields())
            ->registerFileField('UF_FILE')
            ->registerRuntimeField(
                'STATUS',
                new Reference(
                    'STATUS',
                    \Realweb\Site\Model\Enum\Table::getEntity(),
                    Join::on('this.UF_STATUS', 'ref.ID')
                )
            )
            ->addSelect('STATUS.*', 'STATUS_');

        if ($obNav !== null) {
            $arFilter = $obNav->getParam('filter');
            if (($userId = ArrayHelper::getValue($arFilter, 'user_id')) !== null) {
                $obQuery->whereCustom('UF_USER_ID', $userId);
            }
            if (($status = ArrayHelper::getValue($arFilter, 'status')) !== null) {
                $obQuery->whereCustom('STATUS_XML_ID', $status);
            }
            if (($orderId = ArrayHelper::getValue($arFilter, 'order_id')) !== null) {
                $obQuery->whereCustom('UF_ORDER_ID', $orderId);
            }
            if (($moneyTaken = ArrayHelper::getValue($arFilter, 'money_taken')) !== null) {
                $obQuery->whereCustom('UF_MONEY_TAKEN', $moneyTaken);
            }
        }

        return $obQuery;
    }

    public static function getTag()
    {
        return Table::getTableName();
    }

    public static function clearCache()
    {
        global $CACHE_MANAGER;
        $CACHE_MANAGER->ClearByTag(static::getTag());
    }
}
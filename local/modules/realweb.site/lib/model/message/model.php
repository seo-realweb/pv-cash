<?php

namespace Realweb\Site\Model\Message;

use Bitrix\Main\ORM\Fields\ExpressionField;
use Realweb\Site\ArrayHelper;
use Realweb\Site\Data\Orm\Query;
use Realweb\Site\Data\PageNavigation;

class Model extends \Realweb\Site\Data\Model
{
    public static function getCollection()
    {
        return Collection::class;
    }

    public static function getEntity()
    {
        return Entity::class;
    }

    public static function getOrmEntity()
    {
        return Table::getEntity();
    }

    public static function getQuery($obNav)
    {
        $obQuery = Table::query()
            ->setSelect(self::getFields())
            ->setOrder(array('UF_CREATED' => 'ASC'));

        if ($obNav !== null) {
            $arFilter = $obNav->getParam('filter');
            if (($userFromId = ArrayHelper::getValue($arFilter, 'user_from_id')) !== null) {
                $obQuery->whereCustom('UF_USER_FROM_ID', $userFromId);
            }
            if (($userToId = ArrayHelper::getValue($arFilter, 'user_to_id')) !== null) {
                $obQuery->whereCustom('UF_USER_TO_ID', $userToId);
            }
            if (($id = ArrayHelper::getValue($arFilter, 'id')) !== null) {
                $obQuery->whereCustom('ID', $id);
            }
            if (($read = ArrayHelper::getValue($arFilter, 'read')) !== null) {
                $obQuery->whereCustom('UF_READ', $read);
            }
        }

        return $obQuery;
    }

    public static function getUsers(PageNavigation $obNav)
    {
        $arResult = array();
        $obQuery = Table::query()
            ->setSelect(array('UF_USER_FROM_ID', 'UF_USER_TO_ID'))
            ->setGroup(array('UF_USER_FROM_ID', 'UF_USER_TO_ID'))
            ->registerRuntimeField('LAST_ID', new ExpressionField('LAST_ID', 'MAX(%s)', array('ID')))
            ->addSelect('LAST_ID');
        if ($obNav !== null) {
            $arFilter = $obNav->getParam('filter');
            if (($userId = ArrayHelper::getValue($arFilter, 'user_id')) !== null) {
                $obQuery->where(Query::filter()->logic('or')
                    ->where('UF_USER_FROM_ID', '=', $userId)
                    ->where('UF_USER_TO_ID', '=', $userId)
                );
            }
        }
        $arItems = $obQuery->exec()->fetchAll();
        foreach ($arItems as $arItem) {
            foreach (array('UF_USER_FROM_ID', 'UF_USER_TO_ID') as $strField) {
                if ($iLastId = ArrayHelper::getValue($arResult, $arItem[$strField])) {
                    if ($iLastId < $arItem['LAST_ID']) {
                        $arResult[$arItem[$strField]] = $arItem['LAST_ID'];
                    }
                } else {
                    $arResult[$arItem[$strField]] = $arItem['LAST_ID'];
                }
            }
        }

        return $arResult;
    }

    public static function getTag()
    {
        return Table::getTableName();
    }

    public static function clearCache()
    {
        global $CACHE_MANAGER;
        $CACHE_MANAGER->ClearByTag(static::getTag());
    }
}
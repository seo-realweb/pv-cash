<?php


namespace Realweb\Site\Model\Message;

use Bitrix\Main\Type\DateTime;
use Realweb\Site\User;

/**
 * Class Entity
 * @package Realweb\Site\Model\Message
 * @method string getUfMessage()
 * @method DateTime getUfCreated()
 * @method int getUfUserFromId()
 * @method int getUfUserToId()
 * @method int getUfRead()
 * @method Entity setUfMessage(string $string)
 * @method Entity setUfCreated(DateTime $dateTime)
 * @method Entity setUfUserFromId(int $int)
 * @method Entity setUfUserToId(int $int)
 * @method Entity setUfRead(int $int)
 */
class Entity extends \Realweb\Site\Data\Entity
{
    protected static function getModel()
    {
        return Model::class;
    }

    public function isMy()
    {
        return $this->getUfUserFromId() == User::getInstance()->getId();
    }

    public function isRead()
    {
        return intval($this->getUfRead()) > 0;
    }

    public function getCreatedFormatted()
    {
        if ($this->getUfCreated()) {
            return $this->getUfCreated()->format('d.m H:i');
        }
    }

    public function save()
    {
        if (!$this->getUfCreated()) {
            $this->setUfCreated(new DateTime());
        }
        return parent::save();
    }
}
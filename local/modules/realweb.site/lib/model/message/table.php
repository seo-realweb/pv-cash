<?php


namespace Realweb\Site\Model\Message;


use Bitrix\Main,
    Bitrix\Main\Localization\Loc;
use Realweb\Site\Data\Orm\Query;

Loc::loadMessages(__FILE__);

/**
 * Class Table
 * @package Realweb\Site\Model\Message
 */
class Table extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_hl_message';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('MESSAGE_ENTITY_ID_FIELD'),
            ),
            'UF_USER_FROM_ID' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('MESSAGE_ENTITY_UF_USER_FROM_ID_FIELD'),
            ),
            'UF_USER_TO_ID' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('MESSAGE_ENTITY_UF_USER_TO_ID_FIELD'),
            ),
            'UF_MESSAGE' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('MESSAGE_ENTITY_UF_MESSAGE_FIELD'),
            ),
            'UF_CREATED' => array(
                'data_type' => 'datetime',
                'title' => Loc::getMessage('MESSAGE_ENTITY_UF_CREATED_FIELD'),
            ),
            'UF_READ' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('MESSAGE_ENTITY_UF_READ_FIELD'),
            ),
        );
    }

    public static function query()
    {
        return new Query(static::getEntity());
    }
}
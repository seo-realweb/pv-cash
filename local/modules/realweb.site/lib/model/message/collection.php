<?php


namespace Realweb\Site\Model\Message;


use Realweb\Site\User;

class Collection extends \Realweb\Site\Data\Collection
{
    protected static function getModel()
    {
        return Model::class;
    }

    public function addItem($obItem)
    {
        $this->_collection[] = $obItem;
        $this->_keys[] = $obItem->getId();

        return $this;
    }

    public function getByUser(int $userId)
    {
        /** @var Entity $obItem */
        foreach ($this->getCollection() as $obItem) {
            if ($obItem->getUfUserFromId() == $userId || $obItem->getUfUserToId() == $userId) {
                return $obItem;
            }
        }
    }

    public function setRead()
    {
        /** @var Entity $obItem */
        foreach ($this->getCollection() as $obItem) {
            if (!$obItem->isRead() && $obItem->getUfUserToId() == User::getInstance()->getId()) {
                $obItem->setUfRead(1);
                $obItem->save();
            }
        }
    }
}
<?php


namespace Realweb\Site\Model\Rating;


use Bitrix\Main,
    Bitrix\Main\Localization\Loc;
use Realweb\Site\Data\Orm\Query;

Loc::loadMessages(__FILE__);

/**
 * Class Table
 * @package Realweb\Site\Model\Rating
 */
class Table extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_hl_rating';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('RATING_ENTITY_ID_FIELD'),
            ),
            'UF_COUNT_SCREEN' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('RATING_ENTITY_UF_COUNT_SCREEN_FIELD'),
            ),
            'UF_LEVEL' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('RATING_ENTITY_UF_LEVEL_FIELD'),
            ),
            'UF_COUNT_ORDER' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('RATING_ENTITY_UF_COUNT_ORDER_FIELD'),
            ),
            'UF_SHOW_PROFILE' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('RATING_ENTITY_UF_SHOW_PROFILE_FIELD'),
            ),
            'UF_RATING_POINTS' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('RATING_ENTITY_UF_RATING_POINTS_FIELD'),
            ),
            'UF_COMPLAIN_ORDER' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('RATING_ENTITY_UF_COMPLAIN_ORDER_FIELD'),
            ),
            'UF_SEND_MESSAGE' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('RATING_ENTITY_UF_SEND_MESSAGE_FIELD'),
            ),
            'UF_DELAY_FUNDS' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('RATING_ENTITY_UF_DELAY_FUNDS_FIELD'),
            ),
            'UF_COUNT_HIGHLIGHT_ADS' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('RATING_ENTITY_UF_COUNT_HIGHLIGHT_ADS_FIELD'),
            ),
            'UF_UNLIMITED' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('RATING_ENTITY_UF_UNLIMITED_FIELD'),
            ),
            'UF_STATUS' => array(
                'data_type' => 'string',
                'title' => Loc::getMessage('RATING_ENTITY_UF_STATUS_FIELD'),
            ),
            'UF_COUNT_NEW_ORDER' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('RATING_ENTITY_UF_COUNT_NEW_ORDER_FIELD'),
            ),
        );
    }

    public static function query()
    {
        return new Query(static::getEntity());
    }
}
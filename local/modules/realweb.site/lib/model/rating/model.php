<?php

namespace Realweb\Site\Model\Rating;

use Realweb\Site\ArrayHelper;

class Model extends \Realweb\Site\Data\Model
{
    public static function getCollection()
    {
        return Collection::class;
    }

    public static function getEntity()
    {
        return Entity::class;
    }

    public static function getOrmEntity()
    {
        return Table::getEntity();
    }

    public static function getTag()
    {
        return Table::getTableName();
    }

    public static function clearCache()
    {
        global $CACHE_MANAGER;
        $CACHE_MANAGER->ClearByTag(static::getTag());
    }

    public static function getQuery($obNav)
    {
        $obQuery = Table::query()
            ->setSelect(self::getFields())
            ->setOrder(array('UF_LEVEL' => 'ASC'));

        if ($obNav !== null) {
            $arFilter = $obNav->getParam('filter');
            if (($iCountScreenshot = ArrayHelper::getValue($arFilter, 'count_screenshot')) !== null) {
                $obQuery->where('UF_COUNT_SCREEN', '<=', $iCountScreenshot);
            }
        }

        return $obQuery;
    }
}
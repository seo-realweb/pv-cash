<?php


namespace Realweb\Site\Model\Rating;


class Collection extends \Realweb\Site\Data\Collection
{
    protected static function getModel()
    {
        return Model::class;
    }

    public function addItem($obItem)
    {
        $this->_collection[] = $obItem;
        $this->_keys[] = $obItem->getId();

        return $this;
    }

    /**
     * @return Entity
     */
    public function getWithdrawal()
    {
        /** @var Entity $obItem */
        foreach ($this->getCollection() as $obItem) {
            if (intval($obItem->getUfDelayFunds()) > -1) {
                return $obItem;
            }
        }
    }
}
<?php


namespace Realweb\Site\Model\Rating;

use Realweb\Site\Controller\Rating\Index;

/**
 * Class Entity
 * @package Realweb\Site\Model\Rating
 * @method int getUfCountScreen()
 * @method int getUfLevel()
 * @method int getUfCountOrder()
 * @method int getUfShowProfile()
 * @method int getUfRatingPoints()
 * @method int getUfComplainOrder()
 * @method int getUfSendMessage()
 * @method int getUfDelayFunds()
 * @method int getUfCountHighlightAds()
 * @method int getUfUnlimited()
 * @method string getUfStatus()
 * @method int getUfCountNewOrder()
 */
class Entity extends \Realweb\Site\Data\Entity
{
    /**
     * @var Entity
     */
    private $_next_rating = null;

    protected static function getModel()
    {
        return Model::class;
    }

    public function getNextRating()
    {
        if ($this->_next_rating === null) {
            $obController = new Index();
            $obCollection = $obController->actionIndex();
            /** @var \Realweb\Site\Model\Rating\Entity $obItem */
            foreach ($obCollection->getCollection() as $obItem) {
                if ($this->getUfLevel() < $obItem->getUfLevel()) {
                    $this->_next_rating = $obItem;
                    break;
                }
            }
        }

        return $this->_next_rating;
    }
}
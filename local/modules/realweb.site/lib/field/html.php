<?php

namespace Realweb\Site\Field;

/**
 * Class Html
 * @package Realweb\Site\Field
 */
class Html
{

    // ---------------------------------------------------------------------
    // Общие параметры методов класса:
    // @param array $arUserField - метаданные (настройки) свойства
    // @param array $arHtmlControl - массив управления из формы (значения свойств, имена полей веб-форм и т.п.)
    // ---------------------------------------------------------------------
    // Функция регистрируется в качестве обработчика события OnUserTypeBuildList
    function GetUserTypeDescription()
    {
        return array(
            // уникальный идентификатор
            'USER_TYPE_ID' => 'html',
            // имя класса, методы которого формируют поведение типа
            'CLASS_NAME' => __CLASS__,
            // название для показа в списке типов пользовательских свойств
            'DESCRIPTION' => 'HTML/text',
            // базовый тип на котором будут основаны операции фильтра
            'BASE_TYPE' => 'string',
        );
    }

    // Функция вызывается при добавлении нового свойства
    // для конструирования SQL запроса создания столбца значений свойства
    // @return string - SQL
    function GetDBColumnType($arUserField)
    {
        switch (strtolower($GLOBALS['DB']->type)) {
            case 'mysql':
                return 'text';
                break;
        }
    }

    // Функция вызывается при выводе формы метаданных (настроек) свойства
    // @param bool $bVarsFromForm - флаг отправки формы
    // @return string - HTML для вывода
    function GetSettingsHTML($arUserField = false, $arHtmlControl, $bVarsFromForm)
    {
        $result = '';

        return $result;
    }

    // Функция валидатор значений свойства
    // вызвается в $GLOBALS['USER_FIELD_MANAGER']->CheckField() при добавлении/изменении
    // @param array $value значение для проверки на валидность
    // @return array массив массивов ("id","text") ошибок
    function CheckFields($arUserField, $value)
    {
        $aMsg = array();
        return $aMsg;
    }

    // Функция вызывается при выводе формы редактирования значения свойства
    // @return string - HTML для вывода
    function GetEditFormHTML($arUserField, $arHtmlControl)
    {
        if ($arUserField["ENTITY_VALUE_ID"] < 1 && strlen($arUserField["SETTINGS"]["DEFAULT_VALUE"]) > 0)
            $arHtmlControl["VALUE"] = htmlspecialchars($arUserField["SETTINGS"]["DEFAULT_VALUE"]);
        ob_start();
        echo '<div class="html_realweb">';
        \CFileMan::AddHTMLEditorFrame($arHtmlControl["NAME"], $arHtmlControl["VALUE"], "html_" . $arHtmlControl["NAME"], "html", 200, "N", 0, "", "", "s1");
        echo '</div>';
        $b = ob_get_clean();
        return $b;
    }

    // Функция вызывается при выводе формы редактирования значения множественного свойства
    // @return string - HTML для вывода
    function GetEditFormHTMLMulty($arUserField, $arHtmlControl)
    {
        $html = 'Поле не может быть множественным!';
        return $html;
    }

    // Функция вызывается при выводе фильтра на странице списка
    // @return string - HTML для вывода
    function GetFilterHTML($arUserField, $arHtmlControl)
    {
        $sVal = intval($arHtmlControl['VALUE']);
        $sVal = $sVal > 0 ? $sVal : '';

        return \CUserTypeSectionsHtmlField::GetEditFormHTML($arUserField, $arHtmlControl);
    }

    // Функция вызывается при выводе значения свойства в списке элементов
    // @return string - HTML для вывода
    function GetAdminListViewHTML($arUserField, $arHtmlControl)
    {
        return '';
    }

    // Функция вызывается при выводе значения множественного свойства в списке элементов
    // @return string - HTML для вывода
    function GetAdminListViewHTMLMulty($arUserField, $arHtmlControl)
    {

        return '';
    }

    // Функция вызывается при выводе значения свойства в списке элементов в режиме редактирования
    // @return string - HTML для вывода
    function GetAdminListEditHTML($arUserField, $arHtmlControl)
    {
        return '';
    }

    // Функция вызывается при выводе множественного значения свойства в списке элементов в режиме редактирования
    // @return string - HTML для вывода
    function GetAdminListEditHTMLMulty($arUserField, $arHtmlControl)
    {
        return '';
    }

    // Функция должна вернуть представление значения поля для поиска
    // @return string - посковое содержимое
    function OnSearchIndex($arUserField)
    {
        return '';
    }

    // Функция вызывается перед сохранением значений в БД
    // @param mixed $value - значение свойства
    // @return string - значение для вставки в БД
    function OnBeforeSave($arUserField, $value)
    {

        return $value;
    }

}
<?php

namespace Realweb\Site\Data;

/**
 * Class Cache
 * @package Realweb\Site\Data
 */
class Cache
{
    const TIME = 36000;

    /**
     * @var string
     */
    private $_id;

    /**
     * @var string
     */
    private $_dir;

    /**
     * @var integer
     */
    private $_time;

    /**
     * @var array
     */
    private $_tag = array();

    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->_id = md5($this->serialize($id));

        return $this;
    }

    /**
     * @param mixed $value
     * @return string
     */
    public function serialize($value)
    {
        return serialize($value);
    }

    /**
     * @param $dir
     * @return $this
     */
    public function setDir($dir)
    {
        $this->_dir = SITE_ID . '/' . str_replace("\\", "/", $dir);

        return $this;
    }

    /**
     * @param $tag
     * @return $this
     */
    public function setTag($tag)
    {
        $this->_tag[] = $tag;

        return $this;
    }

    /**
     * @return string
     */
    public function getDir()
    {
        return $this->_dir;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @return array
     */
    public function getTag()
    {
        return $this->_tag;
    }

    /**
     *
     * @param integer $iTime
     * @return $this
     */
    public function setTime($iTime)
    {

        $this->_time = $iTime;

        return $this;
    }

    /**
     *
     * @return int
     */
    function getTime()
    {
        if (is_null($this->_time)) {
            $this->_time = self::TIME;
        }

        return $this->_time;
    }

    private function checkParams()
    {
        $strId = $this->getId();
        $strDir = $this->getDir();
        if (strlen($strId) == 0) {
            throw new \Exception("method setId is required");
        }

        if (strlen($strDir) == 0) {
            throw new \Exception("method setDir is required");
        }
    }

    /**
     * @param $callable
     * @return array|mixed
     * @throws \Exception
     */
    public function get($callable)
    {
        $this->checkParams();

        global $CACHE_MANAGER;
        $arResult = [];
        $cache = \Bitrix\Main\Data\Cache::createInstance();

        if ($cache->initCache($this->getTime(), $this->getId(), $this->getDir())) {
            $arResult = $cache->getVars();
        } elseif ($cache->startDataCache()) {
            $arResult = call_user_func($callable);
            if (count($this->getTag()) > 0) {
                $CACHE_MANAGER->StartTagCache($this->getDir());
            }
            if (!empty($arResult)) {
                if (count($this->getTag()) > 0) {
                    foreach ($this->getTag() as $strTag) {
                        $CACHE_MANAGER->RegisterTag($strTag);
                    }
                    $CACHE_MANAGER->endTagCache();
                }
                $cache->endDataCache($arResult);
            } else {
                $cache->abortDataCache();
            }
        }

        return $arResult;
    }
}

<?php

namespace Realweb\Site\Data;

use Bitrix\Main\ORM\Data\AddResult;
use Bitrix\Main\ORM\Data\UpdateResult;
use Bitrix\Main\ORM\Fields\ScalarField;
use Realweb\Site\ArrayHelper;

/**
 * Class Entity
 * @package Realweb\Site\Data
 * @method boolean hasId();
 * @method Entity setId(int $int);
 * @method int getId()
 */
abstract class Entity extends Data
{
    /**
     * @var array
     */
    protected static $instance = array();

    /**
     * @var AddResult|UpdateResult
     */
    protected $_orm_result = null;

    /**
     * @return Model
     */
    abstract protected static function getModel();

    /**
     * DataEntity constructor.
     * @param array $data
     * @param null $primary
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function __construct($data = [], $primary = null)
    {
        if ($data) {
            $this->setData($data);
            if ($primary === null) {
                $primaryField = static::getModel()::getOrmEntity()->getPrimary();
                if (is_array($primaryField)) {
                    foreach ($primaryField as $strField) {
                        $primary[$strField] = ArrayHelper::getValue($data, $strField);
                    }
                } else {
                    $primary = ArrayHelper::getValue($data, $primaryField);
                }
            }
            if ($primary !== null) {
                $this->setPrimary($primary);
                $this->_exist = true;
            }
        } else {
            $this->setPrimary($primary);
            $this->getData();
        }
        if ($this->hasId()) {
            $this->setId($this->_data['ID']);
        }
    }

    /**
     * @return array|false
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function getData()
    {
        if (is_null($this->_data)) {
            $this->_data = array_fill_keys(static::getModel()::getFields(), null);
            $primary = $this->getPrimary();
            if ($primary !== null) {
                $primaryField = static::getModel()::getOrmEntity()->getPrimary();
                $obSelect = static::getModel()::getQuery(null);
                if (is_array($primary)) {
                    $obSelect->setFilter($primary);
                } else {
                    $obSelect->where($primaryField, '=', $primary);
                }
                $rsResult = $obSelect->exec();
                if ($_arData = $rsResult->fetch()) {
                    $this->setData($_arData);
                    $this->_exist = true;
                }
            }
        }

        return $this->_data;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function save()
    {
        $arFields = array();
        $arData = $this->getData();
        foreach (static::getModel()::getFields() as $strFieldCode) {
            if (array_key_exists($strFieldCode, $arData)) {
                $arFields[$strFieldCode] = $arData[$strFieldCode];
            }
        }
        if ($this->isExist()) {
            $rsResult = static::getModel()::getOrmEntity()->getDataClass()::update($this->getPrimary(), $arFields);
        } else {
            foreach (static::getModel()::getOrmEntity()->getFields() as $obField) {
                if ($obField instanceof ScalarField) {
                    if ($obField->isAutocomplete()) {
                        unset($arFields[$obField->getName()]);
                    }
                }
            }
            $rsResult = static::getModel()::getOrmEntity()->getDataClass()::add($arFields);
            if (intval($rsResult->getId()) > 0) {
                $this->setPrimary($rsResult->getId());
                $this->_exist = true;
            }
        }
        $this->setOrmResult($rsResult);
        $this->_changed = false;
        $this->_data = null;
        $this->getData();
        static::getModel()::clearCache();

        return $rsResult->isSuccess();
    }

    public function delete()
    {
        if ($this->isExist()) {
            $rsResult = static::getModel()::getOrmEntity()->getDataClass()::delete($this->getPrimary());
            $this->_exist = false;
            $this->_primary = null;
            $this->_changed = true;

            return $rsResult->isSuccess();
        }
    }

    /**
     * @param $iId
     * @param array $data
     * @return mixed
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getInstance($iId, $data = [])
    {
        $strCode = md5(static::class);
        if (empty(static::$instance[$strCode][$iId])) {
            static::$instance[$strCode][$iId] = new static($iId, $data);
        }

        return static::$instance[$strCode][$iId];
    }

    /**
     * @param $obResult
     * @return $this
     */
    public function setOrmResult($obResult)
    {
        $this->_orm_result = $obResult;

        return $this;
    }

    /**
     * @return AddResult|UpdateResult
     */
    public function getOrmResult()
    {
        if ($this->_orm_result === null) {
            $this->_orm_result = new AddResult();
        }

        return $this->_orm_result;
    }
}

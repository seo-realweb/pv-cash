<?php

namespace Realweb\Site\Data;

/**
 * Class \Realweb\Api\Model\Data\Data
 *
 */
abstract class Data
{

    /**
     * @var array|int
     */
    protected $_primary;

    /**
     * @var bool
     */
    protected $_exist = false;

    /**
     * @var bool
     */
    protected $_changed = false;

    /**
     * @var array
     */
    protected $_data;

    /**
     * @return array
     */
    abstract public function getData();

    /**
     * @param $name
     * @param $arguments
     * @return $this|mixed
     * @throws \Exception
     */
    public function __call($name, $arguments)
    {
        if ((strpos($name, "get") === 0)) {
            $strKey = substr_replace($name, "", 0, 3);
            preg_match_all('/[A-Z][^A-Z]*?/Us', $strKey, $res, PREG_SET_ORDER);
            $arField = array();
            foreach ($res as $arRes) {
                $arField[] = $arRes[0];
            }
            $strField = self::toUpper(implode('_', $arField));
            $strLowerField = self::toLower(implode('_', $arField));
            $arData = $this->getData();
            if (array_key_exists($strField, $arData)) {
                return $arData[$strField];
            } elseif (array_key_exists($strLowerField, $arData)) {
                return $arData[$strLowerField];
            } else {
                throw new \Exception("Call to undefined method {$name}");
            }
        } elseif ((strpos($name, "has") === 0)) {
            $strKey = substr_replace($name, "", 0, 3);
            preg_match_all('/[A-Z][^A-Z]*?/Us', $strKey, $res, PREG_SET_ORDER);
            $arField = array();
            foreach ($res as $arRes) {
                $arField[] = $arRes[0];
            }
            $strField = self::toUpper(implode('_', $arField));
            $strLowerField = self::toLower(implode('_', $arField));
            $arData = $this->getData();
            if (array_key_exists($strField, $arData)) {
                return true;
            } elseif (array_key_exists($strLowerField, $arData)) {
                return true;
            } else {
                return false;
            }
        } elseif ((strpos($name, "set") === 0)) {
            $strKey = substr_replace($name, "", 0, 3);
            preg_match_all('/[A-Z][^A-Z]*?/Us', $strKey, $res, PREG_SET_ORDER);
            $arField = array();
            foreach ($res as $arRes) {
                $arField[] = $arRes[0];
            }
            $strField = self::toUpper(implode('_', $arField));
            $strLowerField = self::toLower(implode('_', $arField));
            $arData = $this->getData();
            if (array_key_exists($strField, $arData)) {
//                if ($this->checkChanges($this->_data[$strField], $arguments[0])) {
//                    $this->_changed = true;
//                }
                $this->_data[$strField] = $arguments[0];
                return $this;
            } elseif (array_key_exists($strLowerField, $arData)) {
//                if ($this->checkChanges($this->_data[$strLowerField], $arguments[0])) {
//                    $this->_changed = true;
//                }
                $this->_data[$strLowerField] = $arguments[0];
                return $this;
            } else {
                $this->_changed = true;
                $this->_data[$strField] = $arguments[0];
                return $this;
            }
        } else {
            throw new \Exception("Call to undefined method {$name}");
        }
    }

    public function setData(array $arData)
    {
        $this->_data = $arData;

        return $this;
    }

    public function getPrimary()
    {
        return $this->_primary;
    }

    public function setPrimary($primary)
    {
        $this->_primary = $primary;

        return $this;
    }

    /**
     * @return bool
     */
    public function isExist()
    {
        return $this->_exist;
    }

    /**
     * @return bool
     */
    public function isChanged()
    {
        return $this->_changed;
    }

    /**
     * @param null $data
     *
     * @return array
     */
    public function toArray($data = null)
    {
        $arArray = array();
        if (is_null($data)) {
            $data = $this->_data;
        }
        foreach ($data as $strKey => $value) {
            if (strpos($strKey, '~') === 0) {
                continue;
            }
            $strLowerKey = self::toLower($strKey);
            if (is_array($value)) {
                $arArray[$strLowerKey] = $this->toArray($value);
            } else {
                $arArray[$strLowerKey] = $value;
            }
        }

        return $arArray;
    }

    /**
     * @param $strString
     * @return string
     */
    public static function toLower($strString)
    {
        return ToLower($strString);
    }

    /**
     * @param $strString
     *
     * @return string
     */
    public static function toUpper($strString)
    {
        return ToUpper($strString);
    }

}

<?php

namespace Realweb\Site\Data\Orm;

use Bitrix\Iblock\ElementTable;
use Bitrix\Main\Entity\ReferenceField;
use Bitrix\Main\ORM\Fields\ExpressionField;
use Bitrix\Main\ORM\Query\Join;

/**
 * class \Realweb\Api\Model\Orm\Query
 *
 */
class Query extends \Bitrix\Main\ORM\Query\Query
{

    /**
     * @param array $select
     * @return Query
     */
    public function setSelect(array $select)
    {
        return parent::setSelect($select);
    }

    /**
     * @param string|null $name
     * @param null $fieldInfo
     * @return Query
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    public function registerRuntimeField($name, $fieldInfo = null)
    {
        return parent::registerRuntimeField($name, $fieldInfo);
    }

    /**
     * @param mixed $order
     * @return Query
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    public function setOrder($order)
    {
        return parent::setOrder($order);
    }

    /**
     * @param string $field
     * @param mixed $value
     * @param string $condition
     * @return $this
     */
    public function whereCustom(string $field, $value, $condition = '=')
    {
        if ((is_array($value) && !empty($value)) || $value instanceof Query) {
            $this->whereIn($field, $value);
        } elseif (is_bool($value)) {
            if ($value) {
                $this->whereNotNull($field);
            } else {
                $this->whereNull($field);
            }
        } else {
            $this->where($field, $condition, $value);
        }

        return $this;
    }

    /**
     * @param string $field
     * @return $this
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    public function registerFileField(string $field)
    {
        $strField = $field . '_SRC';
        $strFileField = $field . '_FILE';
        $this->registerRuntimeField(
            $strFileField, new ReferenceField(
                $strFileField,
                \Bitrix\Main\FileTable::class,
                Join::on('this.' . $field, 'ref.ID')
            )
        );
        $this->registerRuntimeField(
            $strField, new ExpressionField(
                $strField,
                'CONCAT(' . self::_getUploadDirPrefix() . ',%s, "/" ,%s)',
                array($strFileField . '.SUBDIR', $strFileField . '.FILE_NAME')
            )
        );
        $this->addSelect($field . '_SRC');

        return $this;
    }

    protected static function _getUploadDirPrefix()
    {
        return '"/","' . \COption::GetOptionString("main", "upload_dir", "upload") . '","/"';
    }

    public static function getUploadDirPrefix()
    {
        return self::_getUploadDirPrefix();
    }


    /**
     * @param $strName
     * @param $iBlockId
     * @return $this
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\SystemException
     */
    public function registerSectionElementField($strName, $iBlockId)
    {
        $strField = $strName . '_SECTION';
        $this->registerRuntimeField(
            $strField,
            new ReferenceField(
                $strField,
                ElementTable::class,
                Join::on('this.' . $strName, 'ref.ID')
            )
        );
        $this->addSelect($strField . '.ID', $strName . '_ID');
        $this->addSelect($strField . '.NAME', $strName . '_NAME');
        $this->addSelect($strField . '.CODE', $strName . '_CODE');

        return $this;
    }

}

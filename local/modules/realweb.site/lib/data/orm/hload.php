<?php

namespace Realweb\Site\Data\Orm;


use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\Loader;
use Bitrix\Main\SystemException;

/**
 * Class Hload
 * @package Realweb\Api\Model\Orm
 */
abstract class Hload extends Table
{
    /**
     * @var int
     */
    protected $_entity_id = null;

    /**
     * Hload constructor.
     * @throws ArgumentException
     * @throws SystemException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     */
    protected function __construct()
    {
        $tableName = static::getTableName();
        if (Loader::includeModule('highloadblock')) {
            $arBlock = HighloadBlockTable::query()
                ->setSelect(array('*'))
                ->where('TABLE_NAME', '=', $tableName)
                ->exec()
                ->fetch();
            if ($arBlock) {
                $this->setEntityId($arBlock['ID']);
                $this->_orm_entity = HighloadBlockTable::compileEntity($arBlock);
                $this->_fields = $this->_orm_entity->getFields();
            } else {
                throw new \Exception("table {$tableName} not found");
            }
        } else {
            throw new \Exception("module highloadblock not installed");
        }
    }

    public function getEntityId()
    {
        return $this->_entity_id;
    }

    public function setEntityId(int $iId)
    {
        $this->_entity_id = $iId;

        return $this;
    }
}

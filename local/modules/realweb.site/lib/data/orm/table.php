<?php

namespace Realweb\Site\Data\Orm;


use Bitrix\Main\Application;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\Db\SqlQueryException;
use Bitrix\Main\Entity\Base;
use Bitrix\Main\ORM\Data\DataManager;
use Bitrix\Main\ORM\Entity;
use Bitrix\Main\ORM\Fields\ScalarField;
use Bitrix\Main\SystemException;

/**
 * Class Table
 * @package Realweb\Api\Model\Orm
 */
abstract class Table
{
    /**
     * @var Table
     */
    private static $instance = null;

    /**
     * @var Entity
     */
    protected $_orm_entity = null;

    /**
     * @var ScalarField[]
     */
    protected $_fields = null;

    /**
     * @return string
     */
    abstract public static function getTableName();

    /**
     * Table constructor.
     * @throws ArgumentException
     * @throws SqlQueryException
     * @throws SystemException
     */
    protected function __construct()
    {
        $entityName = $this->getClassName();
        $tableName = static::getTableName();
        $this->_fields = array();
        $connection = Application::getConnection();
        if ($connection->isTableExists($tableName)) {
            $this->_fields = $connection->getTableFields($tableName);
        }
        $this->_orm_entity = Base::compileEntity(
            $entityName,
            $this->_fields,
            array(
                'table_name' => $tableName,
                'namespace' => __NAMESPACE__
            )
        );
    }

    /**
     * @return Table
     * @throws ArgumentException
     * @throws SqlQueryException
     * @throws SystemException
     */
    public static function getInstance()
    {
        $strId = static::getTableName();
        if (empty(self::$instance[$strId])) {
            self::$instance[$strId] = new static();
        }

        return self::$instance[$strId];
    }

    /**
     *
     * @return Entity
     */
    public function getOrmEntity()
    {
        return $this->_orm_entity;
    }

    /**
     * @return string
     */
    public function getClassName()
    {
        return 'Orm_' . static::getTableName();
    }

    /**
     * @return Query
     * @throws ArgumentException
     * @throws SystemException
     */
    public function getQuery()
    {
        return new Query($this->getOrmEntity());
    }

    /**
     * @return ScalarField[]
     */
    public function getFields()
    {
        return $this->_fields;
    }

    /**
     * @return DataManager
     */
    public function getDataClass()
    {
        return $this->getOrmEntity()->getDataClass();
    }
}

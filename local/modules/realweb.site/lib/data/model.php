<?php

namespace Realweb\Site\Data;

/**
 * Class Model
 * @package Realweb\Site\Data
 */
abstract class Model
{
    /**
     * @param PageNavigation $obNav
     * @return \Realweb\Site\Data\Orm\Query|\Bitrix\Main\ORM\Query\Query
     */
    abstract public static function getQuery($obNav);

    /**
     * @return \Bitrix\Main\ORM\Entity
     */
    abstract public static function getOrmEntity();

    /**
     * @return Collection
     */
    abstract public static function getCollection();

    /**
     * @return Entity
     */
    abstract public static function getEntity();

    /**
     * @return string
     */
    abstract public static function getTag();

    abstract public static function clearCache();

    /**
     * @return array
     */
    public static function getFields()
    {
        return array_keys(static::getOrmEntity()->getFields());
    }

    /**
     * @param PageNavigation|null $obNav
     * @return array|false
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getRow(PageNavigation &$obNav = null)
    {
        return static::_query($obNav)
            ->exec()
            ->fetch();
    }

    /**
     * @param PageNavigation|null $obNav
     * @return Entity
     */
    public static function getObject(PageNavigation &$obNav = null)
    {
        $entity = static::getEntity();
        $arItem = static::getRow($obNav);

        return new $entity($arItem);
    }

    /**
     * @param PageNavigation|null $obNav
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getAll(PageNavigation &$obNav = null)
    {
        $obQuery = static::_query($obNav);
        if ($obNav) {
            if (!$obNav->getAllRecords()) {
                $obQuery->setLimit($obNav->getLimit())->setOffset($obNav->getOffset());
            }
        }

        $rsResult = $obQuery->exec();
        $arItems = $rsResult->fetchAll();
        if ($obNav) {
            if ($obNav->getAllRecords()) {
                $iCount = count($arItems);
                $obNav->setPageSize($iCount);
                $obNav->setRecordCount($iCount);
            } else {
                $iCount = $rsResult->getCount();
                $obNav->setRecordCount($iCount);
            }
        }

        return $arItems;
    }

    public static function getObjectCollection(PageNavigation &$obNav = null)
    {
        $collection = static::getCollection();
        $entity = static::getEntity();
        $obCollection = new $collection();
        $arItems = static::getAll($obNav);

        foreach ($arItems as $arItem) {
            $obEntity = new $entity($arItem);
            $obCollection->addItem($obEntity);
        }

        return $obCollection;
    }

    /**
     * @param PageNavigation $obNav
     * @return \Bitrix\Main\ORM\Query\Query|\Realweb\Site\Data\Orm\Query|null
     */
    protected static function _query($obNav)
    {
        $obSelect = static::getQuery($obNav)
            ->setCacheTtl(0);
        if ($obNav !== null) {
            if (!$obNav->getAllRecords()) {
                $obSelect->countTotal(true);
            }
        }

        return $obSelect;
    }

    /**
     * @param $primary
     * @return mixed
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getByPrimary($primary)
    {
        $entity = static::getEntity();
        $obQuery = static::getQuery(null);
        if (is_array($primary)) {
            $obQuery->setFilter($primary);
        } else {
            $obQuery->where(static::getOrmEntity()->getPrimary(), '=', $primary);
        }
        $arItem = $obQuery->exec()->fetch();

        return new $entity($arItem);
    }
}

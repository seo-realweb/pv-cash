<?php

namespace Realweb\Site\Data;

use Realweb\Site\Main\Uri;

/**
 * Class PageNavigation
 * @package Realweb\Site\Data
 */
class PageNavigation extends \Bitrix\Main\UI\PageNavigation
{

    const NAV_TYPE_ALL = 'all';
    const NAV_TYPE_SLIDING = 'sliding';

    public static $PAGE_SIZE = "count";
    public static $CLEAR_CACHE = "clear_cache";
    private $query = array();
    private $urls = array();
    private $current_url;
    private $_params;
    private $_all_records = false;
    private $_navigation_type = self::NAV_TYPE_ALL;
    private $_sliding_siblings_count = 10;
    private $_sliding_side_count = 2;

    private $_more_text = null;

    public function __construct($id = null)
    {
        if ($id === null) {
            $id = uniqid();
        }
        parent::__construct($id);
    }

    /**
     * @param $strId
     * @return $this
     */
    public function setId($strId)
    {
        $this->id = $strId;
        return $this;
    }

    /**
     *
     * @return int
     */
    public function getNextPage()
    {
        $iNextPage = null;
        $iCurrentPageNumber = $this->getCurrentPage();
        if ($iCurrentPageNumber < $this->getPageCount()) {
            $iNextPage = $iCurrentPageNumber + 1;
        }
        return $iNextPage;
    }

    /**
     *
     * @return int
     */
    public function getPreviousPage()
    {
        $iPreviousPage = null;
        $iCurrentPageNumber = $this->getCurrentPage();
        if ($iCurrentPageNumber > 1 && $this->getPageCount() > 0) {
            $iPreviousPage = $iCurrentPageNumber - 1;
        }
        return $iPreviousPage;
    }

    /**
     *
     * @param string $strKey
     * @param string $strValue
     * @return $this
     */
    public function setQuery($strKey, $strValue)
    {
        $this->query[$strKey] = $strValue;
        return $this;
    }

    public function getQuery()
    {
        return $this->query;
    }

    /**
     *
     * @param string $strKey
     * @param string|array $strValue
     * @return $this
     */
    public function setParam($strKey, $strValue)
    {
        $this->_params[$strKey] = $strValue;
        return $this;
    }

    public function getParam($strKey, $default = null)
    {
        if (array_key_exists($strKey, $this->_params)) {
            return $this->_params[$strKey];
        }
        return $default;
    }

    public function getParams()
    {
        return $this->_params;
    }

    /**
     *
     * @param string $strKey
     * @param string $strUrl
     * @return $this
     */
    public function setUrl($strKey, $strUrl)
    {
        $this->urls[$strKey] = $strUrl;
        return $this;
    }

    public function getUrls()
    {
        return $this->urls;
    }

    public function getUrl($strKey)
    {
        if (array_key_exists($strKey, $this->urls)) {
            return $this->urls[$strKey];
        }
        return null;
    }

    /**
     *
     * @param string $strType
     * @return $this
     */
    public function setNavigationType($strType)
    {
        $this->_navigation_type = $strType;
        return $this;
    }

    public function getNavigationType()
    {
        return $this->_navigation_type;
    }

    /**
     *
     * @param int $iCount
     * @return $this
     */
    public function setSlidingSiblingsCount($iCount)
    {
        $iCount = IntVal($iCount);
        if ($iCount <= 0)
            $iCount = 10;
        $this->_sliding_siblings_count = $iCount;
        return $this;
    }

    public function getSlidingSiblingsCount()
    {
        return $this->_sliding_siblings_count;
    }

    /**
     *
     * @param int $iCount
     * @return $this
     */
    public function setSlidingSideCount($iCount)
    {
        $iCount = IntVal($iCount);
        if ($iCount <= 0)
            $iCount = 2;
        $this->_sliding_side_count = $iCount;
        return $this;
    }

    public function getSlidingSideCount()
    {
        return $this->_sliding_side_count;
    }

    public function buildPages()
    {
        if ($this->getNavigationType() == self::NAV_TYPE_SLIDING) {
            return $this->_buildPagesSliding();
        } else {
            return $this->_buildPagesAll();
        }
    }

    private function _buildPagesSliding()
    {
        $iCurrentPage = $this->getCurrentPage();

        $iCountPage = $this->getPageCount();
        $iPageRange = $this->getSlidingSiblingsCount();

        if ($iPageRange > $iCountPage) {
            $iPageRange = $iCountPage;
        }

        $iDelta = ceil($iPageRange / 2);

        if ($iCurrentPage - $iDelta > $iCountPage - $iPageRange) {
            $iLowerBound = $iCountPage - $iPageRange + 1;
            $iUpperBound = $iCountPage;
        } else {
            if ($iCurrentPage - $iDelta < 0) {
                $iDelta = $iCurrentPage;
            }
            $iOffset = $iCurrentPage - $iDelta;
            $iLowerBound = $iOffset + 1;
            $iUpperBound = $iOffset + $iPageRange;
        }

        $arPages = array();

        if (($iLowerBound - $this->getSlidingSideCount()) > 0) {
            $arPages = array_merge($arPages, $this->_getPagesInRange(1, 1));
            $iLeftCenter = ceil(($iLowerBound - 2) / 2);
            while ($iLeftCenter < 2) {
                $iLeftCenter++;
            }
            $arPages = array_merge($arPages, $this->_getPagesInRange($iLeftCenter, $iLeftCenter, "..."));
        } else {
            $arPages = array_merge($arPages, $this->_getPagesInRange(1, $iLowerBound - 1));
        }

        $arPages = array_merge($arPages, $this->_getPagesInRange($iLowerBound, $iUpperBound));

        if ($iUpperBound < ($iCountPage - 1)) {
            if ((($iCountPage - 1) - $iUpperBound) > 1) {
                $iRightCenter = $iUpperBound + ceil((($iCountPage - 1) - $iUpperBound) / 2);
                $arPages = array_merge($arPages, $this->_getPagesInRange($iRightCenter, $iRightCenter, '...'));
                $arPages = array_merge($arPages, $this->_getPagesInRange($iCountPage, $iCountPage));
            } else {
                $arPages = array_merge($arPages, $this->_getPagesInRange($iCountPage - 1, $iCountPage));
            }

        } else {
            $arPages = array_merge($arPages, $this->_getPagesInRange($iUpperBound + 1, $iCountPage));
        }

        return $arPages;
    }

    /**
     *
     * @return array
     */
    private function _buildPagesAll()
    {
        $iCountPage = $this->getPageCount();
        return $this->_getPagesInRange(1, $iCountPage);
    }

    /**
     *
     * @param integer $iStart
     * @param integer $iEnd
     * @param string $strTitle
     * @return array
     */
    private function _getPagesInRange($iStart, $iEnd, $strTitle = null)
    {
        $iCurrentPage = $this->getCurrentPage();

        $arPages = array();
        $arFilterParams = $this->getQuery();
        $strId = $this->getId();

        for ($i = $iStart; $i <= $iEnd; $i++) {
            $arPage = array(
                'page' => $i,
                'title' => (is_null($strTitle) ? $i : $strTitle),
                'current' => ($iCurrentPage == $i ? true : false),
                'urls' => array()
            );

            $arUrlParams = $arFilterParams;
            if (intval($i) > 1) {
                $arUrlParams[$strId] = 'page-' . $i;
            } else {
                if (array_key_exists($strId, $arUrlParams)) {
                    unset($arUrlParams[$strId]);
                }
            }
            foreach ($this->getUrls() as $strUrlKey => $strBasicUrl) {
                $obUrl = new Uri($strBasicUrl);
                $obUrl->addParams($arUrlParams);
                $arPage['urls'][$strUrlKey] = $obUrl->getPathQuery();
            }
            if ($arPage['current']) {
                $this->setCurrentUrl($arPage);
            }

            $arPages[] = $arPage;
        }
        return $arPages;
    }

    /**
     *
     * @param array $arPage
     * @return $this
     */
    private function setCurrentUrl($arPage)
    {
        $this->current_url = $arPage;
        return $this;
    }

    public function getCurrentUrl()
    {
        if (is_null($this->current_url)) {
            $this->buildPages();
        }
        return $this->current_url;
    }

    /**
     *
     * @return $this
     */
    public function initFromUri()
    {
        parent::initFromUri();
        return $this;
    }

    /**
     * Returns number of pages or 0 if recordCount is not set.
     * @return int
     */
    public function getPageCount()
    {
        if ($this->allRecords) {
            return 1;
        }
        if (intval($this->recordCount) == 0) {
            return 0;
        }
        $maxPages = floor($this->recordCount / $this->pageSize);
        if (($this->recordCount % $this->pageSize) > 0) {
            $maxPages++;
        }
        return $maxPages;
    }

    /**
     *
     * @param boolean $bFlag
     * @return $this
     */
    public function setAllRecords($bFlag = true)
    {
        $this->_all_records = $bFlag;
        return $this;
    }

    /**
     *
     * @return boolean
     */
    public function getAllRecords()
    {
        return $this->_all_records;
    }

    public function getStructure()
    {
        $arResult = array(
            'next_page_urls' => array(),
            'next_page' => null,
            'previous_page_urls' => array(),
            'previous_page' => null,
            'total_pages' => 0,
            'total_count' => 0,
            'current_page' => 1,
            'filter' => "",
            'params' => array(),
            'hash' => md5(serialize($this)),
        );
        $arParams = $this->getQuery();
        $arResult['params'] = $arParams;

        $iNextPage = $this->getNextPage();
        $iPreviousPage = $this->getPreviousPage();
        $strId = $this->getId();

        if (intval($iNextPage) > 0) {
            $arNextParams = $arParams;
            $arNextParams[$strId] = 'page-' . $iNextPage;
            $arResult['next_page'] = $iNextPage;
            foreach ($this->getUrls() as $strUrlKey => $strBasicUrl) {
                $arResult['next_page_urls'][$strUrlKey] = $strBasicUrl . (strpos($strBasicUrl, "?") === false ? '?' : '&') . http_build_query($arNextParams);
            }
        }

        if (intval($iPreviousPage) > 0) {
            $arPreviousParams = $arParams;
            if (intval($iPreviousPage) > 1)
                $arPreviousParams[$strId] = 'page-' . $iPreviousPage;
            $arResult['previous_page'] = $iPreviousPage;
            foreach ($this->getUrls() as $strUrlKey => $strBasicUrl) {
                $arResult['previous_page_urls'][$strUrlKey] = $strBasicUrl;
                if (strlen($arPreviousParams[$strId]) > 0)
                    $arResult['previous_page_urls'][$strUrlKey] .= (strpos($strBasicUrl, "?") === false ? '?' : '&') . http_build_query($arPreviousParams);
            }
        }

        $arFilterParams = $arParams;
        unset($arFilterParams[$strId]);
        if (isset($arFilterParams['clear'])) {
            unset($arFilterParams['clear']);
        }

        $arResult['filter'] = '?' . http_build_query($arFilterParams);

        $arResult['pages'] = $this->buildPages();
        $arResult['current_url'] = $this->getCurrentUrl();

        $arResult['total_pages'] = $this->getPageCount();
        $arResult['total_count'] = $this->getRecordCount();
        $arResult['current_page'] = $this->getCurrentPage();
        $arResult['page_size'] = $this->getPageSize();
        $arResult['more_text'] = $this->getMoreText();
        $arResult['current_urls'] = array();
        foreach ($this->getUrls() as $strUrlKey => $strBasicUrl) {
            $arResult['current_urls'][$strUrlKey] = $strBasicUrl . (strpos($strBasicUrl, "?") === false ? '?' : '&') . http_build_query($arParams);
        }

        return $arResult;
    }

    public function getMoreCount()
    {
        if ($this->getCurrentPage() + 1 == $this->getPageCount()) {
            return $this->getRecordCount() - $this->getCurrentPage() * $this->getPageSize();
        } else {
            return $this->getPageSize();
        }
    }

    public function setMoreText(string $strMore)
    {
        $this->_more_text = $strMore;

        return $this;
    }

    public function getMoreText()
    {
        if ($this->_more_text === null) {

            $this->_more_text = 'Показать еще ' . $this->getMoreCount();
        }

        return $this->_more_text;
    }
}

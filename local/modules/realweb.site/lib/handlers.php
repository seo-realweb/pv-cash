<?php

namespace Realweb\Site;

class Handlers
{
    function OnBeforeUserAdd(&$arFields)
    {
        if (!$arFields['EMAIL']) {
            $arFields['EMAIL'] = $arFields['LOGIN'] . '@' . Site::getHost();
        }
        if (empty($arFields['UF_CUSTOMER_RATING_ID'])) {
            $arFields['UF_CUSTOMER_RATING_ID'] = 1;
        }
        if (empty($arFields['UF_EXECUTOR_RATING_ID'])) {
            $arFields['UF_EXECUTOR_RATING_ID'] = 1;
        }
        if (User::getInstance()->existBlackList()) {
            global $APPLICATION;
            $APPLICATION->throwException(User::BLACK_LIST_MESSAGE);
            return false;
        }
        //$arFields['LOGIN'] = $arFields['EMAIL'];
    }

    function OnBeforeUserLogin(&$arFields)
    {
        if (User::getInstance()->existBlackList()) {
            global $APPLICATION;
            $APPLICATION->throwException(User::BLACK_LIST_MESSAGE);
            return false;
        }
    }

    function OnAfterUserAdd(&$arFields)
    {
        if (!empty($arFields['ID'])) {
            User::agentUpdateUsers();
            //User::createTestOrderForExecutor($arFields['ID']);
            //User::createTestOrderForCustomer($arFields['ID']);
        }
    }

    public function OnEndBufferContent(&$content)
    {
        if (!defined('ADMIN_SECTION')) {

        }
    }
}
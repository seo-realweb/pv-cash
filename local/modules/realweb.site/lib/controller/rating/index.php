<?php

namespace Realweb\Site\Controller\Rating;

use Realweb\Site\Data\Cache;
use Realweb\Site\Model\Rating\Collection;
use Realweb\Site\Model\Rating\Entity;
use Realweb\Site\Model\Rating\Model;

class Index extends \Realweb\Site\Main\Controller
{
    /**
     * @return array|mixed|Collection
     * @throws \Exception
     */
    public function actionIndex()
    {
        $obNav = $this->getNav();
        $obNav->setAllRecords();
        $obCollection = (new Cache())
            ->setId($obNav)
            ->setDir(__CLASS__)
            ->get(function () use ($obNav) {
                return Model::getObjectCollection($obNav);
            });

        return $obCollection;
    }

    /**
     * @return array|mixed|Collection
     * @throws \Exception
     */
    public function getCollection()
    {
        return $this->actionIndex();
    }

    /**
     * @return array|mixed|Entity
     * @throws \Exception
     */
    public function actionCurrent()
    {
        $iCount = $this->getParam('count_screenshot');
        $obCollection =  $this->actionIndex();
        $obCurrent = new Entity();
        /** @var Entity $obItem */
        foreach ($obCollection->getCollection() as $obItem) {
            if ($iCount >= $obItem->getUfCountScreen()) {
                $obCurrent = $obItem;
            } else {
                break;
            }
        }

        return $obCurrent;
    }
}
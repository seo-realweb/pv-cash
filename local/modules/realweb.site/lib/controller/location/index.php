<?php

namespace Realweb\Site\Controller\Location;

use Realweb\Site\Data\Cache;
use Realweb\Site\Model\Iblock\Section\Collection;
use Realweb\Site\Model\Location\Section\Model;

class Index extends \Realweb\Site\Main\Controller
{
    public function actionIndex()
    {
        $obNav = $this->getNav();
        $obCollection = (new Cache())
            ->setId($obNav)
            ->setDir(__CLASS__)
            ->setTag('iblock_id_' . Model::getIblockId())
            ->get(function () use ($obNav) {
                /** @var Collection $obCollection */
                $obCollection = Model::getObjectCollection($obNav);
                $obCollection->getElements();

                return $obCollection;
            });

        return $obCollection;
    }

    /**
     * @return Collection
     */
    public function getCollection()
    {
        $obCollection = $this->actionIndex();

        return $obCollection;
    }
}
<?php

namespace Realweb\Site\Controller\Tooltip;

use Realweb\Site\Data\Cache;
use Realweb\Site\Model\Tooltip\Collection;
use Realweb\Site\Model\Tooltip\Entity;
use Realweb\Site\Model\Tooltip\Model;

class Index extends \Realweb\Site\Main\Controller
{
    /**
     * @return array|mixed|Collection
     * @throws \Exception
     */
    public function actionIndex()
    {
        $obNav = $this->getNav();
        $obCollection = (new Cache())
            ->setId($obNav)
            ->setDir(__CLASS__)
            ->get(function () use ($obNav) {
                return Model::getObjectCollection($obNav);
            });

        return $obCollection;
    }

    public function actionEdit()
    {
        $arParams = $this->getRequestParams();
        if (!empty($arParams['id'])) {
            /** @var Entity $obTooltip */
            $obTooltip = Model::getByPrimary($arParams['id']);
            if ($obTooltip->isExist() && !\Realweb\Site\Main\Cookie\Collection::getInstance()->has('tooltip_' . $obTooltip->getId())) {
                if ($arParams['action'] == 'useful') {
                    $obTooltip->setUfUseful($obTooltip->getUfUseful() + 1);
                } else {
                    $obTooltip->setUfUseless($obTooltip->getUfUseless() + 1);
                }
                if ($obTooltip->save()) {
                    (new \Realweb\Site\Main\Cookie\Entity())
                        ->setName('tooltip_' . $obTooltip->getId())
                        ->setValue('1')
                        ->setExpireMonth()
                        ->save();

                    return array('result' => 1, 'message' => 'Ваш голос принят');
                }
            } else {
                return array('result' => 0, 'message' => 'Ваш голос уже принят');
            }
        }

        return array('result' => 0, 'message' => 'Ошибка');
    }
}
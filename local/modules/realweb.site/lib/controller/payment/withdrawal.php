<?php

namespace Realweb\Site\Controller\Payment;

use Bitrix\Main\Mail\Event;
use Realweb\Site\Controller\Rating;
use Realweb\Site\Site;
use Realweb\Site\User;

class Withdrawal extends \Realweb\Site\Main\Controller
{
    public function actionIndex()
    {
        if (User::getInstance()->isAuth()) {
            $iSumFull = intval($this->getRequestParams('sum'));
            $strType = $this->getRequestParams('type');
            $strPurse = $this->getRequestParams('purse');
            $iSum = Site::getCommissionSum($iSumFull, 'out');
            if (User::getInstance()->getEntityExecutor()->isWithdrawal()) {
                if ($strType && $strPurse) {
                    if ($iSum > 0) {
                        if ($iSumFull <= User::getInstance()->getEntityExecutor()->getBalanceValue()) {
                            $obTransaction = new \Realweb\Site\Model\Transaction\Entity();
                            $obTransaction->setUfUserId(User::getInstance()->getId());
                            $obTransaction->setUfSum(-$iSum);
                            $obTransaction->setUfStatus(\Realweb\Site\Model\Transaction\Model::getStatus()->getByXmlId('new')->getId());
                            $obTransaction->setParams(array(
                                'type' => $strType,
                                'purse' => $strPurse
                            ));
                            if ($obTransaction->save()) {
                                User::getInstance()->getEntityCustomer()->updateBalance(-$iSumFull, true, true);
                                Event::sendImmediate(array(
                                    "EVENT_NAME" => 'EVENT_WITHDRAWAL_MONEY',
                                    "LID" => "s1",
                                    "C_FIELDS" => array(
                                        'ID' => User::getInstance()->getId(),
                                        'TRANSACTION_ID' => $obTransaction->getId(),
                                        'LOGIN' => User::getInstance()->getEntityExecutor()->getLogin(),
                                    ),
                                ));

                                return array('result' => 1, 'message' => 'Запрос на вывод отправлен');
                            }
                        } else {
                            return array('result' => 0, 'message' => 'На счете не достаточно средств для вывода');
                        }
                    } else {
                        return array('result' => 0, 'message' => 'Не корректная сумма');
                    }
                } else {
                    return array('result' => 0, 'message' => 'Не заполнены обязательные поля');
                }
            } else {
                $obRatingCollection = (new Rating\Index())->getCollection();
                if ($obRating = $obRatingCollection->getWithdrawal()) {
                    return array('result' => 0, 'message' => 'Вывод средств возможен только с ' . $obRating->getUfLevel() . ' ур.');
                } else {
                    return array('result' => 0, 'message' => 'Ошибка');
                }
            }
        }
    }
}
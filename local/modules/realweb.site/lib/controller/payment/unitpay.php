<?php

namespace Realweb\Site\Controller\Payment;

use Realweb\Site\Model\Payment\Unitpay\Payment;
use Realweb\Site\Model\Transaction\Model;
use Realweb\Site\Site;
use Realweb\Site\User;

class Unitpay extends \Realweb\Site\Main\Controller
{
    public function actionIndex()
    {
        if (User::getInstance()->isAuth()) {
            $iSum = intval($this->getRequestParams('sum'));
            if ($iSum > 0) {
                $obTransaction = new \Realweb\Site\Model\Transaction\Entity();
                $obTransaction->setUfUserId(User::getInstance()->getId());
                $obTransaction->setUfSum($iSum);
                if ($obTransaction->save()) {
                    if ($strUrl = $this->getRequestParams('url')) {
                        User::getInstance()->setPaymentBackUrl($strUrl);
                    }
                    $obPayment = new Payment();

                    return $obPayment->getPaymentLink($obTransaction->getId(), Site::getCommissionSum($iSum, 'in'));
                }
            } else {
                return array('result' => 0, 'message' => 'Не корректная сумма');
            }
        }
    }

    public function actionFail()
    {
        User::getInstance()->setFlashMessage('danger', Site::getIncludeText('PAYMENT_FAIL_MESSAGE'));
        if ($strUrl = User::getInstance()->getPaymentBackUrl()) {
            LocalRedirect($strUrl);
        } else {
            LocalRedirect('/');
        }
    }

    public function actionSuccess()
    {
        User::getInstance()->setFlashMessage('success', Site::getIncludeText('PAYMENT_SUCCESS_MESSAGE'));
        if ($strUrl = User::getInstance()->getPaymentBackUrl()) {
            LocalRedirect($strUrl);
        } else {
            LocalRedirect('/');
        }
    }

    public function actionResult()
    {
        $obPayment = new Payment();
        $arParams = $this->getRequestParams();
        //\Bitrix\Main\Diag\Debug::writeToFile($this->getRequestParams());
        if (!empty($arParams['params']) && !empty($arParams['method'])) {
            //if ($obPayment->getFormSignature($arParams['params']['account'], $arParams['params']['orderSum'])) {
            /** @var \Realweb\Site\Model\Transaction\Entity $obItem */
            $obItem = \Realweb\Site\Model\Transaction\Model::getByPrimary($arParams['params']['account']);
            if ($obItem->isExist()) {
                if ($arParams['method'] == 'check') {
                    $obItem->setUfStatus($this->_getStatus()->getByXmlId('process')->getId());
                } elseif ($arParams['method'] == 'pay') {
                    $obItem->setUfStatus($this->_getStatus()->getByXmlId('success')->getId());
                    /** @var \Realweb\Site\Model\User\Entity $obUser */
                    $obUser = \Realweb\Site\Model\User\Model::getByPrimary($obItem->getUfUserId());
                    if ($obUser->isExist()) {
                        $obUser->updateBalance($obItem->getUfSum(), true);
                    }
                } else {
                    $obItem->setUfStatus($this->_getStatus()->getByXmlId('fail')->getId());
                }
                $obItem->save();
            }
            //}
        }

        return array('result' => 0);
    }

    private function _getStatus()
    {
        return Model::getStatus();
    }
}
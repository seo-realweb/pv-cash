<?php

namespace Realweb\Site\Controller\Complaint;

use Realweb\Site\Data\Cache;
use Realweb\Site\Model\Complaint\Model;
use Realweb\Site\Model\Complaint\Entity;
use Realweb\Site\Model\Complaint\Collection;
use Realweb\Site\User;

class Index extends \Realweb\Site\Main\Controller
{
    public function actionIndex()
    {
        $obNav = $this->getNav();
        if ($this->getParam('show_all')) {
            $obNav->setAllRecords(true);
        }
        $arFilter = array(
            'order_id' => $this->getParam('order_id'),
        );
        $obNav->setParam('filter', $arFilter);
        $arResult = (new Cache())
            ->setId($obNav)
            ->setTime(0)
            ->setDir(__CLASS__)
            ->setTag(Model::getTag())
            ->get(function () use ($obNav) {
                $obCollection = Model::getObjectCollection($obNav);

                return array(
                    'collection' => $obCollection,
                    'navigation' => $obNav->getStructure()
                );
            });

        return $arResult;
    }

    /**
     * @return Collection
     */
    public function getCollection()
    {
        $arResult = $this->actionIndex();

        return $arResult['collection'];
    }

    public function actionAdd()
    {
        $arParams = $this->getRequestParams();
        if (!empty($arParams['comment']) && !empty($arParams['id'])) {
            /** @var \Realweb\Site\Model\Order\Element\Entity $obOrder */
            if ($obOrder = \Realweb\Site\Model\Order\Element\Model::getByPrimary($arParams['id'])) {
                $obComplaint = new Entity();
                $obComplaint->setUfUserId(User::getInstance()->getId());
                $obComplaint->setUfOrderId($arParams['id']);
                $obComplaint->setUfCause($arParams['cause']);
                $obComplaint->setUfComment($arParams['comment']);
                if ($obComplaint->save()) {
                    $obOrder->getCustomer()->updateUserRating();
                }

                return array('result' => 1, 'message' => 'Жалоба добавлена');
            }
        }

        return array('result' => 0, 'message' => 'Ошибка при добавлении жалобы');
    }
}
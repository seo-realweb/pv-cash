<?php

namespace Realweb\Site\Controller\Message;

use Realweb\Site\Data\Cache;
use Realweb\Site\Model\Message\Collection;
use Realweb\Site\Model\Message\Entity;
use Realweb\Site\Model\Message\Model;
use Realweb\Site\User;

class Index extends \Realweb\Site\Main\Controller
{
    public function actionIndex()
    {
        $arResult = array();
        if ($userId = $this->getRequestParams('user_id', $this->getParam('current_user_id'))) {
            $obNav = $this->getNav(20);
            $arFilter = array(
                'user_from_id' => array(User::getInstance()->getId(), $userId),
                'user_to_id' => array(User::getInstance()->getId(), $userId),
            );
            $obNav->setParam('filter', $arFilter);
            $arResult = (new Cache())
                ->setId($obNav)
                ->setDir(__CLASS__)
                ->setTag(Model::getTag())
                ->setTime(0)
                ->get(function () use ($obNav) {
                    /** @var Collection $obCollection */
                    $obCollection = Model::getObjectCollection($obNav);
                    $obCollection->setRead();

                    return array(
                        'collection' => $obCollection,
                        'navigation' => $obNav->getStructure()
                    );
                });

            $arResult['user_id'] = $userId;
        }

        return $arResult;
    }

    public function actionGetCollection()
    {
        $obNav = $this->getNav();
        $arFilter = array(
            'id' => $this->getParam('id'),
        );
        $obNav->setParam('filter', $arFilter);
        $obCollection = (new Cache())
            ->setId($obNav)
            ->setDir(__CLASS__)
            ->setTag(Model::getTag())
            ->setTime(0)
            ->get(function () use ($obNav) {
                return Model::getObjectCollection($obNav);
            });

        return $obCollection;
    }

    public function actionUsers()
    {
        $obNav = $this->getNav();
        $arFilter = array(
            'user_id' => User::getInstance()->getId()
        );
        $obNav->setParam('filter', $arFilter);
        $arUsers = (new Cache())
            ->setId($obNav)
            ->setDir(__CLASS__)
            ->setTag(Model::getTag())
            ->setTime(0)
            ->get(function () use ($obNav) {
                return Model::getUsers($obNav);
            });
        if (!$arUsers) {
            $arUsers = array();
        }
        if ($userId = $this->getRequestParams('user_id')) {
            if (empty($arUsers[$userId])) {
                $arUsers[$userId] = 0;
            }
        }
        unset($arUsers[User::getInstance()->getId()]);
        if ($arUsers) {
            $obController = new \Realweb\Site\Controller\User\Index();
            $obController->setParam('id', array_keys($arUsers));
            $arResult = $obController->actionIndex();
            $this->setParam('id', array_values($arUsers));
            $arResult['last_message'] = $this->actionGetCollection();

            return $arResult;
        }
    }

    public function actionAdd()
    {
        $arParams = $this->getRequestParams();
        if ($arParams) {
            $obMessage = new Entity();
            $obMessage->setUfUserFromId(User::getInstance()->getId());
            $obMessage->setUfUserToId($arParams['user_id']);
            $obMessage->setUfMessage($arParams['message']);
            $obMessage->setUfRead(0);
            if ($obMessage->save()) {
                return array('result' => 1, 'message' => 'Сообщение успешно отправлено');
            } else {
                return array('result' => 0, 'message' => $obMessage->getOrmResult()->getErrorMessages());
            }
        } else {
            return array('result' => 0, 'message' => 'Ошибка при добавлении');
        }
    }

    public function actionCount()
    {
        $obNav = $this->getNav();
        $arFilter = array(
            'user_to_id' => $this->getParam('user_to_id'),
            'read' => $this->getParam('read'),
        );
        $obNav->setParam('filter', $arFilter);
        $iCount = (new Cache())
            ->setId($obNav)
            ->setDir(__CLASS__)
            ->setTime(0)
            ->setTag(Model::getTag())
            ->get(function () use ($obNav) {
                $obQuery = Model::getQuery($obNav)
                    ->countTotal(true);

                return $obQuery->exec()->getCount();
            });

        return $iCount;
    }

    public function actionReload()
    {
        return array('result' => 1, 'html' => $this->_renderMessage());
    }

    private function _renderMessage()
    {
        global $APPLICATION;
        ob_start();
        $APPLICATION->IncludeComponent('realweb:blank', 'message', array());

        return ob_get_clean();
    }
}
<?php

namespace Realweb\Site\Controller\Executor;

use Realweb\Site\Data\Cache;
use Realweb\Site\Model\User\Executor\Collection;
use Realweb\Site\Model\User\Executor\Model;

class Index extends \Realweb\Site\Main\Controller
{
    protected $module = 'executor';

    public function actionIndex()
    {
        $obNav = $this->getNav($this->getParam('page_size', 10));
        if ($this->getParam('show_all')) {
            $obNav->setAllRecords();
        }
        $arFilter = array(
            'id' => $this->getParam('id'),
        );
        $obNav->setParam('filter', $arFilter);
        if ($iPosition = $this->getRequestParams('position')) {
            $iPage = ceil($iPosition / $obNav->getPageSize());
            if ($iPage) {
                $obNav->setCurrentPage($iPage);
            }
        }
        $arResult = (new Cache())
            ->setId($obNav)
            ->setDir(__CLASS__)
            ->setTime(0)
            ->setTag(Model::getTag())
            ->get(function () use ($obNav) {
                $obCollection = Model::getObjectCollection($obNav);

                return array(
                    'collection' => $obCollection,
                    'navigation' => $obNav->getStructure()
                );
            });

        return $arResult;
    }

    /**
     * @return Collection
     */
    public function getCollection()
    {
        $arResult = $this->actionIndex();

        return $arResult['collection'];
    }
}
<?php

namespace Realweb\Site\Controller\Custom;

use Realweb\Site\Data\Cache;
use Realweb\Site\Model\Custom\Collection;
use Realweb\Site\Model\Custom\Entity;
use Realweb\Site\Model\Custom\Model;
use Realweb\Site\Site;
use Realweb\Site\User;

class Index extends \Realweb\Site\Main\Controller
{
    /**
     * @return array|mixed|Collection
     * @throws \Exception
     */
    public function actionIndex()
    {
        $obNav = $this->getNav();
        if ($this->getParam('show_all')) {
            $obNav->setAllRecords(true);
        }
        $arFilter = array(
            'user_id' => $this->getParam('user_id'),
            'order_id' => $this->getParam('order_id'),
        );
        $obNav->setParam('filter', $arFilter);
        $obCollection = (new Cache())
            ->setId($obNav)
            ->setDir(__CLASS__)
            ->setTime(0)
            ->setTag(Model::getTag())
            ->get(function () use ($obNav) {
                return Model::getObjectCollection($obNav);
            });

        return $obCollection;
    }

    /**
     * @return array|mixed|Collection
     * @throws \Exception
     */
    public function getCollection()
    {
        return $this->actionIndex();
    }

    public function actionAdd()
    {
        $arParams = $this->getRequestParams();
        if (!empty($arParams['id'])) {
            $this->setParam('user_id', User::getInstance()->getId());
            $this->setParam('order_id', $arParams['id']);
            $obCollection = $this->actionIndex();
            if ($obCollection->count() > 0) {
                $obCustom = $obCollection->getByOrder($arParams['id'], false);
            }
            if (empty($obCustom)) {
                $obCustom = new Entity();
            }
            $obCustom->setUfActive(1);
            $obCustom->setUfOrderId($arParams['id']);
            $obCustom->setUfUserId(User::getInstance()->getId());
            if ($obOrder = $obCustom->getOrder()) {
                $obCustom->setUfPrice($obOrder->getPrice());
                if ($obCustom->save()) {
                    \Realweb\Site\Model\Notice\Entity::create('order_taken', $obOrder->getCreatedBy(), $obOrder->getId());

                    return array(
                        'result' => 1,
                        'message' => 'Заказ взят в работу. Вы можете еще взять ' . User::getInstance()->getEntityExecutor()->getCountTextNewOrder() . ' в работу',
                        'url' => '/executors/work/',
                        'type' => 'popup'
                    );
                }
            }
        }
    }

    public function actionEnd()
    {
        $arParams = $this->getRequestParams();
        if (!empty($arParams['id'])) {
            /** @var Entity $obCustom */
            if ($obCustom = Model::getByPrimary($arParams['id'])) {
                if ($obOrder = $obCustom->getOrder()) {
                    if ($obCustom->isActive()) {
                        $obCustom->setUfActive(0);
                        $obCustom->save();
                        if ($obCollectionScreenshot = User::getInstance()->getEntityExecutor()->getScreenshot()->getByOrder($obCustom->getUfOrderId())->getNull()) {
                            /** @var \Realweb\Site\Model\Screenshot\Entity $obScreenshot */
                            foreach ($obCollectionScreenshot->getCollection() as $obScreenshot) {
                                $obScreenshot->delete();
                            }
                        }
                        if ($obCollectionScreenshot = User::getInstance()->getEntityExecutor()->getScreenshot()->getByOrderToPay($obCustom->getUfOrderId())) {
                            if ($fPrice = $obCollectionScreenshot->getPrice()) {
                                if (User::getInstance()->getEntityExecutor()->updateBalance(Site::getCommissionSum($fPrice, 'order'))) {
                                    $obOrder->setFullPrice($obOrder->getFullPrice() - $fPrice);
                                    $obOrder->save();

                                    /** @var \Realweb\Site\Model\Screenshot\Entity $obScreenshot */
                                    foreach ($obCollectionScreenshot->getCollection() as $obScreenshot) {
                                        $obScreenshot->setUfMoneyTaken(1);
                                        $obScreenshot->save();
                                    }
                                }
                            }
                        }
                    }

                    return array('result' => 1, 'message' => 'Заказ завершон');
                }
            }
        }
    }
}
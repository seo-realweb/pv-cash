<?php

namespace Realweb\Site\Controller\Enum;

use Realweb\Site\Data\Cache;
use Realweb\Site\Model\Enum\Collection;
use Realweb\Site\Model\Enum\Model;

class Index extends \Realweb\Site\Main\Controller
{
    /**
     * @return array|mixed|Collection
     * @throws \Exception
     */
    public function actionIndex()
    {
        $obNav = $this->getNav();
        $arFilter = array(
            'code' => $this->getParam('code'),
            'entity_id' => $this->getParam('entity_id'),
        );
        $obNav->setParam('filter', $arFilter);
        $obCollection = (new Cache())
            ->setId($obNav)
            ->setDir(__CLASS__)
            ->get(function () use ($obNav) {
                return Model::getObjectCollection($obNav);
            });

        return $obCollection;
    }
}
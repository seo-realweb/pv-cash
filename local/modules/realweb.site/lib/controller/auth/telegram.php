<?php

namespace Realweb\Site\Controller\Auth;

use Realweb\Site\Data\PageNavigation;
use Realweb\Site\Model\User\Entity;
use Realweb\Site\Model\User\Model;

class Telegram extends \Realweb\Site\Main\Controller
{
    public function actionAuth()
    {
        if ($strKey = $this->getRequestParams('auth_key')) {
            $obNav = new PageNavigation();
            $obNav->setParam('filter', array('confirm_code' => $strKey));
            /** @var Entity $obUser */
            $obUser = Model::getObject($obNav);
            if ($obUser->isExist()) {
                global $USER;
                $USER->Authorize($obUser->getId());

                return array('result' => 1, 'message' => 'Вы успешно авторизовались', 'url' => '/executors/');
            }
        }

        return array('result' => 0);
    }

    public function actionIndex()
    {
        header("HTTP/1.1 200 OK");
        header('Content-type: text/html; charset=utf-8');
        $obAuth = new \Realweb\Site\Model\Auth\Telegram();
        $obAuth->init();
    }
}
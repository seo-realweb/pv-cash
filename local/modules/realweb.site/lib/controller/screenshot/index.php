<?php

namespace Realweb\Site\Controller\Screenshot;

use Realweb\Site\ArrayHelper;
use Realweb\Site\Data\Cache;
use Realweb\Site\Main\Helper;
use Realweb\Site\Model\Screenshot\Collection;
use Realweb\Site\Model\Screenshot\Model;
use Realweb\Site\Model\Screenshot\Entity;
use Realweb\Site\User;

class Index extends \Realweb\Site\Main\Controller
{
    public function actionIndex()
    {
        $obNav = $this->getNav();
        if ($this->getParam('show_all')) {
            $obNav->setAllRecords(true);
        }
        $arFilter = array(
            'user_id' => $this->getParam('user_id'),
            'status' => $this->getParam('status'),
            'order_id' => $this->getParam('order_id'),
            'money_taken' => $this->getParam('money_taken'),
        );
        $obNav->setParam('filter', $arFilter);
        $arResult = (new Cache())
            ->setId($obNav)
            ->setTime(0)
            ->setDir(__CLASS__)
            ->setTag(Model::getTag())
            ->get(function () use ($obNav) {
                $obCollection = Model::getObjectCollection($obNav);

                return array(
                    'collection' => $obCollection,
                    'navigation' => $obNav->getStructure()
                );
            });
        if ($iScreenshotId = $this->getRequestParams('screenshot_id')) {
            /** @var Entity $obScreenshot */
            if ($obScreenshot = $arResult['collection']->getByKey($iScreenshotId)) {
                $obScreenshot->setExpanded();
            }
        }

        return $arResult;
    }

    /**
     * @return Collection
     */
    public function getCollection()
    {
        $arResult = $this->actionIndex();

        return $arResult['collection'];
    }

    public function actionCount()
    {
        $obNav = $this->getNav();
        $arFilter = array(
            'user_id' => $this->getParam('user_id'),
            'status' => $this->getParam('status'),
            'order_id' => $this->getParam('order_id'),
        );
        $obNav->setParam('filter', $arFilter);
        $iCount = (new Cache())
            ->setId($obNav)
            ->setTime(0)
            ->setTag(Model::getTag())
            ->setDir(__CLASS__)
            ->get(function () use ($obNav) {
                return Model::getQuery($obNav)->countTotal(true)->exec()->getCount();
            });

        return intval($iCount);
    }

    public function actionAdd()
    {
        $arParams = $this->getRequestParams();
        if (!empty($arParams['screenshot_file']) && !empty($arParams['id'])) {
            $arFiles = Helper::makeFileArray($arParams['screenshot_file']);
            foreach ($arFiles as $arFile) {
                if (!empty($arFile['tmp_name'])) {
                    $arFile['MODULE_ID'] = 'main';
                    if ($fileId = \CFile::SaveFile($arFile, 'uf')) {
                        $obScreenshot = new Entity();
                        $obScreenshot->setUfUserId(User::getInstance()->getId());
                        $obScreenshot->setUfOrderId($arParams['id']);
                        $obScreenshot->setUfFile($fileId);
                        $obScreenshot->setUfComment($arParams['comment']);
                        $obScreenshot->setUfMoneyTaken(0);
                        if (!$obScreenshot->getOrder()->isCompleted()) {
                            if ($obScreenshot->save()) {
                                \Realweb\Site\Model\Notice\Entity::create('screenshot_new', $obScreenshot->getOrder()->getCreatedBy(), $obScreenshot->getUfOrderId());
                            }
                        } else {
                            return array('result' => 0, 'message' => 'Заказ уже завершен');
                        }
                    }
                }
            }
            $obOrder = (new \Realweb\Site\Controller\Order\Index())->setParam('id', $arParams['id'])->actionElement();
            $obOrder->setExpanded();
            if ($obOrder->getCount() <= $obOrder->getCountAllScreenshot()) {
                \Realweb\Site\Model\Notice\Entity::create('screenshot_max', $obOrder->getCreatedBy(), $obOrder->getId());
            }

            return array('result' => 1, 'message' => 'Скринот(ы) добавлены', 'html' => $this->_renderOrder($obOrder));
        }

        return array('result' => 0, 'message' => 'Ошибка при добавлении скриншотов');
    }

    public function actionRemove()
    {
        $arParams = $this->getRequestParams();
        if (!empty($arParams['id'])) {
            /** @var Entity $obScreenshot */
            if ($obScreenshot = Model::getByPrimary($arParams['id'])) {
                if ($obScreenshot->getUfUserId() == User::getInstance()->getId()) {
                    if (!$obScreenshot->getOrder()->isCompleted()) {
                        if ($obScreenshot->isNull()) {
                            $obScreenshot->delete();
                            $obOrder = (new \Realweb\Site\Controller\Order\Index())->setParam('id', $obScreenshot->getUfOrderId())->actionElement();
                            $obOrder->setExpanded();

                            return array('result' => 1, 'message' => 'Скринот удален', 'html' => $this->_renderOrder($obOrder));
                        } else {
                            return array('result' => 0, 'message' => 'Скриншот удалить нельзя');
                        }
                    } else {
                        return array('result' => 0, 'message' => 'Заказ уже завершен');
                    }
                }
            }
        }

        return array('result' => 0, 'message' => 'Ошибка при удалении');
    }

    public function actionCancel()
    {
        $arParams = $this->getRequestParams();
        if (!empty($arParams['id'])) {
            /** @var Entity $obScreenshot */
            $obScreenshot = Model::getByPrimary($arParams['id']);
            if ($obScreenshot->isExist()) {
                $obScreenshot->cancel(ArrayHelper::getValue($arParams, 'prompt', ''));
            }
            $obOrder = (new \Realweb\Site\Controller\Order\Index())->setParam('id', $obScreenshot->getUfOrderId())->actionElement();
            $obOrder->setExpanded();

            return array('result' => 1, 'message' => 'Скринот отклонен', 'html' => $this->_renderOrder($obOrder, 'customers_order_item'));
        }

        return array('result' => 0, 'message' => 'Ошибка при добавлении скриншотов');
    }

    private function _renderOrder(\Realweb\Site\Model\Order\Element\Entity $obOrder, string $strComponent = 'executors_order_item')
    {
        global $APPLICATION;
        ob_start();
        $APPLICATION->IncludeComponent('realweb:blank', $strComponent, array(
            'item' => $obOrder,
        ));

        return ob_get_clean();
    }

    private function _renderScreenshot(Entity $obScreenshot)
    {
        global $APPLICATION;
        ob_start();
        $APPLICATION->IncludeComponent('realweb:blank', 'screenshot', array(
            'item' => $obScreenshot,
        ));

        return ob_get_clean();
    }

    public function actionConfirm()
    {
        $arParams = $this->getRequestParams();
        if (!empty($arParams['id'])) {
            /** @var Entity $obScreenshot */
            $obScreenshot = Model::getByPrimary($arParams['id']);
            if ($obScreenshot->isExist()) {
                if (!$obScreenshot->getOrder()->isCompleted()) {
                    if ($obScreenshot->confirm()) {
                        if ($obScreenshot->getOrder()->isCompleted()) {
                            $obScreenshot->getOrder()->remove();
                        }
                        $obOrder = (new \Realweb\Site\Controller\Order\Index())->setParam('id', $obScreenshot->getUfOrderId())->actionElement();
                        $obOrder->setExpanded();

                        return array('result' => 1, 'message' => 'Скринот принят', 'html' => $this->_renderOrder($obOrder, 'customers_order_item'));
                    }
                } else {
                    return array('result' => 0, 'type' => 'popup', 'message' => 'Скринот нельзя принять', 'html' => $this->_renderScreenshot($obScreenshot));
                }
            }
        }

        return array('result' => 0, 'message' => 'Ошибка при добавлении скриншотов');
    }

    public function actionSave()
    {
        $arParams = $this->getRequestParams();
        if (!empty($arParams['id'])) {
            /** @var Entity $obScreenshot */
            $obScreenshot = Model::getByPrimary($arParams['id']);
            if ($obScreenshot->isExist()) {
                if ($iRating = ArrayHelper::getValue($arParams, 'rating')) {
                    $obScreenshot->setUfRating($iRating);
                    $obScreenshot->save();
                }
            }

            return array('result' => 1, 'message' => 'Скринот сохранен', 'html' => $this->_renderScreenshot($obScreenshot));
        }

        return array('result' => 0, 'message' => 'Ошибка при добавлении скриншотов');
    }

    public function actionConfirmAll()
    {
        $orderId = $this->getParam('order_id', $this->getRequestParams('id'));
        if (!empty($orderId)) {
            $this->setParam('order_id', $orderId);
            $this->setParam('status', false);
            $obCollection = $this->getCollection();
            /** @var Entity $obScreenshot */
            foreach ($obCollection->getCollection() as $obScreenshot) {
                if (!$obScreenshot->getOrder()->isCompleted()) {
                    $obScreenshot->confirm();
                }
            }
            $obOrder = (new \Realweb\Site\Controller\Order\Index())->setParam('id', $orderId)->actionElement();
            $obOrder->setExpanded();
            if ($obOrder->isCompleted()) {
                $obOrder->remove();
            }

            return array('result' => 1, 'message' => 'Скриноты приняты', 'html' => $this->_renderOrder($obOrder, 'customers_order_item'));
        }

        return array('result' => 0, 'message' => 'Ошибка при принятии скриншотов');
    }
}
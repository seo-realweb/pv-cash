<?php

namespace Realweb\Site\Controller\Notice;

use Realweb\Site\Data\Cache;
use Realweb\Site\Model\Notice\Entity;
use Realweb\Site\Model\Notice\Model;
use Realweb\Site\Model\Notice\Collection;
use Realweb\Site\User;

class Index extends \Realweb\Site\Main\Controller
{
    protected $module = 'notice';

    public function actionIndex()
    {
        $obNav = $this->getNav(10);
        if ($this->getParam('show_all')) {
            $obNav->setAllRecords();
        }
        $arFilter = array(
            'user_id' => User::getInstance()->getId(),
            'active' => $this->getParam('active')
        );
        $obNav->setParam('filter', $arFilter);
        $arResult = (new Cache())
            ->setId($obNav)
            ->setDir(__CLASS__)
            ->setTag(Model::getTag())
            ->setTime(0)
            ->get(function () use ($obNav) {
                $obCollection = Model::getObjectCollection($obNav);

                return array(
                    'collection' => $obCollection,
                    'navigation' => $obNav->getStructure()
                );
            });

        return $arResult;
    }

    /**
     * @return Collection
     */
    public function getCollection()
    {
        $arResult = $this->actionIndex();

        return $arResult['collection'];
    }

    public function actionClear()
    {
        if (User::getInstance()->isAuth()) {
            User::getInstance()->getEntityExecutor()->getNotice()->clear();

            return array('result' => 1, 'count' => 0, 'html' => $this->_renderComponent('realweb:blank', 'notice'));
        }
    }

}
<?php

namespace Realweb\Site\Controller\Order;

use Bitrix\Main\Type\DateTime;
use Realweb\Site\Data\Cache;
use Realweb\Site\Main\ArrayHelper;
use Realweb\Site\Main\Helper;
use Realweb\Site\Model\Custom;
use Realweb\Site\Model\Order\Element;
use Realweb\Site\Model\Screenshot;
use Realweb\Site\User;

class Index extends \Realweb\Site\Main\Controller
{
    protected $module = 'order';

    public function actionIndex()
    {
        $obNav = $this->getNav($this->getParam('page_size', 10));
        if ($this->getParam('show_all')) {
            $obNav->setAllRecords();
        }
        $arFilter = array(
            'active' => $this->getParam('active', 'Y'),
            'active_count' => $this->getParam('active_count'),
            'active_custom' => $this->getParam('active_custom', 1),
            'completed' => $this->getParam('completed'),
        );
        if ($strServer = $this->getRequestParams('server')) {
            $arFilter['server'] = $strServer;
            $obNav->setQuery('server', $strServer);
        }
        if ($strQuery = $this->getRequestParams('query')) {
            $arFilter['query'] = $strQuery;
            $obNav->setQuery('query', $strQuery);
        }
        if ($executorId = $this->getParam('executor_id')) {
            $arFilter['executor_id'] = $executorId;
        }
        if ($customerId = $this->getParam('customer_id')) {
            $arFilter['customer_id'] = $customerId;
        }
        $obNav->setParam('filter', $arFilter);
        $arResult = (new Cache())
            ->setId($obNav)
            ->setDir(__CLASS__)
            ->setTime(0)
            ->setTag(Element\Model::getTag())
            ->get(function () use ($obNav) {
                /** @var Element\Collection $obCollection */
                $obCollection = Element\Model::getObjectCollection($obNav);
                $obCollection->getLocation();
                $obCollection->getScreenshot();
                $obNav->setMoreText('Показать еще ' . $obNav->getMoreCount() . ' ' . Helper::morph($obNav->getMoreCount(), 'следующий', 'следующих', 'следующих') . ' ' . Helper::morph($obNav->getMoreCount(), 'заказ', 'заказа', 'заказов'));

                return array(
                    'collection' => $obCollection,
                    'navigation' => $obNav->getStructure()
                );
            });
        /** @var Element\Collection $obCollection */
        $obCollection = $arResult['collection'];
        if ($iOrderId = $this->getRequestParams('order_id')) {
            if ($obElement = $obCollection->getByKey($iOrderId)) {
                $obElement->setExpanded();
                $obCollection->prependItem($obElement);
            } else {
                $this->setParam('id', $iOrderId);
                if ($obElement = $this->actionElement()) {
                    $obElement->setExpanded();
                    $obCollection->prependItem($obElement);
                }
            }
            if ($iScreenshotId = $this->getRequestParams('screenshot_id')) {
                /** @var Screenshot\Entity $obScreenshot */
                if ($obScreenshot = $obElement->getScreenshot()->getByKey($iScreenshotId)) {
                    $obScreenshot->setExpanded();
                }
            }
        }

        return $arResult;
    }

    public function getCollection()
    {
        $arResult = $this->actionIndex();

        return $arResult['collection'];
    }

    /**
     * @return array|mixed|Element\Collection
     * @throws \Exception
     */
    public function findCollection()
    {
        $obNav = $this->getNav();
        $obNav->setAllRecords();
        $arFilter = array(
            'id' => $this->getParam('id'),
            'customer_id' => $this->getParam('customer_id'),
        );
        $obNav->setParam('filter', $arFilter);
        $obCollection = (new Cache())
            ->setId($obNav)
            ->setDir(__CLASS__)
            ->setTime(0)
            ->setTag(Element\Model::getTag())
            ->get(function () use ($obNav) {
                /** @var Element\Collection $obCollection */
                $obCollection = Element\Model::getObjectCollection($obNav);
                $obCollection->getLocation();
                $obCollection->getScreenshot();

                return $obCollection;
            });

        return $obCollection;
    }

    /**
     * @return \Realweb\Site\Data\Data|Element\Entity
     * @throws \Exception
     */
    public function actionElement()
    {
        $obNav = $this->getNav(1);
        $arFilter = array(
            'id' => $this->getParam('id'),
            'customer_id' => $this->getParam('customer_id'),
            //'active' => $this->getParam('active', 'Y')
        );
        $obNav->setParam('filter', $arFilter);
        /** @var Element\Collection $obCollection */
        $obCollection = (new Cache())
            ->setId($obNav)
            ->setDir(__CLASS__)
            ->setTime(0)
            ->setTag(Element\Model::getTag())
            ->get(function () use ($obNav) {
                /** @var Element\Collection $obCollection */
                $obCollection = Element\Model::getObjectCollection($obNav);
                $obCollection->getLocation();
                $obCollection->getScreenshot();

                return $obCollection;
            });

        return $obCollection->current();
    }

    public function actionGetSum()
    {
        $arSum = (new Cache())
            ->setId(__FUNCTION__)
            ->setDir(__CLASS__)
            ->setTime(0)
            ->setTag(Element\Model::getTag())
            ->get(function () {
                return Element\Model::getSum();
            });

        return array('result' => 1, 'sum' => number_format($arSum['SUM_PRICE'] * $arSum['SUM_COUNT'], 0, '.', ' ') . '₽');
    }

    public function actionGetRangePrice()
    {
        $arResult = array(
            'min' => 0,
            'max' => 0,
            'range' => array()
        );
        $arPrice = (new Cache())
            ->setId(__FUNCTION__)
            ->setDir(__CLASS__)
            ->setTime(0)
            ->setTag(Element\Model::getTag())
            ->get(function () {
                return Element\Model::getPrice();
            });
        if ($arPrice) {
            $arResult['max'] = $iMax = ArrayHelper::getValue($arPrice, 'MAX_PRICE');
            $arResult['min'] = $iMin = ArrayHelper::getValue($arPrice, 'MIN_PRICE');
            if ($iMax && $iMin) {
                $iDiff = $iMax - $iMin;
                if ($iDiff) {
                    $iStep = round($iDiff / 4);
                    $arResult['range'] = range($iMin, $iMax, $iStep);
                }
            }
        }

        return $arResult;
    }

    public function actionGetStatusPrice()
    {
        if ($fPrice = $this->getParam('price', $this->getRequestParams('price'))) {
            $iStatus = 0;
            $arData = $this->actionGetRangePrice();
            $arRange = $arData['range'];
            if ($fPrice < ArrayHelper::getValue($arRange, 0)) {
                $iStatus = -2;
            } elseif ($fPrice >= ArrayHelper::getValue($arRange, 0) && $fPrice < ArrayHelper::getValue($arRange, 1)) {
                $iStatus = -1;
            } elseif ($fPrice >= ArrayHelper::getValue($arRange, 1) && $fPrice < ArrayHelper::getValue($arRange, 2)) {
                $iStatus = 0;
            } elseif ($fPrice >= ArrayHelper::getValue($arRange, 2) && $fPrice < ArrayHelper::getValue($arRange, 3)) {
                $iStatus = 1;
            } elseif ($fPrice >= ArrayHelper::getValue($arRange, 3)) {
                $iStatus = 2;
            }

            return array('result' => 1, 'status' => $iStatus);
        }
    }

    private function _save(array $arParams)
    {
        if (!empty($arParams['id'])) {
            $obOrder = Element\Model::getByPrimary($arParams['id']);
            $isNew = false;
        } else {
            $obOrder = new Element\Entity();
            $isNew = true;
        }
        if ($isNew && User::getInstance()->getEntityCustomer()->getRating()->getUfCountNewOrder() <= User::getInstance()->getEntityCustomer()->getCountActiveOrder()) {
            return array('result' => 0, 'message' => 'На данном уровне нельзя создать больше заказов. Повышайте свой уровень для того чтобы создать больше заказов.');
        }
        if ($obOrder->isExist() && $obOrder->getCreatedBy() != User::getInstance()->getId()) {
            return array('result' => 0, 'message' => 'Ошибка');
        }
        $fPrice = doubleval(ArrayHelper::getValue($arParams, 'price'));
        $iCount = intval(ArrayHelper::getValue($arParams, 'count'));
        $iCountOld = 0;
        $fPriceOld = 0;
        if ($obOrder->isExist()) {
            $iCountOld = $obOrder->getCount();
            $fPriceOld = $obOrder->getPriceValue();
            if ($obOrder->getPrice() > $fPrice) {
                return array('result' => 0, 'message' => 'Цена не может быть меньше текущей');
            }
            if ($obOrder->getCount() > $iCount) {
                return array('result' => 0, 'message' => 'Количество убийств не может меньше текущих');
            }
        }
        if ($fPrice <= 1) {
            return array('result' => 0, 'message' => 'Минимальная цена на убийство 2 ₽');
        }
        $obOrder->setActive('Y');
        $obOrder->setName('Заказ от ' . date('d.m.Y H:i:s'));
        $obOrder->setCount($iCount);
        $obOrder->setPrice($fPrice);
        $obOrder->setNickname(ArrayHelper::getValue($arParams, 'nickname', ''));
        $obOrder->setLevelId(ArrayHelper::getValue($arParams, 'level'));
        $obOrder->setServerId(ArrayHelper::getValue($arParams, 'server'));
        $obOrder->setLocationId(ArrayHelper::getValue($arParams, 'location'));
        $obOrder->setRush(ArrayHelper::getValue($arParams, 'rush', 0));
        $obOrder->setPreviewText(ArrayHelper::getValue($arParams, 'comment', ''));
        $obOrder->setClass(ArrayHelper::getValue($arParams, 'class', ''));
        $obOrder->setDateCreate(new DateTime());
        $obOrder->setTest(0);
        if ($obOrder->isExist()) {
            $fFullPrice = $obOrder->getCount() * $obOrder->getPriceValue() - $iCountOld * $fPriceOld;
            $obOrder->setFullPrice($obOrder->getFullPrice() + $fFullPrice);
        } else {
            $fFullPrice = $obOrder->getSum();
            $obOrder->setFullPrice($fFullPrice);
        }
        if (User::getInstance()->getEntityCustomer()->getBalanceValue() < $fFullPrice) {
            return array(
                'result' => 0,
                'message' => 'На вашем счете недостаточно средств. <a href="#" data-price="' . ($fFullPrice - User::getInstance()->getEntityCustomer()->getBalanceValue()) . '" data-target="#popup-purse" data-position="center" class="purple js-popup-open js-open-purpose">Пополнить</a>'
            );
        }

        if ($obOrder->save()) {
            if ($fFullPrice) {
                User::getInstance()->getEntityCustomer()->updateBalance(-$fFullPrice);
            }
            if ($isNew) {
                User::getInstance()->setSessionValue('new_order_id', $obOrder->getId());
                \Realweb\Site\Model\Notice\Entity::create('order_new', User::getInstance()->getId(), $obOrder->getId());
                User::getInstance()->getEntityCustomer()->refresh();
                if (User::getInstance()->getEntityCustomer()->getRating()->getUfCountNewOrder() <= User::getInstance()->getEntityCustomer()->getCountActiveOrder()) {
                    \Realweb\Site\Model\Notice\Entity::create('order_max', User::getInstance()->getId());
                }
            } else {
                /** @var Custom\Entity $obCustom */
                foreach ($obOrder->getCustom()->getActive()->getCollection() as $obCustom) {
                    \Realweb\Site\Model\Notice\Entity::create('order_change', $obCustom->getUfUserId(), $obCustom->getUfOrderId());
                }
            }

            return array('result' => 1, 'message' => 'Заказ успешно сохранен', 'url' => '/customers/');
        } else {
            return array('result' => 0, 'message' => $obOrder->getCIblockElement()->LAST_ERROR);
        }
    }

    public function actionSave()
    {
        $arParams = $this->getRequestParams();
        if ($arParams) {
            return $this->_save($arParams);
        } else {
            return array('result' => 0, 'message' => 'Ошибка при добавлении');
        }
    }

    public function actionDelete()
    {
        $arParams = $this->getRequestParams();
        if ($arParams) {
            if (!empty($arParams['id'])) {
                /** @var Element\Entity $obOrder */
                $obOrder = Element\Model::getByPrimary($arParams['id']);
                if ($obOrder->isExist() && $obOrder->getCreatedBy() == User::getInstance()->getId()) {
                    if ($obOrder->remove()) {
                        return array('result' => 1, 'message' => 'Заказ удален');
                    }
                }
            }
        }

        return array('result' => 0, 'message' => 'Ошибка');
    }

    public function actionRepeat()
    {
        $arParams = $this->getRequestParams();
        if ($arParams) {
            if (!empty($arParams['id'])) {
                /** @var Element\Entity $obOrder */
                $obOrder = Element\Model::getByPrimary($arParams['id']);
                if ($obOrder->isExist() && $obOrder->getCreatedBy() == User::getInstance()->getId()) {
                    $arResult = $this->_save(array(
                        'nickname' => $obOrder->getNickname(),
                        'level' => $obOrder->getLevelId(),
                        'server' => $obOrder->getServerId(),
                        'location' => $obOrder->getLocation()->getKeys(),
                        'rush' => $obOrder->getRush(),
                        'comment' => $obOrder->getPreviewText(),
                        'class' => $obOrder->getClass(),
                        'price' => $obOrder->getPrice(),
                        'count' => $obOrder->getCount(),
                    ));
                    $arResult['type'] = 'popup';

                    return $arResult;
                }
            }
        }
    }
}
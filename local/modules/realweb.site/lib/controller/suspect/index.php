<?php

namespace Realweb\Site\Controller\Suspect;

use Realweb\Site\Data\Cache;
use Realweb\Site\Model\Suspect\Collection;
use Realweb\Site\Model\Suspect\Model;

class Index extends \Realweb\Site\Main\Controller
{
    /**
     * @return array|mixed|Collection
     * @throws \Exception
     */
    public function actionIndex()
    {
        $obNav = $this->getNav();
        $obCollection = (new Cache())
            ->setId($obNav)
            ->setDir(__CLASS__)
            ->get(function () use ($obNav) {
                return Model::getObjectCollection($obNav);
            });

        return $obCollection;
    }
}
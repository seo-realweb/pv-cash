<?php

namespace Realweb\Site\Controller\Transaction;

use Realweb\Site\Data\Cache;
use Realweb\Site\Model\Transaction\Model;
use Realweb\Site\Model\Transaction\Collection;
use Realweb\Site\User;

class Index extends \Realweb\Site\Main\Controller
{
    protected $module = 'transaction';

    public function actionIndex()
    {
        $obNav = $this->getNav(10);
        $arFilter = array(
            'user_id' => User::getInstance()->getId(),
        );
        $obNav->setParam('filter', $arFilter);
        $arResult = (new Cache())
            ->setId($obNav)
            ->setDir(__CLASS__)
            ->setTag(Model::getTag())
            ->setTime(0)
            ->get(function () use ($obNav) {
                $obCollection = Model::getObjectCollection($obNav);

                return array(
                    'collection' => $obCollection,
                    'navigation' => $obNav->getStructure()
                );
            });

        return $arResult;
    }

    /**
     * @return Collection
     */
    public function getCollection()
    {
        $arResult = $this->actionIndex();

        return $arResult['collection'];
    }
}
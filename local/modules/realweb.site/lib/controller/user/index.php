<?php

namespace Realweb\Site\Controller\User;

use Bitrix\Main\Mail\Event;
use Realweb\Site\Data\Cache;
use Realweb\Site\Model\User\Collection;
use Realweb\Site\Model\User\Model;
use Realweb\Site\User;
use Realweb\Site\Model\User\Executor;
use Realweb\Site\Model\User\Customer;

class Index extends \Realweb\Site\Main\Controller
{
    protected $module = 'user';

    public function actionIndex()
    {
        $obNav = $this->getNav();
        $arFilter = array(
            'id' => $this->getParam('id'),
        );
        $obNav->setParam('filter', $arFilter);
        $arResult = (new Cache())
            ->setId($obNav)
            ->setDir(__CLASS__)
            ->setTime(0)
            ->setTag(Model::getTag())
            ->get(function () use ($obNav) {
                $obCollection = Model::getObjectCollection($obNav);

                return array(
                    'collection' => $obCollection,
                    'navigation' => $obNav->getStructure()
                );
            });

        return $arResult;
    }

    /**
     * @return mixed|Collection
     */
    public function getCollection()
    {
        $arResult = $this->actionIndex();

        return $arResult['collection'];
    }

    public function actionSave()
    {
        $obUser = User::getInstance()->getEntityExecutor();
        $arParams = $this->getRequestParams();
        if (!empty($arParams['password']) && !empty($arParams['confirm_password'])) {
            if ($arParams['password'] == $arParams['confirm_password']) {
                if ($obUser->setPassword($arParams['password'])->save()) {
                    return array('result' => 1, 'message' => 'Пароль сохранен');
                } else {
                    return array('result' => 0, 'message' => implode("<br>", $obUser->getOrmResult()->getErrorMessages()));
                }
            } else {
                return array('result' => 0, 'message' => 'Пароли не совпадают');
            }
        } elseif (!empty($arParams['email'])) {
            if ($obUser->setEmail($arParams['email'])->save()) {
                return array('result' => 1, 'message' => 'Email сохранен');
            } else {
                return array('result' => 0, 'message' => implode("<br>", $obUser->getOrmResult()->getErrorMessages()));
            }
        } elseif (!empty($arParams['login'])) {
            if ($obUser->setLogin($arParams['login'])->save()) {
                return array('result' => 1, 'message' => 'Никнейм сохранен');
            } else {
                return array('result' => 0, 'message' => implode("<br>", $obUser->getOrmResult()->getErrorMessages()));
            }
        } elseif (!empty($arParams['photo'])) {
            if ($obUser->setPersonalPhoto($arParams['photo'])->save()) {
                return array('result' => 1, 'message' => 'Фото сохранено');
            } else {
                return array('result' => 0, 'message' => implode("<br>", $obUser->getOrmResult()->getErrorMessages()));
            }
        }
    }

    public function actionSupport()
    {
        $arParams = $this->getRequestParams();
        if (!empty($arParams['message'])) {
            Event::sendImmediate(array(
                "EVENT_NAME" => 'EVENT_SUPPORT',
                "LID" => "s1",
                "C_FIELDS" => array(
                    'MESSAGE' => $arParams['message'],
                    'EMAIL' => User::getInstance()->getEntityExecutor()->getEmail(),
                    'LOGIN' => User::getInstance()->getEntityExecutor()->getLogin(),
                ),
            ));

            return array('result' => 1, 'message' => 'Сообщение успешно отправлено');
        } else {
            return array('result' => 0, 'message' => 'Ошибка');
        }
    }

    public function actionReload()
    {
        if (User::getInstance()->isAuth()) {
            $arResult = array(
                'result' => 1,
                'executor' => $this->_renderUser(User::getInstance()->getEntityExecutor()),
                'customer' => $this->_renderUser(User::getInstance()->getEntityCustomer()),
                'notice' => array(
                    'count' => User::getInstance()->getEntityExecutor()->getNotice()->count(),
                    'html' => $this->_renderComponent('realweb:blank', 'notice')
                ),
                'auth' => $this->_renderAuth()
            );
            User::getInstance()->getEntityExecutor()->getNotice()->clear();

            return $arResult;
        } else {
            return array(
                'result' => 0
            );
        }
    }

    public function actionProfile()
    {
        if ($iUserId = $this->getRequestParams('id')) {
            $obExecutor = Executor\Model::getByPrimary($iUserId);
            $obCustomer = Customer\Model::getByPrimary($iUserId);
            $bExtended = !!$this->getRequestParams('extended');
            if ($obExecutor->isExist()) {
                return array(
                    'result' => 1,
                    'type' => 'popup',
                    'message' => $this->_renderUser(array($obExecutor, $obCustomer), $bExtended)
                );
            }
        }
    }

    private function _renderUser($obUser, bool $bExtended = false)
    {
        global $APPLICATION;
        ob_start();
        $APPLICATION->IncludeComponent('realweb:blank', 'user', array(
            'user' => $obUser,
            'extended' => $bExtended
        ));

        return ob_get_clean();
    }

    private function _renderAuth()
    {
        global $APPLICATION;
        ob_start();
        $APPLICATION->IncludeComponent('realweb:blank', 'auth', array());

        return ob_get_clean();
    }

}
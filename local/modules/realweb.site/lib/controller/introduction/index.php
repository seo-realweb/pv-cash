<?php

namespace Realweb\Site\Controller\Introduction;

use Realweb\Site\Data\Cache;
use Realweb\Site\Model\Introduction\Element\Collection;
use Realweb\Site\Model\Introduction\Element\Model;
use Realweb\Site\Model\Introduction\Element\Entity;

class Index extends \Realweb\Site\Main\Controller
{
    public function actionIndex()
    {
        $obNav = $this->getNav();
        $arFilter = array(
            'id' => $this->getParam('id'),
            'code' => $this->getParam('code'),
        );
        $obNav->setParam('filter', $arFilter);
        $arResult = (new Cache())
            ->setId($obNav)
            ->setDir(__CLASS__)
            ->setTag('iblock_id_' . Model::getIblockId())
            ->get(function () use ($obNav) {
                /** @var Collection $obCollection */
                $obCollection = Model::getObjectCollection($obNav);

                return array(
                    'collection' => $obCollection,
                    'navigation' => $obNav->getStructure()
                );
            });

        return $arResult;
    }

    /**
     * @return \Realweb\Site\Data\Data|Entity
     */
    public function actionElement()
    {
        $arResult = $this->actionIndex();
        /** @var Collection $obCollection */
        $obCollection = $arResult['collection'];
        if ($obCollection->current()) {
            return $obCollection->current();
        }
    }

    /**
     * @return Collection
     */
    public function getCollection()
    {
        $arResult = $this->actionIndex();

        return $arResult['collection'];
    }
}
<?php

namespace Realweb\Site\Controller\Level;

use Realweb\Site\Data\Cache;
use Realweb\Site\Model\Level\Collection;
use Realweb\Site\Model\Level\Model;

class Index extends \Realweb\Site\Main\Controller
{
    public function actionIndex()
    {
        $obNav = $this->getNav();
        $obCollection = (new Cache())
            ->setId($obNav)
            ->setDir(__CLASS__)
            ->get(function () use ($obNav) {
                return Model::getObjectCollection($obNav);
            });

        return $obCollection;
    }

    /**
     * @return Collection
     */
    public function getCollection()
    {
        $obCollection = $this->actionIndex();

        return $obCollection;
    }
}
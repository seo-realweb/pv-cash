<?php


namespace Realweb\Site;


use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Type\DateTime;
use Realweb\Site\Controller\Server;
use Realweb\Site\Controller\Location;
use Realweb\Site\Controller\Level;
use Realweb\Site\Controller\Suspect;
use Realweb\Site\Data\PageNavigation;
use Realweb\Site\Model\Blacklist;
use Realweb\Site\Model\Order;
use Realweb\Site\Model\Payment\Unitpay\Payment;
use Realweb\Site\Model\Screenshot;
use Realweb\Site\Model\Transaction;
use Realweb\Site\Model\User\Entity;
use Realweb\Site\Model\User\Executor;
use Realweb\Site\Model\User\Customer;

/**
 * Class User
 * @package Realweb\Site
 */
class User extends \Realweb\Site\Main\User
{
    const FLASH_MESSAGE = 'flash_message';
    const BLACK_LIST_MESSAGE = 'Вы попали в черный список, обратитесь к администратору.';
    const PAYMENT_BACK_URL = 'payment_back_url';
    /**
     * @var Entity
     */
    private $_entity_executor = null;

    /**
     * @var Entity
     */
    private $_entity_customer = null;

    protected function __construct()
    {
        parent::__construct();
        if ($this->isAuth()) {
            $this->updateLastAction();
        }
    }

    public function removeTestOrder()
    {
        if ($this->isAdmin()) {
            /** @var \Realweb\Site\Model\Order\Element\Entity $obItem */
            foreach ($this->getEntityExecutor()->getOrder()->getCollection() as $obItem) {
                if ($obItem->isTest() && $obItem->isCompleted()) {
                    $obItem->remove();
                }
            }
        }
    }

    public function getEntityExecutor()
    {
        if ($this->_entity_executor === null) {
            if ($this->getId()) {
                $this->_entity_executor = Executor\Model::getByPrimary($this->getId());
            }
            if ($this->_entity_executor == null) {
                $this->_entity_executor = new Executor\Entity();
            }
        }

        return $this->_entity_executor;
    }

    public function getEntityCustomer()
    {
        if ($this->_entity_customer === null) {
            if ($this->getId()) {
                $this->_entity_customer = Customer\Model::getByPrimary($this->getId());
            }
            if ($this->_entity_customer == null) {
                $this->_entity_customer = new Customer\Entity();
            }
        }

        return $this->_entity_customer;
    }

    public function updateLastAction()
    {
        if (empty($_SESSION['LAST_ACTION']) || $_SESSION['LAST_ACTION'] + 100 < time()) {
            $_SESSION['LAST_ACTION'] = time();
            $this->getEntityExecutor()->updateLastAction();
        }
    }

    public function existBlackList()
    {
        $obNav = new PageNavigation();
        $obNav->setParam('filter', array(
            'ip' => Site::getIp()
        ));
        $obItem = Blacklist\Model::getObject($obNav);

        return $obItem->isExist();
    }

    public function checkUser()
    {
        if ($this->isAuth() && !$this->isAdmin() && $this->existBlackList()) {
            $this->logout();
            $this->setFlashMessage('danger', self::BLACK_LIST_MESSAGE);
            LocalRedirect('/');
        }
    }

    public function getFlashMessage()
    {
        $arMessages = $this->getSessionValue(self::FLASH_MESSAGE);
        if ($arMessages) {
            $arMessage = array_shift($arMessages);
            $this->setSessionValue(self::FLASH_MESSAGE, $arMessages);

            return $arMessage;
        }
    }

    public function setFlashMessage(string $strType, string $strMessage)
    {
        $arMessages = $this->getSessionValue(self::FLASH_MESSAGE);
        $arMessages[] = array(
            'type' => $strType,
            'message' => $strMessage
        );
        $this->setSessionValue(self::FLASH_MESSAGE, $arMessages);

        return $this;
    }

    public function setPaymentBackUrl(string $strUrl)
    {
        $this->setSessionValue(self::PAYMENT_BACK_URL, $strUrl);

        return $this;
    }

    public function getPaymentBackUrl()
    {
        return $this->getSessionValue(self::PAYMENT_BACK_URL);
    }

    public static function agentUpdateUsers()
    {
        Loader::includeModule('iblock');
        $arItems = Executor\Model::getQuery(null)
            ->setOrder(array('COUNT_SUCCESS_SCREENSHOT' => 'DESC'))
            ->exec()
            ->fetchAll();
        foreach ($arItems as $key => $arItem) {
            $obUser = new Executor\Entity($arItem);
            $iPosition = $key + 1;
            if ($obUser->getUfExecutorPosition() != $iPosition) {
                if ($iPosition < 11) {
                    if ($obUser->getUfExecutorPosition() > 10) {
                        \Realweb\Site\Model\Notice\Entity::create('top', $obUser->getId());
                    }
                    if ($obUser->getUfExecutorPosition()) {
                        $iDiff = $obUser->getUfExecutorPosition() - $iPosition;
                        \Realweb\Site\Model\Notice\Entity::create('top_change', $obUser->getId(), $iDiff > 0 ? ('+' . $iDiff) : $iDiff);
                    }
                }
                $obUser->setUfExecutorPosition($iPosition);
                $obUser->save();
            }
        }

        $arItems = Customer\Model::getQuery(null)
            ->setOrder(array('COUNT_SUCCESS_SCREENSHOT' => 'DESC'))
            ->exec()
            ->fetchAll();
        foreach ($arItems as $key => $arItem) {
            $obUser = new Customer\Entity($arItem);
            $iPosition = $key + 1;
            if ($obUser->getUfCustomerPosition() != $iPosition) {
//                if ($iPosition < 11) {
//                    if ($obUser->getUfCustomerPosition() > 10) {
//                        \Realweb\Site\Model\Notice\Entity::create('top', $obUser->getId());
//                    }
//                    $iDiff = $obUser->getUfCustomerPosition() - $iPosition;
//                    \Realweb\Site\Model\Notice\Entity::create('top_change', $obUser->getId(), $iDiff > 0 ? ('+' . $iDiff) : $iDiff);
//                }
                $obUser->setUfCustomerPosition($iPosition);
                $obUser->save();
            }
        }

        return '\Realweb\Site\User::agentUpdateUsers();';
    }

    public static function agentSendNotice()
    {
        Loader::includeModule('iblock');
        $arItems = Screenshot\Model::getQuery(null)
            ->whereNull('STATUS_XML_ID')
            ->where('UF_MONEY_TAKEN', '=', 0)
            ->where('UF_CREATED', '<=', new DateTime(date('d.m.Y H:i:s', time() - 3600 * 23)))
            ->exec()
            ->fetchAll();
        foreach ($arItems as $arItem) {
            $obScreenshot = new Screenshot\Entity($arItem);
            \Realweb\Site\Model\Notice\Entity::create('screenshot_auto', $obScreenshot->getOrder()->getCreatedBy(), $obScreenshot->getAutoConfirmTime());
        }

        return '\Realweb\Site\User::agentSendNotice();';
    }

    public static function agentConfirmScreenshot()
    {
        Loader::includeModule('iblock');
        $arItems = Screenshot\Model::getQuery(null)
            ->whereNull('STATUS_XML_ID')
            ->where('UF_MONEY_TAKEN', '=', 0)
            ->where('UF_CREATED', '<=', new DateTime(date('d.m.Y H:i:s', strtotime('-1 days'))))
            ->exec()
            ->fetchAll();
        $obCollection = new Screenshot\Collection();
        foreach ($arItems as $arItem) {
            $obCollection->addItem(new Screenshot\Entity($arItem));
        }
        $obCollection->getOrders();
        $obCollection->getCustom();
        if ($obCollection->count() > 0) {
            $obOrderCollection = new Order\Element\Collection();
            /** @var Screenshot\Entity $obItem */
            foreach ($obCollection->getCollection() as $obItem) {
                if (!$obItem->getOrder()->isCompleted()) {
                    $obItem->getOrder()->setCountScreenshot($obItem->getOrder()->getCountScreenshot() + 1);
                    $obItem->confirm();
                } else {
                    $obOrderCollection->addItem($obItem->getOrder());
                }
            }
            /** @var Order\Element\Entity $obItem */
            foreach ($obOrderCollection->getCollection() as $obItem) {
                $obItem->remove();
            }
        }

        return '\Realweb\Site\User::agentConfirmScreenshot();';
    }

    public static function agentCheckUser()
    {
        $arGroupScreenshot = array();
        $arItems = Screenshot\Model::getQuery(null)
            ->whereNotNull('STATUS_XML_ID')
            ->exec()
            ->fetchAll();
        foreach ($arItems as $arItem) {
            $obScreenshot = new Screenshot\Entity($arItem);
            $arGroupScreenshot[$obScreenshot->getUfOrderId()][] = $obScreenshot;
        }
        $arUsers = array();
        foreach ($arGroupScreenshot as $iOrderId => $arItems) {
            if (count($arItems) > 3) {
                $iSuccess = 0;
                $iFail = 0;
                $iFailCount = 0;
                $arFailUser = array();
                $bFail = false;
                /** @var Screenshot\Entity $obItem */
                foreach ($arItems as $obItem) {
                    if ($obItem->isSuccess()) {
                        $iSuccess++;
                        $iFailCount = 0;
                    } else {
                        $arFailUser[$obItem->getUfUserId()] = $obItem->getUfUserId();
                        $iFailCount++;
                        $iFail++;
                        if ($iFailCount >= 3 && count($arFailUser) > 1) {
                            $bFail = true;
                        }
                    }
                }
                if ($iFail > $iSuccess || $bFail) {
                    if ($obItem = current($arItems)) {
                        $arUsers[$obItem->getOrder()->getCreatedBy()] = $obItem->getOrder()->getCreatedBy();
                    }
                }
            }
        }
        if ($arUsers) {
            $obCollectionSuspect = (new Suspect\Index())->actionIndex();
            foreach ($arUsers as $iUserId) {
                if (!$obCollectionSuspect->getByUser($iUserId)) {
                    $obSuspect = new Model\Suspect\Entity();
                    $obSuspect->setUfUserId($iUserId);
                    $obSuspect->save();
                    $obCollectionSuspect->addItem($obSuspect);
                }
            }
        }

        return '\Realweb\Site\User::agentCheckUser();';
    }

    public static function agentMassPayment()
    {
        $arItems = Executor\Model::getQuery(null)
            ->where('UF_BALANCE_HOLD', '>', 0)
            ->exec()
            ->fetchAll();
        foreach ($arItems as $key => $arItem) {
            $obUser = new Executor\Entity($arItem);
            $fBalanceHold = 0;
            $fBalance = 0;

            if ($obUser->isWithdrawal()) {
                $obNav = new PageNavigation();
                $obNav->setAllRecords();
                $arFilter =  array(
                    'user_id' => $obUser->getId(),
                    'status' => array('new', 'process'),
                    'sum' => false,
                );
                if ($obDate = $obUser->getDateWithdrawal()) {
                    $arFilter['date_to'] = $obDate;
                }
                $obNav->setParam('filter', $arFilter);
                $obCollection = Transaction\Model::getObjectCollection($obNav);
                if ($obCollection->count() > 0) {
                    /** @var Transaction\Entity $obItem */
                    foreach ($obCollection->getCollection() as $obItem) {
                        $obPayment = new Payment();
                        if ($obItem->isNew()) {
                            $result = $obPayment->massPayment($obItem->getId(), $obItem->getAbsSum(), $obItem->getParam('purse'), $obItem->getParam('type'));
                            if ($result['result'] == 1) {
                                //$fBalanceHold += $obItem->getAbsSum();
                                $obItem->setProcessStatus();
                                //\Realweb\Site\Model\Notice\Entity::create('other', $obUser->getId(), 'Выплата прошла успешно #' . $obItem->getId());
                            } else {
                                $fBalance += $obItem->getAbsSum();
                                $obItem->setFailStatus();
                                \Realweb\Site\Model\Notice\Entity::create('other', $obUser->getId(), 'Выплата не прошла #' . $obItem->getId() . ' ' . $result['error']);
                            }
                        } else {
                            $result = $obPayment->massPaymentStatus($obItem->getId());
                            if ($result['result'] == 1) {
                                if ($result['status'] == 'success') {
                                    $fBalanceHold += $obItem->getAbsSum();
                                    $obItem->setSuccessStatus();
                                    \Realweb\Site\Model\Notice\Entity::create('other', $obUser->getId(), 'Выплата прошла успешно #' . $obItem->getId());
                                }
                            } else {
                                $fBalance += $obItem->getAbsSum();
                                $obItem->setFailStatus();
                                \Realweb\Site\Model\Notice\Entity::create('other', $obUser->getId(), 'Выплата не прошла #' . $obItem->getId() . ' ' . $result['error']);
                            }
                        }
                    }
                    if ($fBalance) {
                        $obUser->setUfBalance(doubleval($obUser->getUfBalance()) + Site::getCommissionReturnSum(abs(doubleval($fBalance)), 'out'));
                        $obUser->setUfBalanceHold(doubleval($obUser->getUfBalanceHold()) - $fBalance);
                    }
                    if ($fBalanceHold) {
                        $obUser->setUfBalanceHold(doubleval($obUser->getUfBalanceHold()) - $fBalanceHold);
                    }
                    $obUser->save();
                }
            }
        }

        return '\Realweb\Site\User::agentMassPayment();';
    }

    public static function createTestOrderForExecutor(int $iUserId)
    {
        $fPrice = 10;
        $obServerCollection = (new Server\Index)->getCollection();
        $obLocationCollection = (new Location\Index)->getCollection();
        $obLevelCollection = (new Level\Index)->getCollection();
        $obOrder = new \Realweb\Site\Model\Order\Element\Entity();
        $obOrder->setActive('Y');
        $obOrder->setName('Тестовый заказ от ' . date('d.m.Y H:i:s'));
        $obOrder->setCount(2);
        $obOrder->setPrice($fPrice);
        $obOrder->setNickname('Test');
        $obOrder->setLevelId($obLevelCollection->current()->getId());
        $obOrder->setServerId($obServerCollection->current()->getId());
        $obOrder->setLocationId($obLocationCollection->current()->getElements()->current()->getId());
        $obOrder->setRush(0);
        $obOrder->setPreviewText('Тестовый заказ для ознакомления');
        $obOrder->setClass('Тестовый');
        $obOrder->setDateCreate(new DateTime());
        $obOrder->setCreatedBy(1);
        $obOrder->setTest(1);
        $obOrder->setFullPrice($obOrder->getSum());
        if ($obOrder->save()) {
            $obCustom = new Model\Custom\Entity();
            $obCustom->setUfActive(1);
            $obCustom->setUfOrderId($obOrder->getId());
            $obCustom->setUfUserId($iUserId);
            $obCustom->setUfPrice($fPrice);
            if ($obCustom->save()) {
                return true;
            }
        }

        return false;
    }

    public static function createTestOrderForCustomer(int $iUserId)
    {
        $fPrice = 10;
        $obServerCollection = (new Server\Index)->getCollection();
        $obLocationCollection = (new Location\Index)->getCollection();
        $obLevelCollection = (new Level\Index)->getCollection();
        $obOrder = new \Realweb\Site\Model\Order\Element\Entity();
        $obOrder->setActive('Y');
        $obOrder->setName('Тестовый заказ от ' . date('d.m.Y H:i:s'));
        $obOrder->setCount(2);
        $obOrder->setPrice($fPrice);
        $obOrder->setNickname('Test');
        $obOrder->setLevelId($obLevelCollection->current()->getId());
        $obOrder->setServerId($obServerCollection->current()->getId());
        $obOrder->setLocationId($obLocationCollection->current()->getElements()->current()->getId());
        $obOrder->setRush(0);
        $obOrder->setPreviewText('Тестовый заказ для ознакомления');
        $obOrder->setClass('Тестовый');
        $obOrder->setDateCreate(new DateTime());
        $obOrder->setCreatedBy($iUserId);
        $obOrder->setTest(1);
        $obOrder->setFullPrice($obOrder->getSum());
        if ($obOrder->save()) {
            $obCustom = new Model\Custom\Entity();
            $obCustom->setUfActive(1);
            $obCustom->setUfOrderId($obOrder->getId());
            $obCustom->setUfUserId(1);
            $obCustom->setUfPrice($fPrice);
            $obCustom->save();
            foreach (array(1, 0, null, null) as $iStatus) {
                $obScreenshot = new \Realweb\Site\Model\Screenshot\Entity();
                $obScreenshot->setUfUserId(1);
                $obScreenshot->setUfOrderId($obOrder->getId());
                $obScreenshot->setUfFile(\CFile::SaveFile(\CFile::MakeFileArray(Application::getDocumentRoot() . '/upload/example/screenshot.jpg'), 'uf'));
                $obScreenshot->setUfComment('Тестовый скриншот');
                $obScreenshot->setUfMoneyTaken(0);
                $obScreenshot->setUfStatus(null);
                if ($obScreenshot->save()) {
                    if ($iStatus === 1) {
                        $obScreenshot->confirm(5);
                    } elseif ($iStatus === 0) {
                        $obScreenshot->cancel();
                    }
                }
            }

            return true;
        }

        return false;
    }
}
<?

namespace Realweb\Site\Property;

/**
 * Class ElementHighLoad
 * @package Realweb\Site\Property
 */
class ElementHighLoad {

    function GetUserTypeDescription() {
        return array(
            "PROPERTY_TYPE" => "S",
            "USER_TYPE" => "CIBlock_ElementHighLoad",
            "DESCRIPTION" => 'Привязка к элементам highload-блоков',
            "PrepareSettings" => Array(__CLASS__, "PrepareSettings"),
            "GetSettingsHTML" => Array(__CLASS__, "GetSettingsHTML"),
            "GetPropertyFieldHtml" => Array(__CLASS__, "GetPropertyFieldHtml"),
            "ConvertToDB" => Array(__CLASS__, "ConvertToDB"),
            "ConvertFromDB" => Array(__CLASS__, "ConvertFromDB"),
            "GetAdminListViewHTML" => array(__CLASS__, "GetAdminListViewHTML"),
            "GetPublicViewHTML" => array(__CLASS__, "GetPublicViewHTML"),
        );
    }

    function PrepareSettings($arFields) {
        $HL_BLOCK_ID = intval($arFields["USER_TYPE_SETTINGS"]["HL_BLOCK_ID"]);
        if ($HL_BLOCK_ID <= 0)
            $HL_BLOCK_ID = 10;

        $FIELD = 'ID';
        if (isset($arFields['USER_TYPE_SETTINGS']['HL_BLOCK_FIELD']) && strlen($arFields['USER_TYPE_SETTINGS']['HL_BLOCK_FIELD']) > 0) {
            $FIELD = $arFields['USER_TYPE_SETTINGS']['HL_BLOCK_FIELD'];
        }
        $DESCRIPTION_NAME = (isset($arFields['USER_TYPE_SETTINGS']['DESCRIPTION_NAME']) && strlen($arFields['USER_TYPE_SETTINGS']['DESCRIPTION_NAME']) > 0 ? $arFields['USER_TYPE_SETTINGS']['DESCRIPTION_NAME'] : '');


        return array(
            "HL_BLOCK_ID" => $HL_BLOCK_ID,
            'HL_BLOCK_FIELD' => $FIELD,
            "DESCRIPTION_NAME" => $DESCRIPTION_NAME
        );
    }

    function GetSettingsHTML($arProperty, $strHTMLControlName, &$arPropertyFields) {
        $arPropertyFields = array(
            "USER_TYPE_SETTINGS_TITLE" => "HighLoad инфоблок"
        );

        //все HL блоки
        \Bitrix\Main\Loader::includeModule('highloadblock');
        $filter = array(
            'select' => array('ID', 'NAME', 'TABLE_NAME'),
            'filter' => array()
        );
        $VALUE = 0;
        if (isset($arProperty['USER_TYPE_SETTINGS']['HL_BLOCK_ID']) && intval($arProperty['USER_TYPE_SETTINGS']['HL_BLOCK_ID']) > 0) {
            $VALUE = intval($arProperty['USER_TYPE_SETTINGS']['HL_BLOCK_ID']);
        }

        $hlblock_db = \Bitrix\Highloadblock\HighloadBlockTable::getList($filter);

        $html = '<tr>
        <td>HighLoad инфоблок:</td>
        <td>
        <select name="' . $strHTMLControlName["NAME"] . '[HL_BLOCK_ID]">
            <option value="0">Выбрать</option>';
        while ($hl_block = $hlblock_db->fetch()) {
            $selected = '';
            if (intval($hl_block['ID']) == $VALUE) {
                $selected = 'selected="selected"';
            }
            $html .= '<option value="' . $hl_block['ID'] . '"' . $selected . '>' . $hl_block['NAME'] . '[' . $hl_block['ID'] . ']</option>';
        }

        $html .= '</select>
            </td>
        </tr>';
        if (intval($VALUE) > 0) {
            $FIELD = 'ID';
            if (isset($arProperty['USER_TYPE_SETTINGS']['HL_BLOCK_FIELD']) && strlen($arProperty['USER_TYPE_SETTINGS']['HL_BLOCK_FIELD']) > 0) {
                $FIELD = $arProperty['USER_TYPE_SETTINGS']['HL_BLOCK_FIELD'];
            }
            //узнаем свойства
            global $USER_FIELD_MANAGER;
            $_userfields = $USER_FIELD_MANAGER->GetUserFields('HLBLOCK_' . $VALUE, 0, LANGUAGE_ID);
            $html .= '<tr>
        <td>Свойство для вывода в форме редактирования:</td>
        <td>
        <select name="' . $strHTMLControlName["NAME"] . '[HL_BLOCK_FIELD]">
            <option value="ID"' . ($FIELD == 'ID' ? ' selected="selected"' : '') . '>ID</option>';
            foreach ($_userfields as $_userfield) {
                $selected = '';
                if ($_userfield['FIELD_NAME'] == $FIELD) {
                    $selected = 'selected="selected"';
                }
                $html .= '<option value="' . $_userfield['FIELD_NAME'] . '"' . $selected . '>' . $_userfield['EDIT_FORM_LABEL'] . ' [' . $_userfield['FIELD_NAME'] . ']</option>';
            }
            $html .= '</select>
            </td>
        </tr>';
        }

        $DESCRIPTION_NAME = (isset($arProperty['USER_TYPE_SETTINGS']['DESCRIPTION_NAME']) && strlen($arProperty['USER_TYPE_SETTINGS']['DESCRIPTION_NAME']) > 0 ? $arProperty['USER_TYPE_SETTINGS']['DESCRIPTION_NAME'] : '');

        $html .= '<tr>
        <td>Название поля описания:</td>
        <td>
        <input type="text" value="' . $DESCRIPTION_NAME . '"  name="' . $strHTMLControlName["NAME"] . '[DESCRIPTION_NAME]" />
            </td>
        </tr>';



        return $html;
    }

    function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName) {

        $function_name = str_replace('[', 'xx', $strHTMLControlName['VALUE']); //заменяем квадратные скобочки, чтобы их небыло в имени функции
        $function_name = str_replace(']', 'xx', $function_name);
        $html = "<script>function open_win_" . $function_name . "(){ window.open('/local/tools/hl_search.php?lang=ru&FN=" . $strHTMLControlName['FORM_NAME'] . "&FC=" . $strHTMLControlName['VALUE'] . "&ENTITY_ID=" . $arProperty['USER_TYPE_SETTINGS']['HL_BLOCK_ID'] . "', '', 'scrollbars=yes,resizable=yes,width=760,height=500,top='+Math.floor((screen.height - 560)/2-14)+',left='+Math.floor((screen.width - 760)/2-5));}</script>";

        $html .= '<input size="5" type="text" name="' . $strHTMLControlName['VALUE'] . '" id="' . $strHTMLControlName['VALUE'] . '" value="' . $value["VALUE"] . '" /><input class="tablebodybutton" type="button" OnClick="open_win_' . $function_name . '()" value="..."> <span id="div_' . $strHTMLControlName['VALUE'] . '">';
        $HL_VALUE = array('NAME' => "Не найдено");

        $FIELD = 'ID';
        if (isset($arProperty['USER_TYPE_SETTINGS']['HL_BLOCK_FIELD']) && strlen($arProperty['USER_TYPE_SETTINGS']['HL_BLOCK_FIELD']) > 0) {
            $FIELD = $arProperty['USER_TYPE_SETTINGS']['HL_BLOCK_FIELD'];
        }


        // если значение установлено узнаем его
        if ($value["VALUE"]) {
            \Bitrix\Main\Loader::includeModule('highloadblock');
            if (intval($arProperty['USER_TYPE_SETTINGS']['HL_BLOCK_ID']) > 0) {
                $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($arProperty['USER_TYPE_SETTINGS']['HL_BLOCK_ID'])->fetch();
                if (!empty($hlblock)) {
                    global $USER_FIELD_MANAGER;
                    $userfields = array("ID");
                    $_userfields = $USER_FIELD_MANAGER->GetUserFields('HLBLOCK_' . $hlblock['ID'], 0, LANGUAGE_ID);
                    foreach ($_userfields as $_userfield) {
                        $userfields[] = $_userfield['FIELD_NAME'];
                    }
                    // validated successfully. get data
                    $hlDataClass = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock)->getDataClass();
                    $rows = $hlDataClass::getList(array(
                                'select' => $userfields,
                                'order' => array(),
                                'filter' => array('ID' => $value["VALUE"])
                    ));
                    if ($row = $rows->fetch()) {
                        $HL_VALUE = $row;
                        if (isset($row[$FIELD])) {
                            $HL_VALUE['NAME'] = $row[$FIELD];
                        } else {
                            $HL_VALUE['NAME'] = $row['ID'];
                        }
                    }
                }
            }
            $strStyle = "";
            $strTitle = "";
            if (array_key_exists("UF_ACTIVE", $HL_VALUE)) {
                if (intval($HL_VALUE['UF_ACTIVE']) < 1) {
                    $strStyle = "color:#646c7a;";
                    $strTitle = "Не активен";
                }
            }
            $html .= '<a title="'.$strTitle.'" style="' . $strStyle . '" target="_blank" href="/bitrix/admin/highloadblock_row_edit.php?ENTITY_ID=' . $hlblock['ID'] . '&ID=' . $value["VALUE"] . '&lang=' . LANG . '">' . $HL_VALUE["NAME"] . '</a>';
        }

        $html .= '</span>';


        if ($arProperty['WITH_DESCRIPTION'] == "Y") {
            $DESCRIPTION_NAME = (isset($arProperty['USER_TYPE_SETTINGS']['DESCRIPTION_NAME']) && strlen($arProperty['USER_TYPE_SETTINGS']['DESCRIPTION_NAME']) > 0 ? $arProperty['USER_TYPE_SETTINGS']['DESCRIPTION_NAME'] : 'Описание');

            $html .= '&nbsp;' . $DESCRIPTION_NAME . ': <input type="text" name="' . $strHTMLControlName['DESCRIPTION'] . '" id="' . $strHTMLControlName['DESCRIPTION'] . '" value="' . $value["DESCRIPTION"] . '" />';
        }


        return $html;
    }

    function GetAdminListViewHTML($arProperty, $value, $strHTMLControlName) {
        $function_name = str_replace('[', 'xx', $strHTMLControlName['VALUE']); //заменяем квадратные скобочки, чтобы их небыло в имени функции
        $function_name = str_replace(']', 'xx', $function_name);
        $html = "<script>function open_win_" . $function_name . "(){ window.open('/bitrix/admin/hl_search.php?lang=ru&FN=" . $strHTMLControlName['FORM_NAME'] . "&FC=" . $strHTMLControlName['VALUE'] . "&ENTITY_ID=" . $arProperty['USER_TYPE_SETTINGS']['HL_BLOCK_ID'] . "', '', 'scrollbars=yes,resizable=yes,width=760,height=500,top='+Math.floor((screen.height - 560)/2-14)+',left='+Math.floor((screen.width - 760)/2-5));}</script>";

        // вывод местоположений в удобочитаемом виде в списке элементов. со ссылкой на редактирование местоположения
        if ($value["VALUE"]) {
            \Bitrix\Main\Loader::includeModule('highloadblock');

            if (intval($arProperty['USER_TYPE_SETTINGS']['HL_BLOCK_ID']) > 0) {
                $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($arProperty['USER_TYPE_SETTINGS']['HL_BLOCK_ID'])->fetch();
                if (!empty($hlblock)) {
                    global $USER_FIELD_MANAGER;
                    $userfields = array("ID");
                    $_userfields = $USER_FIELD_MANAGER->GetUserFields('HLBLOCK_' . $hlblock['ID'], 0, LANGUAGE_ID);
                    foreach ($_userfields as $_userfield) {
                        $userfields[] = $_userfield['FIELD_NAME'];
                    }
                    // validated successfully. get data
                    $hlDataClass = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock)->getDataClass();
                    $rows = $hlDataClass::getList(array(
                                'select' => $userfields,
                                'order' => array(),
                                'filter' => array('ID' => $value["VALUE"])
                    ));
                    if ($row = $rows->fetch()) {
                        $FIELD = 'ID';
                        if (isset($arProperty['USER_TYPE_SETTINGS']['HL_BLOCK_FIELD']) && strlen($arProperty['USER_TYPE_SETTINGS']['HL_BLOCK_FIELD']) > 0) {
                            $FIELD = $arProperty['USER_TYPE_SETTINGS']['HL_BLOCK_FIELD'];
                            if (!isset($row[$FIELD])) {
                                $FIELD = 'ID';
                            }
                        }
                        $html .= '[' . $row["ID"] . '] <a target="_blank" href="/bitrix/admin/highloadblock_row_edit.php?ENTITY_ID=' . $hlblock['ID'] . '&ID=' . $row["ID"] . '&lang=' . LANG . '">' . $row[$FIELD] . '</a>';
                        return $html;
                    }
                }
            }
        }
        $html .= $value["VALUE"];
        if ($value["VALUE"]) {
            $html .= ' (Не найдено)';
        }
        return $html;
    }

    function ConvertToDB($arProperty, $value) {
        return $value;
    }

    function ConvertFromDB($arProperty, $value) {
        return $value;
    }

    function GetPublicViewHTML($arProperty, $value, $strHTMLControlName) {
        $DISPLAY_VALUE = array();
        if (intval($value['VALUE']) > 0) {
            global $USER_FIELD_MANAGER;
            \Bitrix\Main\Loader::includeModule('highloadblock');
            $hlblock_db = \Bitrix\Highloadblock\HighloadBlockTable::getById($arProperty["USER_TYPE_SETTINGS"]["HL_BLOCK_ID"]);
            if ($hlblock = $hlblock_db->fetch()) {
                $userfieldsArr = array();
                $userfields = array("ID");
                $_userfields = $USER_FIELD_MANAGER->GetUserFields('HLBLOCK_' . $hlblock['ID'], 0, LANGUAGE_ID);
                foreach ($_userfields as $_userfield) {
                    $userfields[] = $_userfield['FIELD_NAME'];
                    $userfieldsArr[$_userfield['FIELD_NAME']] = $_userfield;
                }
                $arFilter = array(
                    "ID" => $value["VALUE"]
                );
                $hlDataClass = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock)->getDataClass();
                $rows = $hlDataClass::getList(array(
                            'select' => $userfields,
                            'filter' => $arFilter
                ));
                if ($row = $rows->fetch()) {
                    foreach ($row as $FIELD => $FIELD_VALUE) {
                        if ($userfieldsArr[$FIELD]['USER_TYPE_ID'] == 'file') {
                            $FIELD_VALUE = CFile::GetFileArray($FIELD_VALUE);
                        }
                        $row[$FIELD] = $FIELD_VALUE;
                    }
                    $DISPLAY_VALUE = $row;
                }
            }
        }
        return $DISPLAY_VALUE;
    }

}

?>
<?

use Bitrix\Main\Context;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;

$module_id = 'realweb.site';
$moduleAccessLevel = $APPLICATION->GetGroupRight($module_id);
if ($moduleAccessLevel >= 'R') {
    Loader::includeModule($module_id);
    Loc::loadMessages(__FILE__);
}

$obRequest = Context::getCurrent()->getRequest();

if ($obRequest->isPost() && check_bitrix_sessid()) {
    Option::set($module_id, 'commission_in_percent', $obRequest->get('commission_in_percent'));
    Option::set($module_id, 'commission_in_min', $obRequest->get('commission_in_min'));
    Option::set($module_id, 'commission_out_percent', $obRequest->get('commission_out_percent'));
    Option::set($module_id, 'commission_out_min', $obRequest->get('commission_out_min'));
    Option::set($module_id, 'commission_order_percent', $obRequest->get('commission_order_percent'));
    Option::set($module_id, 'commission_order_min', $obRequest->get('commission_order_min'));
}

$commission_in_percent = Option::get($module_id, 'commission_in_percent');
$commission_in_min = Option::get($module_id, 'commission_in_min');

$commission_out_percent = Option::get($module_id, 'commission_out_percent');
$commission_out_min = Option::get($module_id, 'commission_out_min');

$commission_order_percent = Option::get($module_id, 'commission_order_percent');
$commission_order_min = Option::get($module_id, 'commission_order_min');

?>
<form action="<?= POST_FORM_ACTION_URI ?>" method="post">
    <?= bitrix_sessid_post() ?>
    <table>
        <tr>
            <td>
                Комиссия на ввод
            </td>
            <td>
                <input type="text" size="3" name="commission_in_percent" placeholder="" value="<?php echo $commission_in_percent; ?>"/> %
            </td>
            <td>
                <input type="text" size="3" name="commission_in_min" placeholder="мин" value="<?php echo $commission_in_min; ?>"/> руб.
            </td>
        </tr>
        <tr>
            <td>
                Комиссия на вывод
            </td>
            <td>
                <input type="text" size="3" name="commission_out_percent" placeholder="" value="<?php echo $commission_out_percent; ?>"/> %
            </td>
            <td>
                <input type="text" size="3" name="commission_out_min" placeholder="мин" value="<?php echo $commission_out_min; ?>"/> руб.
            </td>
        </tr>
        <tr>
            <td>
                Комиссия на заказ
            </td>
            <td>
                <input type="text" size="3" name="commission_order_percent" placeholder="" value="<?php echo $commission_order_percent; ?>"/> %
            </td>
            <td>
                <input type="text" size="3" name="commission_order_min" placeholder="мин" value="<?php echo $commission_order_min; ?>"/> руб.
            </td>
        </tr>
    </table>
    <input class="adm-btn-save" type="submit" value="Сохранить" />
</form>
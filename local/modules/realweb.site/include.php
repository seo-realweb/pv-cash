<?php

class_alias('Realweb\Site\Main\ArrayHelper', 'Realweb\Site\ArrayHelper');

use Bitrix\Main\EventManager;
use Realweb\Site\Site;

EventManager::getInstance()->addEventHandler("main", "OnPageStart", "onPageStart");
EventManager::getInstance()->addEventHandler('iblock', 'OnIBlockPropertyBuildList', array('Realweb\Site\Property\YoutubeVideo', 'GetUserTypeDescription'));
EventManager::getInstance()->addEventHandler("iblock", "OnIBlockPropertyBuildList", array('\Realweb\Site\Property\ElementGroups', 'GetUserTypeDescription'));
EventManager::getInstance()->addEventHandler("iblock", "OnIBlockPropertyBuildList", array('\Realweb\Site\Property\PageType', 'GetUserTypeDescription'));
EventManager::getInstance()->addEventHandler('iblock', 'OnIBlockPropertyBuildList', array('\Realweb\Site\Property\PropertyHtml', 'GetUserTypeDescription'));
EventManager::getInstance()->addEventHandler('iblock', 'OnIBlockPropertyBuildList', array('\Realweb\Site\Property\ElementHighLoad', 'GetUserTypeDescription'));
EventManager::getInstance()->addEventHandler('main', 'OnUserTypeBuildList', array('\Realweb\Site\Field\Html', 'GetUserTypeDescription'));
EventManager::getInstance()->addEventHandler('main', 'OnUserTypeBuildList', array('\Realweb\Site\Field\User', 'GetUserTypeDescription'));
EventManager::getInstance()->addEventHandler("main", "OnBeforeUserAdd", Array("\Realweb\Site\Handlers", "OnBeforeUserAdd"));
EventManager::getInstance()->addEventHandler("main", "OnAfterUserAdd", Array("\Realweb\Site\Handlers", "OnAfterUserAdd"));
EventManager::getInstance()->addEventHandler("main", "OnBeforeUserLogin", Array("\Realweb\Site\Handlers", "OnBeforeUserLogin"));

function onPageStart()
{
    Site::definders();
    \Realweb\Site\User::getInstance()->checkUser();
}
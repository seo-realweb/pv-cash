<?php
define('NEED_AUTH', true);
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');
$APPLICATION->SetTitle('Сообщения');
?>
<?php $APPLICATION->IncludeComponent(
    "realweb:blank", "message",
    array(
        'AJAX_MODE' => 'Y',
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "Y",
        'FOLDER' => '/message/'
    ),
    false,
    array(
        "HIDE_ICONS" => "Y"
    )
); ?>
<?php require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php'); ?>

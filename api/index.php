<?php

require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");

$APPLICATION->IncludeComponent(
    "realweb:api",
    "",
    array(
        "SEF_FOLDER" => "/api/"
    ),
    false,
    array(
        "HIDE_ICONS" => "Y"
    )
);

<?php
define('NEED_AUTH', true);
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');
?>
<? $APPLICATION->IncludeComponent(
    "realweb:blank.route", "order",
    array(
        "SEF_FOLDER" => "/order/",
        "SEF_MODE" => "Y",
        "SET_STATUS_404" => "Y",
        "SEF_URL_TEMPLATES" => array(
            "default" => "",
            "detail" => "#ID#/",
        )
    ), false, array(
        "HIDE_ICONS" => "Y"
    )
); ?>
<?php require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php'); ?>

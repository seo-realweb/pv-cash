<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');
$APPLICATION->SetTitle("Заказчику");
$APPLICATION->SetPageProperty('HIDE_TITLE', "Y");
?><?php $APPLICATION->IncludeComponent(
    "realweb:blank", "customers",
    array(
        'AJAX_MODE' => 'Y',
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        'FOLDER' => '/customers/'
    ),
    false,
    array(
        "HIDE_ICONS" => "Y"
    )
); ?>
<?php require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php'); ?>
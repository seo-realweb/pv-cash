<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');
$APPLICATION->SetTitle('Профиль наемника');
$APPLICATION->SetPageProperty('HIDE_TITLE', "Y");
?>
<?php $APPLICATION->IncludeComponent(
    "realweb:blank", "executors",
    array(
        'AJAX_MODE' => 'Y',
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        'FOLDER' => '/executors/'
    ),
    false,
    array(
        "HIDE_ICONS" => "Y"
    )
); ?>
<?php require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php'); ?>

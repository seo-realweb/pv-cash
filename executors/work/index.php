<?php
define('NEED_AUTH', true);
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');
$APPLICATION->SetPageProperty('HIDE_TITLE', "Y");
$APPLICATION->SetTitle('Профиль наемника');
?>
<?php $APPLICATION->IncludeComponent(
    "realweb:blank", "executors",
    array(
        'AJAX_MODE' => 'Y',
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        'FOLDER' => '/executors/work/',
        'IS_MY' => 'Y'
    ),
    false,
    array(
        "HIDE_ICONS" => "Y"
    )
); ?>
<?php require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php'); ?>

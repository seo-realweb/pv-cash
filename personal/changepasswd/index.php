<?
define("NEED_AUTH", true);
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');
$APPLICATION->SetTitle("Смена пароля");
?>
<? $APPLICATION->IncludeComponent("bitrix:system.auth.changepasswd", ".default", Array(
    "AJAX_MODE" => "Y",
    "AJAX_OPTION_ADDITIONAL" => "",
    "AJAX_OPTION_HISTORY" => "Y",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "Y",
), false
); ?>
<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');
?>
<?php
define('NEED_AUTH', true);
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');
$APPLICATION->SetTitle('Кошелек');
$APPLICATION->SetPageProperty('HIDE_TITLE', "Y");
?>
<?php $APPLICATION->IncludeComponent(
    "realweb:blank", "purse",
    array(
        'FOLDER' => '/personal/purse/'
    ),
    false,
    array(
        "HIDE_ICONS" => "Y"
    )
); ?>
<?php require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php'); ?>

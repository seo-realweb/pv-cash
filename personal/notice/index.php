<?php
define('NEED_AUTH', true);
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');
$APPLICATION->SetTitle('Уведомления');
$APPLICATION->SetPageProperty('HIDE_TITLE', "Y");
?>
<?php $APPLICATION->IncludeComponent(
    "realweb:blank", "notice_history",
    array(
        'FOLDER' => '/personal/notice/',
        'AJAX_MODE' => 'Y'
    ),
    false,
    array(
        "HIDE_ICONS" => "Y"
    )
); ?>
<?php require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php'); ?>

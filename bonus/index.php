<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');
$APPLICATION->SetTitle('Бонусы');
$APPLICATION->SetPageProperty('HIDE_TITLE', "Y");
?>
<?php $APPLICATION->IncludeComponent(
    "realweb:blank", "text",
    array(
        'CODE' => 'BONUS'
    ),
    false,
    array(
        "HIDE_ICONS" => "Y"
    )
); ?>
<?php require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php'); ?>
